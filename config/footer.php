<?php

/**
 * Footer Settings
 * @return Array 
 */

return [

	'column_one'   => 'About us',
	
	'column_two'   => 'Legal',
	
	'column_three' => 'Useful Links',
	
	'column_four'  => 'Help',
	
	'copyright'    => '© 2015- 2018 sanitaryware.org. All rights reserved'

];