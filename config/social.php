<?php 

/**
 * Social Media Links
 */
return [

	'facebook' => 'https://web.facebook.com/sanitaryware.org',
	'twitter'  => 'https://twitter.com/sanitarywareorg',
	'google'   => 'http://google.com/+SanitarywareOrg123',
	'android'  => 'http://www.android.com',
	'iphone'   => 'http://www.apple.com',
	'windows'  => 'http://www.windows.com',

];