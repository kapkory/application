<?php

/*
 * This file is to verify your purchase
 *
 * (c) Abdelhak Ezzaroual <ezzaroual@mail.com>
 *
 * Regular License valid only for one domain
 * Using a nulled version will not work anyway
 */

return [
    

	/**
	* Your purchase code here
	*/
	'purchase_code' => '',

	/**
	* Your domain url here
	* @example http://www.yourdomain.com
	*/
	'domain'        => '',

	/**
	* Your Envato Username here
	*/
	'username'      => '',

];