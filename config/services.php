<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'nexmo' => [
        'key'      => '',
        'secret'   => '',
        'sms_from' => '',
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    '2checkout' => [
        'seller_id'       => '',
        'publishable_Key' => '',
        'private_key'     => '',
        'currency'        => 'USD',
        'account_price'   => '2.50',
        'ad_price'        => '0.70'
    ],

    'facebook' => [
        'client_id'     => '206227890128179',
        'client_secret' => '70380ed5391e2ef0cc5b2dbdaa3ea52b',
        'redirect'      => 'http://sanitaryware.org/auth/facebook/callback',
    ],

    'twitter' => [
        'client_id'     => '',
        'client_secret' => '',
        'redirect'      => 'http://sanitaryware.org/auth/twitter/callback',
    ],

    'google' => [
        'client_id'     => '110659708661-bth18kstqpuagpednvq4q1ub1hubcue4.apps.googleusercontent.com',
        'client_secret' => '0zSD-O19e2kwUPwpeIFn6Y1H',
        'redirect'      => 'http://sanitaryware.org/auth/google/callback',
    ],

    'instagram' => [
        'client_id'     => '',
        'client_secret' => '',
        'redirect'      => 'http://sanitaryware.org/auth/instagram/callback',  
    ], 

    'pinterest' => [
        'client_id'     => '',
        'client_secret' => '',
        'redirect'      => 'http://sanitaryware.org/auth/pinterest/callback',  
    ], 

    'linkedin' => [
        'client_id'     => '',
        'client_secret' => '',
        'redirect'      => 'http://sanitaryware.org/auth/linkedin/callback',  
    ], 

    'vkontakte' => [
        'client_id'     => '',
        'client_secret' => '',
        'redirect'      => 'http://sanitaryware.org/auth/vk/callback',  
    ], 

    'paypal' => [
        'client_id'     => '',
        'secret'        => '',
        'currency'      => 'USD',
        'account_price' => '0.4',
        'ad_price'      => '0.1'
    ],

    'recaptcha' => [
        'secretkey' => '6LdPFU0UAAAAAJ5pUkuniBd21yZ-jjZzuxPSgxNE',
        'sitekey'   => '6LdPFU0UAAAAABV9xxjguh1VrfyOc_P0nKRigG9C',
    ],

    'stripe' => [
        'secret'        => '',
        'currency'      => 'USD',
        'account_price' => '1.90',
        'ad_price'      => '0.80'
    ],

    'mollie' => [
        'client_id'     => '',
        'client_secret' => '',
        'redirect'      => 'http://localhost/everest/checkout/mollie/callback',
        'account_price' => '0.30',
        'ad_price'      => '0.04',
        'currency'      => 'EUR'
    ],

    'barion' => [
        'pos_key'       => '',
        'account_price' => '80',
        'ad_price'      => '10',
        'currency'      => 'HUF'
    ],

    'smscru' => [
        'login'  => '',
        'secret' => '',
        'sender' => 'John_Doe'
    ],

];
