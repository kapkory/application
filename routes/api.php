<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('product-id/{ids}','Api\ProductsController@index');
Route::get('store-id/{ids}','Api\StoreController@index');
Route::get('stores','Api\StoreController@stores');
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
