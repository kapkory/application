<?php return array (
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'laravel/slack-notification-channel' => 
  array (
    'providers' => 
    array (
      0 => 'Illuminate\\Notifications\\SlackChannelServiceProvider',
    ),
  ),
  'laravel/nexmo-notification-channel' => 
  array (
    'providers' => 
    array (
      0 => 'Illuminate\\Notifications\\NexmoChannelServiceProvider',
    ),
  ),
  'pragmarx/firewall' => 
  array (
    'providers' => 
    array (
      0 => 'PragmaRX\\Firewall\\Vendor\\Laravel\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'Firewall' => 'PragmaRX\\Firewall\\Vendor\\Laravel\\Facade',
    ),
  ),
  'artesaos/seotools' => 
  array (
    'providers' => 
    array (
      0 => 'Artesaos\\SEOTools\\Providers\\SEOToolsServiceProvider',
    ),
    'aliases' => 
    array (
      'SEOMeta' => 'Artesaos\\SEOTools\\Facades\\SEOMeta',
      'OpenGraph' => 'Artesaos\\SEOTools\\Facades\\OpenGraph',
      'Twitter' => 'Artesaos\\SEOTools\\Facades\\TwitterCard',
      'SEO' => 'Artesaos\\SEOTools\\Facades\\SEOTools',
    ),
  ),
  'sammyk/laravel-facebook-sdk' => 
  array (
    'providers' => 
    array (
      0 => 'SammyK\\LaravelFacebookSdk\\LaravelFacebookSdkServiceProvider',
    ),
    'aliases' => 
    array (
      'Facebook' => 'SammyK\\LaravelFacebookSdk\\FacebookFacade',
    ),
  ),
  'laravel/socialite' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Socialite\\SocialiteServiceProvider',
    ),
    'aliases' => 
    array (
      'Socialite' => 'Laravel\\Socialite\\Facades\\Socialite',
    ),
  ),
  'socialiteproviders/manager' => 
  array (
    'providers' => 
    array (
      0 => 'SocialiteProviders\\Manager\\ServiceProvider',
    ),
  ),
  'laravelhungary/laravel-barion' => 
  array (
    'providers' => 
    array (
      0 => 'LaravelHungary\\Barion\\BarionServiceProvider',
    ),
  ),
  'albertcht/invisible-recaptcha' => 
  array (
    'providers' => 
    array (
      0 => 'AlbertCht\\InvisibleReCaptcha\\InvisibleReCaptchaServiceProvider',
    ),
  ),
  'spatie/laravel-newsletter' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\Newsletter\\NewsletterServiceProvider',
    ),
    'aliases' => 
    array (
      'Newsletter' => 'Spatie\\Newsletter\\NewsletterFacade',
    ),
  ),
  'jenssegers/agent' => 
  array (
    'providers' => 
    array (
      0 => 'Jenssegers\\Agent\\AgentServiceProvider',
    ),
    'aliases' => 
    array (
      'Agent' => 'Jenssegers\\Agent\\Facades\\Agent',
    ),
  ),
  'intervention/image' => 
  array (
    'providers' => 
    array (
      0 => 'Intervention\\Image\\ImageServiceProvider',
    ),
    'aliases' => 
    array (
      'Image' => 'Intervention\\Image\\Facades\\Image',
    ),
  ),
  'consoletvs/support' => 
  array (
    'providers' => 
    array (
      0 => 'ConsoleTVs\\Support\\SupportServiceProvider',
    ),
  ),
  'jenssegers/date' => 
  array (
    'providers' => 
    array (
      0 => 'Jenssegers\\Date\\DateServiceProvider',
    ),
    'aliases' => 
    array (
      'Date' => 'Jenssegers\\Date\\Date',
    ),
  ),
  'consoletvs/charts' => 
  array (
    'providers' => 
    array (
      0 => 'ConsoleTVs\\Charts\\ChartsServiceProvider',
    ),
    'aliases' => 
    array (
      'Charts' => 'ConsoleTVs\\Charts\\Facades\\Charts',
    ),
  ),
  'mews/purifier' => 
  array (
    'providers' => 
    array (
      0 => 'Mews\\Purifier\\PurifierServiceProvider',
    ),
    'aliases' => 
    array (
      'Purifier' => 'Mews\\Purifier\\Facades\\Purifier',
    ),
  ),
  'propaganistas/laravel-phone' => 
  array (
    'providers' => 
    array (
      0 => 'Propaganistas\\LaravelPhone\\PhoneServiceProvider',
    ),
  ),
  'mollie/laravel-mollie' => 
  array (
    'providers' => 
    array (
      0 => 'Mollie\\Laravel\\MollieServiceProvider',
    ),
    'aliases' => 
    array (
      'Mollie' => 'Mollie\\Laravel\\Facades\\Mollie',
    ),
  ),
  'mattketmo/email-checker' => 
  array (
    'providers' => 
    array (
      0 => 'EmailChecker\\Laravel\\EmailCheckerServiceProvider',
    ),
    'aliases' => 
    array (
      'EmailChecker' => 'EmailChecker\\Laravel\\EmailCheckerFacade',
    ),
  ),
  'spatie/laravel-image-optimizer' => 
  array (
    'providers' => 
    array (
      0 => 'Spatie\\LaravelImageOptimizer\\ImageOptimizerServiceProvider',
    ),
    'aliases' => 
    array (
      'ImageOptimizer' => 'Spatie\\LaravelImageOptimizer\\Facades\\ImageOptimizer',
    ),
  ),
);