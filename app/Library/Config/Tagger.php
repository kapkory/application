<?php
/**
 * Created by PhpStorm.
 * User: levis
 * Date: 12/20/17
 * Time: 9:03 PM
 */

namespace App\Library\Config;


use App\Models\Article;
use App\Models\ArticleTag;
use App\Models\Tag;

class Tagger
{

    public static function removeTags($article)
    {
        $article->tags()->detach();
    }

    public static function articleTag( $article, $tags ){
        /*
          Iterate over all of the tags attaching each one
          to the cafe.
        */
        foreach( $tags as $tag ){
            /*
              Generates a tag name for the entered tag
            */
            $name = self::generateTagName( $tag );

            /*
              Get the tag by name or create a new tag.
            */
//            $new_article_tag     = Tag::firstOrNew( array('name' => $name ) );


            if ($name != null OR $name != "")
            {
                $new_article_tag = Tag::firstOrCreate(['name' => $name]);
//                $article->tags()->attach( $new_article_tag->id );


                /*
                  Apply the tag to the article
                */
                $article->tags()->attach( $new_article_tag->id );
            }

        }
    }


    private static function generateTagName( $tagName ){
        /*
          Trim whitespace from beginning and end of tag
        */
        $name = trim( $tagName );

        /*
          Convert tag name to lower.
        */
        $name = strtolower( $name );

        /*
          Convert anything not a letter or number to a dash.
        */
        $name = preg_replace( '/[^a-zA-Z0-9]/', '-', $name );

        /*
          Remove multiple instance of '-' and group to one.
        */
        $name = preg_replace( '/-{2,}/', '-', $name );
        /*
          Get rid of leading and trailing '-'
        */
        $name = trim( $name, '-' );

        /*
          Returns the cleaned tag name
        */
        return $name;
    }


    public static function getArticleByTag($tag_id)
    {
        $articletag = ArticleTag::where('tag_id',$tag_id)->first();
        if($articletag)
        {
            $article = Article::where('id',$articletag->article_id)->first();

            return $article;
        }
//
    }


    public static function tagNameById($tag_id)
    {
        $tag = Tag::where('id',$tag_id)->first();

        return $tag->name;
    }

}