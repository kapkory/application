<?php

namespace App\Library\Protector;

use EmailChecker;
use Helper;

/**
* Spam Check Controllor
*/
class Spam
{
	
	/*********** Check Spam Email ***********/
	public static function email($email)
	{
		if (EmailChecker::isValid($email)) {
			return false;
		}else{
			return true;
		}
	}

	/**
	 * Check blacklist username
	 */
	public static function blacklist_username($username)
	{
		// Get blacklist usernames
		$blacklist = Helper::settings_security();
		
		$array     = explode(PHP_EOL, $blacklist->blacklist_username);

		if (in_array($username, $array)) {
			return TRUE;
		}

		return FALSE;
	}

}