<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Catalogue;
use Illuminate\Http\Request;
use App\Models\Ad;
use App\Models\Category;
use Helper;

class SitemapController extends Controller
{
   protected $theme = 'themes.default.';

   public function index()
   {
//       if (!Helper::settings_seo()->is_sitemap) {
//           return redirect('/');
//       }

       return response()->view($this->theme.'sitemap.index')
           ->header('Content-Type', 'text/xml');
   }

   	public function browse()
	{

		// Check if sitemap is active
		if (!Helper::settings_seo()->is_sitemap) {
			return redirect('/');
		}

	 	$ad = Ad::where('status', 1)->where('is_trashed', 0)->orderBy('updated_at', 'desc')->first();

	  	return response()->view($this->theme.'sitemap.browse', [
	      	'ad' => $ad
	  	])->header('Content-Type', 'text/xml');

	}

	public function ads()
	{

		// Check if sitemap is active
		if (!Helper::settings_seo()->is_sitemap) {
			return redirect('/');
		}

	    $ads = Ad::where('status', 1)->where('is_trashed', 0)->get();
	    return response()->view($this->theme.'sitemap.ads', [
	        'ads' => $ads,
	    ])->header('Content-Type', 'text/xml');
	}

	public function categories()
	{

		// Check if sitemap is active
		if (!Helper::settings_seo()->is_sitemap) {
			return redirect('/');
		}

	    $categories = Category::where('is_sub', 1)->get();
	    return response()->view($this->theme.'sitemap.categories', [
	        'categories' => $categories,
	    ])->header('Content-Type', 'text/xml');
	}

	public function articles()
    {
        if (!Helper::settings_seo()->is_sitemap) {
            return redirect('/');
        }

        $articles = Article::where('status',1)->get();
        return response()->view($this->theme.'sitemap.articles', [
            'articles' => $articles,
        ])->header('Content-Type', 'text/xml');
    }

    public function articleCategories()
    {
        if (!Helper::settings_seo()->is_sitemap) {
            return redirect('/');
        }

        $article_categories = ArticleCategory::all();
//        dd($article_categories);
        return response()->view($this->theme.'sitemap.article_categories', [
            'article_categories' => $article_categories,
        ])->header('Content-Type', 'text/xml');
    }

    public function catalogues()
    {
        if (!Helper::settings_seo()->is_sitemap) {
            return redirect('/');
        }

        $catalogues = Catalogue::all();
        return response()->view($this->theme.'sitemap.catalogues', [
            'catalogues' => $catalogues,
        ])->header('Content-Type', 'text/xml');
    }

    public function catalogue_countries()
    {
        if (!Helper::settings_seo()->is_sitemap) {
            return redirect('/');
        }

        $catalogue_countries = Catalogue::join('countries','countries.id','=','catalogues.country_id')
            ->select('countries.name','catalogues.created_at')
            ->get();

//        dd($catalogue_countries);
        return response()->view($this->theme.'sitemap.catalogue_countries', [
            'catalogue_countries' => $catalogue_countries,
        ])->header('Content-Type', 'text/xml');
    }

    public function pages()
    {
        if (!Helper::settings_seo()->is_sitemap) {
            return redirect('/');
        }

        return response()->view($this->theme.'sitemap.pages')
            ->header('Content-Type', 'text/xml');
    }

}
