<?php

namespace App\Http\Controllers;

use App\Models\Advertise;
use App\Notifications\AdvertiseWithUsNotification;
use App\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Protocol;
use SEO;
use SEOMeta;
use Helper;
use Theme;

class PagesController extends Controller
{
    public $theme = '';
    
    function __construct()
    {
        $this->theme = Theme::get();
    }

    /**
     * Show Page
     */
    public function show(Request $request, $slug)
    {
    	// Check Slug
    	$page = DB::table('pages')->where('page_slug', $slug)->first();

    	if ($page) {
    		

            // Get Tilte && Description
            $title      = Helper::settings_general()->title;
            $long_desc  = Helper::settings_seo()->description;

            // Manage SEO
            SEO::setTitle($page->page_name.' | '.$title);
            SEO::setDescription($long_desc);
            SEO::opengraph()->setUrl(Protocol::home());

    		// Show Page
    		return view($this->theme.'.pages.show')->with('page', $page);

    	}else{

    		// Return 404 Page
    		return abort(404);

    	}
    }

    public function advertise(){
//        dd($this->theme);
        return view($this->theme.'.pages.advertise-with-us');
    }

    public function createAdvert(Request $request){

        // Make Rules
        $rules = array(
            'full_name' => 'required|max:500',
            'number' => 'required',
        );

        // Make Rules on Inputs
        $validator = Validator::make($request->all(), $rules);

        // Check if Validator Passes
        if ($validator->fails()) {

            return back()->with(['error'=>'Name and Number fields are required']);

        }
        $advert = new Advertise();
        $advert->name = $request->full_name;
        $advert->company_name = $request->company_name;
        $advert->number = $request->number;
        $advert->email = $request->email;
        $advert->remarks = $request->remarks;
        $advert->save();

        Notification::route('mail', 'venkat@sanitaryware.org')->notify(new AdvertiseWithUsNotification($advert));


         return back()->with(['success'=>'Your details has been successfully sent']);
    }
}
