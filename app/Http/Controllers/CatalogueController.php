<?php

namespace App\Http\Controllers;

use App\Models\Catalogue;
use App\Models\CataloguePostingArea;
use App\Models\Country;
use Illuminate\Http\Request;
use Helper;
use Illuminate\Support\Facades\DB;
use SEO;
use SEOMeta;
use Protocol;
use OpenGraph;
use Theme;

class CatalogueController extends Controller
{
    protected $theme = 'themes.default.';


    public function search(Request $request)
    {

        $catalogue = $request->catalogue;
        $country = Country::where('name','=',$catalogue)->first();
        if ($country)
        {
            return $this->country($country->name);
//            $catalogues = Catalogue::where('country_id',$country->id)->paginate(10);

        }
        else
        {
            $catalogues = Catalogue::where('title',$catalogue)->get();
            if ($catalogues->isEmpty())
            {
                $catalogues = Catalogue::where('title','like','%'.$catalogue.'%')
                    ->orWhere('title','like',$catalogue.'%')
                    ->get();
                if ($catalogues->isEmpty())
                    $catalogues= null;
                else
                    $catalogues = Catalogue::where('title','like','%'.$catalogue.'%')->paginate(10);

            }
            else
                $catalogues = Catalogue::where('title','=',$catalogue)->paginate(10);

            SEO::setTitle('Find sanitaryware for the company you are looking for.');
            SEO::setDescription('search and find the sanitary ware company catalogues here for download. No.1 Site for sanitaryware industry.');


        }
        return $this->getCatalogue($catalogues);

    }
    /**
     * Displays all catalogues
     */
    public function catalogue()
    {
        $catalogues = Catalogue::paginate(10);

        return $this->getCatalogue($catalogues);
    }

    public function getCatalogue($catalogues, $meta_title = null,$meta_description = null)
    {
        $countries = Country::join('catalogues','countries.id','=','catalogues.country_id')
            ->select('countries.*')
            ->get();

        SEO::setTitle('Find Sanitaryware Catalogue to Download in PDF Format.');
        SEO::setDescription('Find more than 100 sanitary ware catalogues from different companies and download it in one place. All Indian company sanitary ware catalogues in one place. No.1 Site for sanitaryware industry.');


        return view($this->theme.'catalogue.listings',compact('catalogues','countries','meta_title',',meta_description'));
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Displays catalogue of a given page
     */
    public function index($slug)
    {
        $catalogue = Catalogue::where('slug',$slug)->first();
        if(!$catalogue)
            return back();
        $country_id =$catalogue->country_id;

        $catalogues = Catalogue::where('country_id','=',$country_id)->paginate(3);
//        dd($catalogue->postingArea);
        $title           = Helper::settings_general()->title;
        $keywords        = Helper::settings_seo()->keywords;

        // Create Seo Description
        $seo_description = substr(trim(preg_replace('/\s+/', ' ', $catalogue->content)), 0, 150);

        $meta_title = ($catalogue->meta_title) ? $catalogue->meta_title : $catalogue->title.' | '.$title;
        $meta_description = ($catalogue->meta_description) ? $catalogue->meta_description : $seo_description;
        // Manage SEO
        SEO::setTitle($meta_title);
        SEO::setDescription($meta_description);
        SEO::opengraph()->setUrl(Protocol::home().'/catalogue/download/'.$catalogue->slug);
        OpenGraph::addProperty('type', 'catalogue')->setArticle([
            'published_time'  => $catalogue->created_at,
            'modified_time'   => $catalogue->updated_at,
        ]);
        OpenGraph::addImage($catalogue->image_url);
        return view($this->theme.'catalogue.index',compact('catalogue','catalogues'));
    }

    public function searchCountry(Request $request)
    {
        $country = $request->country;

        $catalogues = Catalogue::where('country_id',$country)->paginate(8);



        return $this->getCatalogue($catalogues);

    }

    public function country($name)
    {
        $name = str_replace('-',' ',$name);
        $country = Country::where('name','=',$name)->first();
      $meta = $this->countryCatalogueMeta($country);
      $meta_title = $meta['title'];
      $meta_description = $meta['description'];

        $catalogues = Catalogue::where('country_id',$country->id)->paginate(10);
        return view($this->theme.'catalogue.listings',compact('catalogues','meta_title','meta_description'));
    }

    public function countryCatalogueMeta($country)
    {
        $count = Catalogue::where('country_id',$country->id)->count();
        $meta_title = " Find & Download Sanitaryware Manufacturer PDF Catalogue on ".$country->name;

        if ($count == 0)
        {
            $meta_description = "You can search and download sanitaryware manufacturer pdf catalogue on $country->name in this section,
         Best site for sanitaryware industry. ";
        }
        else
        {
            $meta_description = "You can search and download sanitaryware manufacturer pdf catalogue on $country->name in this section,
        We have $count sanitaryware manufacturer and supplier catalogs in $country->name  sanitaryware catalogue download section in sanitaryware.org.
         Best site for sanitaryware industry. ";
        }
        if (isset($meta_title))
        {
            SEO::setTitle($meta_title);
            SEO::setDescription($meta_description);
        }

        $meta['title'] = $meta_title;
        $meta['description'] = $meta_description;

        return $meta;

    }
}
