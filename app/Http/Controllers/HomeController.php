<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Image;
use App\Models\Redirect;
use App\Models\Store;
use Illuminate\Http\Request;
use App\Models\Ad;
use App\User;
use App\Models\Stats;
use App\Models\Country;
use App\Models\State;
use DB;
use Helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Protocol;
use SEO;
use SEOMeta;
use IP;
use Firewall;
use Profile;
use Theme;

class HomeController extends Controller {

    public $theme = '';

    function __construct() {

        $this->middleware('install');
        $this->theme = Theme::get();
    }

    /**
     * Home Page
     */
    public function prototype(){
        // Get Ads
        $latest_ads = Ad::where('status', 1)->where('is_archived', 0)->where('is_trashed', 0)->orderBy('id', 'desc')->paginate(4);

        // Get featured ads
        $featured_ads = Ad::where('status', 1)->where('is_archived', 0)->where('is_trashed', 0)->where('is_featured', 1)->orderByRaw('RAND()')->paginate(8);

        // Get Countries
        $countries = Country::all();

        // Get Random 12 countries
        $rand_countries = Country::orderByRaw('RAND()')->take(12)->get();

        // Get Quick Stats
        $total_ads = Ad::count();
        $total_views = Stats::count();
        $total_stores = DB::table('stores')->count();
        $total_users = User::count();

        // Get geo settings
        $settings_geo = Helper::settings_geo();

        // Check if internation
        if (!$settings_geo->is_international) {
            $states = State::where('country_id', $settings_geo->default_country)->get();
        } else {
            $states = null;
        }
//        inRandomOrder()->

        $articles = Article::join('users as c', 'c.id', '=', 'articles.user_id')
            ->where('articles.status','=',1)
            ->orderBy('articles.created_at','desc')
            ->select('articles.*','c.first_name')
            ->paginate(4);
        $stores = Store::latest()->paginate(6);
        if (isset($_GET['t'])) {
            echo '<pre/>';
            print_R($articles);
            print_R($articles[0]->first_name);
            die;
        }
        // send data
        $data = array(
            'latest_ads' => $latest_ads,
            'articles' => $articles,
            'stores' => $stores,
            'countries' => $countries,
            'rand_countries' => $rand_countries,
            'states' => $states,
            'total_ads' => $total_ads,
            'total_views' => $total_views,
            'total_stores' => $total_stores,
            'total_users' => $total_users,
            'settings_geo' => $settings_geo,
        );
        // Get Tilte && Description
        $title = Helper::settings_general()->title;
        $short_desc = Helper::settings_general()->description;
        $long_desc = Helper::settings_seo()->description;

        // Manage SEO
        SEO::setTitle('Sanitaryware.Org-Find Manufacturer, Supplier for Sanitaryware & Faucets');
        SEO::setDescription('Find Sanitaryware & Faucet Products from Multiple Manufacturers & Suppliers around the world for your business. Import and export to gain more profit in sanitaryware business. And tons of article about sanitaryware products & production process. Open an online showroom for your business to reach globally.');
        SEO::opengraph()->setUrl(Protocol::home());
        // Show Home Page

        return view($this->theme . '.protype_home')->with($data);

    }



    public function index() {

        // Get Ads
        $latest_ads = Ad::where('status', 1)->where('is_archived', 0)->where('is_trashed', 0)->orderBy('id', 'desc')->paginate(4);                                                                          

        // Get featured ads
        $featured_ads = Ad::where('status', 1)->where('is_archived', 0)->where('is_trashed', 0)->where('is_featured', 1)->orderByRaw('RAND()')->paginate(8);

        // Get Countries
        $countries = Country::all();

        // Get Random 12 countries
        $rand_countries = Country::orderByRaw('RAND()')->take(12)->get();

        // Get Quick Stats
        $total_ads = Ad::count();
        $total_views = Stats::count();
        $total_stores = DB::table('stores')->count();
        $total_users = User::count();

        // Get geo settings
        $settings_geo = Helper::settings_geo();

        // Check if internation
        if (!$settings_geo->is_international) {
            $states = State::where('country_id', $settings_geo->default_country)->get();
        } else {
            $states = null;
        }
//        inRandomOrder()->

        $articles = Article::join('users as c', 'c.id', '=', 'articles.user_id')
                ->where('articles.status','=',1)
            ->orderBy('articles.created_at','desc')
            ->select('articles.*','c.first_name')
            ->paginate(4);
        $stores = Store::latest()->paginate(6);
        if (isset($_GET['t'])) {
            echo '<pre/>';
            print_R($articles);
            print_R($articles[0]->first_name);
            die;
        }
        // send data
        $data = array(
            'latest_ads' => $latest_ads,
            'articles' => $articles,
            'stores' => $stores,
            'countries' => $countries,
            'rand_countries' => $rand_countries,
            'states' => $states,
            'total_ads' => $total_ads,
            'total_views' => $total_views,
            'total_stores' => $total_stores,
            'total_users' => $total_users,
            'settings_geo' => $settings_geo,
        );
        // Get Tilte && Description
        $title = Helper::settings_general()->title;
        $short_desc = Helper::settings_general()->description;
        $long_desc = Helper::settings_seo()->description;

        // Manage SEO
        SEO::setTitle('Sanitaryware.Org-Find Manufacturer, Supplier for Sanitaryware & Faucets');
        SEO::setDescription('Find Sanitaryware & Faucet Products from Multiple Manufacturers & Suppliers around the world for your business. Import and export to gain more profit in sanitaryware business. And tons of article about sanitaryware products & production process. Open an online showroom for your business to reach globally.');
        SEO::opengraph()->setUrl(Protocol::home());
        // Show Home Page
        return view($this->theme . '.index')->with($data);
    }

    /**
     * Site is under maintenance
     */
    public function maintenance() {
        // Check maintenance mode
        $settings = DB::table('settings_general')->where('id', 1)->first();

        if ($settings->is_maintenance) {

            // Site is under maintenance
            return view($this->theme . '.maintenance');
        } else {

            // Site is working
            return redirect('/');
        }
    }

    public function loadRedirect() {
        $redirects = Storage::disk('local')->get('images/redirect.json');
        $results = json_decode($redirects, true);
        foreach ($results as $result) {
            $slug = str_replace('http://sanitaryware.org/', '', $result['FIELD1']);
            $redirect_to = str_replace('http://sanitaryware.org/', '', $result['FIELD2']);
            dd($redirect_to);
            DB::table('redirects')
                    ->insert([
                        'old_url' => $slug,
                        'redirect_to' => $redirect_to
                    ]);
        }
    }

    public function getRedirect($slug) {
//        dd($slug);
        $redir = Redirect::where('old_url', 'LIKE', $slug . '%')->first();
        if ($redir) {
            return \redirect($redir->redirect_to);
        } else {
            abort(404);
//           return \redirect('404');
        }
    }

    public function redirectToJob($job) {
        return \redirect('page/job');
    }

    public function redirectToDirectory($slug) {
        return \redirect('page/directory');
    }

    //redirects for category
    public function redirectToCategory($slug) {
        $redir = Redirect::where('old_url', 'LIKE', 'category/' . $slug . '%')->first();
        if ($redir) {
            return \redirect($redir->redirect_to);
        } else {
            abort(404);
        }
    }

    public function testing() {
        $dir = 'uploads/general';
        $files = scandir($dir, 0);
//        dd($files);
        for ($i = 2; $i < count($files); $i++) {
//            print $i.' '.$files[$i]."<br>";
            $data = explode('.', $files[$i]);
//            echo "<pre>";
//            echo $data[0].'          '.$data[1];
//            echo "</pre>";
            $image = new Image();
            $image->name = $data[0];
            $image->path = 'uploads/general/' . strtolower($files[$i]);
            $image->file_type = $data[1];
            $image->file_size = 154;
            $image->alternate_text = $data[0];
            $image->user_id = Auth::id();
            $image->save();
        }
    }

}
