<?php

namespace App\Http\Controllers\Api;

use App\Models\Article;
use App\Models\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
    public function index($ids){
        $ids = explode(',',$ids) ;
        $response = Store::whereIn('id',$ids)->get();
        return $response;
    }

    public function stores(){
        $stores = Store::latest()->select('title','username','logo')->paginate(6);
        $articles = Article::join('users as c', 'c.id', '=', 'articles.user_id')
            ->where('articles.status','=',1)
            ->orderBy('articles.created_at','desc')
            ->select('articles.*','c.first_name')
            ->paginate(4);
        $data = [];
        $data['stores'] = $stores;
        $data['articles'] = $articles;
        return $data;
    }
}
