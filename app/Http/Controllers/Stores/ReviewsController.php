<?php

namespace App\Http\Controllers\Stores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Store;
use App\Models\Ad;
use App\Models\Rating;
use Theme;

class ReviewsController extends Controller
{
    public $theme = '';
    
    function __construct()
    {
        $this->theme = Theme::get();
    }

    /**
	* Get ad reviews
    */
    public function reviews(Request $request, $username, $id)
    {
    	// Check store
    	$store = Store::where('username', $username)->where('status', 1)->first();

    	if ($store) {
    		
    		// Check ad
    		$ad = Ad::where('ad_id', $id)->where('user_id', $store->owner_id)->where('status', true)->where('is_trashed', false)->first();

    		if ($ad) {
    			
    			// Ad exists, get reviews
    			$reviews = Rating::where('ad_id', $ad->ad_id)->where('is_approved', true)->paginate(30);

    			// Send data
    			$data = array(
    				'reviews' => $reviews, 
    				'ad'      => $ad, 
    				'store'   => $store, 
    			);

    			return view($this->theme.'.stores.reviews', $data);

    		}else{

    			// Ad not found
    			return redirect('/')->with('error', 'Oops! Ad not found.');

    		}

    	}else{

    		// Store not found
    		return redirect('/')->with('error', 'Oops! Store not found.');

    	}
    }
}
