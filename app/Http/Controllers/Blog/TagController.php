<?php

namespace App\Http\Controllers\Blog;

use App\Models\Article;
use App\Models\Tag;
use SEO;
use SEOMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    protected $theme = 'themes.default.';
    public function index($tag)
    {
        $tags = Tag::where('name',$tag)->first();
        $latests = Article::latest()->paginate(10);


        $response = Tag::join('article_tags','tags.id','=','article_tags.tag_id')
            ->join('articles','articles.id','=','article_tags.article_id')
            ->join('article_categories','article_categories.id','=','articles.article_category_id')
            ->where('tags.id','=',$tags->id)
            ->select('articles.*','article_categories.name as category_name','article_categories.category_slug as category_slug')
                     ->paginate(8);
        SEO::setTitle('sanitaryware articles related to '.$tag);
        SEO::setDescription('Find Article related to '.$tag.' here in sanitaryware.org.  A No.1 Website for sanitaryware industry.');

//        ->get();
//        dd($response);
        return view($this->theme.'blog.tags',['articles'=>$response,'latests'=>$latests]);
    }
}
