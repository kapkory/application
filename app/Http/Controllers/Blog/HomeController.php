<?php

namespace App\Http\Controllers\Blog;

use App\Models\ArticleCategory;
use App\Models\ArticleComment;
use App\Models\ArticleTag;
use App\Models\Comment;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Helper;
use Illuminate\Support\Facades\DB;
use SEO;
use SEOMeta;
use session;
use Protocol;
use OpenGraph;
use Theme;
use Mail;
use Auth;

class HomeController extends Controller {

    public $theme = '';

    function __construct() {
        $this->theme = Theme::get();
    }

    /**
     * Get Blog home page
     */
    public function index() {

        // Get Articles
//        $articles = DB::table('articles as a')
//                ->join('article_categories as b', 'b.id', '=', 'a.article_category_id')
//                ->join('users as c', 'c.id', '=', 'a.user_id')
//                ->orderBy('a.id', 'desc')
//                ->where('a.status','=',1)
//                ->get(array('a.id as article_id', 'c.first_name', 'a.title', 'b.*', 'b.id as catid', 'b.name as cat_name', 'a.*'));
//        $cat_info = $articles_cat_arr = array();

//        foreach ($articles as $a) {
//            $articles_cat_arr [$a->catid][] = $a;
//            $cat_info[$a->catid] = $a->cat_name;
//        }
//        echo '<pre/>';
//        print_R($articles_cat_arr);
//        die;
        // Get Tilte && Description

        $other_articles = Article::join('article_categories','articles.article_category_id','=','article_categories.id')
            ->where('status',1)
            ->where('is_featured',0)
            ->select('articles.*','article_categories.name as category_name','article_categories.category_slug')
            ->orderBy('updated_at','desc');

        $articles = Article::join('article_categories','articles.article_category_id','=','article_categories.id')
            ->where('status',1)
            ->where('is_featured',1)
            ->select('articles.*','article_categories.name as category_name','article_categories.category_slug')
            ->union($other_articles)
            ->orderBy('is_featured','desc')
            ->orderBy('created_at','desc')
            ->simplePaginate(10);
        $title = Helper::settings_general()->title;
        $long_desc = Helper::settings_seo()->description;

        // Manage SEO
        SEO::setTitle('Articles for Sanitary ware Industry – Sanitaryware.org');
        SEO::setDescription('More than 100 articles related to sanitaryware field are published here. Grow your knowledge in the sanitaryware field. Show your expertise by writing an article in sanitaryware.org website');
        SEO::opengraph()->setUrl(Protocol::home() . '/article');

        return view($this->theme . '.blog.categories', compact('articles'));
    }

    public function mail_test() {

        $email = 'honeylehra4@gmail.com,shawinderjit.singh@hotmail.com';
        echo $emails;
//        $err_insfo = array(
//            'ip' => 'Request::ip()',
//            'file' => 'fateh',
//            'line' => 'fateh1',
//            'message' => 'fateh2',
//            'params' => 'fateh3',
//            'route' => 'fate4',
//            'username' => 'fate4',
//        );
//        Mail::to($email)
//                ->send(new \App\Mail\NotifyError($err_info));
    }

    /**
     * Show article
     */
    public function article($slug) {

        // Check article slug
        $article = Article::where('slug', $slug)->first();
        if (isset($article->tags)) {
            $tags = $article->tags;
        } else {
            $tags = null;
        }


        if ($article) {
            $comments = $article->articleComments()->where('parent_id', 0)->where('status', '=', 1)->orderBy('id', 'desc')->get();
            if ($comments)
                $comment_count = $article->articleComments()->where('status', '=', 1)->count();
            else
                $comment_count = 0;

            // Get Tilte && Description
            $title = Helper::settings_general()->title;

            // Create Seo Description
            $seo_description = substr(trim(preg_replace('/\s+/', ' ', $article->content)), 0, 150);

            $meta_title = ($article->meta_title) ? $article->meta_title : $article->title . ' | ' . $title;
            $meta_description = ($article->meta_description) ? $article->meta_description : $seo_description;
            // Manage SEO
            SEO::setTitle($meta_title);
            SEO::setDescription($meta_description);
            SEO::opengraph()->setUrl(Protocol::home() . '/article/' . $article->slug);
            OpenGraph::addProperty('type', 'article')->setArticle([
                'published_time' => $article->created_at,
                'modified_time' => $article->updated_at,
            ]);
            OpenGraph::addImage(Protocol::home() . '/uploads/articles/' . $article->cover);

            // Show post
            return view($this->theme . '.blog.article', compact('article', 'comments', 'tags', 'comment_count'));
        }else {

            // Not found
            abort('404');
        }
    }

    public function articleCategory($category_name, $sub_category = null) {
        $articleCategory = ArticleCategory::where('category_slug', $category_name)->where('is_sub', false)->first();
        $latests = Article::latest()->where('status',1)->paginate(10);

        if ($articleCategory) {
            $sub_category = DB::table('article_categories')->where('category_slug', $sub_category)->where('parent_category', $articleCategory->id)->first();
            $articles = [];
            if ($sub_category) {
                SEO::setTitle($sub_category->meta_title);
                SEO::setDescription($sub_category->meta_description);
               $articles = Article::where('article_category_id', '=', $sub_category->id)->latest()->where('status',1)->paginate(8);
            } else {


                $other_articles = Article::join('article_categories','articles.article_category_id','=','article_categories.id')
                    ->where('articles.article_category_id', '=', $articleCategory->id)
                    ->where('status',1)
                    ->where('is_featured',0)
                    ->select('articles.*','article_categories.name as category_name','article_categories.category_slug')
                    ->orderBy('updated_at','desc');

                $articles = Article::join('article_categories','articles.article_category_id','=','article_categories.id')
                    ->where('articles.article_category_id', '=', $articleCategory->id)
                    ->where('status',1)
                    ->where('is_featured',1)
                    ->select('articles.*','article_categories.name as category_name','article_categories.category_slug')
                    ->union($other_articles)
                    ->orderBy('is_featured','desc')
                    ->orderBy('created_at','desc')
                    ->simplePaginate(10);

//                 dd($articles);
                SEO::setTitle($articleCategory->meta_title);
                SEO::setDescription($articleCategory->meta_description);
//                $articles = Article::where('article_category_id', '=', $articleCategory->id)->latest()->where('status',1)->paginate(8);
            }
        } else {
            abort(404);
//            $articles = [];
        }
//        category_listings
        return view($this->theme . '.blog.categories', [
                    'articles' => $articles
                        ]
        );
    }

}
