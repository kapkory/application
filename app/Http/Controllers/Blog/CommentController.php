<?php

namespace App\Http\Controllers\Blog;

use App\Models\ArticleComment;
use App\Notifications\newArticleComment;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{


    public function create(Request $request)
    {
//
        $admin = User::where('email','admin@sanitaryware.org')->first();
        $message =[];
      if ($request->user())
      {
          if ($request->user()->is_admin == 1){
              $request->user()->articleComments()->create([
                  'article_id'=>$request->article_id,
                  'content'=>$request->get('comment'),
                  'status'=>1
              ]);
              $message = "Your comment has been successfully entered";

          }
         else
         {
             $request->user()->articleComments()->create([
                 'article_id'=>$request->article_id,
                 'content'=>$request->get('comment'),
             ]);
             $admin->notify(new newArticleComment($request->article_id));
             $message = "Your comment has been successfuly posted, it shall be approved so as to be displayed, thank you";
         }

      }else
      {
          $email = $request->email;
          $user = User::where('email',$email)->get();

          if ($user->isEmpty())
          {
              $this->validate($request,[
                  'name' => 'required',
                  'email' => 'required'
              ]);
              $article_comment = new ArticleComment();
              $article_comment-> content =$request->get('comment');
              $article_comment->user_id =0 ;
              $article_comment->article_id =$request->article_id;
              $article_comment->name =$request->name;
              $article_comment->email =$request->email;
              $article_comment->website =$request->website;
              $article_comment->save();

              $admin->notify(new newArticleComment($article_comment->id));
              $message = "Your comment has been successfuly posted, it shall be approved so as to be displayed, thank you";

          }
          else
          {
              $message ='The email you entered already exists, login ro proceed';

              return back()->with('error', $message);

          }
//          Auth::loginUsingId($id);



      }

        return back()->with('success',$message);
    }
    /**
     * replies
     */

    public function reply(Request $request)
    {

        if ($request->parent_id ==0){
      // Make rules
      $rules = array(
          'comment' => 'required',
          'g-recaptcha-response' => 'required|captcha'
      );

      // Make Validation
      $validator = Validator::make($request->all(), $rules);

      if ($validator->fails()) {

          // error
          return back()->withErrors($validator)->withInput();

      }
  }


        $admin = User::where('email','admin@sanitaryware.org')->first();
          if ($request->user())
          {
//              dd($request->user());
              $request->user()->articleComments()->create([
                  'article_id'=>$request->article_id,
                  'content'=>$request->get('comment'),
                  'parent_id'=>$request->parent_id,
                  'status'=>($request->user()->is_admin == 1)?1:0
              ]);

//dd()

              if($request->user()->is_admin != 1)
              {
                  $articleComment = ArticleComment::where('user_id',\auth()->id())->where('article_id','=',$request->article_id)->first();
                  $admin->notify(new newArticleComment($articleComment->id));
                  $message = "Your comment has been successfuly posted, it shall be approved so as to be displayed, thank you";

              }
              else
                  $message = "Your comment has been successfully entered";
          }
          else
          {
              $this->validate($request,[
                  'name' => 'required',
                  'email' => 'required'
              ]);

              $article_comment = new ArticleComment();
              $article_comment-> content =$request->get('comment');
              $article_comment->user_id =0 ;
              $article_comment->article_id =$request->article_id;
              $article_comment->parent_id = $request->parent_id;
              $article_comment->name =$request->name;
              $article_comment->email =$request->email;
              $article_comment->website =$request->website;
              $article_comment->save();

              $admin->notify(new newArticleComment($article_comment->id));
              $message = "Your comment has been successfuly posted, it shall be approved so as to be displayed, thank you";
          }
//
        return back()->with('success',$message);

    }
}
