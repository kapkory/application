<?php

namespace App\Http\Controllers\Blog;

use App\Library\Tools\Theme;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{
    function __construct() {
        $this->theme = Theme::get();
    }
    public function search(){
    $search = \request()->s;
        $articles = Article::join('article_categories','articles.article_category_id','=','article_categories.id')
            ->where('articles.status','=',1)
            ->where('articles.title','like','%'.$search.'%')
            ->orWhere('articles.title','like',$search.'%')
            ->orWhere('articles.title','like','%'.$search)
            ->select('articles.*','article_categories.name as category_name','article_categories.category_slug')
            ->paginate(10);
        return view($this->theme . '.blog.categories', [
                'articles' => $articles
            ]
        );
    }
}
