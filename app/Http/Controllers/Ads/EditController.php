<?php

namespace App\Http\Controllers\Ads;

use App\Http\Controllers\Controller;
use App\Models\AdCountry;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Auth;
use DB;
use App\Models\Ad;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\User;
use Uploader;
use SEO;
use SEOMeta;
use Helper;
use Protocol;
use Validator;
use Redirect;
use Carbon\Carbon;
use Random;
use Theme;
use Profile;

/**
* EditController
*/
class EditController extends Controller
{
    public $theme = '';
	
	function __construct()
	{
		$this->middleware('auth');
        $this->theme = Theme::get();
	}

	/**
	 * Edit Ad
	 */
	public function edit(Request $request, $ad_id)
	{
		// Get user id
		$user_id = Auth::id();

		// Check ad id
		$ad = Ad::where('ad_id', $ad_id)->where('user_id', $user_id)->where('status', 1)->where('is_trashed', 0)->first();
		if ($ad) {
			
			// Get user
			$user          = User::where('id', Auth::id())->first();
			
			// Get GEO Settings
			$settings_geo  = Helper::settings_geo();

			// Check if this site config for a international
//			if ($settings_geo->is_international) {
//
//				// Get country
//				$country   = Country::where('sortname', $user->country_code)->first();
//
//				$countries = Country::all();
//				$states    = State::where('country_id', $country->id)->get();
//				$cities    = City::where('state_id', $user->state)->get();
//
//			}else{
//
//				// Get States
//				$countries = Country::where('id', $settings_geo->default_country)->get();
//				$states    = State::where('country_id', $settings_geo->default_country)->get();
//				$cities    = City::where('state_id', $settings_geo->default_state)->get();
//
//			}

			// Send data
			$data = array(
				'user'      => $user,
				'users'      =>User::all(),
				'ad'        => $ad,
			);
			// Get Tilte && Description
			$title      = Helper::settings_general()->title;
			$short_desc = Helper::settings_general()->description;
			$long_desc  = Helper::settings_seo()->description;
			$keywords   = Helper::settings_seo()->keywords;

			// Manage SEO
			SEO::setTitle(__('title.lang_edit_ad').' | '.$title);
	        SEO::setDescription($long_desc);

			// Ad found
			return view($this->theme.'.account.ads.edit')->with($data);

		}else{
			// Not found
			return redirect('account/ads')->with('error', __('return/error.lang_ad_not_found'));
		}
	}

	/**
	 * Update Ad
	 */
	public function update(Request $request, $ad_id)
	{
		// Get user id
		$user_id = Auth::id();

		// Check ad id
		$ad = Ad::where('ad_id', $ad_id)->where('user_id', $user_id)->where('status', 1)->where('is_trashed', 0)->where('is_archived', 0)->first();

		if ($ad) {
//            Rule::exists('categories', 'id')->where(function ($query) {
//                $query->where('is_sub', 1);
//            }),
			// Make Rules
			$rules = array(
				'title'         => 'required|max:100', 
				'descript'   => 'required',
				'category'      => [
				'required',
				'numeric',
				], 
				'deliverable_to'  => 'required',
				'minimum_order_quantity'     => 'required',
				'supply_time'      => 'required',
				'oos'            => 'boolean',
				'currency'       => 'required|exists:currencies,code',
			);

			// Make rules on inputs
			$validator = Validator::make($request->all(), $rules);

			// Check if validation fails
			if ($validator->fails()) {
				// Error
				return Redirect::back()->withErrors($validator);

			}else{
				// Get Inputs values
				$title       = $request->get('title');
				$description = $request->get('descript');
				$category    = $request->get('category');
				$price       = $request->get('price');
				$supply_time  = $request->get('supply_time');
				$minimum_order_quantity   = $request->get('minimum_order_quantity');
				$currency    = $request->get('currency');
				$photos      = $request->file('photos');
                $deliverable_to        = $request->get('deliverable_to');
				$meta_title        = $request->get('meta_title');
				$meta_description        = $request->get('meta_description');

				// Generate AD Slug
				$slug        = Random::slug($title, $ad_id);

				// Check Price
				if (!Helper::check_price($price)) {
					// Error price
					return back()->with('error', __('return/error.lang_price_format_invalid'));
				}

				// Check if user has store
			if (Profile::hasStore(Auth::id())) {

				// Has Store
				$youtube        = $request->get('youtube') ? : NULL ;
				$affiliate_link = $request->get('affiliate_link') ? : NULL;
				$regular_price  = $request->get('regular_price') ? : NULL;
				$is_oos         = $request->get('oos') ? : FALSE;

				// Check Regular Price
				if ($regular_price && !Helper::check_price($regular_price)) {
					return back()->with('error', __('return/error.lang_price_format_invalid'))->withInput();
				}

				// Check youtube
				if ($youtube && (!Protocol::isValidYoutubeURL($youtube))) {
					return redirect('create')->with('error', __('update.lang_invalid_youtube_url'))->withInput();
				}

			}else{

				// Has no store
				$youtube        = NULL;
				$regular_price  = NULL;
				$affiliate_link = NULL;
				$is_oos         = FALSE;

			}
				// Update Ad
				$dd = Ad::where('ad_id', $ad_id)->update([
					'title'          => $title, 
					'affiliate_link' => $affiliate_link,
					'slug'           => $slug, 
					'description'    => $description, 
					'price'          => $price, 
					'regular_price'  => $regular_price,
                    'deliverable_to' => $deliverable_to,
					'category'       => $category, 
					'minimum_order_quantity'     => $minimum_order_quantity,
					'currency'       => $currency, 
					'youtube'        => $youtube, 
					'supply_time'        => $supply_time,
					'is_oos'         => $is_oos, 
					'meta_title'         => $meta_title,
					'meta_description'         => $meta_description,
					'updated_at'     => Carbon::now(),
				]);

                if ($deliverable_to == 2 && count($request->get('country'))) {
                    foreach ($request->get('country') as $country) {

                        $adc = new AdCountry();
                        $adc->ad_id = $ad->id;
                        $adc->country_id = $country;
                        $adc->save();
                    }
                }

				if ($photos) {

					// Count Photos
					$count_photos = count($photos);

					// Replace Photos
					$is_uploaded = Uploader::edit($photos, $ad_id);

					// Check if photos uploaded
					if ($is_uploaded) {
						
						// Get Previews Photos
						$previews          = implode('||', $is_uploaded['previews_array']);

						// Get Thumbnails Photos
						$thumbnails        = implode('||', $is_uploaded['thumbnails_array']);

						// Count Photos
						$photos_number     = count($photos);

						// Update Ad
						Ad::where('ad_id', $ad_id)->update([
							'photos'        => $previews,
							'thumbnails'    => $thumbnails,
							'photos_number' => $photos_number,
						]);

					}else{

						// Error uploading photos
						return back()->with('error', __('return/error.lang_error_uploading_images'));

					}
				}

				return back()->with('success', __('return/success.lang_ad_updated'));

			}

		}else{
			// Not found
			return redirect('account/ads')->with('error', __('return/error.lang_ad_not_found'));
		}
	}

}