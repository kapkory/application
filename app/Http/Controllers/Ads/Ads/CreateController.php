<?php
namespace App\Http\Controllers\Ads;

use App\Http\Controllers\Controller;
use App\Models\Store;
use Auth;
use Illuminate\Http\Request;
use App\Mail\AlertMatchFound;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use App\Notifications\Admin\AdPending;
use Validator;
use DB;
use Input;
use Image;
use Redirect;
use Uploader;
use EverestCloud;
use Random;
use Carbon\Carbon;
use App\User;
use App\Models\Ad;
use App\Models\Country;
use App\Models\State;
use Protocol;
use Profile;
use SEO;
use SEOMeta;
use Helper;
use Facebook;
use Twitter;
use File;
use Countries;
use Theme;

/**
* CreateController
*/
class CreateController extends Controller
{
    public $theme = '';
	
	function __construct()
	{
		$this->middleware('auth');
        $this->theme = Theme::get();
	}

	/**
	 * Create New Ad
	 */
	public function create()
	{

		// Check if not admin, and check for warnings
		if (!Auth::user()->is_admin) {

			$settings_auth = Helper::settings_auth();

			// Check if user has too many warnings
			$warnings = DB::table('notifications_warnings')->where('user_id', Auth::id())->count();

			if ($warnings >= $settings_auth->max_warnings) {
				
				return redirect('/')->with('error', __('return/error.lang_too_many_warnings'));

			}

		}

		// Get membership settings
		$settings_membership = Helper::settings_membership();

		// check ads per day by user
		$this_day_ads        = Ad::where('user_id', Auth::id())->where('created_at', '>=', Carbon::now()->subDay())->count();

		// if user not admin or moderator
		if (Profile::hasStore(Auth::id())) {

			if ($this_day_ads >= $settings_membership->pro_ads_per_day) {
				// try again tomorrow
				return redirect('/')->with('error', __('return/error.lang_you_can_add_up_to_x_ads_per_day', ['ads' => $settings_membership->pro_ads_per_day]));
			}else{

				$access = true;

			}

		}else{

			if ($this_day_ads >= $settings_membership->free_ads_per_day) {
				// try again tomorrow
				return redirect('/')->with('error', __('return/error.lang_you_can_add_up_to_x_ads_per_day', ['ads' => $settings_membership->free_ads_per_day]));
			}else{

				$access = true;

			}

		}

		
		// check admin
		if (Auth::user()->is_admin) {
			$access = TRUE;
		}

		if ($access) {

			// Get user
			$user          = User::where('id', Auth::id())->first();
			
			// Get GEO Settings
			$settings_geo  = Helper::settings_geo();

			// Check if this site config for a international
			if ($settings_geo->is_international) {

				// Get country
				$country   = Country::where('sortname', $user->country_code)->first();
				
				$countries = Country::all();
				$states    = State::where('country_id', $country->id)->get();

			}else{

				// Get States
				$countries = Country::where('id', $settings_geo->default_country)->get();
				$states    = State::where('country_id', $settings_geo->default_country)->get();


			}
            $stores = Store::where('owner_id','=',Auth::id())->get();
			// Send data
			$data = array(
				'countries' => $countries, 
				'states'    => $states, 
                'users'      =>User::all(),
                'user'      => $user,
                'stores'      => $stores,
			);

			// Get Tilte && Description
			$title      = Helper::settings_general()->title;
			$long_desc  = Helper::settings_seo()->description;
			$keywords   = Helper::settings_seo()->keywords;

			// Manage SEO
			SEO::setTitle(__('title.lang_create_ad').' | '.$title);
	        SEO::setDescription($long_desc);
	        SEO::opengraph()->setUrl(Protocol::home().'/create');

			return view($this->theme.'.ads.create')->with($data);

		}

	}

	/**
	 * Insert New Ad
	 */
	public function insert(Request $request)
	{
//	    dd($request->get('title'));
		$store_id = 0;
		if (!Auth::user()->is_admin) {

            $store  = Store::where('owner_id','=',$request->user()->id)->first();

            if ($store)
            {
                $store_id = $store->id;
            }
            else
            {
                return back()->with('error', 'You need to create a store first');

            }

            $settings_auth = Helper::settings_auth();

			// Check if user has too many warnings
			$warnings = DB::table('notifications_warnings')->where('user_id', Auth::id())->count();

			if ($warnings >= $settings_auth->max_warnings) {
				
				return redirect('/')->with('error', __('return/error.lang_too_many_warnings'));

			}
		
		}
		else
        {
            $store_id  = $request->get('store');
        }

//        dd($store_id,$request->user()->id);

		// Get GEO Settings
		$settings_geo = Helper::settings_geo();

		// Check if states enabled
		if ($settings_geo->states_enabled) {

			$state_rule = 'required|exists:states,id';

		}else{

			$state_rule = '';

		}



		// Make Rules
		$rules = array(
			'title'                => 'required|max:100',
			'description'          => 'required', 
			'category'             => [
				'required',
				'numeric',
				Rule::exists('categories', 'id')->where(function ($query){
					$query->where('is_sub', 1);
				})
			], 
			'country'              => 'required|exists:countries,sortname', 
			'state'                => $state_rule, 
			'price'                => 'required',
			'minimum_order_quantity'=> 'required',
			'supply_time'          => 'required',
			'currency'             => 'required|exists:currencies,code',
			'terms'                => 'required',
			'photos'               => 'required',
			'affiliate_link'       => 'active_url', 
		);

		// Make rules on inputs
		$validator = Validator::make($request->all(), $rules);

		// Check if validation fails
		if ($validator->fails()) {
			// Error
			return Redirect::to('create')->withErrors($validator)
										 ->withInput();

		}else{

			// Get Inputs Values
			$title       = $request->get('title');
			$description = $request->get('description');
			$category    = $request->get('category');
			$country     = $request->get('country');
			$state       = $request->get('state');
			$price       = $request->get('price');
			$minimum_order_quantity       = $request->get('minimum_order_quantity');
			$supply_time       = $request->get('supply_time');
			$currency    = $request->get('currency');
            $meta_title      = $request->get('meta_title');
            $meta_description      = $request->get('meta_description');
            $user_id      = $request->get('user');
			// Check if user has store
			if (Profile::hasStore(Auth::id())) {

				// Has Store
				$youtube        = $request->get('youtube') ? : NULL ;
				$affiliate_link = $request->get('affiliate_link') ? : NULL;
				$regular_price  = $request->get('regular_price') ? : NULL;

				// Check Regular Price
				if ($regular_price && !Helper::check_price($regular_price)) {
					return redirect('create')->with('error', __('return/error.lang_price_format_invalid'))->withInput();
				}

				// Check youtube
				if ($youtube && (!Protocol::isValidYoutubeURL($youtube))) {
					return redirect('create')->with('error', __('update.lang_invalid_youtube_url'))->withInput();
				}

			}else{

				// Has no store
				$youtube        = NULL;
				$regular_price  = NULL;
				$affiliate_link = NULL;

			}

			// Check Price
			if (!Helper::check_price($price)) {
				return redirect('create')->with('error', __('return/error.lang_price_format_invalid'))->withInput();
			}

			// Create New Ad ID
			$ad_id       = Random::unique();

			// Generate AD Slug
			$slug        = Random::slug($title, $ad_id);
			
			// Get User ID
			$user_id     = Auth::id();
			
			// Create Ad Dates
			$created_at  = Carbon::now();
			$updated_at  = Carbon::now();
			$ends_at     = Helper::ad_ends_at();
			
			// Upload Photos
			$photos      = Input::file('photos');

			// Get general settings
			$general = DB::table('settings_general')->where('id', 1)->first();

			// Check where to upload photos
			if ($general->default_host == 'local') {
				
				// Upload Files to Localhost
				$is_uploaded = Uploader::upload($photos, $ad_id);
				$images_host = 'local';

			}elseif ($general->default_host == 'amazon') {
				
				// Upload Files to Amazon
				$is_uploaded = EverestCloud::uploadToAmazon($photos, $ad_id);
				$images_host = 'amazon';

			}

			// Check if Photos has been successfully uploaded
			if ($is_uploaded) {

				// Get Previews Photos
				$previews          = implode('||', $is_uploaded['previews_array']);

				// Get Thumbnails Photos
				$thumbnails        = implode('||', $is_uploaded['thumbnails_array']);

				// Count Photos
				$photos_number     = count($photos);

				// Check Ad Status
				$status            = Helper::status(true, false);
				
				// Check if Ad Featured
				if (Auth::user()->is_admin || Profile::hasStore(Auth::id())) {
					$is_featured = 1;
				}else{
					$is_featured = 0;
				}
				// Insert Ad
				$ad                 = new Ad;
				$ad->ad_id          = $ad_id;
				$ad->affiliate_link = $affiliate_link;
				$ad->slug           = $slug;
				$ad->user_id        = $user_id;
				$ad->price          = $price;
				$ad->regular_price  = $regular_price;
				$ad->currency       = $currency;
				$ad->category       = $category;
				$ad->photos         = $previews;
				$ad->thumbnails     = $thumbnails;
				$ad->photos_number  = $photos_number;
				$ad->images_host    = $images_host;
				$ad->youtube        = $youtube;
				$ad->supply_time     = $supply_time;
				$ad->title          = $title;
				$ad->description    = $description;
				$ad->minimum_order_quantity        = $minimum_order_quantity;
				$ad->country        = $country;
				$ad->store_id        = $store_id;
				$ad->state          = $state;
				$ad->status         = $status;
				$ad->is_featured    = $is_featured;
				$ad->ends_at        = $ends_at;
                $ad->meta_title     = $meta_title;
                $ad->meta_description = $meta_description;
                $ad->user_id        =$user_id;
				$ad->save();

				// Check for alerts
				$alerts = DB::table('search_alert')->where('keyword', 'LIKE', '%'. $title .'%')->orWhere('keyword', 'LIKE', '%'. $description .'%')->get();

				if (count($alerts)) {
					
					// send a notification to all emails 
					foreach ($alerts as $alert) {
						
						$beautymail = app()->make(Snowfire\Beautymail\Beautymail::class);
					    $beautymail->send('emails.alert_found', [$ad_id], function($message)
					    {
					        $message->to($alert->email)
									->subject('New alert found');
					    });

						// Send alert message
						//Mail::to($alert->email)->send(new AlertMatchFound($ad_id));
						
					}

				}

				// Check if ad need admin review
				if (!$status) {
					
					// Send notification to admins via dashboard
					DB::table('notifications_ads')->insert([
						'user_id'    => $user_id,
						'ad_id'      => $ad_id,
						'created_at' => $created_at,
						'updated_at' => $updated_at,
					]);

					$users = User::where('is_admin', 1)->get();

					foreach ($users as $user) {
						// Send notification to admins via email
                    	$user->notify(new AdPending());
					}

					return Redirect::to('/')->with('success', __('return/success.lang_ad_under_review'));

				}

				// Check if user setting autoshare system
				$autoshare = DB::table('auto_share')->where('user_id', $user_id)->first();

				if ($autoshare) {
					
					// Check if autoshare via twitter is active
					if ($autoshare->tw_active) {
						
						try {
							
							// Generate Status
							$status = Protocol::home().'/product/'.$slug;
							
							$media  = Twitter::uploadMedia([
								'media' => File::get(public_path('uploads/images/'.$ad_id.'/previews/preview_0.jpg'))
							]);
							
							$data   = array(
								'status'    => $status, 
								'media_ids' => $media->media_id_string,
								'format'    => 'json'
							);

							Twitter::postTweet($data);

						} catch (\Exception $e) {
							// Error
							return redirect(Protocol::home().'/product/'.$slug)->with('error', 'Oops!', $e->getMessage());
						}

					}

					// Check if autoshare via facebook is active
					if ($autoshare->fb_active) {
						
						try {

							// Get Facebook App Settings
							$fb = new Facebook([
								'app_id'                => $autoshare->fb_app_id,
								'app_secret'            => $autoshare->fb_app_secret,
								'default_graph_version' => 'v2.2',
							]);

							// Get Access Token
							$accessToken = $autoshare->fb_access_token;

							// Post property to Facebook
							$linkData = [
								'message'     => $title,
								'caption'     => Protocol::home(),
								'name'        => $title,
								'link'        => Protocol::home().'/product/'.$slug,
								'picture'     => Protocol::home().'/uploads/images/'.$ad_id.'/previews/preview_0.jpg',
								'description' => $description
							];

							// Share ad to facebook
							$response = $fb->post('/me/feed', $linkData, $accessToken);

						} catch (\Exception $e) {
							// Error
							return redirect(Protocol::home().'/product/'.$slug)->with('error', 'Oops!', $e->getMessage());
						}

					}

				}

				// Ad Post with success, Show Ad
				return Redirect::to(Protocol::home().'/product/'.$slug);

			}else{

				return redirect('create')->with('error', __('return/error.lang_error_uploading_images'))->withInput();

			}

		}	
			
	}
}