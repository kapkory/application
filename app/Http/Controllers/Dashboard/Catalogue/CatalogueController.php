<?php

namespace App\Http\Controllers\Dashboard\Catalogue;

use App\Models\Catalogue;
use App\Models\CataloguePostingArea;
use App\Models\Country;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;
use Auth;

class CatalogueController extends Controller
{
    public $path = 'dashboard.catalogue.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        return view($this->path.'catalogue',compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // Make rules
        $rules = array(
            'title'   => 'required|max:255|unique:articles',
            'cover'   => 'required|image|mimes:png,jpg,jpeg|max:5000',
            'content' => 'required'
        );
        // Make Validation
        $validator = Validator::make($request->all(), $rules);

        $title             = $request->get('title');
        $cover             = $request->file('cover');
        $slug              = isset($request->slug) ? $request->slug : str_slug($title, '-');
        $content           = $request->get('content');

        // Upload Cover
        $cover_name        = $slug.'.jpg';
        $cover_img         = Image::make($cover->getRealPath());
        $cover_img->encode('jpg', 60);
        $path = 'uploads/general/'.$cover_name;
        $download_path = url('uploads/catalogues').'/'.$cover_name;
        $cover_img->save($path, 60);



        $catalogue = new Catalogue();
        $catalogue->title = $title;
        $catalogue->slug = strtolower($slug);
        $catalogue->image_url = $path;
        $catalogue->document_url = $request->document_url;
        $catalogue->country_id = $request->country;
        $catalogue->content =$content;
        $catalogue->user_id = $request->user()->id;
        $catalogue->meta_title = $request->meta_title;
        $catalogue->meta_description = $request->meta_description;
        $catalogue->save();


        return back()->with('success','You have successfully created a catalogue');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catalogue = Catalogue::whereId($id)->first();
        $countries = Country::all();
        return view($this->path.'edit',compact('catalogue','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Make rules
        $rules = array(
            'title'   => 'required|max:255|unique:articles',
            'cover'   => 'required|image|mimes:png,jpg,jpeg|max:5000',
            'content' => 'required'
        );

        // Make Validation
        $validator = Validator::make($request->all(), $rules);
        $title             = $request->get('title');
        $cover             = $request->file('cover');
        $slug              = isset($request->slug) ? $request->slug : str_slug($title, '-');
        $content           = $request->get('content');
        // Upload Cover
       if ($request->cover)
       {
        $cover_name        = $slug.'.jpg';
        $cover_img         = Image::make($cover->getRealPath());
        $cover_img->encode('jpg', 60);
//        $path = '/home/123480.cloudwaysapps.com/nznszrptrc/public_html/uploads/catalogues'.'/'.$cover_name;
           $path = 'uploads/general/'.$cover_name;
        $cover_img->save($path, 60);
       }
       else
       {
           $path = $request->image_path;
       }

        Catalogue::whereId($id)
            ->update([
                'content'=>$content,
                'title'=>$title,
                'slug'=>$slug,
                'image_url'=> strtolower($path),
                'document_url'=>$request->document_url,
                'country_id'=>$request->country,
                'user_id'=>$request->user()->id,
                'meta_title'=>$request->meta_title,
                'meta_description'=>$request->meta_description,
                'updated_at'     => Carbon::now(),
            ]);
//        dd($request->all());
        return back()->with('success','Catalogue has been successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Catalogue::whereId($id)->delete();

        return back()->with('success','item has been successfully deleted');
    }

    /**
     * view catalogues
     */

    public function displayCatalogues()
    {
        $catalogues = Catalogue::paginate(8);
        return view('dashboard.catalogue.catalogues')->with(['catalogues'=>$catalogues]);

    }
}
