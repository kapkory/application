<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Ad;
use App\Models\Article;
use App\Models\Catalogue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;


class ImageController extends Controller
{
    protected $theme = 'dashboard.images.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = \App\Models\Image::join('articles','images.id','=','articles.image_id')
            ->paginate(10);

        return view($this->theme.'index',compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->theme.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location = 'images';
//      dd($location);
        $file = $request->file('image');
        $size = $request->file('image')->getClientSize();
        $file_name = $request->file('image')->getClientOriginalName();
        $extension = $request->file('image')->getClientOriginalExtension();
//dd($request->file('image')->hashName());
        $cover_img         = Image::make($file->getRealPath());
        $cover_img->encode('jpg', 60);
        $path = 'uploads/general/'.$file_name;

//        $path = public_path($location).$file_name;
        //Enye iko kwa server
//			$path = '/home/123480.cloudwaysapps.com/nznszrptrc/public_html/uploads/articles/'.$cover_name;
        $data = $cover_img->save($path, 60);
        $file = new \App\Models\Image();
        $file->name = $file_name;
        $file->path = strtolower($path);
        $file->file_size = $size;
        $file->file_type = $extension;
        $file->alternate_text = $file_name;
        $file->user_id = $request->user()->id;
        $file->save();

        return back()->with('success','Image has been successfully uploaded');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Models\Image::whereId($id)->delete();

            return back()->with('success','image has been successfully deleted');
    }

    public function view()
    {
        $images = \App\Models\Image::paginate(10);
//        dd($images);
        $articles = Article::paginate(10);
        $products = Ad::paginate(10);
        $catalogues = Catalogue::paginate(10);

//        dd($articles);
//            ->select('articles.title as ')
//dd($articles);
        return view('dashboard.images.index',compact('images','articles','products','catalogues'));

    }
}
