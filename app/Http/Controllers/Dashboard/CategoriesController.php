<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\ArticleCategory;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Store;
use App\Models\Ad;
use Validator;
use DB;
use Helper;
use Carbon\Carbon;
use Image;
use Protocol;

/**
* CategoriesController
*/
class CategoriesController extends Controller
{
	function __construct()
	{
		$this->middleware('admin');
	}

	/**
	 * Get Categories
	 */
	public function categories()
	{
		// Categories
		$categories = DB::table('categories')->orderBy('id', 'desc')->paginate(30);
		$article_categories = ArticleCategory::orderBy('id', 'desc')->paginate(30);

		return view('dashboard.categories.categories')
            ->with(
                ['categories'=>$categories,
                    'article_categories'=>$article_categories
                ]);
	}

	/**
	 * Create New Category
	 */
	public function create()
	{
		return view('dashboard.categories.create');
	}


    /**
     * Create New Article Category
     */
    public function createArticleCategory()
    {
        return view('dashboard.categories.articles');
    }



	/**
	 * Insert New Category
	 */
	public function insert(Request $request)
	{
		// Make Rules
		$rules = array(
			'category_name'   => 'required', 
			'category_slug'   => 'required',
			'is_sub'          => 'boolean',
			'icon' 			  => 'required|image|mimes:png,jpg,jpeg|max:500',
		);

		// Make Rules on Inputs
		$validator = Validator::make($request->all(), $rules);

		// Check if Catch errors
		if ($validator->fails()) {
			
			// Return error catched
			return back()->withErrors($validator)->withInput();

		}else{

			// Get Inputs values
			$category_name = $request->input('category_name');
			$category_slug = $request->input('category_slug');
			$meta_title = $request->input('meta_title');
			$meta_description = $request->input('meta_description');
			$is_sub        = $request->input('is_sub');
			$icon          = $request->file('icon');

			// Get parent category
			if ($is_sub) {
				$parent_category = $request->get('parent_category');
               
				// Check parent category
				$check_parent = DB::table('categories')->where('id', $parent_category)->where('is_sub', 0)->first();

				if (!$check_parent) {
					return back()->with('error', 'Oops! Parent Category not found. Please try again.');
				}
			}else{
				$parent_category = NULL;
				$is_sub          = 0;
			}

			// Check if category slug or name already taken
			$check_category = DB::table('categories')->where('category_name', $category_name)->orWhere('category_slug', $category_slug)->first();

			if ($check_category) {
				return back()->with('error', 'Oops! Category name or slug already taken.');
			}

			// Upload icon
			$icon_name = md5(time()).'.png';
				
			// Upload Icon
			$icon_img  = Image::make($icon->getRealPath());
			
			// Resize Icon
			$icon_img->resize(50, 50);
			
			// Save Avatar
			$icon_img->save('uploads/categories/'.$icon_name);
			
			// Create icon url
			$icon_url  = Protocol::home().'/uploads/categories/'.$icon_name;


			// Insert New Category
			DB::table('categories')->insert([
				'category_name'   => $category_name,
				'category_slug'   => $category_slug,
				'is_sub'          => $is_sub,
				'parent_category' => $parent_category,
				'icon'            => $icon_url,
				'meta_title'            => $meta_title,
				'meta_description'    => $meta_description,
				'created_at'      => Carbon::now(),
				'updated_at'      => Carbon::now(),
			]);

			// Success
			return back()->with('success', 'Congratulations! Category has been successfully added.');

		}
	}

	public function insertArticleCategory(Request $request)
    {
        $rules = array(
            'category_name'   => 'required',
        );

        // Make Rules on Inputs
        $validator = Validator::make($request->all(), $rules);

        // Check if Catch errors
        if ($validator->fails()) {

            // Return error catched
            return back()->withErrors($validator)->withInput();

        }
        else

        {

            if ($request->parent_category == 0)
            {
                $parent_category = null;
                $is_sub = false;
            }
            else
            {
                $parent_category = $request->parent_category;
                $is_sub = 1;
            }
            $a_category = new ArticleCategory();
            $a_category->name = $request->category_name;
            $a_category->description = $request->description;
            $a_category->meta_description = $request->meta_description;
            $a_category->meta_title = $request->meta_title;
            $a_category->parent_category = $parent_category;
            $a_category->category_slug = strtolower(str_slug($request->category_name));
            $a_category->is_sub = $is_sub;
            $a_category->category_slug = $request->category_slug;
            $a_category->save();

        }
        return back()->with('success', 'Congratulations! Article Category has been successfully added.');

    }

	/**
	 * Edit Category
	 */
	public function edit(Request $request, $id)
	{
		// Check category id
		$category = DB::table('categories')->where('id', $id)->first();

		if ($category) {
			
			return view('dashboard.categories.edit')->with('category', $category);

		}else{
			// Not found
			return redirect('/dashboard/categories')->with('error', 'Oops! Category not found.');
		}
	}

	/**
	 * Update Category
	 */
	public function update(Request $request, $id)
	{

		// Check if category exists
		$category = DB::table('categories')->where('id', $id)->first();

		if (!$category) {
			
			// Not found
			return redirect('/dashboard/categories')->with('error', 'Oops! Category not found.');

		}
		// Make Rules
		$rules = array(
			'category_name'   => 'required', 
			'category_slug'   => 'required',
			'icon' 			  => 'image|mimes:png,jpg,jpeg|max:500',
		);

		// Make Rules on Inputs
		$validator = Validator::make($request->all(), $rules);

		// Check if Catch errors
		if ($validator->fails()) {
			
			// Return error catched
			return back()->withErrors($validator);

		}else{

			// Get Inputs values
			$category_name = $request->input('category_name');
			$category_slug = $request->input('category_slug');
			$icon          = $request->file('icon');
            $meta_title = $request->input('meta_title');
            $meta_description = $request->input('meta_description');

			// Check if category slug or name already taken
			$check_category = DB::table('categories')->where('id', '!=', $id)->first();

			if ($check_category) {
				if (($check_category->category_slug == $category_slug) OR ($check_category->category_name == $category_name)) {
					return back()->with('error', 'Oops! Category name or slug already taken. Please try again.');
				}
			}

            if ($request->parent_category == 0)
            {
                $parent_category = null;
            }
            else
            {
                $parent_category = $request->parent_category;
            }

			// Insert New Category
			DB::table('categories')->where('id', $id)->update([
				'category_name'   => $category_name,
				'category_slug'   => $category_slug,
				'updated_at'      => Carbon::now(),
                'parent_category'            => $parent_category,
                'meta_title'            => $meta_title,
                'meta_description'    => $meta_description,
			]);

			if ($icon) {
				
				// Upload icon
				$icon_name = md5(time()).'.png';
					
				// Upload Icon
				$icon_img  = Image::make($icon->getRealPath());
				
				// Resize Icon
				$icon_img->resize(50, 50);
				
				// Save Avatar
				$icon_img->save('uploads/categories/'.$icon_name);
				
				// Create icon url
				$icon_url  = Protocol::home().'/uploads/categories/'.$icon_name;

				DB::table('categories')->where('id', $id)->update([
					'icon'   => $icon_url,
				]);

			}

			// Success
			return back()->with('success', 'Congratulations! Category has been successfully updated.');

		}
	}

	/**
	 * Delete Category
	 */
	public function delete(Request $request, $id)
	{
		// Check category
		$category = Category::where('id', $id)->where('is_sub', 1)->first();

		if ($category) {
			
			// Check if other category exists
			$other_category = Category::where('id', '!=', $id)->where('is_sub', 1)->first();

			if (!$other_category) {
				// Other Category Not found
				return redirect('/dashboard/categories')->with('error', 'Oops! There no other categories. Please try again.');
			}

			// Move ads to other category
			Ad::where('category', $id)->update([
				'category' => $other_category->id
			]);


			// Delete Category
			Category::where('id', $id)->delete();

			// Succescategories/des
			return redirect('/dashboard/categories')->with('success', 'Category has been successfully deleted.');

		}else{
		    $subCategories = Category::where('parent_category',$id)->where('is_sub',0)->get();
		    if ($subCategories->isEmpty())
            {

                Category::where('id', $id)->delete();
                return redirect('/dashboard/categories')->with('success', 'Parent Category has been successfully deleted.');

            }
            else
            {
                return redirect('/dashboard/categories')->with('error', 'Parent Category has Sub Categories, delete its subcategories to proceed.');

            }

			// Not found
			return redirect('/dashboard/categories')->with('error', 'Oops! Category not found or is not a sub category.');
		}
	}

}