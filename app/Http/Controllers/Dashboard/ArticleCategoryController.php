<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Article;
use App\Models\ArticleCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;

class ArticleCategoryController extends Controller
{
    /**
     * Edit Category
     */
    public function edit( $id)
    {
        // Check category id
        $article_category = ArticleCategory::where('id', $id)->first();

        if ($article_category) {

            return view('dashboard.categories.articles.edit')->with('article_category', $article_category);

        }else{
            // Not found
            return redirect('/dashboard/categories')->with('error', 'Oops! Category not found.');
        }
    }


    /**
     * Update Article Category
     */
    public function update(Request $request, $id)
    {

        // Check if category exists
        $category = ArticleCategory::where('id', $id)->first();

        if (!$category) {

            // Not found
            return redirect('/dashboard/categories')->with('error', 'Oops! Category not found.');

        }
        // Make Rules
        $rules = array(
            'category_name'   => 'required',
            'category_slug'   => 'required',
            'meta_title' 	  => 'required',
            'meta_description' 	  => 'required',
        );

        // Make Rules on Inputs
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), $rules);

        // Check if Catch errors
        if ($validator->fails()) {

            // Return error catched
            return back()->withErrors($validator);

        }else{

            // Get Inputs values
            $category_name = $request->input('category_name');
            $category_slug = $request->input('category_slug');
            $meta_title = $request->input('meta_title');
            $meta_description = $request->input('meta_description');


            // Check if category slug or name already taken
            $check_category = ArticleCategory::where('id', '!=', $id)->first();

            if ($check_category) {
                if (($check_category->category_slug == $category_slug) OR ($check_category->name == $category_name)) {
                    return back()->with('error', 'Oops! Category name or slug already taken. Please try again.');
                }
            }

            // Insert New Category
           DB::table('article_categories')->where('id', $id)->update([
                'name'   => $category_name,
                'category_slug'   => $category_slug,
                'meta_title'   => $meta_title,
                'meta_description'   => $meta_description,
                'updated_at'      => Carbon::now(),
            ]);


            // Success
            return back()->with('success', 'Congratulations! Category has been successfully updated.');

        }
    }


    /**
     * Delete Article Category
     */
    public function delete($id)
    {
        // Check category
        $category = ArticleCategory::where('id', $id)->first();

        if ($category) {
            if (count($category->articles) == 0)
            {
                ArticleCategory::where('id', $id)->delete();
                return redirect('/dashboard/categories')->with('success', 'Category has been successfully deleted.');
            }

            if ($category->is_sub == 1){

                // Check if other category exists
                $other_category = ArticleCategory::where('id', '!=', $id)->where('is_sub', 1)->first();

                if (!$other_category) {
                    // Other Category Not found
                    return redirect('/dashboard/categories')->with('error', 'Oops! There no other categories. Please try again.');
                }



                // Change store Category
                Article::where('article_category_id', $id)->update([
                    'article_category_id' => $other_category->id
                ]);

                // Delete Category
                ArticleCategory::where('id', $id)->delete();
                // Success
                return redirect('/dashboard/categories')->with('success', 'Category has been successfully deleted.');


            }
            else
            {
                return redirect('/dashboard/categories')->with('error', 'Oops! Article Category not found or is not a sub category.');

            }


        }

        else{

            // Not found
            return redirect('/dashboard/categories')->with('error', 'Oops! Article Category not found or is not a sub category.');
        }
    }
}
