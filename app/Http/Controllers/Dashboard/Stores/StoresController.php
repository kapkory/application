<?php

namespace App\Http\Controllers\Dashboard\Stores;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Store;
use Validator;
use Image;
use Protocol;
use Uploader;
use Helper;
use Purifier;
use Illuminate\Support\Facades\DB;

/**
* StoresController
*/
class StoresController extends Controller
{
    protected $theme = 'dashboard.stores.';
	function __construct()
	{
		$this->middleware('admin');
	}

	/**
	 * Get Stores
	 */
	public function stores()
	{
		// Stores
		$stores = Store::orderBy('id', 'desc')->paginate(30);

		return view($this->theme.'stores')->with('stores', $stores);
	}

	public function create()
    {
        $countries = Country::all();
        $users = User::all();
        return view($this->theme.'create',compact('countries','users'));
    }

    public function store(Request $request)
    {

            // Check geo settings
            $settings_geo = Helper::settings_geo();

            if ($settings_geo->is_international) {

                // Country rule
                $country_rule = 'required|exists:countries,sortname';

            }else{

                // Country rule
                $country_rule = '';

            }


            // Make Rules
            $rules = array(
                'username'   => [
                    'required',
                    'min:3'
                ],
                'title'      => [
                    'required',
                    'min:3'
                ],
                'short_desc' => 'required',
                'long_desc'  => 'required',
                'country'    => $country_rule,
                'state'      => 'numeric|exists:states,id',
                'fb_page'    => 'active_url',
                'tw_page'    => 'active_url',
                'go_page'    => 'active_url',
                'yt_page'    => 'active_url',
                'website'    => 'active_url',
                'logo'       => 'image|mimes:jpg,jpeg,png|max:5000',
                'cover'      => 'image|mimes:jpg,jpeg,png|max:5000',
            );

            // Make Validation
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                // error
                return back()->withErrors($validator);
            }else{

                 // Get Inputs
                $username   = str_slug($request->get('username'));
                $title      = $request->get('title');
                $short_desc = $request->get('short_desc');
                $long_desc  = Purifier::clean($request->get('long_desc'));
                $country    = $request->get('country');
                $state      = $request->get('state');
                $fb_page    = $request->get('fb_page');
                $tw_page    = $request->get('tw_page');
                $go_page    = $request->get('go_page');
                $yt_page    = $request->get('yt_page');
                $website    = $request->get('website');
                $tawk       = $request->get('tawk');
                $address    = $request->get('address');
                $status    = $request->get('status');
                $logo       = $request->file('logo');
                $cover      = $request->file('cover');
                $meta_title      = $request->get('meta_title');
                $meta_description      = $request->get('meta_description');
                $user_id      = $request->get('user');

                if ($logo) {

                    // Get Store folder
                    $store_path =  'uploads/stores/' . $username;
//                  dd($store_path);
                    // Check if Store Folder exists
                    if (!is_dir($store_path)) {
                         mkdir( 'uploads/stores/' . $username, 0777);
                    } else {
                        // Delete old files
                        Uploader::deleteFolderFiles($store_path);
                    }


//                    dd($logo->getRealPath());
                    // Make new name
                    $logo_name = $username.'.png';
                    // Upload Logo
                    $path = $store_path .'/'. $logo_name;
                        $logo_img = Image::make($logo->getRealPath())
                            ->resize(200, 200)
                            ->save($path);



                    $logo_url = Protocol::home() . '/uploads/stores/' . $username . '/' . $logo_name;

                }


                // Upload new store cover
                if ($cover) {

                    // Make new name
                    $cover_name  = md5(time().uniqid().rand()).'.png';
//                  dd($cover_name);
                    // Get Covers folder
                    $covers_path = 'uploads/covers';

                    // Upload Cover
                    $cover_img   = Image::make($cover->getRealPath());

                    // Save Cover
                    $cover_img->save($covers_path.'/'.$cover_name);

                    // Cover link
                    $cover       = Protocol::home().'/uploads/covers/'.$cover_name;


                }




                // Update Store
                DB::table('stores')->insert([
                    'username'   => $username,
                    'title'      => $title,
                    'short_desc' => $short_desc,
                    'long_desc'  => $long_desc,
                    'address'    => $address,
                    'fb_page'    => $fb_page,
                    'tw_page'    => $tw_page,
                    'go_page'    => $go_page,
                    'yt_page'    => $yt_page,
                    'website'    => $website,
                    'tawk'       => $tawk,
                    'logo'       => $logo_url,
                    'country' => $country,
                    'state' => $state,
                    'status'=>$status,
//                    'city' => $city,
                    'cover' => $cover,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'owner_id' => $user_id,
                    'meta_title'       => $meta_title,
                    'meta_description'       => $meta_description,
                ]);



                // Success
                return redirect()->back()->with('success', 'congratulation your store has been successfully created');

            }

//        }else{
//            // Not found
//            return redirect('/')->with('error', __('return/error.lang_you_dont_have_store'));
//        }
    }

}