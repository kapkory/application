<?php

namespace App\Http\Controllers\Dashboard\Ads;

use App\Models\Brand;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    public function create(){
        $validator = Validator::make(\request()->all(), ['name'=>'required']);

        // Check if validation fails
        if ($validator->fails()) {

            // Error
            return back()->withErrors($validator);

        }
        $brand = new Brand();
        $brand->name = \request()->name;
        $brand->description = \request()->description;
        $brand->save();
        return back()->with('success', 'Congratulations! Brand has been successfully created.');
    }

    public function delete($id){
        $brand = Brand::find($id);
        $brand->delete();
        return back()->with('success', 'Congratulations! Brand has been successfully deleted.');

    }
}
