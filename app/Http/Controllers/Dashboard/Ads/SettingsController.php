<?php

namespace App\Http\Controllers\Dashboard\Ads;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Validator;
use DB;
use Helper;
use App\Models\Ad;

/**
* SettingsController
*/
class SettingsController extends Controller
{
	function __construct()
	{
		$this->middleware('admin');
	}

	/**
	 * Ads Settings
	 */
	public function settings()
	{
		// Get Ads
		$ads = Ad::orderBy('id', 'desc')->paginate(30);
        $brands = Brand::paginate(15);
		return view('dashboard.ads.index',compact('ads','brands'));
	}

}