<?php

namespace App\Http\Controllers\Dashboard\Ads;

use App\Http\Controllers\Controller;
use App\Models\Store;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use DB;
use Helper;
use App\Models\Ad;
use Redirect;
use Uploader;
use Carbon\Carbon;
use Auth;

/**
* EditController
*/
class EditController extends Controller
{

	function __construct()
	{
		$this->middleware('admin');
	}

	/**
	 * Edit Ad
	 */
	public function edit(Request $request, $ad_id)
	{
		// Check ad
		$ad = Ad::where('ad_id', $ad_id)->first();

		if ($ad) {
			
			// Get user id
			$user_id = Auth::id();

			// Check ad owner
			if (($user_id != 1) && ($ad->user_id == 1)) {
				
				// You cannot edit admin ads
				return redirect('/dashboard/ads')->with('error', 'Oops! You cannot edit this ad.');

			}
            $stores = Store::where('owner_id','=',$user_id)->get();

			return view('dashboard.ads.edit')->with(['ad'=> $ad,'users'=>User::all(),'stores'=>$stores]);

		}else{
			// Not found
			return redirect('/dashboard/ads')->with('error', 'Oops! Ad not found.');
		}
	}

	/**
	 * Update Ad
	 */
	public function update(Request $request, $ad_id)
	{
		// Check ad
		$ad = Ad::where('ad_id', $ad_id)->first();

		if ($ad) {
			
			// Get user id
			$user_id = Auth::id();

			// Check ad owner
			if (($user_id != 1) && ($ad->user_id == 1)) {
				
				// You cannot edit admin ads
				return redirect('/dashboard/ads')->with('error', 'Oops! You cannot edit this ad.');

			}

			// Make Rules
			$rules = array(
				'title'       => 'required|max:100', 
				'description' => 'required',
                'minimum_order_quantity'     => 'required',
                'supply_time'      => 'required',
				'status'      => 'required|boolean', 
				'featured'    => 'required|boolean', 
				'archived'    => 'required|boolean', 
				'description' => 'required', 
				'currency'    => 'required|exists:currencies,code', 
				'youtube'     => 'active_url', 
				'category'    => [
			        'required',
			        'numeric',
			    ],
			    'affiliate_link' => 'active_url',
			);

			// Make rules on inputs
			$validator = Validator::make($request->all(), $rules);

			// Check if validation fails
			if ($validator->fails()) {
				
				// Error
				return Redirect::back()->withErrors($validator);

			}else{

				// Get Inputs values
				$title       = $request->get('title');
				$description = $request->get('description');
				$category    = $request->get('category');
				$price       = $request->get('price');
                $supply_time  = $request->get('supply_time');
                $minimum_order_quantity  = $request->get('minimum_order_quantity');
				$status      = $request->get('status');
				$featured    = $request->get('featured');
				$archived    = $request->get('archived');
				$currency    = $request->get('currency');
				$photos      = $request->file('photos');
				$store      = $request->get('store');
                $meta_title        = $request->get('meta_title');
                $meta_description        = $request->get('meta_description');

				// Check Price
				if (!Helper::check_price($price)) {
					// Error price
					return back()->with('error', 'Oops! Price format is not valid.');
				}
                $brand = $request->get('brand') ? : NULL;

				// Has Store
				$youtube        = $request->get('youtube') ? : NULL ;
				$affiliate_link = $request->get('affiliate_link') ? : NULL;
				$regular_price  = $request->get('regular_price') ? : NULL;

				// Check Regular Price
				if (!Helper::check_price($regular_price)) {
					return back()->with('error', __('return/error.lang_price_format_invalid'))->withInput();
				}

				// Update Ad
				Ad::where('ad_id', $ad_id)->update([
					'title'          => $title, 
					'affiliate_link' => $affiliate_link, 
					'regular_price'  => $regular_price, 
					'description'    => $description, 
					'price'          => $price, 
					'store_id'          => $store,
                    'brand_id'          => $brand,
					'category'       => $category,
                    'minimum_order_quantity'     => $minimum_order_quantity,
					'status'         => $status,
                    'supply_time'        => $supply_time,
					'is_featured'    => $featured, 
					'is_archived'    => $archived,
                    'meta_title'         => $meta_title,
                    'meta_description' => $meta_description,
					'currency'       => $currency, 
					'youtube'        => $youtube, 
					'updated_at'     => Carbon::now(),
				]);

				if ($photos) {

					// Count Photos
					$count_photos = count($photos);

					// Replace Photos
					$is_uploaded = Uploader::edit($photos, $ad_id);

					// Check if photos uploaded
					if ($is_uploaded) {
						
						// Get Previews Photos
						$previews          = implode('||', $is_uploaded['previews_array']);

						// Get Thumbnails Photos
						$thumbnails        = implode('||', $is_uploaded['thumbnails_array']);

						// Count Photos
						$photos_number     = count($photos);

						// Update Ad
						Ad::where('ad_id', $ad_id)->update([
							'photos'        => $previews,
							'thumbnails'    => $thumbnails,
							'photos_number' => $photos_number,
						]);

					}else{

						// Error uploading photos
						return back()->with('error', 'Oops! Something went wrong while uploading photos.');

					}
				}

				// check if update ad status
				if (!$ad->status) {
					if ($status) {
						// Send notification
						DB::table('notifications_ads_accepted')->insert([
							'user_id'    => $ad->user_id, 
							'ad_id'      => $ad->ad_id, 
							'created_at' => Carbon::now(), 
						]);
					}
				}

				return back()->with('success', 'Congratulations! Ad has been successfully updated.');

			}

		}else{
			// Not found
			return redirect('dashboard/ads')->with('error', 'Oops! Ad not found.');
		}
	}

}