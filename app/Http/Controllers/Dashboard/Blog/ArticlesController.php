<?php

namespace App\Http\Controllers\Dashboard\Blog;

use App\Library\Config\Tagger;
use App\Models\ArticleCategory;
use App\Models\Tag;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Validator;
use Image;
use Auth;

class ArticlesController extends Controller
{
    function __construct()
    {
    	$this->middleware('admin');
    }

    /**
     * Get All articles
     */
    public function articles()
    {
    	// Get articles
    	$articles = Article::orderBy('id', 'desc')->paginate(30);

    	return view('dashboard.blog.articles', compact('articles'));
    }

    /**
     * Create new Article
     */
    public function create()
    {
        $categories = ArticleCategory::all();
        $users = User::where('is_author','=',1)->orWhere('is_admin','=',1)->get();
    	return view('dashboard.blog.create',[
    	    'categories'=>$categories,
    	    'users'=>$users
        ]);
    }

    /**
     * Insert Article
     */
    public function insert(Request $request)
    {
    	// Make rules
    	$rules = array(
			'title'   => 'required|max:255|unique:articles', 
			'cover'   => 'required|image|mimes:png,jpg,jpeg|max:5000', 
			'content' => 'required'
    	);

    	// Make Validation
    	$validator = Validator::make($request->all(), $rules);

    	if ($validator->fails()) {
    		
    		// error
    		return redirect('/dashboard/articles')->withErrors($validator)->withInput();

    	}else{
			// Get Inputs
			$title             = $request->get('title');
			$cover             = $request->file('cover');
			$content           = $request->get('content');
			$meta_title           = $request->get('meta_title');
			$meta_description           = $request->get('meta_description');
            $user_id = ($request->user_id) ? $request->user_id : $request->user()->id;
            $is_featured = ($request->is_featured) ? 1 : 0;
            $show_author = ($request->show_author) ? 1 : 0;

			// Get Online admin username
			$username          = Auth::user()->username;
			
			// Generate Slug
			$slug              = isset($request->slug) ? str_slug($request->slug,'-') : str_slug($title, '-');

			$checkSlug = Article::where('slug',$slug)->get();
			if (!$checkSlug->isEmpty())
                return redirect()->back()->with('error', 'The Article Slug is already in use.')->withInput();

            // Upload Cover
			$cover_name        = strtolower($slug.'.jpg');
			$cover_img         = Image::make($cover->getRealPath());
			$cover_img->encode('jpg', 60);
                       $path = 'uploads/articles/'.$cover_name;
              //Enye iko kwa server
//			$path = '/home/123480.cloudwaysapps.com/nznszrptrc/public_html/uploads/articles/'.$cover_name;
			$data = $cover_img->save($path, 60);


            // Save Article
			$article           = new Article;
			$article->username = $username;
			$article->title    = $title;
			$article->slug     = $slug;
			$article->cover    = strtolower($cover_name);
//			$article->tags     = json_encode($request->tags);
			$article->article_category_id    = $request->article_category;
			$article->user_id = $user_id;
			$article->content  = $content;
			$article->show_author  = $show_author;
			$article->is_featured  = $is_featured;
			$article->meta_title  = $meta_title;
			$article->meta_description  = $meta_description;
			$article->save();

            if ($request->get('tags')){

                $tags = explode(',',$request->get('tags'));
                Tagger::articleTag($article,$tags);
            }


			// Success
			return redirect('/dashboard/articles')->with('success', 'Article has been successfully added.');

    	}

    }




}
