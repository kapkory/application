<?php

namespace App\Http\Controllers\Dashboard\Blog;

use App\Models\ArticleComment;
use App\Notifications\CommentActivated;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;

class CommentController extends Controller
{
    public function index()
    {
        // Get comments
        $comments = DB::table('article_comments')->orderBy('id', 'desc')->paginate(30);

        return view('dashboard.blog.comments')->with('comments', $comments);
    }

    /**
     * Read Article Comment
     */
    public function read($id)
    {

        // Check comment
        $comment = DB::table('article_comments')->where('id', $id)->first();

        if ($comment) {

            return view('dashboard.blog.comments.read')->with('comment', $comment);

        }else{
            // Not found
            return redirect('dashboard/article-comments')->with('error', 'Oops! Comment not found.');
        }
    }

    /**
     * Delete Article Comment
     */
    public function delete($id)
    {
        // Check comment
        $comment = DB::table('article_comments')->where('id', $id)->first();

        if ($comment) {

            // Get auth id
            $user_id = Auth::id();

            // You cannot delete admin comment
            if (($user_id != 1) && ($comment->user_id == 1)) {
                return redirect('/dashboard/comments')->with('error', 'Oops! You cannot delete admin comments');
            }


            // Delete Comment
            DB::table('article_comments')->where('id', $id)->delete();

            return redirect('/dashboard/article-comments')->with('success', 'Comment has been successfully deleted.');

        }else{

            // Not Found
            return redirect('/dashboard/article-comments')->with('error', 'Oops! Comment not found.');

        }
    }



    /**
     * Active Comment
     */
    public function active($id)
    {
        // Check comment
        $comment = DB::table('article_comments')->where('id', $id)->where('status', 0)->first();

        if ($comment) {
            if ($comment->user_id == 0)
            {
                $title = 'Your comment has been approved';
                $content = 'Your comment has been approved by the admin';
                Mail::send('emails.send', ['title' => $title, 'content' => $content], function ($message) use ($comment)
                {

                    $message->from('admin@sanitaryware.org', 'Sanitaryware.org');

                    $message->to($comment->email);

                });
            }
            else
            {
                $user= User::where('id',$comment->user_id)->first();
                $user->notify(new CommentActivated());

            }

            // Active comment
            DB::table('article_comments')->where('id', $id)->update([
                'status' => 1
            ]);

            // Success
            return redirect('/dashboard/article-comments')->with('success', 'Congratulations! Comment has been successfully updated.');

        }else{
            // Not found
            return redirect('dashboard/article-comments')->with('error', 'Oops! Comment not found or already active.');
        }

    }



    /**
     * Inactive Comment
     */
    public function inactive(Request $request, $id)
    {
        // Check comment
        $comment = DB::table('article_comments')->where('id', $id)->where('status', 1)->first();

        if ($comment) {

            $user_id = Auth::id();
            // You cannot inactive admin comment
            if (($user_id != 1) && ($comment->user_id == 1)) {
                return redirect('dashboard/article-comments')->with('error', 'Oops! You cannot inactive admin comments');
            }

            // Inactive comment
            DB::table('article_comments')->where('id', $id)->update([
                'status' => 0
            ]);

            // Success
            return redirect('dashboard/article-comments')->with('success', 'Congratulations! Comment has been successfully updated.');

        }else{
            // Not found
            return redirect('dashboard/article-comments')->with('error', 'Oops! Comment not found or already inactive.');
        }

    }

    /**
     * Edit Comment
     */
    public function edit( $id)
    {
        // Check comment
        $comment = DB::table('article_comments')->where('id', $id)->first();

        if ($comment) {

            // Get user id
            $user_id = Auth::id();

            // You cannot edit admin comment
            if (($user_id != 1) && ($comment->user_id == 1)) {
                return redirect('/dashboard/article-comments')->with('error', 'Oops! You cannot edit admin comments');
            }

            return view('dashboard.blog.comments.edit')->with('comment', $comment);

        }else{
            // Not found
            return redirect('dashboard/article-comments')->with('error', 'Oops! Comment not found.');
        }

    }


    /**
     * Update Comment
     */
    public function update(Request $request, $id)
    {
        // Check comment
        $comment = DB::table('article_comments')->where('id', $id)->first();

        if ($comment) {

            // Get user id
            $user_id = Auth::id();

            // You cannot edit admin comment
            if (($user_id != 1) && ($comment->user_id == 1)) {
                return redirect('/dashboard/comments')->with('error', 'Oops! You cannot edit admin comments');
            }

            // Check comment content
            $validator = Validator::make($request->all(), [
                'content' => 'required'
            ]);

            if ($validator->fails()) {
                return redirect('/dashboard/article-comments')->withErrors($validator);
            }else{

                // Update comment
                DB::table('article_comments')->where('id', $id)->update([
                    'content' => $request->get('content')
                ]);

                // Success
                return redirect('/dashboard/article-comments')->with('success', 'Congratulations! Comment has been successfully updated.');

            }

        }else{
            // Not found
            return redirect('dashboard/article-comments')->with('error', 'Oops! Comment not found.');
        }

    }

}
