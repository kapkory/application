<?php

namespace App\Http\Controllers\Dashboard\Blog;

use App\Library\Config\Tagger;
use App\Models\ArticleCategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\Article;
use Image;

class OptionsController extends Controller
{
    
	/**
	 * Edit Article
	 */
	public function edit(Request $request, $id)
	{
		// check post id
		$article = Article::where('id', $id)->first();
        $categories = ArticleCategory::all();
//        $users = User::all();
        $users = User::where('is_author','=',1)->orWhere('is_admin','=',1)->get();

		if ($article) {
			
			// Edit
			return view('dashboard.blog.edit', compact('article','categories','users'));

		}else{

			// Not found
			return redirect('dashboard/articles')->with('error', 'Oops! Article not found.');

		}
	}

	/**
	 * Update Article
	 */
	public function update(Request $request, $id)
	{
//	    dd($request->all());
		// Check Article
		$article = Article::where('id', $id)->first();

		if ($article) {
			
			// Validate Form
			$request->validate([
				'title'   => [
				Rule::unique('articles')->ignore($id),
				'required',
				'max:255',
				],
				'slug'=>'required',
				'cover'   => 'image|mimes:png,jpg,jpeg|max:5000', 
				'content' => 'required'
			]);

			// Get Form Data
			$title             = $request->get('title');
			$cover             = $request->file('cover');
			$content           = $request->get('content');
			$slug           = $request->get('slug');
			$article_category           = $request->get('article_category');
			$meta_title           = $request->get('meta_title');
			$meta_description           = $request->get('meta_description');
            $tags = explode(',',$request->get('tags'));
            $user_id = ($request->user_id) ? $request->user_id : $request->user()->id;
            $is_featured = isset($request->is_featured) ? 1 : 0;
            $show_author = isset($request->show_author) ? 1 : 0;
			// Generate Slug
//			$slug              = str_slug($title, '-');

			// Check if request has file
			if ($request->hasFile('cover')) {

				// Upload Cover
				$cover_name        = $slug.'.jpg';
				$cover_img         = Image::make($cover->getRealPath());
				$cover_img->encode('jpg', 60);
				$path = 'uploads/articles/'.$cover_name;
//               $path = '/home/123480.cloudwaysapps.com/nznszrptrc/public_html/uploads/articles/'.$cover_name;

                $cover_img->save($path, 60);

				// Update Article
                $article->title  = $title;
                $article->cover  = strtolower($cover_name);
                $article->slug  = $slug;
                $article->content  = $content;
                $article->article_category_id  = $article_category;
                $article->meta_title  = $meta_title;
                $article->is_featured  = $is_featured;
                $article->meta_description  = $meta_description;
                $article->user_id  = $user_id;
                $article->show_author  = $show_author;
                $article->save();

                Tagger::removeTags($article);
                Tagger::articleTag($article,$tags);
				// Success
				return redirect('dashboard/articles')->with('success', 'Congratulations! Article has been successfully updated.');

			}else{
				// Update Article
                $article->title  = $title;
                $article->slug  = $slug;
                $article->content  = $content;
                $article->article_category_id  = $article_category;
                $article->meta_title  = $meta_title;
                $article->is_featured  = $is_featured;
                $article->meta_description  = $meta_description;
                $article->user_id  = $user_id;
                $article->show_author  = $show_author;
                $article->save();

                Tagger::removeTags($article);
                Tagger::articleTag($article,$tags);

                // Success
				return redirect('dashboard/articles')->with('success', 'Congratulations! Article has been successfully updated.');

			}



		}else{

			// Not found
			return redirect('dashboard/articles')->with('error', 'Oops! Article not found.');

		}
	}

	/**
	 * Delete Article
	 */
	public function delete(Request $request, $id)
	{
		// Check article
		$article = Article::where('id', $id)->first();

		if ($article) {
			
			// Delete Article
			$article->delete();

			// Success
			return redirect('dashboard/articles')->with('success', 'Congratulations! Article has been successfully deleted');

		}else{

			// Not found
			return redirect('dashboard/articles')->with('error', 'Oops! Article not found.');

		}
	}


	public function publish($id){
	    if (!auth())
	        return redirect('/');

        $article = Article::where('id', $id)->first();
        $article->status = 1;
        $article->save();
        return redirect('dashboard/articles')->with('success', $article->title.' article has been published.');
    }

}
