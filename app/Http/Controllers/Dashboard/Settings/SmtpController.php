<?php

namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;

/**
* SmtpController
*/
class SmtpController extends Controller
{
	function __construct()
	{
		$this->middleware('admin');
	}

	/**
	 * Get smtp settings
	 */
	public function smtp()
	{
		return view('dashboard.settings.smtp');
	}

	/**
	 * Update Settings
	 */
	public function update(Request $request)
	{
		// Make Rules
		$rules = array(
			'host'     => 'required', 
			'port'     => 'required|numeric', 
			'username' => 'required', 
			'password' => 'required', 
			'email'    => 'required|email', 
			'name'     => 'required', 
		);

		// Run rules 
		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			// error
			return back()->withErrors($validator);
		}else{

			// Get inputs
			$host     = $request->get('host');
			$port     = $request->get('port');
			$username = $request->get('username');
			$password = $request->get('password');
			$email    = $request->get('email');
			$name     = $request->get('name');

			// Update Settings
			$config = new \Larapack\ConfigWriter\Repository('mail');

			$config->set('host', $host);
			$config->set('port', $port);
			$config->set('username', $username);
			$config->set('password', $password);
			$config->set('from.address', $email);
			$config->set('from.name', $name);

			$config->save();

			// Success
			return back()->with('success', 'Congratulations! Update has been successfully updated.');

		}
	}

}