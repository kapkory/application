<?php

namespace App\Http\Controllers\Dashboard\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Currency;
use Helper;

/**
* GeoController
*/
class GeoController extends Controller
{
	function __construct()
	{
		$this->middleware('admin');
	}

	/**
	 * Edit App Geo Settings
	 */
	public function edit()
	{
		// Get Settings
		$settings   = DB::table('settings_geo')->where('id', 1)->first();
		
		// Get Countries
		$countries  = Country::get();
		
		// Get States
		$states     = State::where('country_id', $settings->default_country)->get();
		
		// Get cities
		$cities     = City::where('state_id', $settings->default_state)->get();
		
		// Get Currencies
		$currencies = Currency::get();
		
		// Send data
		$data       = array(
			'settings'   => $settings, 
			'countries'  => $countries, 
			'states'     => $states, 
			'cities'     => $cities, 
			'currencies' => $currencies, 
		);

		return view('dashboard.settings.geo')->with($data);
	}

	/**
	 * Update Settings
	 */
	public function update(Request $request)
	{
		// Make Rules
		$rules = array(
			'is_international' => 'required|boolean', 
			'states_enabled'   => 'required|boolean', 
			'cities_enabled'   => 'required|boolean', 
			'default_country'  => 'required|numeric|exists:countries,id', 
			'default_state'    => 'required|numeric|exists:states,id', 
			'default_city'     => 'required|numeric|exists:cities,id', 
			'default_currency' => 'required|exists:currencies,code', 
		);

		// Run rules on requested inputs
		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {
			
			// Get Inputs values
			$is_international = $request->get('is_international');
			$default_country  = $request->get('default_country');
			$default_state    = $request->get('default_state');
			$default_city     = $request->get('default_city');
			$default_currency = $request->get('default_currency');
			$states_enabled   = $request->get('states_enabled');
			$cities_enabled   = $request->get('cities_enabled');

			// Check if country, state, city are correct
			$state = State::where('id', $default_state)->where('country_id', $default_country)->first();

			if ($state) {
				
				// Check city
				$city = City::where('id', $default_city)->where('state_id', $default_state)->first();

				if (!$city) {
					
					// City not found
					return redirect('dashboard/settings/geo')->with('error', 'Oops! City not found in this state.');

				}	

			}else{

				// State not found
				return redirect('dashboard/settings/geo')->with('error', 'Oops! State not found in this country.');

			}

			// Update Settings
			DB::table('settings_geo')->where('id', 1)->update([
				'is_international' => $is_international,
				'default_country'  => $default_country,
				'default_state'    => $default_state,
				'default_city'     => $default_city,
				'default_currency' => $default_currency,
				'states_enabled'   => $states_enabled,
				'cities_enabled'   => $cities_enabled,
			]);

			// success
			return back()->with('success', 'Congratulations! Settings has been successfully updated.');

		}else{

			// Error Happend
			return back()->withErrors($validator);

		}
	}

}