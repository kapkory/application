<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ad;
use App\Models\Stats;
use App\User;
use DB;
use Charts;
use Config;

/**
* HomeController
*/
class HomeController extends Controller
{
	function __construct()
	{
		$this->middleware('admin');
	}

	/**
	 * Get Index Page
	 */
	public function index()
	{
        // Get Stats
        $total_ads      = Ad::count();
        $total_cats     = DB::table('categories')->count();
        $total_stores   = DB::table('stores')->count();
        $total_users    = DB::table('users')->count();
        $total_messages = DB::table('admin_mailbox')->count();
        $total_comments = DB::table('comments')->count();
        $total_views    = DB::table('stats')->count();
        $total_pages    = DB::table('pages')->count();

        // Get Ad Visits
        $visits    = Charts::database(Stats::get(), 'line', 'highcharts')
            ->title('Ad Visits')
            ->elementLabel("Total Visits")
            ->responsive(false)
            ->dimensions(0,500)
            ->lastByDay(7, true);

        // Get Latest Users
        $latest_users  = User::orderBy('id', 'desc')->take(10)->get();
        $latest_stores = DB::table('stores')->orderBy('id', 'desc')->take(10)->get();

        // Send Data
        $data = array(
            'total_ads'      => $total_ads,
            'total_cats'     => $total_cats,
            'total_stores'   => $total_stores,
            'total_users'    => $total_users,
            'total_messages' => $total_messages,
            'total_comments' => $total_comments,
            'total_views'    => $total_views,
            'total_pages'    => $total_pages,
            'visits'         => $visits,
            'latest_users'   => $latest_users,
            'latest_stores'  => $latest_stores,
        );

//		dd($data);
        return view('dashboard.index')->with($data);
	}

	/**
	 * Maintenance Mode
	 */
	public function maintenance()
	{
		// Get Maintenance Status
		$maintenance = DB::table('settings_general')->where('id', 1)->first();

		return view('dashboard.maintenance')->with('maintenance', $maintenance);
	}

	/**
	 * Update Maintenance Mode status
	 */
	public function update_maintenance(Request $request)
	{
		// Get Maintenance Status
		$maintenance = $request->get('maintenance');

		if (!is_bool($maintenance)) {
			
			// Error
			return redirect('/dashboard/maintenance')->with('error', 'Oops! Something went wrong please try again.');

		}

		// Update maintenance status
		DB::table('settings_general')->where('id', 1)->update([
			'is_maintenance' => $maintenance
		]);

		// Success
		return redirect('/dashboard/maintenance')->with('success', 'Maintenance mode has been successfully updated.');
	}

}