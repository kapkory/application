<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function index($id){
        $ad = Ad::whereId($id)->first();
        if (!$ad)
            return back();

        return redirect($ad->affiliate_link);
    }

//    was to update all ads to have external links similar to their ids
//    public function updateAll(){
//        $ads = Ad::whereNotNull('affiliate_link')->get();
//        foreach ($ads as $ad){
//            $product = Ad::whereId($ad->id)->first();
//            $product->external_link = 'external/'.$ad->id;
//            $product->save();
//        }
//        return back();
//    }
}
