<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Theme;

class MaintenanceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try {

            if (DB::connection()->getPdo()) {

                // Check maintenance mode
                $settings = DB::table('settings_general')->where('id', 1)->first();

                if ($settings->is_maintenance) {
                    
                    // Site is under maintenance
                    return view(Theme::get().'.maintenance');

                }

                return $next($request);

            }
            
        } catch (\Exception $e) {
            return $next($request);
        }
        
    }
}
