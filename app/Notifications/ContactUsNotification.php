<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactUsNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $full_name;
    protected $email;
    protected $phone;
    protected $message;
    protected $subject;
    protected $message_id;

    public function __construct($details)
    {
        $this->full_name = $details['full_name'];
        $this->email = $details['email'];
        $this->phone = $details['phone'];
        $this->message = $details['message'];
        $this->subject = $details['subject'];
        $this->message_id = $details['message_id'];
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject($this->subject)
                    ->line($this->full_name.' has tried to contact you.')
                    ->line('Email: '.$this->email)
                    ->line('Phone: '.$this->phone)
                    ->line('Contact Message: ')
                    ->line($this->message)
                    ->action('Read Message', url('dashboard/messages/admin/read/'.$this->message_id));

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
