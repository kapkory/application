<?php

namespace App\Notifications;

use App\Models\Article;
use App\Models\ArticleComment;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class newArticleComment extends Notification
{
    use Queueable;
    protected $article_comment_id;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($article_comment_id)
    {
        $this->article_comment_id = $article_comment_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $articleComment = ArticleComment::whereId($this->article_comment_id)->first();
        if ($articleComment->user_id == 0)
        {
            $name = $articleComment->name;
            $email = $articleComment->email;
        }
        else{
            $user = User::whereId($articleComment->user_id)->first();
            $name = $user->name;
            $email = $user->email;
        }
        $article = Article::whereId($articleComment->article_id)->first();
        return (new MailMessage)
            ->greeting('Hello '.$notifiable->first_name)
            ->subject('You have new comment on #'.$article->title )
            ->line('Name: '.$name)
            ->line('User Email: '.$email)
            ->line('Message: ')
            ->line($articleComment->content)
            ->action('View Comment', url('dashboard/article-comments/read/'.$articleComment->id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
