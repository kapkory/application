<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Catalogue extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function cataloguepostingarea()
    {
        return $this->hasOne(CataloguePostingArea::class,'catalogue_id');
    }
}
