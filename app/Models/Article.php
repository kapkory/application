<?php

namespace App\Models;

use App\Models\ArticleCategory;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    protected $table = 'articles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'slug', 
        'cover', 
        'title',
        'user_id',
        'is_featured',
        'content'
    ];

    public function articleComments()
    {
        return $this->hasMany(ArticleComment::class);
    }

    public function articleCategory()
    {
        return $this->belongsTo(ArticleCategory::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class,'article_tags');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

}
