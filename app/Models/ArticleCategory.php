<?php

namespace App\Models;

use App\Models\Article;
use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $fillable = ['name','description','meta_description','meta_title','category_slug','is_sub','parent_category'];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
