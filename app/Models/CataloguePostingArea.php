<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CataloguePostingArea extends Model
{
    public function catalogue()
    {
        return $this->belongsTo(Catalogue::class);
    }
}
