<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'sortname',
    	'name',
    	'phonecode',
    ];

    public $timestamps = false;

    public function catalogues()
    {
        return $this->hasMany(Catalogue::class);
    }


    public function getNameAttribute($name)
    {
        return strtolower($name);
    }

    public function adcountries()
    {
        return $this->belongsTo(AdCountry::class);
    }
}
