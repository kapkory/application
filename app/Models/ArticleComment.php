<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Notifiable;

class ArticleComment extends Model
{
 protected $fillable = ['parent_id','article_id','user_id','content','status'];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function children()
    {
        return $this->hasMany(ArticleComment::class,'parent_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
