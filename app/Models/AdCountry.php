<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdCountry extends Model
{
    public function ad()
    {
        return $this->belongsTo(Ad::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);

    }
}
