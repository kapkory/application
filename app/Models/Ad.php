<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{

    protected $table = 'ads';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ad_id', 
        'slug', 
        'user_id', 
        'category', 
        'title',
        'store_id',
        'minimum_order_quantity',
        'supply_time',
        'description',
        'country',
        'city',
        'state',
        'price',
        'currency',
        'status',
        'meta_title',
        'meta_description',
        'is_new',
        'is_featured',
        'is_archived',
        'photos_number',
        'images_host',
        'youtube',
        'ends_at',
    ];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function adcountries()
    {
        return $this->hasMany(AdCountry::class);
    }

    public function getCountries(){
        $countries = AdCountry::join('countries','ad_countries.country_id','=','countries.id')
                     ->join('ads','ad_countries.ad_id','=','ads.id')
                    ->where('ad_countries.ad_id',$this->id)
                    ->select('ad_countries.id as id','countries.name as text')
                    ->get();
        return $countries;
    }

}
