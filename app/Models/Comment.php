<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id','ad_id','parent_id','content','is_pinned','status'];

    public function ad()
    {
        return $this->belongsTo(Ad::class);
    }

    public function children()
    {
        return $this->hasMany(Comment::class,'parent_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
