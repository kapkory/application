<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Protocol;
use App\Models\Ad;

class NotifyError extends Mailable {

    use Queueable,
        SerializesModels;

    /**

     * The user instance.

     *

     * @var Order

     */
    public $error_data;

    /**

     * Create a new message instance.

     *

     * @return void

     */
    public function __construct($error_data) {

        $this->error_data = $error_data;
    }

    /**

     * Build the message.

     *

     * @return $this

     */
    public function build() {
//        echo '<pre/>';
//        print_r($this->error_data);
//        die;

        return $this->subject($this->error_data['subject'])
                        ->view('emails.notify_error')
                        ->with('t', $this->error_data);
    }

}
