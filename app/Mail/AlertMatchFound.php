<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AlertMatchFound extends Mailable
{
    use Queueable, SerializesModels;

    public $ad_id = "";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ad_id)
    {
        $this->ad_id = $ad_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.alert_found')->with('ad_id', $this->ad_id);
    }
}
