<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Response;
use Input;
use Route;
use Illuminate\Notifications\Messages\MailMessage;
use Request;
use Mail;
use Auth;

class Handler extends ExceptionHandler
{
/**
 * A list of the exception types that should not be reported.
 *
 * @var array
 */
protected $dontReport = [
\Illuminate\Auth\AuthenticationException::class,
 \Illuminate\Auth\Access\AuthorizationException::class,
 \Symfony\Component\HttpKernel\Exception\HttpException::class,
 \Illuminate\Database\Eloquent\ModelNotFoundException::class,
 \Illuminate\Session\TokenMismatchException::class,
 \Illuminate\Validation\ValidationException::class,
];

/**
 * Report or log an exception.
 *
 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
 *
 * @param  \Exception  $exception
 * @return void
 */
public function report(Exception $exception)
{
parent::report($exception);
}

/**
 * Render an exception into an HTTP response.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \Exception  $exception
 * @return \Illuminate\Http\Response
 */
public function render($request, Exception $exception)
{

$message = $exception->getMessage();


$line = $exception->getLine();
$file = $exception->getFile();

$route = $request->path();

$params = '<b>Parameters passed by user </b>: ';
foreach (Input::all() as $key => $val) {
if (!is_array($val)) {
$params .= '<br>' . $key . ' => ' . $val;
}
}
$route_missing_file = 'RouteCollection.php';
$route_missing_file2 = 'Application.php';
$subject = 'Error : Sanitaryware.org ';
if(strpos($file, $route_missing_file)!== false || strpos($file, $route_missing_file2)!== false ) {
$message = 'Wrong URL Entered';
$subject .= '| Wrong URL Entered';
}
$ip = $_SERVER['REMOTE_ADDR'];
$username = '';
if (Auth::check())
{
$username = Auth::user()->email;
}
//$user = Auth::user();
//echo Auth::user()->email;
//die;
$err_info = array(
'ip' => $ip,
 'file' => $file,
 'line' => $line,
 'message' => $message,
 'params' => $params,
 'username' => $username,
 'route' => $route,
 'subject' => $subject,
);


$email = ["testmail7794@gmail.com","vm3567@gmail.com","iandancun@gmail.com"];
Mail::to($email)
->send(new \App\Mail\NotifyError($err_info));
//echo '<pre/>';
//print_R($err_info);
//die;

if ($exception instanceof \Illuminate\Session\TokenMismatchException) {

// Check if ajax request
if ($request->ajax()) {
$response = array(
'status' => 'error',
 'msg' => __('return/error.lang_session_expired'),
);
return Response::json($response);
}
return back()->with('error', __('return/error.lang_session_expired'));
}

return parent::render($request, $exception);
}

/**
 * Convert an authentication exception into an unauthenticated response.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  \Illuminate\Auth\AuthenticationException  $exception
 * @return \Illuminate\Http\Response
 */
protected function unauthenticated($request, AuthenticationException $exception)
{
if ($request->expectsJson()) {
return response()->json(['error' => 'Unauthenticated.'], 401);
}

return redirect()->guest('auth/login');
}
}
