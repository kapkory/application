<?php

namespace App;

use App\Models\Ad;
use App\Models\Article;
use App\Models\ArticleComment;
use App\Models\Catalogue;
use App\Models\Comment;
use App\Models\Country;
use App\Models\Image;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cache;
use Auth;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'first_name', 
        'last_name', 
        'email', 
        'password',
        'avatar',
        'country_code',
        'state',
        'city',
        'gender',
        'phone',
        'phonecode',
        'phone_hidden',
        'account_type',
        'is_admin',
        'is_moderator',
        'status',
        'has_store',
        'facebook_id',
        'twitter_id',
        'google_id',
        'instagram_id',
        'pinterest_id',
        'linkedin_id',
        'vk_id',
        'is_author',
        'identifyme_id',
        'is_2fa',
        'last_login_ip',
        'last_login_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'google2fa_secret'
    ];

    /**
     * Check if user online
     */
    public static function isOnline($id)
    {
        return Cache::has('user-online-'.$id);
    }

    /**
     * Route notifications for the Nexmo channel.
     *
     * @return string
     */
    public function routeNotificationForNexmo()
    {
        return $this->phone;
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function articleComments()
    {
        return $this->hasMany(ArticleComment::class);
    }

    public function catalogues()
    {
        return $this->hasMany(Catalogue::class);
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function ads()
    {
        return $this->hasMany(Ad::class);
    }


}
