<?php
$data = 'test';
?>
@extends (Theme::get().'.layout.app')

@section('styles')

   <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/404.css">
    <title> 404 NOT FOUND </title>

<style>
    .footer{
        display: none !important;
    }

    .navbar {

        clear: both;

    }

</style>
@endsection



@section ('content')

    <div class="page-container page-container-responsive">
        <div class="row">
            <div class="row space-top-8 space-8 row-table pull-left" style="margin-top: 20px; ">
                <div class="col-5 col-sm-5 col-middle text-center">
                    <img src="{{ url('content/assets/front-end/images/404.jpg') }}" alt="SanitaryWare 404 Page Icon." width="313" height="428">
                </div>
                <div class="col-5 col-sm-5 col-middle" style="color: black; margin-bottom: 20px">
                    <h1 class="text-jumbo text-ginormous" style="font-weight: 700;font-size: 60px;">Oops!</h1>
                    <h3>We can't seem to find the page you're looking for</h3>
                    <h6>404 not found</h6>
                    <h4>Here are some helpful links instead:</h4>
                    <ul class="list-unstyled text-center" id="list-unstyled">
                        <li>
                            <a class="btn-sm ui2-button ui2-button-primary ui2-button-minor ui2-button-large ma-button-start-order"
                               href="{{ url('/') }}" style="padding: 5px 10px;">Home</a></li>
                        <li>
                            <a class="btn-sm ui2-button ui2-button-primary ui2-button-minor ui2-button-large ma-button-start-order "
                               href="{{ url('article') }}" style="padding: 5px 10px;">Articles</a></li>
                        <li>
                            <a class="btn-sm ui2-button ui2-button-primary ui2-button-minor ui2-button-large ma-button-start-order"
                               href="{{ url('stores') }}" style="padding: 5px 10px;">Market</a></li>
                        <li>
                            <a class="btn-sm ui2-button ui2-button-primary ui2-button-minor ui2-button-large ma-button-start-order"
                               href="{{ url('catalogue') }}" style="padding: 5px 10px;">Catalogue</a></li>
                        <li>
                            <a class="btn-sm ui2-button ui2-button-primary ui2-button-minor ui2-button-large ma-button-start-order"
                               href="{{ url('auth/login') }}" style="padding: 5px 10px;">Account</a></li>
                        <li>
                            <a class="btn-sm ui2-button ui2-button-primary ui2-button-minor ui2-button-large ma-button-start-order"
                               href="{{ url('contact') }}" style="padding: 5px 10px; margin-top: 4px;">Contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection