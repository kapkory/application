@extends (Theme::get().'.layout.app')

@section('styles')
    {{--	<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/article.css" >--}}

@endsection
@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('content')
    <div class="row-padding">
    <div class="col-md-12">


        <div class="row">
            <div class="col-md-8 col-md-offset-1">

                <!-- Sessions Message -->
                @if (Session::has('error'))
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        {{ Session::get('error') }}
                    </div>
                @endif

                @if (Session::has('success'))
                    <div class="alert bg-success alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                        {{ Session::get('success') }}
                    </div>
                @endif

                <form action="{{ url('create-advert') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object border-blue text-blue"><i class="icon-envelop5"></i></div>
                            <h5 class="content-group">{{ Lang::get('contact.lang_get_in_touch') }} <small class="display-block">{{ Lang::get('contact.lang_contact_us_directly') }}</small></h5>
                        </div>

                        <div class="form-group  {{ $errors->has('full_name') ? 'has-error' : '' }}">
                            <label>{{ Lang::get('contact.lang_full_name') }} *</label>
                            <input type="text" name="full_name" class="form-control" placeholder="{{ Lang::get('contact.lang_full_name_placeholder') }}" value="{{ old('email') }}" value="{{ old('full_name') }}">
                            @if ($errors->has('full_name'))
                                <span class="help-block">{{ $errors->first('full_name') }}</span>
                            @endif
                        </div>

                        <!-- Email Address -->
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
                            <label>Company Name</label>
                            <input type="text" name="company_name" class="form-control" placeholder="Company Name" value="{{ old('company_name') }}">
                            @if ($errors->has('company_name'))
                                <span class="help-block">{{ $errors->first('company_name') }}</span>
                            @endif
                        </div>

                        <!-- Phone Number -->
                        <div class="form-group  {{ $errors->has('number') ? 'has-error' : '' }}">
                            <label>Number *</label>
                            <input type="text" name="number" class="form-control" placeholder="Enter Your Number" value="{{ old('number') }}">
                            @if ($errors->has('number'))
                                <span class="help-block">{{ $errors->first('number') }}</span>
                            @endif
                        </div>


                        <!-- Your Message -->
                        <div class="form-group  {{ $errors->has('remarks') ? 'has-error' : '' }}">
                            <label>Remarks:</label>
                            <textarea rows="5" cols="5" class="form-control" placeholder="{{ Lang::get('contact.lang_your_message_placeholder') }}" name="remarks">{{ old('remarks') }}</textarea>
                            @if ($errors->has('remarks'))
                                <span class="help-block">{{ $errors->first('remarks') }}</span>
                            @endif
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-default">{{ Lang::get('contact.lang_send_message') }}</button>
                        </div>

                    </div>
                </form>

            </div>

        </div>
    </div>
    </div>
@endsection