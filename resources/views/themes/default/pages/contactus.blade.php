@extends (Theme::get().'.layout.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/contact.css?p={{time()}}" >

@endsection
@section ('seo')

{!! SEO::generate() !!}

@endsection
@section ('content')
<div class="col-md-12">
    <div class="row-padding">
        <div class="panel panel-body">
            <div class="row">
                <div class="col-md-7 col-md-offset-3 col-lg-offset-3 col-sm-offset-3 center">
                    <div class="col-md-9">
                        <div class="cntct-outr ">
                            <div class="cntct-img cntct-img col-lg-offset-4 col-sm-offset-4">
                                <img src="{{ Protocol::home() }}/content/assets/front-end/images/Venkatmani.jpg" />
                            </div>
                            <div class="cntct-abt">
                                <div class="cntct-name">
                                    Venkat Mani
                                </div>
                                <div class="cntct-abt">
                                    I 'm a <span class="dark">Ceramic Engineer</span> from India. Working in <span class="dark">Sanitaryware Manufacturing</span> industry for 10 Years. I can speak Tamil, Telugu, Hindi and English.
                                </div>
                                <div class="cntct-socail">
                                    <a class="fa-2x fa fa-facebook" title="Facebook" target="_blank" href="https://www.facebook.com/venkat.mani.58">&nbsp;</a>
                                    <a class="fa-2x fa fa-google-plus" title="Google Plus" target="_blank" href="https://plus.google.com/+VenkatmaniM"></a>
                                    <a class="fa-2x fa fa-envelope-o" title="Email" target="_blank" href="mailto:venkat@sanitaryware.org"></a>
                                </div>
                                <div class="cntct-phone">
                                    <i class="fa fa-phone" >&nbsp;</i> &nbsp; +919500693318, +60163867944
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-md-5 ">
                    <div class="cntct-outr cntct-form">

                        <!-- Sessions Message -->
                        @if (Session::has('error'))
                            <div class="alert bg-danger alert-styled-left">
                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        @if (Session::has('success'))
                            <div class="alert bg-success alert-styled-left">
                                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                {{ Session::get('success') }}
                            </div>
                        @endif

                        <form action="{{ Protocol::home() }}/contact" method="POST">
                            {{ csrf_field() }}
                            <div class=" login-form">
                                <div class="text-center cntct-frm-hd">
                                    How Can I help you?  I reply almost every query. <br>Type your query, hit the {{Lang::get('contact.lang_send_message')}} button. <br>You will get my reply soon.
                                </div>
                                <div class="form-group  {{ $errors->has('full_name') ? 'has-error' : '' }}">
                                    <input type="text" name="full_name" class="cntct-input" placeholder="{{ Lang::get('contact.lang_full_name_placeholder') }}" value="{{ old('full_name') }}" value="{{ old('full_name') }}">
                                    @if ($errors->has('full_name'))
                                        <span class="help-block">{{ $errors->first('full_name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <input type="email" name="email" class="cntct-input" placeholder="{{ Lang::get('contact.lang_email_address_placeholder') }}" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group  {{ $errors->has('phone') ? 'has-error' : '' }}">
                                    <input type="text" name="phone" class="cntct-input" placeholder="{{ Lang::get('contact.lang_phone_placeholder') }}" value="{{ old('phone') }}">
                                    @if ($errors->has('phone'))
                                        <span class="help-block">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>
                                <div class="form-group  {{ $errors->has('subject') ? 'has-error' : '' }}">
                                    <input type="text" name="subject" class="cntct-input" placeholder="{{ Lang::get('contact.lang_subject_placeholder') }}" value="{{ old('subject') }}">
                                    @if ($errors->has('subject'))
                                        <span class="help-block">{{ $errors->first('subject') }}</span>
                                    @endif
                                </div>
                                <div class="form-group  {{ $errors->has('message') ? 'has-error' : '' }}">
                                    <textarea rows="5" cols="5" class="cntct-input" placeholder="{{ Lang::get('contact.lang_your_message_placeholder') }}" name="message">{{ old('message') }}</textarea>
                                    @if ($errors->has('message'))
                                        <span class="help-block">{{ $errors->first('message') }}</span>
                                    @endif
                                </div>
                                {!! app('captcha')->render('en'); !!}
                                <div class="text-right">
                                    <button type="submit" class="cntct-btn">{{ Lang::get('contact.lang_send_message') }}</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection