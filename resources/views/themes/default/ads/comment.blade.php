
                    <li class="media commentScroll">
                        <div class="media-left">
                            @if (Profile::hasStore($comment->user_id))
                                <a href="{{ Protocol::home() }}/store/{{ Profile::hasStore($comment->user_id)->username }}"><img src="{{ Profile::picture($comment->user_id) }}" class="img-circle img-sm" alt="{{ Profile::hasStore($comment->user_id)->title }}"></a>
                            @else
                                <a href="#"><img src="{{ Profile::picture($comment->user_id) }}" class="img-circle img-sm" alt=""></a>
                            @endif
                        </div>

                        <div class="media-body">
                            <div class="media-heading comments-heading">
                                <a href="{{ Profile::hasStore($comment->user_id) ? Protocol::home().'/store/'.Profile::hasStore($comment->user_id)->username : '#' }}" class="{{ Profile::hasStore($comment->user_id) ? 'trusted-badge' : '' }} label label-primary label-rounded">{{   Profile::first_name($comment->user_id) }}</a>
                                <span class="media-annotation dotted">{{ Helper::date_ago($comment->created_at) }}</span>
                                @if ($comment->is_pinned)
                                    <span class="media-annotation dotted text-black text-uppercase">Pinned</span>
                                @endif
                            </div>

                            <p style="margin-top: 10px;" class="emojioneareaCm">{!! nl2br($comment->content) !!}</p>

                            <ul class="list-inline list-inline-separate text-size-small">

                            @if (Auth::check())
                                @if ((Auth::id() === $ad->user_id) && (Auth::id() === $comment->user_id))

                                    <!-- Pin Comment -->
                                        <li><a class="pinComment" data-comment-id="{{ $comment->id }}" data-ad-id="{{ $ad->ad_id }}">{{ Lang::get('ads/show.lang_pin_comment') }}</a></li>

                                @endif

                                @if (Auth::id() === $comment->user_id)
                                    <!-- Edit Comment -->
                                        <li><a href="{{ Protocol::home() }}/account/comments/edit/{{ $comment->id }}">{{ Lang::get('ads/show.lang_edit_comment') }}</a></li>

                                        <!-- Delete Comment -->
                                        <li><a href="{{ Protocol::home() }}/account/comments/delete/{{ $comment->id }}">{{ Lang::get('ads/show.lang_delete_comment') }}</a></li>

                                    @else
                                    {{--<li><a href="">Reply</a> </li>--}}
                                        <li><a  class="display_reply"  data-comment-id="{{ $comment->id }}">{{ Lang::get('ads/show.lang_reply_comment') }}</a></li>
                                    @endif

                                    <li><a href="#" class="reportComment" data-comment-id="{{ $comment->id }}">{{ Lang::get('ads/show.lang_report_comment') }}</a></li>

                                @endif

                            </ul>

                        </div>

                    </li>
                    <hr>


                    @if ($comment->children->count() > 0)
                        {{--<a  id="replies">Replies</a>--}}
                        @foreach ($comment->children as $comment)
                            <div style="margin-left: 50px; " class="replies">
                                @include('themes.default.ads.comment', ['comment' => $comment])
                            </div>
                        @endforeach
                    @endif

                    <div>
                        <form action="{{  Protocol::home() }}/comments/reply" method="POST" id="replyComment"  style="display: none">

                           {!! csrf_field() !!}

                            <div class="content-group" id="spinnerDark">
                                <textarea id="comment" rows="5" cols="5" class="form-control" placeholder="{{ Lang::get('ads/show.lang_add_comment_placeholder') }}" name="comment" id="commentContent"></textarea>
                            </div>
                            <input type="hidden" name="parent_id">
                            <input type="hidden" name="ad_id">

                            <div class="text-right">
                                <button type="submit" class="btn bg-blue" id="submit_reply"><i id="spinnerIcon" style="display: none;" class="icon-spinner4 spinner position-left"></i> {{ Lang::get('ads/show.lang_reply_comment_btn') }}
                                </button>
                            </div>

                        </form>
                    </div>





<script>



    {{--$('#submit_reply').click(function (e) {--}}
        {{--e.preventDefault();--}}
        {{--var comment =  $('#comment').val();--}}
        {{--var parent_id =  $('#parent_id').val();--}}
        {{--var url = "{{ url('account/comments/reply') }}";--}}
        {{--$.post(url,{'parent_id':parent_id, 'comment': comment},function (response) {--}}
        {{--console.log(response);--}}
        {{--});--}}
    {{--});--}}
//    $('#replies').click(function () {
//        $('.replies').show();
//    });
</script>