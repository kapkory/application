@if ($comment->children->count() > 0)
    @foreach ($comment->children as $comment)
        <div style="margin-left: 30px">
            @include('themes.default.ads.comment', ['comment' => $comment])
        </div>
    @endforeach
@endif