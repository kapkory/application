

@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('content')
<style type="text/css">
	.card .card-ad-covers {
		border-top-left-radius: 8px;
		border-top-right-radius: 8px;
		max-width: 100%;
		object-fit: cover;
		display: block;
		width: 100%;
		height: 300px;
		max-height: 350px;
		background-position: 50%;
		background-repeat: no-repeat;
	}
</style>
<div class="row-padding">
	
	<!-- Ads -->
	<div class="col-md-9">

		<!-- Filter -->
		<div class="row ">
			<div class="col-md-12">
				<div>
					<h1>Browse all products in our listing</h1>
				</div>
				<div class="navbar navbar-default navbar-xs navbar-component">
					<ul class="nav navbar-nav no-border visible-xs-block">
						<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
					</ul>

					<div class="navbar-collapse collapse" id="navbar-filter">
						<p class="navbar-text">{{ Lang::get('category.lang_filter') }}</p>
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-time-asc position-left"></i> {{ Lang::get('category.lang_by_date') }} <span class="caret"></span></a>
								<ul class="dropdown-menu" style="margin-top: 5px;">
									<li><a href="{{ Protocol::home() }}/browse">{{ Lang::get('category.lang_show_all') }}</a></li>
									<li class="divider"></li>
									<li><a href="{{ Protocol::home() }}/browse?date=today">{{ Lang::get('category.lang_today') }}</a></li>
									<li><a href="{{ Protocol::home() }}/browse?date=yesterday">{{ Lang::get('category.lang_yesterday') }}</a></li>
									<li><a href="{{ Protocol::home() }}/browse?date=week">{{ Lang::get('category.lang_week') }}</a></li>
									<li><a href="{{ Protocol::home() }}/browse?date=month">{{ Lang::get('category.lang_month') }}</a></li>
									<li><a href="{{ Protocol::home() }}/browse?date=year">{{ Lang::get('category.lang_year') }}</a></li>
								</ul>
							</li>

							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-amount-desc position-left"></i> {{ Lang::get('category.lang_by_status') }} <span class="caret"></span></a>
								<ul class="dropdown-menu" style="margin-top: 5px;">
									<li><a href="{{ Protocol::home() }}/browse">{{ Lang::get('category.lang_show_all') }}</a></li>
									<li class="divider"></li>
									<li><a href="{{ Protocol::home() }}/browse?status=featured">{{ Lang::get('category.lang_featured') }}</a></li>
									<li><a href="{{ Protocol::home() }}/browse?status=normal">{{ Lang::get('category.lang_not_featured') }}</a></li>
								</ul>
							</li>

							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort position-left"></i> {{ Lang::get('category.lang_by_condition') }} <span class="caret"></span></a>
								<ul class="dropdown-menu" style="margin-top: 5px;">
									<li><a href="{{ Protocol::home() }}/browse">{{ Lang::get('category.lang_show_all') }}</a></li>
									<li class="divider"></li>
									<li><a href="{{ Protocol::home() }}/browse?condition=used">{{ Lang::get('category.lang_used') }}</a></li>
									<li><a href="{{ Protocol::home() }}/browse?condition=new">{{ Lang::get('category.lang_new') }}</a></li>
								</ul>
							</li>
						</ul>

						<div class="navbar-right text-right">
							<a href="{{ Protocol::home() }}/random" class="text-muted text-uppercase" style="line-height: 47px;">{{ Lang::get('search.lang_random') }}</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Browse Ads -->
		<div class="row">

            @if (count($ads))
            @foreach ($ads as $ad)

            <div class="col-md-4" style="padding-left: 5px; padding-right: 5px;">

                <div class="card card-blog">
                    <ul class="tags">
                        @if ($ad->is_featured)
                        <li>{{ Lang::get('home.lang_featured') }}</li>
                        @endif

                        @if ($ad->is_oos)
                        <li class="oos">{{ Lang::get('update_three.lang_out_of_stock') }}</li>
                        @endif
                    </ul>
                    <div class="card-image">
                        <a href="{{ Protocol::home() }}/product/{{ $ad->slug }}" {{ !is_null($ad->affiliate_link) ? 'target="_blank"' : '' }}>
                            <div class="img card-ad-covers" style="background-image: url({{ EverestCloud::getThumnail($ad->ad_id, $ad->images_host) }});" title="{{ $ad->title }}"></div>
                        </a>
                    </div>
                    <div class="card-block">
						<h6 class="card-title"
							style="line-height: 1.25em;margin-top: 0; padding: 10px; font-size: 15px">
							<a href="{{ Protocol::home() }}/product/{{ $ad->slug }}">{{ $ad->title }}</a>
						</h6>
                        <div class="card-footer">
                            <div id="price">
                                @if (!is_null($ad->regular_price))
                                <span class="price price-old"> {{ number_format($ad->regular_price, 2) }} {{ $ad->currency }}</span>
                                @endif
                                <span class="price price-new"> {{ ($ad->price) ? number_format($ad->price, 2) .' '. $ad->currency : 'Contact seller for price' }}</span>
                            </div>
                            <div class="author">
                                <div class="card__avatar"><a href="{{ Profile::hasStore($ad->user_id) ? Protocol::home().'/store/'.Profile::hasStore($ad->user_id)->username : '#' }}" class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img src="{{ Profile::picture($ad->user_id) }}" alt="{{ Profile::hasStore($ad->user_id) ? Profile::hasStore($ad->user_id)->title : Profile::full_name($ad->user_id) }}" class="avatar" width="40" height="40">@if (Profile::hasStore($ad->user_id))<i class="icon-checkmark3" data-popup="tooltip" data-placement="top" data-container="body" title="{{ Lang::get('update_two.lang_verified_account') }}"></i>@endif</a></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            @endforeach
            <div class="col-md-12 text-center mb-20">
            	{{ $ads->links() }}
            </div>
            @else 
            <div class="col-md-12">
	            <div class="alert bg-info alert-styled-left">
					<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
					@lang ('return/info.lang_nothing_to_show')
			    </div>
		    </div>
            @endif

        </div>

	</div>

	<!-- Left Side -->
	<div class="col-md-3">
		
		<!-- Advertisement -->
		<div class="advertisment">
			{!! Helper::advertisements()->ad_sidebar !!}
		</div>

	</div>

</div>

@endsection