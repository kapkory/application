@extends (Theme::get().'.layout.app')
@section ('seo')

    {!! SEO::generate() !!}

@endsection
@section ('styles')
    <link href="{{ Protocol::home() }}/contents/assets/front-end/css/shop.css" type="text/css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{{ Protocol::home() }}/content/assets/front-end/css/flexslider.css" type="text/css" media="screen"/>

    <link href="{{ Protocol::home() }}/content/assets/front-end/style.css" type="text/css" rel="stylesheet" media="all">
    <!--<link href="plugins/style.css" type="text/css" rel="stylesheet" media="all">-->
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/product.css" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    {{--    <link href="{{ Protocol::home() }}/css/fontawesome-all.min.css" rel="stylesheet">--}}
    <style type="text/css">

        .affiliate {
            margin: 0;
            padding: 0;
            left: 10px;
            top: 10px;
            list-style: none;
            position: absolute;
            z-index: 9;
            height: 30px;
        }

        .affiliate li::before {
            content: "";
            float: left;
            position: absolute;
            top: 0;
            left: -12px;
            width: 0;
            height: 0;
            border-color: transparent #2d5ead transparent transparent;
            border-style: solid;
            border-width: 12px 12px 12px 0;
        }

        .affiliate li {
            float: right;
            height: 24px;
            line-height: 24px;
            position: relative;
            margin: 2px 5px 2px 12px;
            padding: 0 10px 0 12px;
            background: #2d5ead;
            color: #fff;
            text-decoration: none;
            -moz-border-radius-bottom-right: 2px;
            -webkit-border-bottom-right-radius: 2px;
            border-bottom-right-radius: 2px;
            -moz-border-radius-top-right: 2px;
            -webkit-border-top-right-radius: 2px;
            border-top-right-radius: 2px;
            font-family: 'Fira Sans', 'Droid Arabic Kufi', sans-serif;
            text-transform: uppercase;
            font-size: 12px;
            letter-spacing: 1px;
        }

        .affiliate li::after {
            content: "";
            position: absolute;
            top: 10px;
            left: 0;
            float: left;
            width: 4px;
            height: 4px;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
            background: #fff;
            -moz-box-shadow: -1px -1px 2px #004977;
            -webkit-box-shadow: -1px -1px 2px #004977;
            box-shadow: -1px -1px 2px #004977;
        }

        .ma-main-operate {
            /*background: #f5f7fa;*/
             padding: 0 18px 16px;
        }

        .scc-wrapper .ui2-button {
            box-sizing: border-box;
        }

        .ma-button-contact-supplier {
            padding: 0 15px;
            font-size: 16px;
            min-width: 176px;
            box-sizing: border-box;
            vertical-align: top;
        }

        .ui2-button-primary, button.ui2-button-primary {
            background: #ff7519;
            color: #fff;
            border-color: #ff7519;
        }

        .ui2-button-large {
            height: 40px;
            padding: 0 20px;
            line-height: 40px;
            font-size: 18px;
        }

        .ui2-button-normal, .ui2-button-primary, .ui2-button-secondary {
            border: 1px solid transparent;
            border-top-color: transparent;
            border-right-color: transparent;
            border-bottom-color: transparent;
            border-left-color: transparent;
            border-radius: 3px;
            vertical-align: baseline;
            font-style: normal;
            font-family: inherit;
            color: #333;
            background: transparent;
            cursor: pointer;
        }

        .ui2-button {
            position: relative;
            display: inline-block;
            box-sizing: border-box;
        }

        .ui2-button-primary:hover, a.ui2-button-primary:hover {
            background: #eb650c;
            border-color: #eb650c;
            color: #fff;
        }

        .ma-button-start-order {
            margin-left: 16px;
            min-width: 150px;
            box-sizing: border-box;
            text-align: center;
            background: #fff0e5;
            border: 1px solid #ff6a00;
            border-radius: 3px;
            color: #ff6a00;
        }

        button.ui2-button-minor, button.ui2-button-minor {
            background: #fff0e5;
            border: 1px solid #ff6a00;
            border-radius: 3px;
            color: #ff6a00;
        }

        .single-right-left h3 {

            text-transform: capitalize;
            font-size: 19px;
            color: #000;
            margin: 0;
            font-weight: 600;

        }

        body {
            font-family: source sans pro, sans-serif;

        }

        table th {
            color: #212529;
            font-weight: 500;
            line-height: 1.6;
            padding: 10px;
            border: none;
            font-size: 1em;
        }

    </style>
    <script src="{{ Protocol::home() }}/content/assets/front-end/js/jquery-2.2.3.min.js"></script>
    <script src="{{ Protocol::home() }}/content/assets/front-end/js/imagezoom.js"></script>
    <script src="{{ Protocol::home() }}/content/assets/front-end/js/owl.carousel.js"></script>
@endsection


@section ('content')
    <div class=" row-padding">
        @if (Auth::check())
            @if ($ad->user_id == Auth::id())
                <div class="col-md-12">
                    <div class="alert bg-info alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span>
                        </button>
                        @lang ('return/info.lang_this_is_one_of_your_ads')
                    </div>
                </div>
            @endif

            @if ($ad->is_trashed)
                <div class="col-md-12">
                    <div class="alert bg-danger alert-styled-left">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span>
                        </button>
                        @lang ('return/error.lang_this_ad_is_trashed')
                    </div>
                </div>
            @endif
        @endif

        @if (Auth::check())
            @if (!$ad->status)
                <div class="col-md-12">
                    <div class="alert alert-danger alert-styled-left alert-bordered">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span>
                        </button>
                        @lang ('return/error.lang_this_ad_is_under_review')
                    </div>
                </div>
            @endif
        @endif
    </div>
    <div class="row-padding">
        <div class="row">
            <!-- Page Breadcrumb -->
            <div class="col-md-12">

                <div class="breadcrumb-line breadcrumb-line-component content-group-lg">
                    <ul class="breadcrumb text-uppercase">
                        <li><a href="{{ Protocol::home() }}/"><i
                                        class="icon-home2 position-left"></i> {{ Lang::get('header.lang_home') }}</a>
                        </li>
                        <li>
                            <a href="{{ Helper::get_category($ad->category, true) }}">{{ Helper::get_category($ad->category) }}</a>
                        </li>
                        <li class="active">{{ $ad->title }}</li>
                    </ul>

                    <ul class="breadcrumb-elements text-uppercase">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-gear position-left"></i>
                                {{ Lang::get('options.lang_options') }}
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-left">

                            @if (Auth::check())

                                @if (Auth::id() == $ad->user_id)
                                    <!-- Upgrade Ad -->
                                        <li><a href="{{ Protocol::home() }}/account/ads/upgrade/{{ $ad->ad_id }}"><i
                                                        class="icon-chess-queen"></i> {{ Lang::get('options.lang_upgrade_ad') }}
                                            </a></li>

                                        <!-- Archive Ad -->
                                        <li><a href="{{ Protocol::home() }}/account/ads/archive/{{ $ad->ad_id }}"><i
                                                        class="icon-archive"></i> {{ Lang::get('options.lang_archive_ad') }}
                                            </a></li>

                                        <!-- Archive Ad -->
                                        <li><a href="{{ Protocol::home() }}/account/ads/stats/{{ $ad->ad_id }}"><i
                                                        class="icon-stats-bars2"></i> {{ Lang::get('options.lang_statistics') }}
                                            </a></li>
                                    @endif
                                    <li><a href="#" data-toggle="modal" data-target="#make_offer"><i
                                                    class="icon-wallet pull-left"></i> {{ Lang::get('options.lang_make_offer') }}
                                        </a></li>

                                    <li><a href="#" id="addToFavorite" data-ad-id="{{ $ad->ad_id }}"><i
                                                    class="icon-heart6 pull-left"></i> {{ Lang::get('options.lang_add_to_favorite') }}
                                        </a></li>
                                @endif

                                <li><a href="#send" data-toggle="modal" data-target="#sendToFriend"><i
                                                class="icon-envelope pull-left"></i> {{ Lang::get('options.lang_send_to_friend') }}
                                    </a></li>


                            </ul>
                        </li>
                    </ul>
                </div>

                @if (Auth::check())
                    <div id="make_offer" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header bg-success">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title text-uppercase">{{ Lang::get('ads/show.lang_make_offer') }}</h5>
                                </div>

                                <form action="{{ Protocol::home() }}/offers/make" method="POST" id="sendOffer">
                                    <div class="modal-body">

                                        <meta name="csrf-token" content="{{ csrf_token() }}">

                                        <!-- Your Price -->
                                        <div class="form-group">
                                            <label>{{ Lang::get('ads/show.lang_your_price') }}</label>
                                            <input type="text"
                                                   placeholder="{{ Lang::get('ads/show.lang_your_price_placeholder') }}"
                                                   id="offerPrice" class="form-control" name="price">
                                            <span class="help-block">{{ Lang::get('ads/show.lang_the_amount_required') }} <b>{{ ($ad->price) ? number_format($ad->price, 2).' '. $ad->currency : 'Contact Seller For Price' }}</b></span>
                                        </div>

                                        <!-- Post ID -->
                                        <div class="form-group">
                                            <label>{{ Lang::get('ads/show.lang_post_id') }}</label>
                                            <input type="text" readonly=""
                                                   placeholder="{{ Lang::get('ads/show.lang_post_id') }}" id="postID"
                                                   value="{{ $ad->ad_id }}" class="form-control" name="ad_id">
                                        </div>

                                    </div>

                                    <div class="modal-footer">
                                        <button type="submit"
                                                class="btn btn-success">{{ Lang::get('ads/show.lang_send_offer') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            @endif

            <!-- Send to friend -->
                <div id="sendToFriend" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-success text-uppercase">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title text-uppercase">{{ Lang::get('ads/show.lang_send_to_friend') }}</h5>
                            </div>

                            <form action="{{ Protocol::home() }}/tools/send" method="POST" id="sendFriend">
                                <div class="modal-body">

                                    <meta name="csrf-token" content="{{ csrf_token() }}" id="sendToFriendToken">

                                    <input hidden="" name="ad_id" content="{{ $ad->ad_id }}" id="sendToFriendAdId">

                                    <!-- Your Email Address -->
                                    <div class="form-group">
                                        <label>{{ Lang::get('ads/show.lang_your_email_address') }}</label>
                                        <input type="email" required=""
                                               placeholder="{{ Lang::get('ads/show.lang_your_email_address_placeholder') }}"
                                               id="sender_email" class="form-control" name="sender_email">
                                    </div>

                                    <!-- Friend Email Address -->
                                    <div class="form-group">
                                        <label>{{ Lang::get('ads/show.lang_your_friend_email_address') }}</label>
                                        <input type="email" required=""
                                               placeholder="{{ Lang::get('ads/show.lang_your_friend_email_address_placeholder') }}"
                                               id="friend_email" class="form-control" name="friend_email">
                                    </div>

                                    <!-- Message Content -->
                                    <div class="form-group">
                                        <label>{{ Lang::get('ads/show.lang_your_message') }}</label>
                                        <textarea rows="4" required=""
                                                  placeholder="{{ Lang::get('ads/show.lang_your_message_placeholder') }}"
                                                  id="messageContent" class="form-control"
                                                  name="messageContent"></textarea>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <button type="submit"
                                            class="btn btn-success">{{ Lang::get('ads/show.lang_send_message') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Single -->
    <div class="innerf-pages section ">
        <div class="container">
            <div class="row my-sm-5" style="margin-top: 0.1rem">
                <div class="col-lg-4 single-right-left" style="margin-right: 0px !important;">
                    {{--<div class="grid images_3_of_2">--}}
                    <div class="flexslider1">

                        <div class="clearfix"></div>
                        <div class="flex-viewport" style="overflow: hidden; position: relative;">
                            <ul class="slides" id="product_image">
                                <li style="width: 100%; background-size: cover"
                                    data-thumb="{!! Helper::getAdPhoto($ad->photos, $ad->images_host) !!}"
                                    style="width: 350px; float: left; display: block;" class="flex-active-slide">
                                    <div class="thumb-image">
                                        <img src="{!! Helper::getAdPhoto($ad->photos, $ad->images_host) !!}"
                                             data-imagezoom="true" alt=" " class="img-fluid"></div>
                                </li>
                            </ul>
                        </div>
                        {!! Helper::getAdPhotos($ad->photos, $ad->images_host) !!}
                        <ul class="flex-direction-nav">
                            <li><a class="flex-prev" href="#">Previous</a></li>
                            <li><a class="flex-next" href="#">Next</a></li>
                        </ul>
                    </div>
                    {{--</div>--}}
                </div>

                <div class="col-lg-5 mt-lg-0 mt-5 single-right-left simpleCart_shelfItem" style="max-width: 520px">
                    <h3>
                        {{ $ad->title }}
                    </h3>
                    <hr>
                    <!--<div class="caption">-->
                    <!--<div class="clearfix"></div>-->
                    <!--<h6 style="color: #eb650c; font-weight: 500;">INR 870 / Piece</h6>-->
                    <!--</div>-->
                    <div class="desc_single">
                        <div class="card-profile">
                            <div class="card-profile-body">
                                <table class="table">
                                    <tbody>
                                    <tr style="">
                                        <th style="font-size: 20px;">Price</th>
                                        <td style="font-size: 20px;font-weight: 700;color: #eb650c; line-height: 4px; !important;">
                                            @if($ad->price)
                                                <span style="font-size: 20px;font-weight: 700;color: #eb650c;float: right; line-height: 24px; !important;"> {{  $ad->currency.' '. number_format($ad->price, 2)  }}
                                                    /Piece </span>
                                                &nbsp;
                                            @else
                                                Contact Seller For Price
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Supplier Country</th>
                                        <td>{{ ucwords(Helper::supplier_country($ad->user->country_code)->name.' ') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Supplier State</th>
                                        <td>{{ ucwords(Helper::supplier_state($ad->user->state)->name.' ') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Country Products delivered to</th>
                                        <td>
                                            @if($ad->deliverable_to == 1)
                                                <span class="badge badge-primary">Globally</span>
                                            @elseif($ad->deliverable_to == 2)
                                                @foreach($ad->adcountries as $adcountry)
                                                    <span class="badge badge-primary">{{ $adcountry->country->name }}</span>
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                    @if($ad->external_link && !is_null($ad->brand_id))
                                        <tr>
                                            <th> Brand</th>
                                            <td><b>{{ @$ad->brand->name }}</b></td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <th>{{ Lang::get('ads/show.lang_minimum') }}</th>
                                        <td><b>{{ $ad->minimum_order_quantity }}Piece/Pieces<br></span>(Min. Order)</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Supply Date(days)</th>
                                        <td><b>{{ $ad->supply_time }}</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="ma-main-operate" style="padding-top: 10px">
                                    @if(is_null($ad->external_link))
                                        <button type="button"
                                                class="ui2-button ui2-button-primary ui2-button-large ma-button-contact-supplier"
                                                data-toggle="modal" data-target="#show_phone_number" rel="nofollow"><i
                                                    class="icon-phone2"></i> {{ Lang::get('update_two.lang_phone_number') }}
                                        </button>
                                        <button type="button"
                                                class="ui2-button ui2-button-primary ui2-button-minor ui2-button-large ma-button-start-order"
                                                data-toggle="modal" @auth data-target="#contact_seller"
                                                @endauth @guest data-target="#loginToContact" @endguest><i
                                                    class="icon-info3"></i>{{ Lang::get('update_two.lang_contact_seller') }}
                                        </button>

                                    @else
                                        <a href="{{ url($ad->external_link) }}" target="_self">
                                            <button type="button"
                                                    class="ui2-button ui2-button-primary ui2-button-large ma-button-contact-supplier"
                                                    rel="nofollow"><i
                                                        class="glyphicon glyphicon-shopping-cart"></i> {{ Lang::get('update_two.lang_shopping') }}
                                            </button>
                                        </a>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 cl-sm-3 mt-lg-0 mt-5 single-right-left simpleCart_shelfItem">
                    <div class="sidebar-widget">
                        <h3><b>Seller Info</b></h3>
                        <div class="sidebar-user-info">
                            <div class="row">
                                @if($ad->store_id == 0)
                                    <div class="col-md-3 col-xs-3">
                                        <img src="{{ url(@$ad->user->avatar) }}" height="57.5" width="57.5"
                                             class="img-circle img-responsive">
                                    </div>
                                    <div class="col-md-9 col-xs-3">
                                        <h5>{{ @$ad->user->first_name.' '.$ad->user->last_name }}</h5>
                                        <p class="text-muted"><i class="fa fa-map-marker"></i>
                                            {{ @\App\Library\Config\Helper::getCountry($ad->user->country_code)   }}</p>
                                    </div>
                                @else
                                    <div class="col-md-3 col-xs-3">
                                        <img src="{{ url(@$ad->store->logo) }}" height="57.5" width="57.5"
                                             class="img-circle img-responsive">
                                    </div>
                                    <div class="col-md-9 col-xs-3">
                                        <h5>{{ @$ad->store->title }}</h5>
                                        <p class="text-muted"><i class="fa fa-map-marker"></i>
                                            <b>{{ @\App\Library\Config\Helper::getCountry($ad->store->country)   }}</b> {{ @$ad->store->address }}
                                        </p>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="sidebar-user-link">

                            @if($ad->store_id != 0)
                                <a style="background: blue; color: white" class="btn btn-block"
                                   href="{{ url('store/'.$ad->store->username) }}"><b></b> Visit Store</a>
                            @else
                                <div class="alert alert-info">
                                    The user has no store
                                </div>
                            @endif

                            @auth
                                <button class="btn btn-block" style="background:#eb650c;color: white "
                                        data-toggle="modal" data-target="#make_offer">
                                    <i class="fa fa-chain"> {{ Lang::get('options.lang_make_offer') }}</i>
                                </button>
                            @endauth

                            <ul class="ad-action-list">
                                @auth
                                    <li><a href="#" id="addToFavorite" data-ad-id="{{ $ad->ad_id }}"><i
                                                    class="fa fa-heart pull-left"></i> {{ Lang::get('options.lang_add_to_favorite') }}
                                        </a></li>
                                @endauth
                                <li><a href="#report" data-ad-id="{{ $ad->ad_id }}" id="reportAd"><i
                                                class="fa fa-ban"></i> {{ Lang::get('options.lang_report_abuse') }}</a>
                                </li>
                                <br>
                            </ul>
                            <ul class="footer-social ">

                            @if($isPhone)
                                <!-- Share on WhatsApp -->
                                    <li class="mx-1"><a
                                                href="whatsapp://send?text={{ $ad->title }} {{ Protocol::home() }}/product/{{ $ad->slug }}"><img
                                                    data-popup="tooltip" data-placement="top" data-container="body"
                                                    title="{{ Lang::get('tooltips.lang_share_via_whatsapp') }}"
                                                    src="{{ Protocol::home() }}/content/assets/front-end/images/brands/whatsapp.png"
                                                    class="img-circle img-xs" alt=""></a></li>
                            @endif
                            <!-- Share on Digg -->
                                <li class="mx-1"><a
                                            href="https://www.digg.com/submit?url={{ Protocol::home() }}/product/{{ $ad->slug }}"
                                            target="_blank"><img data-popup="tooltip" data-placement="top"
                                                                 data-container="body"
                                                                 title="{{ Lang::get('tooltips.lang_share_on_digg') }}"
                                                                 src="{{ Protocol::home() }}/content/assets/front-end/images/brands/digg.png"
                                                                 class="img-circle img-xs" alt=""></a></li>



                                <!-- Share on Facebook -->
                                <li style="padding-right: 10px;"><a
                                            href="https://www.facebook.com/sharer.php?u={{ Protocol::home() }}/product/{{ $ad->slug }}"
                                            target="_blank"><img data-popup="tooltip" data-placement="top"
                                                                 data-container="body"
                                                                 title="{{ Lang::get('tooltips.lang_share_on_facebook') }}"
                                                                 src="{{ Protocol::home() }}/content/assets/front-end/images/brands/facebook.png"
                                                                 class="img-circle img-xs" alt=""></a></li>

                                <!-- Share on Twitter -->
                                <li style="padding-right: 10px;"><a
                                            href="https://twitter.com/share?url={{ Protocol::home() }}/product/{{ $ad->slug }}&text={{ $ad->title }}"
                                            target="_blank"><img data-popup="tooltip" data-placement="top"
                                                                 data-container="body"
                                                                 title="{{ Lang::get('tooltips.lang_share_on_twitter') }}"
                                                                 src="{{ Protocol::home() }}/content/assets/front-end/images/brands/twitter.png"
                                                                 class="img-circle img-xs" alt=""></a></li>

                                <!-- Share on Google Plus -->
                                <li style="padding-right: 10px;"><a
                                            href="https://plus.google.com/share?url={{ Protocol::home() }}/product/{{ $ad->slug }}"
                                            target="_blank"><img data-popup="tooltip" data-placement="top"
                                                                 data-container="body"
                                                                 title="{{ Lang::get('tooltips.lang_share_on_google') }}"
                                                                 src="{{ Protocol::home() }}/content/assets/front-end/images/brands/google.png"
                                                                 class="img-circle img-xs" alt=""></a></li>

                                <li style="padding-right: 10px;"><a data-toggle="modal" data-target="#scanQrCode"
                                                                    href="#"><img data-popup="tooltip"
                                                                                  data-placement="top"
                                                                                  data-container="body"
                                                                                  title="{{ Lang::get('tooltips.lang_scan_qr_code') }}"
                                                                                  src="{{ Protocol::home() }}/content/assets/front-end/images/brands/qr-code.png"
                                                                                  class="img-circle img-xs" alt=""></a>
                                </li>

                                <!-- Qr Code Scan -->
                                <div id="scanQrCode" class="modal fade">
                                    <div class="modal-dialog modal-xs" style="width: 30%;">
                                        <div class="modal-content">
                                            <div class="modal-header bg-success">
                                                <button type="button" class="close" data-dismiss="modal">&times;
                                                </button>
                                                <h5 class="modal-title text-uppercase">{{ Lang::get('ads/show.lang_scan_qr_code') }}</h5>
                                            </div>

                                            <div class="modal-body">

                                                {!! $qrCode !!}

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </ul>

                        </div>


                    </div>

                </div>

            </div>
            <hr>
        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-xs-12">
                <div class="ads-detail bg-white">
                    <h4 class="ads-detail-title">Description</h4>
                    <p>
                        {!! nl2br($ad->description) !!}
                    </p>

                </div>
            </div>

            <div class="col-sm-4 col-xs-12">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Product page display ad -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-6803454939055727"
                     data-ad-slot="4917604470"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
                {{--                {!! Helper::advertisements()->ad_middle !!}--}}
                {{--<img src="images/400.png" class="img-responsive">--}}
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="carousel-header">
                    <h4><a href="">
                            Users Also Viewed </a>
                    </h4>
                </div>
                <hr>
                <div class="row">
                    <!-- Related Ads -->
                    @if (count($related_ads))
                        @foreach ($related_ads as $related)
                            <div class="col-md-3 col-sm-12">
                                <div class="card card-blog">
                                    <ul class="affiliate" style="color: blue">
                                        @if (!is_null($related->affiliate_link))
                                            <li>
                                                <a href="{{ $related->affiliate_link }}">{{ Lang::get('update_two.lang_shopping') }}</a>
                                            </li>
                                        @endif
                                    </ul>

                                    <ul class="tags">
                                        @if ($related->is_featured)
                                            <li>{{ Lang::get('home.lang_featured') }}</li>
                                        @endif

                                        @if ($related->is_oos)
                                            <li class="oos">{{ Lang::get('update_three.lang_out_of_stock') }}</li>
                                        @endif
                                    </ul>
                                    <div class="card-image">
                                        <a href="{{ Protocol::home() }}/product/{{ $related->slug }}" {{ !is_null($related->affiliate_link) ? 'target="_blank"' : '' }}>
                                            <div class="img card-ad-cover"
                                                 style="background-image: url({{ EverestCloud::getThumnail($related->ad_id, $related->images_host) }});"
                                                 title="{{ $related->title }}"></div>
                                        </a>
                                    </div>
                                    <div class="card-block">
                                        <h5 class="card-title">
                                            <a href="{{ Protocol::home() }}/product/{{ $related->slug }}">{{ $related->title }}</a>
                                        </h5>
                                        <div class="card-footer">
                                            <div id="price">
                                                @if (!is_null($related->regular_price))
                                                    <span class="price price-old"> {{ number_format($related->regular_price, 2) }} {{ $related->currency }}</span>
                                                @endif
                                                <span class="price price-new"> {{ ($related->price) ? number_format($related->price, 2) .' ' . $related->currency : 'Contact seller for Price' }}</span>
                                            </div>
                                            <div class="author">
                                                <div class="card__avatar"><a
                                                            href="{{ Profile::hasStore($related->user_id) ? Protocol::home().'/store/'.Profile::hasStore($related->user_id)->username : '#' }}"
                                                            class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                                src="{{ Profile::picture($related->user_id) }}"
                                                                alt="{{ Profile::hasStore($related->user_id) ? Profile::hasStore($related->user_id)->title : Profile::full_name($related->user_id) }}"
                                                                class="avatar" width="40"
                                                                height="40">@if (Profile::hasStore($related->user_id))
                                                            <i class="icon-checkmark3" data-popup="tooltip"
                                                               data-placement="top" data-container="body"
                                                               title="Verified Account"></i>@endif</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        @endforeach
                    @endif


                    {{--<div class="owl-controls">--}}
                    {{--<div class="owl-nav">--}}
                    {{--<div class="owl-prev" style=""><i class="fa fa-angle-left "></i></div>--}}
                    {{--<div class="owl-next" style=""><i class="fa fa-angle-right"></i></div>--}}
                    {{--</div>--}}
                    {{--<div style="" class="owl-dots">--}}
                    {{--<div class="owl-dot active"><span></span></div>--}}
                    {{--<div class="owl-dot"><span></span></div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.product_thumbnail').click(function () {
            var image_url = $(this).data('thumb');
            var html = ' <li data-thumb="' + image_url + '" style="width: 350px; float: left; display: block;" class="flex-active-slide">\n' +
                '                                        <div class="thumb-image">\n' +
                '                                            <img src="' + image_url + '" data-imagezoom="true" alt=" " class="img-fluid"> </div>\n' +
                '                                    </li>';
            $('#product_image').empty().append(html);
            return false;
        });


    </script>



    {{--Show phone number modal--}}
    <!-- Show Phone Number -->
    <div id="show_phone_number" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-success">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title text-uppercase">{{ Lang::get('ads/show.lang_seller_phone_number') }}</h5>
                </div>

                <div class="modal-body">

                    <!-- Phone Number -->
                    <div class="form-group has-feedback has-feedback-left">
                        <input type="text" class="form-control" readonly=""
                               value="{{ $getUser->phone_hidden ? 'PHONE HIDDEN BY THIS MEMBER' : $getUser->phone }}">
                        <div class="form-control-feedback">
                            <i class="icon-phone2"></i>
                        </div>
                    </div>

                    @if ($callQRCode)
                        {!! $callQRCode !!}
                    @endif

                    <div class="text-center text-muted">
                        <span class="text-danger">{{ Lang::get('ads/show.lang_phone_warning') }}</span> {{ Lang::get('ads/show.lang_phone_warning_p') }}
                    </div>

                </div>

            </div>
        </div>
    </div>



    @if (Auth::check())

        <!-- Contact Seller -->
        <div id="contact_seller" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title text-uppercase">{{ Lang::get('ads/show.lang_contact_seller') }}</h5>
                    </div>

                    <form action="{{ Protocol::home() }}/account/inbox/create" method="POST" id="contactSeller">

                        <meta name="csrf-token" content="{{ csrf_token() }}">

                        <div class="modal-body">

                            <!-- Subject -->
                            <div class="form-group">
                                <input type="text" placeholder="{{ Lang::get('ads/show.lang_subject') }}"
                                       class="form-control" name="msgSubject" id="msgSubject">
                            </div>

                            <!-- Message -->
                            <div class="form-group">
                                <textarea rows="5" name="msgContent" id="msgContent" class="form-control"
                                          placeholder="{{ Lang::get('ads/show.lang_your_message_placeholder') }}"></textarea>
                            </div>

                            <!-- Show Email OR Phone Number -->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <select data-placeholder="Show Email Address" class="select" name="show_email"
                                                id="show_email">

                                            <option value="0">{{ Lang::get('ads/show.lang_hide_email_address') }}</option>

                                            <option value="1">{{ Lang::get('ads/show.lang_show_email_address') }}</option>


                                        </select>
                                    </div>

                                    <div class="col-sm-6">
                                        <select data-placeholder="Show Phone Number" class="select" name="show_phone"
                                                id="show_phone">

                                            <option value="0">{{ Lang::get('ads/show.lang_hide_phone_number') }}</option>

                                            <option value="1">{{ Lang::get('ads/show.lang_show_phone_number') }}</option>


                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="modal-footer">
                            <button type="submit"
                                    class="btn btn-success">{{ Lang::get('ads/show.lang_send_message') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    @else

        <!-- Login to contact seller -->
        <div id="loginToContact" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">

                    <form action="{{ Protocol::home() }}/auth/login?redirect={{ $ad->ad_id }}" method="POST">

                        {{ csrf_field() }}

                        <div class="panel-body login-form">
                            <div class="text-center">
                                <div class="icon-object border-blue text-blue"><i class="fa fa-key fa-rotate-90"></i>
                                </div>
                                <h5 class="content-group">{{ Lang::get('auth/login.lang_login_to_your_account') }}
                                    <small class="display-block">{{ Lang::get('auth/login.lang_your_credentials') }}</small>
                                </h5>
                            </div>

                            <div class="form-group has-feedback has-feedback-left {{ $errors->has('email') ? 'has-error' : '' }}">
                                <input type="text" class="form-control"
                                       placeholder="{{ Lang::get('auth/login.lang_email_address') }}" name="email"
                                       value="{{ old('email') }}">
                                <div class="form-control-feedback">
                                    <i class="icon-envelop text-muted"></i>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>

                            <div class="form-group has-feedback has-feedback-left {{ $errors->has('password') ? 'has-error' : '' }}">
                                <input type="password" class="form-control"
                                       placeholder="{{ Lang::get('auth/login.lang_password') }}" name="password">
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif
                            </div>

                            <div class="form-group login-options">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="checkbox-inline text-grey-400">
                                            <input type="checkbox" class="styled" name="remember" checked="">
                                            {{ Lang::get('auth/login.lang_remember_me') }}
                                        </label>
                                    </div>

                                    <div class="col-sm-8 text-right">
                                        <ul class="list-inline list-inline-separate heading-text">
                                            @if (Helper::settings_auth()->activation_type == 'sms')
                                                <li>
                                                    <a href="{{ Protocol::home() }}/auth/activation/phone/resend">{{ Lang::get('auth/login.lang_resend_activation_code') }}</a>
                                                </li>
                                            @elseif (Helper::settings_auth()->activation_type == 'email')
                                                <li>
                                                    <a href="{{ Protocol::home() }}/auth/activation/resend">{{ Lang::get('auth/login.lang_resend_activation_link') }}</a>
                                                </li>
                                            @else
                                            @endif
                                            <li>
                                                <a href="{{ Protocol::home() }}/auth/password/reset">{{ Lang::get('auth/login.lang_forgot_password') }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="recaptcha">

                                @if (Helper::settings_security()->recaptcha)
                                    @captcha
                                @endif

                            </div>

                            <script type="text/javascript">
                                $(".styled, .multiselect-container input").uniform({
                                    radioClass: 'choice',
                                    wrapperClass: 'border-grey-400 text-grey-400'
                                });
                            </script>

                            <div class="form-group">
                                <button type="submit"
                                        class="btn bg-blue btn-block">{{ Lang::get('auth/login.lang_login') }}</button>
                            </div>

                            <div class="content-divider text-muted form-group">
                                <span>{{ Lang::get('auth/login.lang_or_sign_in_with') }}</span></div>
                            <ul class="list-inline form-group list-inline-condensed text-center list-inline-social">

                                <!-- Facebook -->
                            <!--<li><a href="{{ Protocol::home() }}/auth/facebook" class="btn border-indigo text-indigo btn-flat btn-icon btn-rounded"><i class="icon-facebook"></i></a></li>-->

                                <!-- Twitter -->
                            <!--<li><a href="{{ Protocol::home() }}/auth/twitter" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-twitter"></i></a></li>-->

                                <!-- Google -->
                                <li><a href="{{ Protocol::home() }}/auth/google"
                                       class="btn border-danger-400 text-danger-400 btn-flat btn-icon btn-rounded"><i
                                                class="fa fa-google-plus"></i></a></li>

                            </ul>

                            <div class="content-divider text-muted form-group">
                                <span>{{ Lang::get('auth/login.lang_dont_have_account') }}</span></div>
                            <a href="{{ Protocol::home() }}/auth/register"
                               class="btn btn-default btn-block content-group">{{ Lang::get('auth/login.lang_sigh_up') }}</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    @endif


    <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "name": "{{ $ad->title }}",
  "image": [
    "{!! Helper::get_ad_seo_photos($ad->photos, $ad->images_host) !!}"

   ],
  "description": "{{ $ad->description }}",
  "mpn": "{{ $ad->ad_id }}",
  "brand": {
    "@type": "Thing",
    "name": "SanitaryWare"
  },
  "offers": {
    "@type": "Offer",
    "priceCurrency": "{{ $ad->currency }}",
    "price": "{{ $ad->price }}",
    "priceValidUntil": "{{ $ad->ends_at }}",
    "itemCondition": "http://schema.org/NewCondition",
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "SanitaryWare"
    }
  }
}



    </script>

    <script type="application/ld+json">
    {
     "@context": "http://schema.org",
     "@type": "BreadcrumbList",
     "itemListElement":
     [
      {
        "@type": "ListItem",
        "position": 1,
        "item":
       {
        "@id": "{{ url('/') }}",
        "name": "Home"
        }
      },
      {
        "@type": "ListItem",
        "position": 2,
        "item":
       {
         "@id": "{{ url( Helper::get_category($ad->category, true) ) }}",
         "name": "{{ Helper::get_category_name($ad->category) }}"
       }
      },
      {
        "@type": "ListItem",
        "position": 3,
        "item":
       {
         "@id": "{{ url('product/'.$ad->slug.'') }}",
         "name": "{{ $ad->title }}"
       }
      }
     ]
    }



    </script>

@endsection