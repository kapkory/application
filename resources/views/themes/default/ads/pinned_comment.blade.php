<ul class="media-list content-group-lg stack-media-on-mobile">
    <li class="media">
        <div class="media-left">
            @if (Profile::hasStore($pinned_comment->user_id))
                <a href="{{ Protocol::home() }}/store/{{ Profile::hasStore($pinned_comment->user_id)->username }}"><img src="{{ Profile::picture($pinned_comment->user_id) }}" class="img-circle img-sm" alt="{{ Profile::hasStore($pinned_comment->user_id)->title }}"></a>
            @else
                <a href="#"><img src="{{ Profile::picture($pinned_comment->user_id) }}" class="img-circle img-sm" alt=""></a>
            @endif
        </div>

        <div class="media-body">
            <div class="media-heading">
                <a href="{{ Profile::hasStore($pinned_comment->user_id) ? Protocol::home().'/store/'.Profile::hasStore($pinned_comment->user_id)->username : '#' }}" class="{{ Profile::hasStore($pinned_comment->user_id) ? 'trusted-badge' : '' }} label label-primary label-rounded">{{ Profile::hasStore($pinned_comment->user_id) ? Profile::hasStore($pinned_comment->user_id)->title : Profile::full_name($pinned_comment->user_id) }}</a>
                <span class="media-annotation dotted">{{ Helper::date_ago($pinned_comment->created_at) }}</span>
                @if ($pinned_comment->is_pinned)
                    <span class="media-annotation dotted text-black text-uppercase">Pinned</span>
                @endif
            </div>

            <p style="margin-top: 10px;" class="emojioneareaCm">{!! nl2br($pinned_comment->content) !!}</p>

            <ul class="list-inline list-inline-separate text-size-small">

            @if (Auth::check())
                @if (Auth::id() == $pinned_comment->user_id)
                    <!-- Edit Comment -->
                        <li><a href="{{ Protocol::home() }}/account/comments/edit/{{ $pinned_comment->id }}">Edit</a></li>

                        <!-- Delete Comment -->
                        <li><a href="{{ Protocol::home() }}/account/comments/delete/{{ $pinned_comment->id }}">Delete</a></li>
                    @endif

                    <li><a href="#" class="reportComment" data-comment-id="{{ $pinned_comment->id }}">Report Abuse</a></li>

                @endif

            </ul>

        </div>
    </li>
    <hr>
</ul>