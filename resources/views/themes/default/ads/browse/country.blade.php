@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('content')

<div class="row">
	<div class="col-md-12">
		
		<!-- Country Ads -->
        <div class="spec ">
            <h3>{{ $country->name }}</h3>
            <div class="ser-t">
                <b></b>
                <span><i></i></span>
                <b class="line"></b>
            </div>
        </div>

		@if (count($ads))
        @foreach ($ads as $ad)

        <div class="col-md-3">

            <div class="card card-blog">
                @if ($ad->is_featured)
                <div class="ribbon ribbon-top-left"><span>{{ Lang::get('badges.lang_featured') }}</span></div>
                @endif
                <div class="card-image">
                    <a href="{{ Protocol::home() }}/listing/{{ $ad->slug }}" {{ !is_null($ad->affiliate_link) ? 'target="_blank"' : '' }}>
                        <div class="img card-ad-cover" style="background-image: url({{ Helper::img_src_base64(Protocol::home().'/application/public/uploads/images/'.$ad->ad_id.'/thumbnails/thumbnail_0.jpg') }});" title="{{ $ad->title }}"></div>
                    </a>
                </div>
                <div class="card-block">
                    <h5 class="card-title">
                        <a href="{{ Protocol::home() }}/listing/{{ $ad->slug }}">{{ $ad->title }}</a>
                    </h5>
                    <div class="card-footer">
                        <div id="price">
                            @if (!is_null($ad->regular_price))
                            <span class="price price-old"> {{ $ad->currency }}{{ $ad->regular_price }}</span>
                            @endif
                            <span class="price price-new"> {{ $ad->currency }}{{ $ad->price }}</span>
                        </div>
                        <div class="author">
                            <div class="card__avatar"><a href="{{ Profile::hasStore($ad->user_id) ? Protocol::home().'/store/'.Profile::hasStore($ad->user_id)->username : '#' }}" class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img src="{{ Profile::picture($ad->user_id) }}" alt="{{ Profile::hasStore($ad->user_id) ? Profile::hasStore($ad->user_id)->title : Profile::full_name($ad->user_id) }}" class="avatar" width="40" height="40">@if (Profile::hasStore($ad->user_id))<i class="icon-checkmark3" data-popup="tooltip" data-placement="top" data-container="body" title="Verified Account"></i>@endif</a></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        @endforeach

		<!-- Pagination -->
		<div class="col-md-12 text-center mb-20">
        	{{ $ads->links() }}
        </div>

        @else

		<!-- No Ads Right now -->
		<div class="alert bg-info alert-styled-left">
		@lang ('return/info.lang_nothing_to_show')
    	</div>

        @endif

	</div>
</div>

@endsection