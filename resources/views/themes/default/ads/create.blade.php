@extends (Theme::get().'.layout.app')

@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('styles')
    <style type="text/css">
        .jFiler-input-choose-btn.blue {
            color: #48a0dc;
            border: 1px solid #48a0dc;
        }
        .panel-footer {
            padding: 8px 20px;
            padding-right: 20px;
            padding-left: 20px;
            background-color: #fcfcfc;
            border-top: 1px solid #ddd;
            border-bottom-right-radius: 2px;
            border-bottom-left-radius: 2px;
        }
        .checkbox-inline {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 9px;

        }
    </style>
{{--    <script src="{{ url('ckeditor/ckeditor.js') }}"></script>--}}
    <script src="//cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>

    <link href="{{ Protocol::home() }}/content/assets/jquery.filer.css" rel="stylesheet">
    <link href="{{ Protocol::home() }}/content/assets/jquery.filer-dragdropbox-theme.css" rel="stylesheet">
@endsection

@section ('javascript')

    @if (Auth::check())
        @if (Profile::hasStore(Auth::id()))
            <script type="text/javascript">
                var _limit_images = "{{ Helper::settings_membership()->pro_ad_images }}";
            </script>
        @else
            <script type="text/javascript">
                var _limit_images = "{{ Helper::settings_membership()->free_ad_images }}";
            </script>
        @endif
    @else
        <script type="text/javascript">
            var _limit_images = 0;
        </script>
    @endif
    <script src="{{ Protocol::home() }}/content/assets/custom.js" type="text/javascript"></script>
    <script src="{{ Protocol::home() }}/content/assets/jquery.filer.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/editors/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/editors/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/editors/wysihtml5/parsers.js"></script>
    <script>
        $(function() {

            // Simple toolbar
            $('.wysihtml5-min').wysihtml5({
                parserRules:  wysihtml5ParserRules,
                stylesheets: ["{{ Protocol::home() }}/content/assets/front-end/css/components.css"],
                "font-styles": false, // Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, // Italics, bold, etc. Default true
                "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, // Button which allows you to edit the generated HTML. Default false
                "link": true, // Button to insert a link. Default true
                "image": true, // Button to insert an image. Default true,
                "action": false, // Undo / Redo buttons,
                "color": true, // Button to change color of font
            });

        });
    </script>

@endsection

@section ('content')
<?php
//echo '<pre/>';
//print_R(Session::get('error'));
//print_R($errors);
//foreach($errors->all() as $error){
//    print_R($error);
//}
//die;

?>
    <!-- New Ad -->
    <div class="row row-padding">

        <!-- Session Messages -->
        <div class="col-md-12">
            
            @if (Session::has('error'))
                <div class="alert bg-danger alert-styled-left">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('error') }}
                </div>
            @endif
            
            @foreach($errors->all() as $error) 
            <div class="alert bg-danger alert-styled-left">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    {{ $error }}
            </div>
            @endforeach 
            @if (Session::has('success'))
                <div class="alert bg-success alert-styled-left">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('success') }}
                </div>
            @endif

        </div>

        <div class="col-md-8">

            <form class="form-horizontal form-validate-jquery" action="{{ Protocol::home() }}/create" method="POST" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="card panel-flat">

                    <div class="card-body">

                        <div class="text-center mb-20">
                            <div class="icon-object border-info text-info"><i class="fa fa-edit"></i></div>
                            <h5 class="content-group text-uppercase">Publish your ad now for free!<small class="display-block text-uppercase">Enter your ad defailts below</small></h5>
                        </div>

                        <fieldset style="margin-top: 60px">

                            <!-- Ad Title -->
                            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('create/ad.lang_title_placeholder') }}" name="title" value="{{ old('title') }}" required="">
                                    @if ($errors->has('title'))
                                        <span class="help-block">{{ $errors->first('title') }}</span>
                                    @endif
                                </div>
                            </div>



                        <!-- Content -->
                            <div class="form-group form-md-line-input">
                                {{--<label class="col-md-2 control-label" for="content">Article Content</label>--}}
                                <div class="col-md-10">
                                    <textarea placeholder="{{ Lang::get('create/ad.lang_description_placeholder') }}" name="descript">{{ old('description') }}</textarea>
                                    <script>
                                        // CKEDITOR.replace( 'description' ,{
                                        //     extraPlugins: 'imageuploader'
                                        // });
                                        CKEDITOR.replace( 'descript', {
                                            extraPlugins: 'uploadimage',
                                            height: 300,

                                            // Upload images to a CKFinder connector (note that the response type is set to JSON).
                                            uploadUrl: '{{ url('ad/upload/image') }}',

                                            // Configure your file manager integration. This example uses CKFinder 3 for PHP.
                                            // filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                                            filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
                                            // filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                                            filebrowserImageUploadUrl: '{{ url("ad/upload/image") }}',

                                            // The following options are not necessary and are used here for presentation purposes only.
                                            // They configure the Styles drop-down list and widgets to use classes.

                                            stylesSet: [
                                                { name: 'Narrow image', type: 'widget', widget: 'image', attributes: { 'class': 'image-narrow' } },
                                                { name: 'Wide image', type: 'widget', widget: 'image', attributes: { 'class': 'image-wide' } }
                                            ],

                                            // Load the default contents.css file plus customizations for this sample.
                                            contentsCss: [ CKEDITOR.basePath + 'contents.css', 'https://sdk.ckeditor.com/samples/assets/css/widgetstyles.css' ],

                                            // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
                                            // resizer (because image size is controlled by widget styles or the image takes maximum
                                            // 100% of the editor width).
                                            image2_alignClasses: [ 'image-align-left', 'image-align-center', 'image-align-right' ],
                                            image2_disableResizer: true
                                        } );
                                    </script>
                                </div>
                            </div>

                            <!-- Ad Category -->
                            <div class="form-group select-size-lg {{ $errors->has('category') ? 'has-error' : '' }}">
                                <div class="col-lg-12">
                                    <select required="" class="select" data-placeholder="{{ Lang::get('create/ad.lang_select_category') }}" name="category">
                                        <option></option>
                                        @if(count(Helper::parent_categories()))
                                            @foreach (Helper::parent_categories() as $parent)
                                                <optgroup label="{{ $parent->category_name }}">
                                                    @if (count(Helper::sub_categories($parent->id)))
                                                        @foreach (Helper::sub_categories($parent->id) as $sub)
                                                            <option {{ old('category') == $sub->id ? 'selected' : '' }} value="{{ $sub->id }}">{{ $sub->category_name }}</option>
                                                        @endforeach
                                                        @else
                                                        <option value="{{ $parent->id }}">{{ $parent->category_name }}</option>
                                                    @endif
                                                </optgroup>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <!-- Country -->
                            <div class="form-group select-size-lg {{ $errors->has('deliverable_to') ? 'has-error' : '' }}">
                                <div class="col-lg-12">
                                    <select id="deliverable_to" required="" class="select-search" name="deliverable_to" onchange="getCountries(this.value)" data-placeholder="Deliverable to">
                                        <option value="">Deliverable to</option>
                                        <option value="1">Globally</option>
                                        <option value="2">Selected Countries</option>
                                    </select>
                                    @if ($errors->has('deliverable_to'))
                                    <span class="help-block">{{ $errors->first('deliverable_to') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Country -->
                            <div id="country_form" class="form-group select-size-lg {{ $errors->has('country') ? 'has-error' : '' }}">
                                <div class="col-lg-12">
                                    <select id="country" class="select-search" name="country[]" multiple="multiple" data-placeholder="{{ Lang::get('create/ad.lang_select_country') }}">

                                    </select>
                                    @if ($errors->has('country'))
                                        <span class="help-block">{{ $errors->first('country') }}</span>
                                    @endif
                                </div>
                            </div>




                            @if(Auth::user()->is_admin)
                                <div class="form-group select-size-lg {{ $errors->has('stores') ? 'has-error' : '' }}">
                                    <div class="col-lg-12">
                                        <select  class="select-search" name="store"  data-placeholder="{{ Lang::get('create/ad.lang_store') }}">
                                            @foreach ($stores as $store)
                                                <option  value="{{ $store->id }}">{{ $store->username }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('stores'))
                                            <span class="help-block">{{ $errors->first('stores') }}</span>
                                        @endif
                                    </div>
                                </div>
                            @endif

                        <!-- Ad Regular, Sale Price & Currency -->
                            <div class=" form-group {{ $errors->has('price') ? 'has-error' : '' }}">
                             <div class="row" style="margin-right: 0; margin-left: 0px;">
                                <!-- Sale Price -->
                                <div class="{{ Profile::hasStore(Auth::id()) ? 'col-md-4' : 'col-lg-8' }}">
                                    <input  type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update_two.lang_sale_price') }}" name="price" value="{{ old('price') }}">
                                    @if ($errors->has('price'))
                                        <span class="help-block">{{ $errors->first('price') }}</span>
                                    @endif
                                </div>

                            @if ( Profile::hasStore(Auth::id()) )
                                <!-- Regular Price -->
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update_two.lang_regular_price') }}" name="regular_price" value="{{ old('regular_price') }}">
                                        @if ($errors->has('regular_price'))
                                            <span class="help-block">{{ $errors->first('regular_price') }}</span>
                                        @endif
                                    </div>
                            @endif

                            <!-- Select Currency -->
                                <div class="col-lg-4 select-size-lg">
                                    <select required="" class="select" name="currency" data-placeholder="{{ Lang::get('update.lang_select_currency') }}">
                                        <option></option>
                                        @foreach (App\Models\Currency::get() as $currency)
                                            <option {{ old('currency') == $currency->code ? 'selected' : '' }} value="{{ $currency->code }}">{{ $currency->code }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('currency'))
                                        <span class="help-block">{{ $errors->first('currency') }}</span>
                                    @endif
                                </div>
                                </div>
                            </div>

                            <!-- Minimum Order -->
                            <div class="form-group {{ $errors->has('minimum_order_quantity') ? 'has-error' : '' }}">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('create/ad.lang_minimum_order') }}" name="minimum_order_quantity" value="{{ old('minimum_order_quantity') }}" required="">
                                    @if ($errors->has('minimum_order_quantity'))
                                        <span class="help-block">{{ $errors->first('minimum_order_quantity') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Supply Time -->
                            <div class="form-group {{ $errors->has('supply_time') ? 'has-error' : '' }}">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('create/ad.lang_supply') }}" name="supply_time" value="{{ old('supply_time') }}" required="">
                                    @if ($errors->has('supply_time'))
                                        <span class="help-block">{{ $errors->first('supply_time') }}</span>
                                    @endif
                                </div>
                            </div>

                            @if(auth()->user()->is_admin == 1)
                            <!-- Affiliate LINK -->
                            <div class="form-group {{ $errors->has('affiliate_link') ? 'has-error' : '' }}">
                                <div class="col-lg-12">
                                    @if ( Profile::hasStore(Auth::id()) )
                                        <input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update_two.lang_affiliate_link_placeholder') }}" name="affiliate_link" value="{{ old('affiliate_link') }}">
                                    @else
                                        <input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update_two.lang_affiliate_link_placeholder') }}" readonly="" name="affiliate_link" value="{{ old('affiliate_link') }}" data-popup="tooltip" data-placement="top" data-container="body" title="{{ Lang::get('update.lang_youtube_video_not_available') }}">
                                    @endif
                                    @if ($errors->has('affiliate_link'))
                                        <span class="help-block">{{ $errors->first('affiliate_link') }}</span>
                                    @endif
                                </div>
                            </div>
                            @endif
                            <?php
                            $brands = \App\Models\Brand::all();
                            ?>
                            <div id="brand" class="form-group select-size-lg {{ $errors->has('brand') ? 'has-error' : '' }}" style="display: none">
                                <div class="col-lg-12">
                                    <select  data-placeholder="Select A Brand" class="select-search" name="brand">
                                       <option value="0">Select A Brand</option>
                                        @foreach ($brands as $brand)
                                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <!-- Youtube Video -->
                            <div class="form-group {{ $errors->has('youtube') ? 'has-error' : '' }}">
                                <div class="col-lg-12">
                                    @if ( Profile::hasStore(Auth::id()) )
                                        <input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update.lang_youtube_video_placeholder') }}" name="youtube" value="{{ old('youtube') }}">
                                    @else
                                        <input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update.lang_youtube_video_placeholder') }}" readonly="" name="youtube" value="{{ old('youtube') }}" data-popup="tooltip" data-placement="top" data-container="body" title="{{ Lang::get('update.lang_youtube_video_not_available') }}">
                                    @endif
                                    @if ($errors->has('youtube'))
                                        <span class="help-block">{{ $errors->first('youtube') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Upload Photos -->
                            <div class="form-group {{ $errors->has('photos') ? 'has-error' : '' }}">
                                <div class="col-lg-12">
                                    <input required="" type="file" name="photos[]" id="uploader" multiple="multiple" accept="image/*">
                                    <span class="help-block">{{ Lang::get('create/ad.lang_accepted_formats') }}</span>
                                    @if ($errors->has('photos'))
                                        <span class="help-block">{{ $errors->first('photos') }}</span>
                                    @endif
                                </div>
                            </div>

                            @if(Auth::user())

                                @if(Auth::user()->is_admin == 1)
                                <!-- Meta Title -->
                                    <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('create/ad.lang_meta_title') }}" name="meta_title" value="{{ old('meta_title') }}">

                                            @if ($errors->has('meta_title'))
                                                <span class="help-block">{{ $errors->first('meta_title') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <!-- Meta Title -->
                                    <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                                        <div class="col-lg-12">
									<textarea rows="4" cols="40" class="form-control" name="meta_description" placeholder="{{ Lang::get('create/ad.lang_meta_description') }}" value="">

									</textarea>

                                            @if ($errors->has('meta_description'))
                                                <span class="help-block">{{ $errors->first('meta_description') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            @endif


                            @if(Auth::check())

                                @if(Auth::user()->is_admin == 1)
                                    <div class="form-group select-size-lg {{ $errors->has('user') ? 'has-error' : '' }}">
                                        <div class="col-lg-12">
                                            <select required="" data-placeholder="{{ Lang::get('create/ad.lang_select_user') }}" class="select-search" name="user">
                                                @foreach ($users as $ad_user)
                                                    <option value="{{ $ad_user->id }}"  {{ $user->id == $ad_user->id ? 'selected' : '' }}>{{ $ad_user->first_name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('user'))
                                                <span class="help-block">{{ $errors->first('user') }}</span>
                                @endif
                                        </div>
                                    </div>
                                @endif
                            @endif

                        </fieldset>

                    </div>

                    <div class="panel-footer">
                        <div class="heading-elements">
                            <div class="checkbox pull-left  {{ $errors->has('terms') ? 'has-error' : '' }}" style="margin-top: -8px;">
                                <label class="checkbox-inline text-grey-400">
                                    <input checked type="checkbox" class="styled" name="terms">
                                    {{ Lang::get('create/ad.lang_i_have_confirm') }} <a style="color: #166dba;" href="{{ url('page/terms') }}" target="_blank">{{ Lang::get('create/ad.lang_terms_of_service') }}</a>
                                </label>
                                @if ($errors->has('terms'))
                                    <span class="help-block">{{ $errors->first('terms') }}</span>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary heading-btn pull-right">{{ Lang::get('create/ad.lang_create_ad') }}</button>
                        </div>
                    </div>

                </div>

            </form>

        </div>

        <div class="col-md-4">

            <div class="card">
                <div class="card-body text-center">
                    <div class="icon-object border-blue text-blue"><i class="fa fa-book"></i></div>
                    <h5 class="text-semibold">{{ Lang::get('create/ad.lang_terms_of_service') }}</h5>
                    <p class="mb-15">{{ Lang::get('create/store.lang_terms_of_service_p') }}</p>
                    <a style="color: #FFf" href="{{ url('page/terms') }}" target="_blank" class="btn btn-primary">{{ Lang::get('create/ad.lang_terms_of_service') }}</a>
                </div>
            </div>

            <!-- Contact us if you have any questions -->
            <div class="card card-body media-rtl" style="margin-top: -14px;">
                <div class="media no-margin stack-media-on-mobile">
                    <div class="media-left media-middle">
                        <i class="icon-lifebuoy icon-3x text-muted no-edge-top"></i>
                    </div>

                    <div class="media-body">
                        <h6 class="media-heading text-semibold">{{ Lang::get('create/ad.lang_got_question') }}</h6>
                        <span class="text-muted">{{ Lang::get('contact.lang_contact_us_directly') }}</span>
                    </div>

                    <div class="media-right media-middle">
                        <a style="color:#FFf;" href="{{ Protocol::home() }}/contact" class="btn btn-primary">{{ Lang::get('create/ad.lang_contact') }}</a>
                    </div>
                </div>
            </div>

            <div class="card panel-flat">
                <div class="card-title">
                    <h6 class="card-title text-uppercase"> {{ Lang::get('create/ad.lang_how_to_sell_quickly') }} </h6>
                </div>

                <div class="card-body" style="padding-top: 2px">
                    <ul class="media-list">
                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-primary text-primary btn-flat btn-icon btn-rounded btn-sm legitRipple">
                                    <i class="icon-pencil4"></i>
                                </a>
                            </div>

                            <div class="media-body" style="line-height: 40px;">
                                {{ Lang::get('create/ad.lang_how_to_sell_brief_title') }}
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-danger text-danger btn-flat btn-icon btn-rounded btn-sm legitRipple">
                                    <i class="icon-list2"></i>
                                </a>
                            </div>

                            <div class="media-body" style="line-height: 40px;">
                                {{ Lang::get('create/ad.lang_how_to_sell_correct_category') }}
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-slate text-slate btn-flat btn-icon btn-rounded btn-sm legitRipple">
                                    <i class="icon-image2"></i>
                                </a>
                            </div>

                            <div class="media-body" style="line-height: 40px;">
                                {{ Lang::get('create/ad.lang_how_to_sell_nice_photos') }}
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-success text-success btn-flat btn-icon btn-rounded btn-sm legitRipple">
                                    <i class="icon-cash3"></i>
                                </a>
                            </div>

                            <div class="media-body" style="line-height: 40px;">
                                {{ Lang::get('create/ad.lang_how_to_sell_reasonable_price') }}
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <a href="#" class="btn border-blue text-blue btn-flat btn-icon btn-rounded btn-sm legitRipple">
                                    <i class="icon-rotate-ccw3"></i>
                                </a>
                            </div>

                            <div class="media-body" style="line-height: 40px;">
                                {{ Lang::get('create/ad.lang_how_to_sell_check_the_item') }}
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

    </div>


    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();


        });

        function getCountries(value) {

            var option = '';


            if (value == 1)
            {
                option = "<option value='0'>All countries has been selected</option>";
                $('#country').empty().append(option).find('option:first')
                    .attr("selected","selected");

            }
            else if (value == 2)
            {
                $('#country').empty();
                $.get('api/countries',function (rsults) {

                    for (var i =0 ;i < rsults.length; i++)
                    {
                        var resp = rsults[i];
                        option = "<option value="+resp.id+">"+resp.name+"</option>";
                        $('#country').append(option);

                    }
                });
            }
            else
            {
                alert("select countries where your product can be delivered");
            }


        }

        $('input[name="affiliate_link"]').click(function () {
            $('#brand').show();
        })
    </script>

@endsection
