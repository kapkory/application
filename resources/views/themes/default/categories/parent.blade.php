@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('content')

<div class="row">
	
	<!-- Sub Categories -->
	<div class="col-md-12">

		<!-- Browse By Countries -->
        <div class="specs ">
            <h3>{{ $parent_category->category_name }}</h3>
            <div class="ser-t">
                <b></b>
                <span><i></i></span>
                <b class="line"></b>
            </div>
        </div>


        @if(count($sub_categories))

        <div class="row cat_single_wrap">

            @foreach ($sub_categories as $category)
            <div class="col-md-2 text-center">

                <div class="cat_single">
                    <div class="cat_single_bg">
                        <div class="overlay_color panel" style="transform: skewX(-6deg);">
                        </div>
                    </div>
                    <div class="cat_single_content">
                        <a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent_category->category_slug }}/{{ $category->category_slug }}" style="color: rgb(255, 255, 255);">
                            <img src="{{ url($category->icon) }}" alt="{{  ucwords($category->category_name).' Manufacturer and Supplier' }}">
                            <span class="cat_title">{{ $category->category_name }}</span>
                        </a>
                    </div>
                </div>

            </div>
            @endforeach

        </div>

        @else

        <!-- Nothing to show right now -->
		<div class="alert bg-info alert-styled-left">
		@lang ('return/info.lang_nothing_to_show')
    	</div>

        @endif

	</div>

</div>

@endsection