@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('content')

<div class="row row-padding">
	
	<!-- Category Ads -->
	<div class="col-md-9">

		<!-- Filter -->
		<div class="row ">
			<div class="col-md-12">
				<div class="navbar navbar-default navbar-xs navbar-component">
					<ul class="nav navbar-nav no-border visible-xs-block">
						<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
					</ul>

					<div class="navbar-collapse collapse" id="navbar-filter">
						<p class="navbar-text">{{ Lang::get('category.lang_filter') }}</p>
						<ul class="nav navbar-nav">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-time-asc position-left"></i> {{ Lang::get('category.lang_by_date') }} <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent }}/{{ $sub }}">{{ Lang::get('category.lang_show_all') }}</a></li>
									<li class="divider"></li>
									<li><a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent }}/{{ $sub }}?date=today">{{ Lang::get('category.lang_today') }}</a></li>
									<li><a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent }}/{{ $sub }}?date=yesterday">{{ Lang::get('category.lang_yesterday') }}</a></li>
									<li><a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent }}/{{ $sub }}?date=week">{{ Lang::get('category.lang_week') }}</a></li>
									<li><a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent }}/{{ $sub }}?date=month">{{ Lang::get('category.lang_month') }}</a></li>
									<li><a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent }}/{{ $sub }}?date=year">{{ Lang::get('category.lang_year') }}</a></li>
								</ul>
							</li>

							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-amount-desc position-left"></i> {{ Lang::get('category.lang_by_status') }} <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent }}/{{ $sub }}">{{ Lang::get('category.lang_show_all') }}</a></li>
									<li class="divider"></li>
									<li><a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent }}/{{ $sub }}?status=featured">{{ Lang::get('category.lang_featured') }}</a></li>
									<li><a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent }}/{{ $sub }}?status=normal">{{ Lang::get('category.lang_not_featured') }}</a></li>
								</ul>
							</li>


						</ul>

						<div class="navbar-right text-right">
							<a href="{{ Protocol::home() }}/random" class="text-muted text-uppercase" style="line-height: 35px;">{{ Lang::get('search.lang_random') }}</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		

		<!-- Category Ads -->
		<div class="row">

            @if (count($ads))
            @foreach ($ads as $ad)

            <div class="col-md-4" style="padding-right: 5px; padding-left: 5px;">

                <div class="card card-blog">
                    <ul class="tags">
                        @if ($ad->is_featured)
                        <li>{{ Lang::get('home.lang_featured') }}</li>
                        @endif

                        @if ($ad->is_oos)
                        <li class="oos">{{ Lang::get('update_three.lang_out_of_stock') }}</li>
                        @endif
                    </ul>
                    <div class="card-image">
                        <a href="{{ Protocol::home() }}/product/{{ $ad->slug }}" {{ !is_null($ad->affiliate_link) ? 'target="_blank"' : '' }}>
                            <div class="img card-ad-cover" style="background-image: url({{ EverestCloud::getThumnail($ad->ad_id, $ad->images_host) }});" title="{{ $ad->title }}"></div>
                        </a>
                    </div>
                    <div class="card-block">
                        <h6 class="card-title" style="padding: 8px">
                            <a href="{{ Protocol::home() }}/product/{{ $ad->slug }}">{{ $ad->title }}</a>
                        </h6>
                        <div class="card-footer">
                            <div id="price">
                                @if (!is_null($ad->regular_price))
                                <span class="price price-old"> {{ number_format($ad->regular_price, 2) }} {{ $ad->currency }}</span>
                                @endif
                                <span class="price price-new"> {{ ($ad->price) ? number_format($ad->price, 2) .' '. $ad->currency : 'Contact seller for price' }}</span>
                            </div>
                            <div class="author">
                                <div class="card__avatar"><a href="{{ Profile::hasStore($ad->user_id) ? Protocol::home().'/store/'.Profile::hasStore($ad->user_id)->username : '#' }}" class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img src="{{ Profile::picture($ad->user_id) }}" alt="{{ Profile::hasStore($ad->user_id) ? Profile::hasStore($ad->user_id)->title : Profile::full_name($ad->user_id) }}" class="avatar" width="40" height="40">@if (Profile::hasStore($ad->user_id))<i class="icon-checkmark3" data-popup="tooltip" data-placement="top" data-container="body" title="{{ Lang::get('update_two.lang_verified_account') }}"></i>@endif</a></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            @endforeach
            <div class="col-md-12 text-center mb-20">
            	{{ $ads->links() }}
            </div>
            @else 
            <div class="col-md-12">
	            <div class="alert bg-info alert-styled-left">
					<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
					@lang ('return/info.lang_nothing_to_show')
			    </div>
		    </div>
            @endif

        </div>

	</div>

	<!-- Right Side -->
	<div class="col-md-3" style="padding-right: 0px; padding-left: 15px;">

		@if (count($stores) > 0)

		<div class="card panel-flat">

			<div class="card-body">
				<ul class="media-list">
					@foreach ($stores as $store)
					<li class="media">
						<div class="media-left media-middle">
							<a href="{{ Protocol::home() }}/store/{{ $store->username }}">
								<img src="{{ $store->logo }}" class="img-circle img-md" alt="{{ $store->title }}">
							</a>
						</div>

						<div class="media-body">
							<div class="media-heading text-semibold">{{ $store->title }}</div>
							{{--<span class="text-muted">{{ $store->username }}</span>--}}
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list text-nowrap">
		                    	<li class="dropdown">
		                    		<a target="_blank" href="{{ Protocol::home() }}/store/{{ strtolower($store->username) }}"><i class="icon-new-tab"></i></a>
		                    	</li>
	                    	</ul>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
		</div>

		@endif

		@if (Helper::ifCanSeeAds())
		<!-- Advertisement -->
		<div class="advertisment">
			{!! Helper::advertisements()->ad_sidebar !!}
		</div>
		@endif

		@if (Helper::ifCanSeeAds())
		<!-- Advertisement -->
		<div class="advertisment" style="margin-top: 20px">
			{!! Helper::advertisements()->ad_sidebar !!}
		</div>
		@endif

	</div>

</div>

@endsection