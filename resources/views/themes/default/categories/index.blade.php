@extends (Theme::get().'.layout.app')

@section ('seo')

    {!! SEO::generate() !!}

@endsection
{{--@section ('styles')--}}
  {{----}}
{{--@endsection--}}

@section ('content')
    {{--<link href="{{url('css/shop.css')}}" type="text/css" rel="stylesheet" media="all">--}}
    {{--<link href="{{url('css/left_category.css')}}" type="text/css" rel="stylesheet" media="all">--}}
    {{--<link href="{{url('css/style.css')}}" type="text/css" rel="stylesheet" media="all">--}}
    {{--<link href="{{url('css/style.css')}}" type="text/css" rel="stylesheet" media="all">--}}
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/categories/shop.css" type="text/css" rel="stylesheet" media="all">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/categories/categories.css" type="text/css" rel="stylesheet" media="all">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/categories/left_category.css" type="text/css" rel="stylesheet" media="all">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/categories/left_category.css" type="text/css" rel="stylesheet" media="all">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/categories/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- Filter -->


    <div class="row ">
        <div class="innerf-pages section">
            <div class="navigation-trigger hidden-xl-up"  id="cat" style="position: fixed; display: none; z-index: 99 !important; ">
                <div class="navigation-trigger__inner" id="trigger">
                    <i class="navigation-trigger__line"></i>
                    <i class="navigation-trigger__line"></i>
                    <i class="navigation-trigger__line"></i>
                </div>
            </div>
        <div class="fh-container mx-auto">

            <div class="row my-lg-5 mb-5 " style="margin-bottom: 1em!important;">
                {{--on small screens make overflox to be scroll--}}
                <div class="side-bar col-lg-3 " id="categ"

                     style="margin-bottom: 1em!important; margin-top: 9px; padding-right: 5px!important; padding-left: 0!important; ">
                    <div class="categories">
                        <h2>Categories</h2>
                        <ul class="cate">
                            @foreach($categories as $category)
                            <li><a href="{{ url('supplier-manufacturer/'.$category->category_slug) }}"><i class="fa fa-arrow-right" aria-hidden="true"></i>{{ $category->category_name }}</a></li>
                             @if($category->hasChildren())
                                    <ul>
                                 @foreach($category->hasChildren() as $child)

                                    <li><a href="{{ url('supplier-manufacturer/'.$category->category_slug.'/'.$child->category_slug) }}"><i class="fa fa-arrow-right" aria-hidden="true"></i>{{ $child->category_name }} <span class="badge badge-primary">{{ \App\Library\Config\Helper::count_ads_by_category($child->id) }}</span></a></li>
                                 @endforeach
                                    </ul>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- grid right -->
                <div class="col-lg-9 mt-lg-0 mt-5 right-product-grid" style="padding-right: 8px!important; padding-left: 5px!important;">
                    <!-- card group  -->
                    <div style="background-color: #f5f5f5" class="navbar navbar-default navbar-xs navbar-component">
                        <ul class="nav navbar-nav no-border visible-xs-block">
                            <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                        </ul>

                        <div class="navbar-collapse collapse" id="navbar-filter">
                            <p class="navbar-text">{{ Lang::get('category.lang_filter') }}</p>
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-time-asc position-left"></i> {{ Lang::get('category.lang_by_date') }} <span class="caret"></span></a>
                                    <ul class="dropdown-menu" style="margin-top: 5px;">
                                        <li><a href="{{ Protocol::home() }}/browse">{{ Lang::get('category.lang_show_all') }}</a></li>
                                        <li class="divider"></li>
                                        <li><a href="{{ Protocol::home() }}/browse?date=today">{{ Lang::get('category.lang_today') }}</a></li>
                                        <li><a href="{{ Protocol::home() }}/browse?date=yesterday">{{ Lang::get('category.lang_yesterday') }}</a></li>
                                        <li><a href="{{ Protocol::home() }}/browse?date=week">{{ Lang::get('category.lang_week') }}</a></li>
                                        <li><a href="{{ Protocol::home() }}/browse?date=month">{{ Lang::get('category.lang_month') }}</a></li>
                                        <li><a href="{{ Protocol::home() }}/browse?date=year">{{ Lang::get('category.lang_year') }}</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-amount-desc position-left"></i> {{ Lang::get('category.lang_by_status') }} <span class="caret"></span></a>
                                    <ul class="dropdown-menu" style="margin-top: 5px;">
                                        <li><a href="{{ Protocol::home() }}/browse">{{ Lang::get('category.lang_show_all') }}</a></li>
                                        <li class="divider"></li>
                                        <li><a href="{{ Protocol::home() }}/browse?status=featured">{{ Lang::get('category.lang_featured') }}</a></li>
                                        <li><a href="{{ Protocol::home() }}/browse?status=normal">{{ Lang::get('category.lang_not_featured') }}</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort position-left"></i> {{ Lang::get('category.lang_by_condition') }} <span class="caret"></span></a>
                                    <ul class="dropdown-menu" style="margin-top: 5px;">
                                        <li><a href="{{ Protocol::home() }}/browse">{{ Lang::get('category.lang_show_all') }}</a></li>
                                        <li class="divider"></li>
                                        <li><a href="{{ Protocol::home() }}/browse?condition=used">{{ Lang::get('category.lang_used') }}</a></li>
                                        <li><a href="{{ Protocol::home() }}/browse?condition=new">{{ Lang::get('category.lang_new') }}</a></li>
                                    </ul>
                                </li>
                            </ul>

                            <div class="navbar-right text-right">
                                <a href="{{ Protocol::home() }}/random" class="text-muted text-uppercase" style="line-height: 47px;">{{ Lang::get('search.lang_random') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-group ">
                        @if($products)
                            @foreach($products as $product)
                        <div class="col-lg-4 col-sm-6 p-1">
                            <div class="card product-men p-3">
                                <ul class="affiliate" style="color: blue">
                                    @if (!is_null($product->affiliate_link))
                                        <li><a style="color: white" href="{{ url($product->external_link.'') }}">{{ Lang::get('update_two.lang_shopping') }}</a> </li>
                                    @endif

                                </ul>
                                <ul class="tags">
                                    @if ($product->is_featured)
                                        <li>{{ Lang::get('home.lang_featured') }}</li>
                                    @endif
                                </ul>
                                <div class="men-thumb-item">
                                    <a href="{{ Protocol::home() }}/product/{{ $product->slug }}">
                                        <img style="height: 180px; width: 100%" height="180" src="{{ EverestCloud::getThumnail($product->ad_id, $product->images_host) }}" alt="{{ $product->title }}" class="card-img-top">

                                    </a>
                                </div>
                                <!-- card body -->
                                <div class="card-body  py-3 px-2">

                                    <h5 class="card-title text-capitalize"  style="line-height: 1.25em;margin-top: 0; padding: 1px; font-size: 15px"> <a href="{{ url('product/'.$product->slug) }}">{{ $product->title }}</a></h5>
                                    <div class="card-text d-flex justify-content-between">
                                        <p class="text-dark font-weight-bold">{{ ($product->price) ? number_format($product->price, 2) .' '. $product->currency : 'Contact seller for price' }}</p>
                                        @if (!is_null($product->regular_price))
                                        <p  style="text-decoration: line-through;">{{ number_format($product->regular_price, 2) }} {{ $product->currency }}</p>
                                        @endif
                                    </div>
                                </div>
                                <!-- card footer -->
                                <div class="card-footer  ">
                                    <div class="card__avatar"><a
                                                href="{{ Profile::hasStore($product->user_id) ? Protocol::home().'/store/'.Profile::hasStore($product->user_id)->username : '#' }}"
                                                class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                    src="{{ Profile::picture($product->user_id) }}"
                                                    alt="{{ Profile::hasStore($product->user_id) ? Profile::hasStore($product->user_id)->title : Profile::full_name($product->user_id) }}" class="avatar" width="30"
                                                    height="30"></a>
                                    </div>
                                      <a href="{{ url('product/'.$product->slug) }}" class="pull-right">
                                          <i class="fa fa-eye"  aria-hidden="true"></i>&nbsp;View

                                      </a>
                                </div>
                            </div>
                        </div>

                        @if($loop->iteration %5 == 0)
                                    <div class="col-lg-3 col-sm-6 p-1">
                                        <div class="card product-men p-3">

                                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> <ins class="adsbygoogle" style="display:block" data-ad-format="fluid" data-ad-layout-key="-70+d4-34-4m+tt" data-ad-client="ca-pub-6803454939055727" data-ad-slot="8351553304"></ins> <script> (adsbygoogle = window.adsbygoogle || []).push({}); </script>

                                        </div>
                                    </div>
                            @endif
                        @endforeach
                        @endif

                    </div>

                </div>

                <!-- //grid left -->
            </div>
            <div class="col-md-12 single-left" style="margin-left: -15px!important; margin-bottom: 3em!important;">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title font-weight-bold"> Title</h5>
                        <p class="card-text">Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus suscipit
                            tortor eget
                            felis porttitor volutpat. Proin eget tortor risus. Curabitur non nulla sit amet nisl
                            tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec,
                            egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat..</p>
                        <div id="collapse" style="display:none">
                            <p>Sed eleifend lectus id semper accumsan. Sed lobortis id ligula eget blandit. Integer interdum
                                iaculis nunc, sed porttitor magna tincidunt in. Interdum et malesuada fames ac ante ipsum
                                primis
                                in faucibus. Aliquam lobortis accumsan tempor. Aliquam sollicitudin pulvinar est, quis
                                convallis
                                tellus.</p>
                        </div>
                        <a href="#collapse" class="nav-toggle card-link" style="font-size: .98rem; color: #2196F3;display: inline-block; margin-top: 1rem; transition: color .3s; text-transform: uppercase;">Read More...</a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>

    <script type="text/javascript">
        $("#cat").on('click', function (e) {
            console.log('clicked');
            if($('#categ').hasClass('show')){
                $("#categ").removeClass('show')

            }else{
                $('#categ').addClass('show')
            }

        });
        $(document).click(function(event) {
            //if you click on anything except the categoery itself or the "open modal" link, close the modal
            if (!$(event.target).closest("#cat").length) {
                $("body").find("#categ").removeClass("show");
            }
        });
        $(document).ready(function () {

            $('.nav-toggle').click(function () {
                var collapse_content_selector = $(this).attr('href');
                var toggle_switch = $(this);
                $(collapse_content_selector).toggle(function () {
                    if ($(this).css('display') == 'none') {
                        toggle_switch.html('Read More');
                    } else {
                        toggle_switch.html('Read Less');
                    }
                });
            });

        });
        $(document).ready(function () {
            $('ul li.dropdown').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
            }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
            });
        });
    </script>
    @endsection