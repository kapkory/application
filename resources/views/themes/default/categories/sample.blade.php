@extends (Theme::get().'.layout.app')

@section ('content')

    {{--<link href="{{url('css/bootstrap.css')}}" type="text/css" rel="stylesheet" media="all">--}}
    <!-- shop css -->
    <link href="{{url('css/shop.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{url('css/left_category.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{url('css/style.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="{{url('css/style.css')}}" type="text/css" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/css?family=Elsie+Swash+Caps:400,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i"
          rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
    <link href="https://fonts.googleapis.com/css?family=Elsie+Swash+Caps:400,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i"
          rel="stylesheet">

    <div class="innerf-pages section">
        <div class="navigation-trigger hidden-xl-up" id="cat"
             style="position: fixed; display: none; z-index: 99 !important; ">
            <div class="navigation-trigger__inner" id="trigger">
                <i class="navigation-trigger__line"></i>
                <i class="navigation-trigger__line"></i>
                <i class="navigation-trigger__line"></i>
            </div>
        </div>
        <div class="fh-container mx-auto">
            <div class="row my-lg-5 mb-5 " style="margin-bottom: 1em!important;">
                <div class="side-bar col-lg-3 " id="categ"
                     style="margin-bottom: 1em!important; padding-right: 15px!; padding-left: 0!important; ">
                    <div class="categories">
                        <h2>Categories</h2>
                        <ul class="cate">
                            <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>SanitaryWare</a></li>
                            <ul>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Squatting Pan <span
                                                class="badge badge-primary">5</span></a></li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>One Piece Toilet
                                        <span class="badge badge-primary">5</span></a></li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Urinal <span
                                                class="badge badge-primary">5</span></a></li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Bidet <span
                                                class="badge badge-primary">5</span></a></li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Accessories <span
                                                class="badge badge-primary">5</span></a></li>
                            </ul>
                            <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Services</a></li>
                            <ul>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Squatting Pan<span
                                                class="badge badge-primary">5</span></a></li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>One Piece
                                        Toilet<span class="badge badge-primary">5</span></a></li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Urinal<span
                                                class="badge badge-primary">5</span></a></li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Bidet<span
                                                class="badge badge-primary">5</span></a></li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Accessories<span
                                                class="badge badge-primary">5</span></a></li>
                            </ul>
                            <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>SanitaryWare</a></li>
                            <ul>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Squatting Pan</a>
                                </li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>One Piece Toilet</a>
                                </li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Urinal</a></li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Bidet</a></li>
                                <li><a href="#"><i class="fa fa-arrow-right" aria-hidden="true"></i>Accessories</a></li>
                            </ul>
                        </ul>
                    </div>
                </div>
                <!-- grid right -->
                <div class="col-lg-9 mt-lg-0 mt-5 right-product-grid"
                     style="padding-right: 8px!important; padding-left: 5px!important;">
                    <!-- card group  -->
                    <div class="card-group ">
                        <!-- //card -->
                        <!-- card -->
                        <!--col-lg-3 col-xs-6 col-sm-6-->
                        <div class="col-lg-3 col-sm-6 p-1">
                            <div class="card product-men" style="padding: .50rem!important;">
                                <ul class="tags">
                                    <li>Featured</li>
                                </ul>
                                <div class="men-thumb-item">
                                    <img src="images/thumbnail_0.jpg" alt="img" class="card-img-top">
                                </div>
                                <!-- card body -->
                                <div class="card-body  py-3 px-2">
                                    <div class="" style="max-height: 100px;min-height: 100px; overflow: auto;">
                                        <h5 class="card-title text-capitalize"><a
                                                    href="https://sanitaryware.org/product/eva-ceramic-rectangular-single-piece-wall-mount-water-closet-535x35x325cm-by-aquavit-3323268030">Eva-Ceramic
                                                Rectangular Single Piece Wall Mount Water Closet (53.5x35x32.5cm) by
                                                Aquavit</a>
                                        </h5>
                                    </div>
                                    <div class="clearfix clear"></div>
                                    <div class="card-text d-flex justify-content-between">
                                        <p class="text-dark font-weight-bold">12,480.00 INR</p>
                                        <p class="line-through " style="float: right!important;">12,480.00 INR</p>
                                    </div>
                                </div>
                                <!-- card footer -->
                                <div class="card-footer  ">
                                    <div class="card__avatar"><a
                                                href="https://sanitaryware.org/store/buy-sanitaryware-online-in-india"
                                                class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                    src="https://sanitaryware.org/uploads/stores/buy-sanitaryware-online-in-india/logo.png"
                                                    alt="Buy Sanitaryware Online in India" class="avatar" width="40"
                                                    height="40"></a>
                                    </div>
                                    <button type="submit" class="hub-cart phub-cart btn float-right">
                                        <i class="fa fa-eye" aria-hidden="true"></i> View
                                    </button>

                                </div>
                            </div>
                        </div>
                        <!-- //card -->
                        <div class="col-lg-3 col-sm-6 p-1">
                            <div class="card product-men" style="padding: .50rem!important;">
                                <ul class="tags">
                                    <li>Featured</li>
                                </ul>
                                <div class="men-thumb-item">
                                    <img src="images/thumbnail_2.jpg" alt="img" class="card-img-top">
                                </div>
                                <!-- card body -->
                                <div class="card-body  py-3 px-2">
                                    <div class="" style="max-height: 100px;min-height: 100px; overflow: auto;">
                                        <h5 class="card-title text-capitalize">
                                            <a href="https://sanitaryware.org/product/gerber-maxwell-ceramic-single-piece-elongated-bowl-wall-mounted-western-toilet-by-freehand-2043012782">Gerber
                                                Maxwell-Ceramic Single Piece Elongated Bowl Wall Mounted Western Toilet
                                                by FreeHand</a>
                                        </h5>
                                    </div>
                                    <div class="clearfix clear"></div>
                                    <div class="card-text d-flex justify-content-between">
                                        <p class="text-dark font-weight-bold">$28.00</p>
                                        <p class="line-through">$35.99</p>
                                    </div>
                                </div>
                                <!-- card footer -->
                                <div class="card-footer  ">
                                    <div class="card__avatar"><a
                                                href="https://sanitaryware.org/store/buy-sanitaryware-online-in-india"
                                                class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                    src="https://sanitaryware.org/uploads/stores/buy-sanitaryware-online-in-india/logo.png"
                                                    alt="Buy Sanitaryware Online in India" class="avatar" width="40"
                                                    height="40"></a>
                                    </div>
                                    <button type="submit" class="hub-cart phub-cart btn float-right">
                                        <i class="fa fa-eye" aria-hidden="true"></i> View
                                    </button>

                                </div>
                            </div>
                        </div>
                        <!-- card -->
                        <div class="col-lg-3 col-sm-6 p-1">
                            <div class="card product-men" style="padding: .50rem!important;">
                                <ul class="tags">
                                    <li>Featured</li>
                                </ul>
                                <div class="men-thumb-item">
                                    <img src="images/thumbnail_6.jpg" alt="img" class="card-img-top">
                                </div>
                                <!-- card body -->
                                <div class="card-body  py-3 px-2">
                                    <div class="" style="max-height: 100px;min-height: 100px; overflow: auto;">
                                        <h5 class="card-title text-capitalize"><a href="https://sanitaryware.org/product/cleo-ceramic-elongated-single-piece-wall-mount-water-closet55x37x37cmby-aquavit-3956248036">Cleo-Ceramic Elongated Single Piece Wall Mount Water Closet(55x37x37cm)by Aquavit</a>
                                        </h5>
                                    </div>
                                    <div class="clearfix clear"></div>
                                    <div class="card-text d-flex justify-content-between">
                                        <p class="text-dark font-weight-bold">$28.00</p>
                                        <p class="line-through">$35.99</p>
                                    </div>
                                </div>
                                <!-- card footer -->
                                <div class="card-footer  ">
                                    <div class="card__avatar"><a
                                                href="https://sanitaryware.org/store/buy-sanitaryware-online-in-india"
                                                class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                    src="https://sanitaryware.org/uploads/stores/buy-sanitaryware-online-in-india/logo.png"
                                                    alt="Buy Sanitaryware Online in India" class="avatar" width="40"
                                                    height="40"></a>
                                    </div>
                                    <button type="submit" class="hub-cart phub-cart btn float-right">
                                        <i class="fa fa-eye" aria-hidden="true"></i> View
                                    </button>

                                </div>
                            </div>
                        </div>
                        <!-- //card -->
                        <!-- card -->
                        <div class="col-lg-3 col-sm-6 p-1">
                            <div class="card product-men" style="padding: .50rem!important;">
                                <ul class="tags">
                                    <li>Featured</li>
                                </ul>
                                <div class="men-thumb-item">
                                    <img src="images/thumbnail_6.jpg" alt="img" class="card-img-top">
                                </div>
                                <!-- card body -->
                                <div class="card-body  py-3 px-2">
                                    <div class="" style="max-height: 100px; min-height: 100px; overflow: auto;">
                                        <h5 class="card-title text-capitalize"><a
                                                    href="https://sanitaryware.org/product/eva-ceramic-rectangular-single-piece-wall-mount-water-closet-535x35x325cm-by-aquavit-3323268030">Eva-Ceramic
                                                Rectangular Single Piece Wall Mount Water Closet (53.5x35x32.5cm) by
                                                Aquavit</a>
                                        </h5>
                                    </div>
                                    <div class="clearfix clear"></div>
                                    <div class="card-text d-flex justify-content-between">
                                        <p class="text-dark font-weight-bold">$28.00</p>
                                        <p class="line-through">$35.99</p>
                                    </div>
                                </div>
                                <!-- card footer -->
                                <div class="card-footer  ">
                                    <div class="card__avatar"><a
                                                href="https://sanitaryware.org/store/buy-sanitaryware-online-in-india"
                                                class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                    src="https://sanitaryware.org/uploads/stores/buy-sanitaryware-online-in-india/logo.png"
                                                    alt="Buy Sanitaryware Online in India" class="avatar" width="40"
                                                    height="40"></a>
                                    </div>
                                    <button type="submit" class="hub-cart phub-cart btn float-right">
                                        <i class="fa fa-eye" aria-hidden="true"></i> View
                                    </button>

                                </div>
                            </div>
                        </div>
                        <!-- //card -->
                        <!-- //row  -->
                    </div>
                    <div class="card-group ">
                        <!-- //card -->
                        <!-- card -->
                        <!--col-lg-3 col-xs-6 col-sm-6-->
                        <div class="col-lg-3 col-sm-6 p-1">
                            <div class="card product-men" style="padding: .50rem!important;">
                                <ul class="tags">
                                    <li>Featured</li>
                                </ul>
                                <div class="men-thumb-item">
                                    <img src="images/thumbnail_6.jpg" alt="img" class="card-img-top">
                                </div>
                                <!-- card body -->
                                <div class="card-body  py-3 px-2">
                                    <div class="" style="max-height: 100px;min-height: 100px; overflow: auto;">
                                        <h5 class="card-title text-capitalize"><a
                                                    href="https://sanitaryware.org/product/eva-ceramic-rectangular-single-piece-wall-mount-water-closet-535x35x325cm-by-aquavit-3323268030">Eva-Ceramic
                                                Rectangular Single Piece Wall Mount Water Closet (53.5x35x32.5cm) by
                                                Aquavit</a>
                                        </h5>
                                    </div>
                                    <div class="clearfix clear"></div>
                                    <div class="card-text d-flex justify-content-between">
                                        <p class="text-dark font-weight-bold">12,480.00 INR</p>
                                        <p class="line-through " style="float: right!important;">12,480.00 INR</p>
                                    </div>
                                </div>
                                <!-- card footer -->
                                <div class="card-footer  ">
                                    <div class="card__avatar"><a
                                                href="https://sanitaryware.org/store/buy-sanitaryware-online-in-india"
                                                class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                    src="https://sanitaryware.org/uploads/stores/buy-sanitaryware-online-in-india/logo.png"
                                                    alt="Buy Sanitaryware Online in India" class="avatar" width="40"
                                                    height="40"></a>
                                    </div>
                                    <button type="submit" class="hub-cart phub-cart btn float-right">
                                        <i class="fa fa-eye" aria-hidden="true"></i> View
                                    </button>

                                </div>
                            </div>
                        </div>
                        <!-- //card -->
                        <div class="col-lg-3 col-sm-6 p-1">
                            <div class="card product-men" style="padding: .50rem!important;">
                                <ul class="tags">
                                    <li>Featured</li>
                                </ul>
                                <div class="men-thumb-item">
                                    <img src="images/thumbnail_6.jpg" alt="img" class="card-img-top">
                                </div>
                                <!-- card body -->
                                <div class="card-body  py-3 px-2">
                                    <div class="" style="max-height: 100px;min-height: 100px; overflow: auto;">
                                        <h5 class="card-title text-capitalize"><a
                                                    href="https://sanitaryware.org/product/eva-ceramic-rectangular-single-piece-wall-mount-water-closet-535x35x325cm-by-aquavit-3323268030">Eva-Ceramic
                                                Rectangular Single Piece Wall Mount Water Closet (53.5x35x32.5cm) by
                                                Aquavit</a>
                                        </h5>
                                    </div>
                                    <div class="clearfix clear"></div>
                                    <div class="card-text d-flex justify-content-between">
                                        <p class="text-dark font-weight-bold">8,880.00 INR</p>
                                        <p class="line-through">8,880.00 INR</p>
                                    </div>
                                </div>
                                <!-- card footer -->
                                <div class="card-footer  ">
                                    <div class="card__avatar"><a
                                                href="https://sanitaryware.org/store/buy-sanitaryware-online-in-india"
                                                class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                    src="https://sanitaryware.org/uploads/stores/buy-sanitaryware-online-in-india/logo.png"
                                                    alt="Buy Sanitaryware Online in India" class="avatar" width="40"
                                                    height="40"></a>
                                    </div>
                                    <button type="submit" class="hub-cart phub-cart btn float-right">
                                        <i class="fa fa-eye" aria-hidden="true"></i> View
                                    </button>

                                </div>
                            </div>
                        </div>
                        <!-- card -->
                        <div class="col-lg-3 col-sm-6 p-1">
                            <div class="card product-men" style="padding: .50rem!important;">
                                <ul class="tags">
                                    <li>Featured</li>
                                </ul>
                                <div class="men-thumb-item">
                                    <img src="images/thumbnail_6.jpg" alt="img" class="card-img-top">
                                </div>
                                <!-- card body -->
                                <div class="card-body  py-3 px-2">
                                    <div class="" style="max-height: 100px;min-height: 100px; overflow: auto;">
                                        <h5 class="card-title text-capitalize"><a
                                                    href="https://sanitaryware.org/product/eva-ceramic-rectangular-single-piece-wall-mount-water-closet-535x35x325cm-by-aquavit-3323268030">Eva-Ceramic
                                                Rectangular Single Piece Wall Mount Water Closet (53.5x35x32.5cm) by
                                                Aquavit</a>
                                        </h5>
                                    </div>
                                    <div class="clearfix clear"></div>
                                    <div class="card-text d-flex justify-content-between">
                                        <p class="text-dark font-weight-bold">$28.00</p>
                                        <p class="line-through">$35.99</p>
                                    </div>
                                </div>
                                <!-- card footer -->
                                <div class="card-footer  ">
                                    <div class="card__avatar"><a
                                                href="https://sanitaryware.org/store/buy-sanitaryware-online-in-india"
                                                class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                    src="https://sanitaryware.org/uploads/stores/buy-sanitaryware-online-in-india/logo.png"
                                                    alt="Buy Sanitaryware Online in India" class="avatar" width="40"
                                                    height="40"></a>
                                    </div>
                                    <button type="submit" class="hub-cart phub-cart btn float-right">
                                        <i class="fa fa-eye" aria-hidden="true"></i> View
                                    </button>

                                </div>
                            </div>
                        </div>
                        <!-- //card -->
                        <!-- card -->
                        <div class="col-lg-3 col-sm-6 p-1">
                            <div class="card product-men" style="padding: .50rem!important;">
                                <ul class="tags">
                                    <li>Featured</li>
                                </ul>
                                <div class="men-thumb-item">
                                    <img src="images/thumbnail_6.jpg" alt="img" class="card-img-top">
                                </div>
                                <!-- card body -->
                                <div class="card-body  py-3 px-2">
                                    <div class="" style="max-height: 100px; min-height: 100px; overflow: auto;">
                                        <h5 class="card-title text-capitalize"><a
                                                    href="https://sanitaryware.org/product/eva-ceramic-rectangular-single-piece-wall-mount-water-closet-535x35x325cm-by-aquavit-3323268030">Eva-Ceramic
                                                Rectangular Single Piece Wall Mount Water Closet (53.5x35x32.5cm) by
                                                Aquavit</a>
                                        </h5>
                                    </div>
                                    <div class="clearfix clear"></div>
                                    <div class="card-text d-flex justify-content-between">
                                        <p class="text-dark font-weight-bold">8,880.00 INR</p>
                                        <p class="line-through">8,880.00 INR</p>
                                    </div>
                                </div>
                                <!-- card footer -->
                                <div class="card-footer  ">
                                    <div class="card__avatar"><a
                                                href="https://sanitaryware.org/store/buy-sanitaryware-online-in-india"
                                                class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                    src="https://sanitaryware.org/uploads/stores/buy-sanitaryware-online-in-india/logo.png"
                                                    alt="Buy Sanitaryware Online in India" class="avatar" width="40"
                                                    height="40"></a>
                                    </div>
                                    <button type="submit" class="hub-cart phub-cart btn float-right">
                                        <i class="fa fa-eye" aria-hidden="true"></i> View
                                    </button>

                                </div>
                            </div>
                        </div>
                        <!-- //card -->
                        <!-- //row  -->
                    </div>
                </div>

                <!-- //grid left -->
            </div>
            <div class="col-md-12 single-left" style="margin-left: -15px!important; margin-bottom: 3em!important;">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title font-weight-bold"> Title</h5>
                        <p class="card-text">Quisque velit nisi, pretium ut lacinia in, elementum id enim. Vivamus
                            suscipit
                            tortor eget
                            felis porttitor volutpat. Proin eget tortor risus. Curabitur non nulla sit amet nisl
                            tempus convallis quis ac lectus. Praesent sapien massa, convallis a pellentesque nec,
                            egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat..</p>
                        <div id="collapse" style="display:none">
                            <p>Sed eleifend lectus id semper accumsan. Sed lobortis id ligula eget blandit. Integer
                                interdum
                                iaculis nunc, sed porttitor magna tincidunt in. Interdum et malesuada fames ac ante
                                ipsum
                                primis
                                in faucibus. Aliquam lobortis accumsan tempor. Aliquam sollicitudin pulvinar est, quis
                                convallis
                                tellus.</p>
                        </div>
                        <a href="#collapse" class="nav-toggle card-link"
                           style="font-size: .98rem; color: #2196F3;display: inline-block; margin-top: 1rem; transition: color .3s; text-transform: uppercase;">Read
                            More...</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#cat").on('click', function (e) {
            console.log('clicked');
            if ($('#categ').hasClass('show')) {
                $("#categ").removeClass('show')

            } else {
                $('#categ').addClass('show')
            }

        });
        $(document).click(function (event) {
            //if you click on anything except the categoery itself or the "open modal" link, close the modal
            if (!$(event.target).closest("#cat").length) {
                $("body").find("#categ").removeClass("show");
            }
        });
        $(document).ready(function () {

            $('.nav-toggle').click(function () {
                var collapse_content_selector = $(this).attr('href');
                var toggle_switch = $(this);
                $(collapse_content_selector).toggle(function () {
                    if ($(this).css('display') == 'none') {
                        toggle_switch.html('Read More');
                    } else {
                        toggle_switch.html('Read Less');
                    }
                });
            });

        });
        $(document).ready(function () {
            $('ul li.dropdown').hover(function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
            }, function () {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
            });
        });
    </script>
    <script src="{{url('js/jquery-2.2.3.min.js')}}"></script>
    <script src="{{url('js/bootstrap.js')}}"></script>

@endsection