<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Website down for maintenance</title>
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet"> 
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,700' rel='stylesheet'>
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet"> 
	<style type="text/css">
		
		body{
			background-image: url("{{ Protocol::home() }}/content/assets/front-end/images/backgrounds/maintenance.jpg");
			background-size: cover;
			background-repeat: no-repeat;
			background-position: 0% 0%;
		}
		.maintenance{
			width: 80%;
			margin: 250px auto;
			text-align: center;
		}
		.maintenance h1{
			text-transform: uppercase;
			color: #FFF;
			font-size: 60px;
			font-family: Roboto;
		}
		.maintenance p{
			color: #F65F5F;
			font-size: 16px;
			font-family: Roboto;
			text-transform: uppercase;
			line-height: 31px;
		}

	</style>
</head>
<body>

	<div class="maintenance">
		<h1>Will Be Back Soon</h1>
		<p>Thank you for being patient. We are doing some work on the site and we will be back shortly.</p>
	</div>

</body>
</html>