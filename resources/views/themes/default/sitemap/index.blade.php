<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>{{ url('sitemap/ads.xml') }}</loc>
        <lastmod>{{ \Carbon\Carbon::now()->subDays(5)->format('Y-m-d') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/categories.xml') }}</loc>
        <lastmod>{{ \Carbon\Carbon::now()->subWeek(2)->format('Y-m-d') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/articles.xml') }}</loc>
        <lastmod>{{ \Carbon\Carbon::now()->subWeek(1)->format('Y-m-d') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/article-categories.xml') }}</loc>
        <lastmod>{{ \Carbon\Carbon::now()->subWeek(2)->format('Y-m-d') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/catalogues.xml') }}</loc>
        <lastmod>{{ \Carbon\Carbon::now()->subWeek(1)->format('Y-m-d') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/catalogues-country.xml') }}</loc>
        <lastmod>{{ \Carbon\Carbon::now()->subWeek(3)->format('Y-m-d') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/pages.xml') }}</loc>
        <lastmod>{{ \Carbon\Carbon::now()->subWeek(3)->format('Y-m-d') }}</lastmod>
    </sitemap>
    <sitemap>
        <loc>{{ url('sitemap/browse.xml') }}</loc>
        <lastmod>{{ \Carbon\Carbon::now()->subDay(1)->format('Y-m-d') }}</lastmod>
    </sitemap>
</sitemapindex>



