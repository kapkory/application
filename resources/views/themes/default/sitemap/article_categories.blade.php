@if ($article_categories)
    <?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        @foreach ($article_categories as $article_category)
            <url>
                @if(is_null($article_category->parent_category))
                    <loc> {{ url('article-category/'.$article_category->category_slug) }}</loc>
                    @else
                <loc> {{ url('article-category/'.Helper::articleCategory( $article_category->parent_category)->category_slug).'/'.$article_category->category_slug  }}</loc>
                @endif
                <lastmod>{{ $article_category->created_at->tz('UTC')->toAtomString() }}</lastmod>
                <changefreq>weekly</changefreq>
                <priority>0.6</priority>
            </url>
        @endforeach
    </urlset>
@endif