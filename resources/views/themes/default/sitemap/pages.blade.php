<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            <url>
                <loc>{{ url('catalogue') }}</loc>
                <changefreq>Daily</changefreq>
                <priority>0.6</priority>
            </url>

        <url>
            <loc>{{ url('article') }}</loc>
            <changefreq>Daily</changefreq>
            <priority>0.6</priority>
        </url>

        <url>
            <loc>{{ url('stores') }}</loc>
            <changefreq>Daily</changefreq>
            <priority>0.7</priority>
        </url>

    </urlset>
