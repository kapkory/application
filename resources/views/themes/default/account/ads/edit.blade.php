@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('styles')
	<link href="{{ Protocol::home() }}/content/assets/jquery.filer.css" rel="stylesheet">
	<link href="{{ Protocol::home() }}/content/assets/jquery.filer-dragdropbox-theme.css" rel="stylesheet">
@endsection

@section ('javascript')
	<script src="//cdn.ckeditor.com/4.9.1/standard/ckeditor.js"></script>
	@if (Auth::check())
	@if (Profile::hasStore(Auth::id()))
	<script type="text/javascript">
		var _limit_images = {{ Helper::settings_membership()->pro_ad_images }};
	</script>
	@else 
	<script type="text/javascript">
		var _limit_images = {{ Helper::settings_membership()->free_ad_images }};
	</script>
	@endif
	@else 
	<script type="text/javascript">
		var _limit_images = 0;
	</script>
	@endif
	<script src="{{ Protocol::home() }}/content/assets/custom.js" type="text/javascript"></script>
	<script src="{{ Protocol::home() }}/content/assets/jquery.filer.js" type="text/javascript"></script><script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/editors/wysihtml5/wysihtml5.min.js"></script>
	<script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/editors/wysihtml5/toolbar.js"></script>
	<script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/editors/wysihtml5/parsers.js"></script>
	<script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js"></script>
	<script>
		$(function() {

		    // Simple toolbar
		    $('.wysihtml5-min').wysihtml5({
		        parserRules:  wysihtml5ParserRules,
		        stylesheets: ["{{ Protocol::home() }}/content/assets/front-end/css/components.css"],
		        "font-styles": false, // Font styling, e.g. h1, h2, etc. Default true
		        "emphasis": true, // Italics, bold, etc. Default true
		        "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers. Default true
		        "html": false, // Button which allows you to edit the generated HTML. Default false
		        "link": true, // Button to insert a link. Default true
		        "image": true, // Button to insert an image. Default true,
		        "action": false, // Undo / Redo buttons,
		        "color": true // Button to change color of font
		    });

		});
	</script>

@endsection

@section ('content')

<!-- Edit Ad -->
<div class="row">

	<!-- Session Messages -->
	<div class="col-md-12">
		@if (Session::has('success'))
		<div class="alert bg-success alert-styled-left">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
			{{ Session::get('success') }}
	    </div>
	    @endif
	    @if (Session::has('error'))
		<div class="alert bg-danger alert-styled-left">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
			{{ Session::get('error') }}
	    </div>
	    @endif
	</div>

	@include (Theme::get().'.account.include.sidebar')
	
	<!-- Edit Ad -->
	<div class="col-md-9">
	
		<div class="card panel-flat bg-flat">

			<form class="form-horizontal form-validate-jquery" action="{{ Protocol::home() }}/account/ads/edit/{{ $ad->ad_id }}" method="POST" enctype="multipart/form-data">

				{{ csrf_field() }}

				<div class="card panel-flat">

					<div class="card-body">

						<fieldset>

							<!-- Ad Title -->
							<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
								<div class="col-lg-12">
									<label>Title</label>
									<input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('create/ad.lang_title_placeholder') }}" name="title" value="{{ $ad->title }}" required="">
									@if ($errors->has('title'))
									<span class="help-block">{{ $errors->first('title') }}</span>
									@endif
								</div>
							</div>


							<!-- Content -->
							<div class="form-group form-md-line-input">
								{{--<label class="col-md-2 control-label" for="content">Article Content</label>--}}
								<div class="col-md-12">
									<textarea placeholder="{{ Lang::get('create/ad.lang_description_placeholder') }}" name="descript">{{ $ad->description }}</textarea>
									<script>
                                        // CKEDITOR.replace( 'description' ,{
                                        //     extraPlugins: 'imageuploader'
                                        // });
                                        CKEDITOR.replace( 'descript', {
                                            extraPlugins: 'uploadimage',
                                            height: 300,

                                            // Upload images to a CKFinder connector (note that the response type is set to JSON).
                                            uploadUrl: '{{ url('ad/upload/image') }}',

                                            // Configure your file manager integration. This example uses CKFinder 3 for PHP.
                                            // filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                                            filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
                                            // filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                                            filebrowserImageUploadUrl: '{{ url("ad/upload/image") }}',

                                            // The following options are not necessary and are used here for presentation purposes only.
                                            // They configure the Styles drop-down list and widgets to use classes.

                                            stylesSet: [
                                                { name: 'Narrow image', type: 'widget', widget: 'image', attributes: { 'class': 'image-narrow' } },
                                                { name: 'Wide image', type: 'widget', widget: 'image', attributes: { 'class': 'image-wide' } }
                                            ],

                                            // Load the default contents.css file plus customizations for this sample.
                                            contentsCss: [ CKEDITOR.basePath + 'contents.css', 'https://sdk.ckeditor.com/samples/assets/css/widgetstyles.css' ],

                                            // Configure the Enhanced Image plugin to use classes instead of styles and to disable the
                                            // resizer (because image size is controlled by widget styles or the image takes maximum
                                            // 100% of the editor width).
                                            image2_alignClasses: [ 'image-align-left', 'image-align-center', 'image-align-right' ],
                                            image2_disableResizer: true
                                        } );
									</script>
								</div>
							</div>

							<!-- Ad Description -->
							{{--<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">--}}
								{{--<div class="col-lg-12">--}}
									{{--<label>Description</label>--}}
									{{--<textarea rows="10" cols="5" class="form-control wysihtml5 wysihtml5-min" placeholder="{{ Lang::get('create/ad.lang_description_placeholder') }}" name="description">{{ $ad->description }}</textarea>--}}
								{{--</div>--}}
							{{--</div>--}}

							<!-- Ad Category -->
							<div class="form-group select-size-lg {{ $errors->has('category') ? 'has-error' : '' }}">
								<div class="col-lg-12">
									<label>Category</label>
									<select required="" class="select" data-placeholder="{{ Lang::get('create/ad.lang_select_category') }}" name="category">
	                                <option></option>
	                                @if(count(Helper::parent_categories()))
	                                @foreach (Helper::parent_categories() as $parent)
	                                <optgroup label="{{ $parent->category_name }}">
	                                    @if (count(Helper::sub_categories($parent->id)))
	                                    @foreach (Helper::sub_categories($parent->id) as $sub)
	                                    <option {{ $ad->category == $sub->id ? 'selected' : '' }} value="{{ $sub->id }}">{{ $sub->category_name }}</option>
	                                    @endforeach
	                                    @endif
	                                </optgroup>
	                                @endforeach
	                                @endif
	                            </select>
								</div>
							</div>

							<!-- Country -->
							{{--<div class="form-group select-size-lg {{ $errors->has('country') ? 'has-error' : '' }}">--}}
	                            {{--<div class="col-lg-12">--}}
	                            	{{--<select required="" class="select-search" name="country" onchange="getStates(this.value)" data-placeholder="{{ Lang::get('create/ad.lang_select_country') }}">--}}
										{{--@foreach ($countries as $country)--}}
	                                    {{--<option {{ $ad->country == $country->sortname ? 'selected' : '' }} value="{{ $country->sortname }}">{{ $country->name }}</option>--}}
	                                    {{--@endforeach--}}
									{{--</select>--}}
		                            {{--@if ($errors->has('country'))--}}
									{{--<span class="help-block">{{ $errors->first('country') }}</span>--}}
									{{--@endif--}}
	                            {{--</div>--}}
	                        {{--</div>--}}
							<div class="form-group select-size-lg {{ $errors->has('deliverable_to') ? 'has-error' : '' }}">
								<div class="col-lg-12">
									<label>Deliverable to</label>
									<select id="deliverable_to" required="" class="select-search" name="deliverable_to" onchange="getCountries(this.value)" data-placeholder="Deliverable to">
										<option  value="">Deliverable to</option>
										<option {{ $ad->deliverable_to==1 ? "selected" : "" }} value="1">Globally</option>
										<option {{ $ad->deliverable_to==2 ? "selected" : "" }} value="2">Selected Countries</option>
									</select>
									@if ($errors->has('deliverable_to'))
										<span class="help-block">{{ $errors->first('deliverable_to') }}</span>
									@endif
								</div>
							</div>


							<!-- Country -->
							<div id="country_form" class="form-group select-size-lg {{ $errors->has('country') ? 'has-error' : '' }}">
								<div class="col-lg-12">
									{{--@if($ad->deliverable_to == 2)--}}
										{{--{{ $ad->adcountries }}--}}
										{{--@endif--}}
									<select id="country" class="select-search" name="country[]" multiple="multiple" data-placeholder="{{ Lang::get('create/ad.lang_select_country') }}">

									</select>
									@if ($errors->has('country'))
										<span class="help-block">{{ $errors->first('country') }}</span>
									@endif
								</div>
							</div>

							{{--@if (Helper::settings_geo()->states_enabled)--}}
	                        {{--<!-- State -->--}}
							{{--<div class="form-group select-size-lg {{ $errors->has('state') ? 'has-error' : '' }}">--}}
	                            {{--<div class="col-lg-12">--}}
	                                {{--<select required="" data-placeholder="{{ Lang::get('create/ad.lang_select_state') }}" class="select-search" name="state" onchange="getCities(this.value)" id="putStates">--}}
	                                    {{--@foreach ($states as $state)--}}
	                                    {{--<option value="{{ $state->id }}" {{ $ad->state == $state->id ? 'selected' : '' }}>{{ $state->name }}</option>--}}
	                                    {{--@endforeach--}}
	                                {{--</select>--}}
		                            {{--@if ($errors->has('state'))--}}
									{{--<span class="help-block">{{ $errors->first('state') }}</span>--}}
									{{--@endif--}}
	                            {{--</div>--}}
	                        {{--</div>--}}
	                        {{--@endif--}}

							{{--@if (Helper::settings_geo()->cities_enabled)--}}
	                        {{--<!-- City -->--}}
							{{--<div class="form-group select-size-lg {{ $errors->has('city') ? 'has-error' : '' }}">--}}
	                            {{--<div class="col-lg-12">--}}
	                            	{{--<select required="" data-placeholder="{{ Lang::get('create/ad.lang_select_city') }}" class="select-search" name="city" id="putCities">--}}
										{{--@foreach ($cities as $city)--}}
	                                    {{--<option value="{{ $city->id }}" {{ $ad->city == $city->id ? 'selected' : '' }}>{{ $city->name }}</option>--}}
	                                    {{--@endforeach--}}
	                                    {{--<option></option>--}}
	                                {{--</select>--}}
		                            {{--@if ($errors->has('city'))--}}
									{{--<span class="help-block">{{ $errors->first('city') }}</span>--}}
									{{--@endif--}}
	                            {{--</div>--}}
	                        {{--</div>--}}
	                        {{--@endif--}}

							<!-- Ad Regular, Sale Price & Currency -->
							<div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">

								<!-- Sale Price -->
								<div class="{{ Profile::hasStore(Auth::id()) ? 'col-md-4' : 'col-lg-8' }}">
									<label>{{ Lang::get('update_two.lang_sale_price') }}</label>
									<input required="" type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update_two.lang_sale_price') }}" name="price" value="{{ $ad->price }}">
									@if ($errors->has('price'))
									<span class="help-block">{{ $errors->first('price') }}</span>
									@endif
								</div>
								
								@if ( Profile::hasStore(Auth::id()) )
								<!-- Regular Price -->
								<div class="col-lg-4">
									<label>Regular Price</label>
									<input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update_two.lang_regular_price') }}" name="regular_price" value="{{ $ad->regular_price }}">
									@if ($errors->has('regular_price'))
									<span class="help-block">{{ $errors->first('regular_price') }}</span>
									@endif
								</div>
								@endif

								<!-- Select Currency -->
								<div class="col-lg-4 select-size-lg">
									<label>Currency</label>
									<select required="" class="select" name="currency" data-placeholder="{{ Lang::get('update.lang_select_currency') }}">
										<option></option>
										@foreach (App\Models\Currency::get() as $currency)
	                                    <option {{ $ad->currency == $currency->code ? 'selected' : '' }} value="{{ $currency->code }}">{{ $currency->code }}</option>
	                                    @endforeach
	                                </select>
		                            @if ($errors->has('currency'))
									<span class="help-block">{{ $errors->first('currency') }}</span>
									@endif
								</div>

							</div>

							<!-- Minimum Order -->
							<div class="form-group {{ $errors->has('minimum_order_quantity') ? 'has-error' : '' }}">

								<label>Minimum Order Quantity</label>
								<div class="col-lg-12">
									<input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('create/ad.lang_minimum_order') }}" name="minimum_order_quantity" value="{{ $ad->minimum_order_quantity }}" required="">
									@if ($errors->has('minimum_order_quantity'))
										<span class="help-block">{{ $errors->first('minimum_order_quantity') }}</span>
									@endif
								</div>
							</div>

							<!-- Supply Time -->
							<div class="form-group {{ $errors->has('supply_time') ? 'has-error' : '' }}">
								<div class="col-lg-12">
									<label>Supply Time</label>
									<input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('create/ad.lang_supply') }}" name="supply_time" value="{{ $ad->supply_time }}" required="">
									@if ($errors->has('supply_time'))
										<span class="help-block">{{ $errors->first('supply_time') }}</span>
									@endif
								</div>
							</div>

							{{--<!-- Is Negotiable -->--}}
							{{--<div class="form-group select-size-lg {{ $errors->has('negotiable') ? 'has-error' : '' }}">--}}
								{{--<div class="col-lg-12">--}}
									{{--<select required="" class="select" data-placeholder="{{ Lang::get('create/ad.lang_negotiable') }}" name="negotiable">--}}
										{{--@if ($ad->negotiable)--}}
										{{--<option value="1">{{ Lang::get('create/ad.lang_negotiable') }}</option>--}}
										{{--<option value="0">{{ Lang::get('create/ad.lang_not_negotiable') }}</option>--}}
										{{--@else --}}
										{{--<option value="0">{{ Lang::get('create/ad.lang_not_negotiable') }}</option>--}}
										{{--<option value="1">{{ Lang::get('create/ad.lang_negotiable') }}</option>--}}
										{{--@endif--}}
									{{--</select>--}}
									{{--@if ($errors->has('negotiable'))--}}
									{{--<span class="help-block">{{ $errors->first('negotiable') }}</span>--}}
									{{--@endif--}}
								{{--</div>--}}
							{{--</div>--}}

	            			<!-- Condition -->
							{{--<div class="form-group select-size-lg {{ $errors->has('condition') ? 'has-error' : '' }}">--}}
								{{--<div class="col-lg-12">--}}
									{{--<select required="" class="select" data-placeholder="{{ Lang::get('create/ad.lang_item_condition') }}" name="condition">--}}
										{{--@if ($ad->is_used)--}}
										{{--<option value="1">{{ Lang::get('category.lang_used') }}</option>--}}
										{{--<option value="0">{{ Lang::get('category.lang_new') }}</option>--}}
										{{--@else --}}
										{{--<option value="0">{{ Lang::get('category.lang_new') }}</option>--}}
										{{--<option value="1">{{ Lang::get('category.lang_used') }}</option>--}}
										{{--@endif--}}
									{{--</select>--}}
									{{--@if ($errors->has('condition'))--}}
									{{--<span class="help-block">{{ $errors->first('condition') }}</span>--}}
									{{--@endif--}}
								{{--</div>--}}
							{{--</div>--}}

							@if (Profile::hasStore(Auth::id()))
	            			<!-- Out Of Stock -->
							<div class="form-group select-size-lg {{ $errors->has('oos') ? 'has-error' : '' }}">
								<div class="col-lg-12">
									<label>Order Availability</label>
									<select required="" class="select" data-placeholder="Out of stock" name="oos">
										@if ($ad->is_oos)
										<option value="1">Unavailable</option>
										<option value="0">Available</option>
										@else 
										<option value="0">Available</option>
										<option value="1">Unavailable</option>
										@endif
									</select>
								</div>
								@if ($errors->has('oos'))
								<span class="help-block">{{ $errors->first('oos') }}</span>
								@endif
							</div>
							@endif

							<!-- Affiliate LINK -->
							<div class="form-group {{ $errors->has('affiliate_link') ? 'has-error' : '' }}">
								<div class="col-lg-12">
									<label>Affiliate Link</label>
									@if ( Profile::hasStore(Auth::id()) )
									<input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update_two.lang_affiliate_link_placeholder') }}" name="affiliate_link" value="{{ $ad->affiliate_link }}">
									@else
									<input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update_two.lang_affiliate_link_placeholder') }}" readonly="" name="affiliate_link" value="{{ $ad->affiliate_link }}" data-popup="tooltip" data-placement="top" data-container="body" title="{{ Lang::get('update.lang_youtube_video_not_available') }}">
									@endif
									@if ($errors->has('affiliate_link'))
									<span class="help-block">{{ $errors->first('affiliate_link') }}</span>
									@endif
								</div>
							</div>

							<!-- Youtube Video -->
							<div class="form-group {{ $errors->has('youtube') ? 'has-error' : '' }}">
								<div class="col-lg-12">
									<label>Youtube Video Link</label>
									@if ( Profile::hasStore(Auth::id()) )
									<input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update.lang_youtube_video_placeholder') }}" name="youtube" value="{{ old('youtube') }}">
									@else
									<input type="text" class="form-control input-xlg" placeholder="{{ Lang::get('update.lang_youtube_video_placeholder') }}" readonly="" name="youtube" value="{{ old('youtube') }}" data-popup="tooltip" data-placement="top" data-container="body" title="{{ Lang::get('update.lang_youtube_video_not_available') }}">
									@endif
									@if ($errors->has('youtube'))
									<span class="help-block">{{ $errors->first('youtube') }}</span>
									@endif
								</div>
							</div>

	            			<!-- Upload Photos -->
	            			<div class="form-group {{ $errors->has('photos') ? 'has-error' : '' }}">
								<div class="col-lg-12">
									<input type="file" name="photos[]" id="uploader" multiple="multiple" accept="image/*">
									<span class="help-block">{{ Lang::get('create/ad.lang_accepted_formats') }}</span>
									@if ($errors->has('photos'))
									<span class="help-block">{{ $errors->first('photos') }}</span>
									@endif
								</div>
	            			</div>

						</fieldset>

					</div>

					<div class="card-footer">
						<div class="heading-elements">
							<div class="checkbox pull-left  {{ $errors->has('terms') ? 'has-error' : '' }}" style="margin-top: -8px;">
								<label class="checkbox-inline text-grey-400">
									<input required="" checked type="checkbox" class="styled" name="terms">
									{{ Lang::get('create/ad.lang_i_have_confirm') }} <a href="{{ config('pages.terms') }}" target="_blank">{{ Lang::get('create/ad.lang_terms_of_service') }}</a>
								</label>
								@if ($errors->has('terms'))
								<span class="help-block">{{ $errors->first('terms') }}</span>
								@endif
							</div>

							<button type="submit" class="btn btn-primary heading-btn pull-right">{{ Lang::get('create/ad.lang_create_ad') }}</button>
						</div>
					</div>

				</div>

			</form>
		</div>

	</div>

	@if($ad->deliverable_to == 1)
		<script>
            option = "<option value='0'>All countries has been selected</option>";
            $('#country').empty().append(option).find('option:first')
                .attr("selected","selected");
		</script>
		@else
		<script>
			var data = '{!! $ad->getCountries() !!}';
			// for (dat in data){
			//     console.log(dat.text)
			// }
            var url = "{{ url('api/countries') }}";
            $.get(url,function (rsults) {

                for (var i =0 ;i < rsults.length; i++)
                {
                    dat = JSON.parse(data);

                        console.log('res is '+dat[0]).id;

                    var resp = rsults[i];
                    option = "<option value="+resp.id+">"+resp.name+"</option>";
                    $('#country').append(option);

                }
            });
            // {'data':JSON.parse(data),  tags: true}
            // $('#country').select2();
		</script>
		@endif
	<script type="text/javascript">
	function getCountries(value) {

	var option = '';


	if (value == 1)
	{
	option = "<option value='0'>All countries has been selected</option>";
	$('#country').empty().append(option).find('option:first')
	.attr("selected","selected");

	}
	else if (value == 2)
	{
	$('#country').empty();
        getAllCountries();
	}
	else
	{
	alert("select countries where your product can be delivered");
	}


	}

	function getAllCountries(){
        var url = "{{ url('api/countries') }}";
        $.get(url,function (rsults) {

            for (var i =0 ;i < rsults.length; i++)
            {
                var resp = rsults[i];
                option = "<option value="+resp.id+">"+resp.name+"</option>";
                $('#country').append(option);

            }
        });
	}


    </script>

</div>



@endsection