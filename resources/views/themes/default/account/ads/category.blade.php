@extends (Theme::get().'.layout.app')

@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('content')

    <!-- account ads -->
    <div class="row row-padding">

        <!-- Session Messages -->
        <div class="col-md-12">
            @if (Session::has('success'))
                <div class="alert bg-success alert-styled-left">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert bg-danger alert-styled-left">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    {{ Session::get('error') }}
                </div>
            @endif
        </div>

    @include (Theme::get().'.account.include.sidebar')

    <!-- Account Trashed Ads -->
        <div class="col-md-9">

            <div class="card">
                <div class="card-body " style="margin-bottom: 35px">
                <form action="{{ Protocol::home() }}/account/ads/category" method="POST" >
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                            <div class="form-group">
                                <label>{{ Lang::get('ads/show.lang_category') }}</label>
                                <input required="" class="form-control input-sm" placeholder="{{ Lang::get('ads/show.lang_category') }}" type="text" value="" name="category_name">

                            </div>
                            </div>

                        </div>
                    </div>
                    {!! csrf_field() !!}

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>{{ Lang::get('ads/show.lang_category_slug') }}</label>
                                    <input required="" class="form-control input-sm" placeholder="{{ Lang::get('ads/show.lang_category_slug') }}" type="text" value="" name="category_slug">

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary legitRipple">{{ Lang::get('ads/show.save_category') }}</button>
                    </div>

                </form>
            </div>
            </div>


            <div class="card">
                <div class="card-body" style="margin-bottom: 35px">
                    <form action="{{ Protocol::home() }}/account/ads/subcategory" method="POST"  >

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>{{ Lang::get('ads/show.lang_sub_category') }}</label>
                                        <input required="" class="form-control input-sm" placeholder="{{ Lang::get('ads/show.lang_sub_category') }}" type="text" value="" name="category_name">

                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>{{ Lang::get('create/store.lang_store_category') }}</label>
                                        <select required="" data-placeholder="{{ Lang::get('create/store.lang_store_category_select') }}" class="select" name="parent_category">
                                            @if(count(Helper::parent_categories()))
                                                @foreach (Helper::parent_categories() as $parent)
                                                    <option value="{{ $parent->id }}">{{ $parent->category_name }}</option>
                                                    @endforeach
                                            @endif
                                        </select>

                                    </div>
                                </div>

                            </div>
                        </div>
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>{{ Lang::get('ads/show.lang_category_slug') }}</label>
                                        <input required="" class="form-control input-sm" placeholder="{{ Lang::get('ads/show.lang_category_slug') }}" type="text" value="" name="category_slug">

                                    </div>
                                </div>

                            </div>
                        </div>

                        <input type="hidden" name="is_sub" value="1">

                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary legitRipple">{{ Lang::get('ads/show.save_subcategory') }}</button>
                        </div>

                    </form>
                </div>
            </div>

        </div>

    </div>




@endsection