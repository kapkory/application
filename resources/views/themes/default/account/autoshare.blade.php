@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('content')

<!-- AutoShare Settings -->
<div class="row">

	<!-- Session Messages -->
	<div class="col-md-12">
		@if (Session::has('success'))
		<div class="alert bg-success alert-styled-left">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
			{{ Session::get('success') }}
	    </div>
	    @endif
	    @if (Session::has('error'))
		<div class="alert bg-danger alert-styled-left">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
			{{ Session::get('error') }}
	    </div>
	    @endif
	</div>

	@include (Theme::get().'.account.include.sidebar')
	
	<!-- Auto Share Settings -->
	<div class="col-md-9">

		<div class="card">

			<div class="card-body">
				<form action="{{ Protocol::home() }}/account/autoshare" method="POST">

					{{ csrf_field() }}

					<div class="form-group">
						<div class="row">

							<!-- Auto share to Facebook -->
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('fb_active') ? 'has-error' : '' }}">
									<label>{{ Lang::get('account/autoshare.lang_facebook') }}</label>
									<select class="select-icons" name="fb_active">
										@if ($autoshare->fb_active)
										<option data-icon="facebook2" value="1">Active</option>
										<option data-icon="facebook2" value="0">Inactive</option>
										@else 
										<option data-icon="facebook2" value="0">Inactive</option>
										<option data-icon="facebook2" value="1">Active</option>
										@endif
									</select>
									@if ($errors->has('fb_active'))
									<span class="help-block">{{ $errors->first('fb_active') }}</span>
									@endif
								</div>
							</div>

							<!-- Auto share to Twitter -->
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('tw_active') ? 'has-error' : '' }}">
									<label>{{ Lang::get('account/autoshare.lang_twitter') }}</label>
									<select class="select-icons" name="tw_active">
										@if ($autoshare->tw_active)
										<option data-icon="twitter" value="1">Active</option>
										<option data-icon="twitter" value="0">Inactive</option>
										@else 
										<option data-icon="twitter" value="0">Inactive</option>
										<option data-icon="twitter" value="1">Active</option>
										@endif
									</select>
									@if ($errors->has('tw_active'))
									<span class="help-block">{{ $errors->first('tw_active') }}</span>
									@endif
								</div>
							</div>

						</div>
					</div>

					<div class="form-group">
						<div class="row">

							<!-- Facebook App ID -->
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('fb_app_id') ? 'has-error' : '' }}">
									<label>Facebook App ID</label>
									<input value="{{ $autoshare->fb_app_id }}" class="form-control" type="text" placeholder="Facebook App ID" name="fb_app_id">
									@if ($errors->has('fb_app_id'))
									<span class="help-block">{{ $errors->first('fb_app_id') }}</span>
									@endif
								</div>
							</div>

							<!-- Twitter Consumer Key -->
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('tw_consumer_key') ? 'has-error' : '' }}">
									<label>Twitter Consumer Key</label>
									<input value="{{ $autoshare->tw_consumer_key }}" class="form-control" type="text" placeholder="Twitter Consumer Key" name="tw_consumer_key">
									@if ($errors->has('tw_consumer_key'))
									<span class="help-block">{{ $errors->first('tw_consumer_key') }}</span>
									@endif
								</div>
							</div>

						</div>
					</div>

					<div class="form-group">
						<div class="row">

							<!-- Facebook App Secret -->
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('fb_app_secret') ? 'has-error' : '' }}">
									<label>Facebook App Secret</label>
									<input value="{{ $autoshare->fb_app_secret }}" class="form-control" type="text" placeholder="Facebook App Secret" name="fb_app_secret">
									@if ($errors->has('fb_app_secret'))
									<span class="help-block">{{ $errors->first('fb_app_secret') }}</span>
									@endif
								</div>
							</div>

							<!-- Twitter Consumer Secret -->
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('tw_consumer_secret') ? 'has-error' : '' }}">
									<label>Twitter Consumer Secret</label>
									<input value="{{ $autoshare->tw_consumer_secret }}" class="form-control" type="text" placeholder="Twitter Consumer Secret" name="tw_consumer_secret">
									@if ($errors->has('tw_consumer_secret'))
									<span class="help-block">{{ $errors->first('tw_consumer_secret') }}</span>
									@endif
								</div>
							</div>

						</div>
					</div>

					<div class="form-group">
						<div class="row">

							<!-- Facebook Token Access -->
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('fb_access_token') ? 'has-error' : '' }}">
									<label>Facebook Token Access</label>
									<input value="{{ $autoshare->fb_access_token }}" class="form-control" type="text" placeholder="Facebook Token Access" name="fb_access_token">
									@if ($errors->has('fb_access_token'))
									<span class="help-block">{{ $errors->first('fb_access_token') }}</span>
									@endif
								</div>
							</div>

							<!-- Twitter Token Access Key -->
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('tw_access_token') ? 'has-error' : '' }}">
									<label>Twitter Token Access Key</label>
									<input value="{{ $autoshare->tw_access_token }}" class="form-control" type="text" placeholder="Twitter Token Access Key" name="tw_access_token">
									@if ($errors->has('tw_access_token'))
									<span class="help-block">{{ $errors->first('tw_access_token') }}</span>
									@endif
								</div>
							</div>

						</div>
					</div>

					<div class="form-group">
						<div class="row">

							
							<div class="col-md-6">
							</div>

							<!-- Twitter Token Access Secret -->
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('tw_access_token_secret') ? 'has-error' : '' }}">
									<label>Twitter Token Access Secret</label>
									<input value="{{ $autoshare->tw_access_token_secret }}" class="form-control" type="text" placeholder="Twitter Token Access Secret" name="tw_access_token_secret">
									@if ($errors->has('tw_access_token_secret'))
									<span class="help-block">{{ $errors->first('tw_access_token_secret') }}</span>
									@endif
								</div>
							</div>

						</div>
					</div>

					<button type="submit" style="width: 100%" class="btn btn-primary">{{ Lang::get('account/store/settings.lang_save_changes') }}</button>

				</form>
			</div>

		</div>

	</div>

</div>

@endsection