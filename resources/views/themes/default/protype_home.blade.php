@extends (Theme::get().'.layout.app')

@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('styles')

    <link type="text/css" rel="stylesheet"
          href="{{ Protocol::home() }}/content/assets/front-end/home/css/style_3.14.css">
    <!--<link type="text/css" rel="stylesheet" href="assets/css/style.css">-->
    <link type="text/css" rel="stylesheet"
          href="{{ Protocol::home() }}/content/assets/front-end/home/css/style_custom.css">
    <!--<script rel="preload" src="assets/js/jquery.min.js"></script>-->
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
    <!--<script rel="preload" src="assets/js/new_home_js_2.39_min.js" type="text/javascript"></script>-->
    <script async='async' src='{{Protocol::home()}}/content/assets/front-end/home/js/gpt.js'></script>
    <!--<script async='async' src='assets/optimize_40.js'></script>-->
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">--}}
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>--}}

    <style type="text/css">
        #storageFrame {
            display: none

        }

        /* Make the image fully responsive */
        .carousel-inner img {
            width: 100%;
            height: 100%;
        }

        a:hover {
            color: #0056b3;
            text-decoration: none;
        }
    </style>
@endsection

@section ('javascript')

    <script type='text/javascript'
            src='{{ Protocol::home() }}/content/assets/front-end/home/js/imlogin-v270.js'></script>
    {{--<script type='text/javascript' src='{{ Protocol::home() }}/content/assets/front-end/home/js/jq-ac-ui-v330.js'--}}
    {{--crossorigin="anonymous"></script>--}}

@endsection

@section ('pageHeader')
    <!-- Advance Search -->


@endsection

@section ('content')



    <section id="menu-bar">
        <div class="row row-eq-height menu-bar-content" style="flex-wrap: nowrap; margin-left: 15px">
            <div class="width16 left menu columns" style="display: block; opacity: 1;">
                <div class="demo-container clear">
                    <div class="dcjq-vertical-mega-menu">
                        <ul id="mega-1" class="menu right" style="opacity: 1;">
                            <li id="menu-item-0"><a class="dc-mega">Browse By Industries </a></li>
                            @foreach (Helper::parent_categories() as $parent_category)
                                <li id="menu-item-1" class="dc-mega-li"><a
                                            href="{{ url('supplier-manufacturer/'.$parent_category->category_slug) }}"
                                            class="dc-mega1"> <span
                                                class="fttl">{{ $parent_category->category_name }}</span><span
                                                class="dc-mega-icon"></span></a>

                                </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
            <div class="width80 float-right" style="opacity: 1;">
                <div class="maxwidth banner" style="display: block; visibility: visible;">
                    <div class="bx-wrapper" style="max-width: 100%;">
                        <div class="bx-viewport" aria-live="polite"
                             style="width: 100%; overflow: hidden; position: relative; height: auto;">
                            <div id="demo" class="carousel slide" data-ride="carousel">

                                <!-- Indicators -->
                                <ul class="carousel-indicators">
                                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                                    <li data-target="#demo" data-slide-to="1"></li>
                                    <li data-target="#demo" data-slide-to="2"></li>
                                </ul>

                                <!-- The slideshow -->
                                <div class="carousel-inner">
                                    <div class="carousel-item">
                                        <img src="{{ Protocol::home() }}/content/assets/front-end/images/backgrounds/banner.jpg"
                                             alt="banner" width="1100" >
                                    </div>
                                    <div class="carousel-item active">
                                        <img src="{{ Protocol::home() }}/content/assets/front-end/images/backgrounds/background-store.jpg"
                                             alt="stores"  width="1100" >
                                    </div>

                                    <div class="carousel-item">
                                        <img src="{{ Protocol::home() }}/content/assets/front-end/images/backgrounds/pricing-bg.jpg"
                                             alt="pricing"  width="1100" >
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="bx-controls bx-has-controls-direction">
                            <div class="bx-controls-direction">
                                <a class="bx-prev" href="#demo" data-slide="prev">Prev</a>
                                <a class="bx-next" href="#demo" data-slide="next">Next</a>
                            </div>
                        </div>
                    </div>
                    <div id="bx-pager">
                        <a data-slide-to="0" href="#" data-target="#demo" class="active">Payment Protection</a>
                        <a data-slide-to="1" href="#" data-target="#demo" class="slider-post-req">Post Your
                            Requirement</a>
                        <a data-slide-to="2" href="#" data-target="#demo" class="">Trending Now</a>
                        <a data-slide-to="0" href="#" data-target="#demo" class="">Top Brands</a>
                    </div>
                    <!--maps for the sliders-->
                    <!--write code here-->
                </div>
            </div>
        </div>

        <div class="recent-wrap">
            <div id="recom_widget5"></div>
        </div>
        <div class="maxwidth space">
            <div class="tabs-content left">
                <div class="content">
                    <div class="city-link"><h3>Browse Categories</h3>

                        <ul>
                            @foreach (Helper::parent_categories() as $parent_category)
                                <li class="ct1"><a
                                            href="{{ url('supplier-manufacturer/'.$parent_category->category_slug) }}">
                                    <span class="img">
                                         <img src="{{ $parent_category->icon }}"
                                              alt="{{ ucwords($parent_category->category_name).' Manufacturer and Supplier' }}">
                                    </span>
                                        <span class="txt cat_title">{{ $parent_category->category_name }}</span>
                                        {{--<span class="txt">Delhi</span>--}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>


    @if (count($stores) > 0)
        <section id="products">
            <div class="row" style="margin-left: 15px">
                <div class="large-12 text-center">
                    <h3>Our Stores</h3>
                    <div class="bx-wrapper" style="max-width: 1300px;">
                        <div class="bx-viewport" aria-live="polite"
                             style="width: 100%; overflow: hidden; position: relative; height: 220px;">
                            <div class="content-bottom-in">
                                {{--latest stores--}}

                                <ul id="flexiselDemo1">
                                    @foreach($stores as $store)
                                        <li>
                                            <div class="team-grid text-center">
                                                <div class="team-img">
                                                    <img class="img-fluid rounded"
                                                         src="{{ $store->logo }}"
                                                         alt="">
                                                </div>
                                                <a href="{{ Protocol::home() }}/store/{{ $store->username }}">
                                                    <div class="team-info">
                                                        <h4><a style="color: #fff;"
                                                               href="{{ Protocol::home() }}/store/{{ $store->username }}">{{ $store->title }}</a>
                                                        </h4>
                                                        <span><a style="color: #fff;"
                                                                 class="btn btn-outline-primary btn-xs"
                                                                 href="{{ Protocol::home() }}/store/{{ $store->username }}">View Store</a> </span>
                                                        <ul class="d-flex justify-content-center py-3 social-icons">
                                                            <li>
                                                                <a href="#">
                                                                    <!--<i class="fab fa-facebook-f"></i>-->
                                                                </a>
                                                            </li>
                                                            <li class="mx-3">
                                                                <a href="#">
                                                                    <!--<i class="fab fa-twitter"></i>-->
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <!--<i class="fab fa-google-plus-g"></i>-->
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </a>

                                            </div>
                                        </li>
                                    @endforeach
                                </ul>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </section>
    @endif

    <div class="payx-banner" style="display: none;">
        <div class="payx row"><a href="#" class="payx-buyer-bner"

            ></a></div>
    </div>
    <section>
        <div class="row-bg-img">

            <!-- Section Title -->
            <div class="spec flat-row-title center ">
                <h2 style="font-family: 'Open Sans', sans-serif;">{{ Lang::get('home.lang_latest_ads') }}</h2>
                {{--<div class="large-12 text-center columns"><h3>Trusted By Largest Of Businesses</h3></div>--}}
                {{--<p>Search and Find what--}}
                {{--you are looking for. Best spots are here for you--}}
                {{--</p>--}}
            </div>

            <div class=" flat-row row">
                @if (count($latest_ads))
                    @foreach ($latest_ads as $ad)
                        <div class="col-md-3">
                            <div class="card card-blog">
                                <ul class="tags">
                                    @if ($ad->is_featured)
                                        <li>{{ Lang::get('home.lang_featured') }}</li>
                                    @endif
                                    @if ($ad->is_oos)
                                        <li class="oos">{{ Lang::get('update_three.lang_out_of_stock') }}</li>
                                    @endif
                                </ul>
                                <div class="card-image">
                                    <a href="{{ Protocol::home() }}/product/{{ $ad->slug }}" {{ !is_null($ad->affiliate_link) ? 'target="_blank"' : '' }}>
                                        <div class="img card-ad-cover lozad"
                                             data-background-image="{{ EverestCloud::getThumnail($ad->ad_id, $ad->images_host) }}"
                                             title="{{ $ad->title }}"
                                             style="background-image: url({{ EverestCloud::getThumnail($ad->ad_id, $ad->images_host) }});"
                                             data-loaded="true"></div>
                                    </a>
                                </div>
                                <div class="card-block">
                                    <h6 class="card-title"
                                        style="line-height: 1.25em;margin-top: 0; padding: 10px; font-size: 15px">
                                        <a href="{{ Protocol::home() }}/product/{{ $ad->slug }}">{{ $ad->title }}</a>
                                    </h6>
                                    <p style="color: rgba(51,51,51,.7);padding: 10px;"> {!!str_limit(strip_tags($ad->description),20)!!}</p>
                                    <div class="card-footer" style="padding-bottom: 10px;">
                                        <div id="price">
                                            @if (!is_null($ad->regular_price))
                                                <span class="price price-old pull-left"> {{ number_format($ad->regular_price, 2) }} {{ $ad->currency }}</span>
                                            @endif
                                            <span class="price price-new"> {{ number_format($ad->price, 2) }} {{ $ad->currency }}</span>
                                        </div>
                                        {{--undefined variable stores--}}
                                        <div class="author">
                                            <div class="card__avatar">
                                                <a
                                                        href="{{ Protocol::home() }}/store/{{ $store->username }}"
                                                        class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img
                                                            data-src="{{ $store->logo }}"
                                                            alt="Island" class="avatar lozad"
                                                            src="{{ $store->logo }}"
                                                            data-loaded="true" width="40" height="40"><i
                                                            class="icon-checkmark3" data-popup="tooltip"
                                                            data-placement="top" data-container="body"
                                                            title="Shop at {{ $store->logo }}"
                                                            data-original-title="{{ $store->username }}"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    @endforeach
                @endif


            </div>

            <!-- Browse All -->
            <div class="btn-morphing"><a style="color: white" href="{{ Protocol::home() }}/browse"
                                         class="btn btn-default btn-round btn-toggle legitRipple">{{ Lang::get('home.lang_see_more') }}</a>
            </div>

        </div>
    </section>
    <div class="snd-top">
        <div class="top-block"><img class="bg nr top-img"
                                    src="{{ Protocol::home() }}/content/assets/front-end/home/img/z.gif" border="0"
                                    alt="top    ">
            <div class="top-txt">Top</div>
        </div>
    </div>
    <div id="inactivity-form"></div><!--for download app starts-->
    <div>  <!-- Footer Start Here::-->
        <footer id="new-footer"></footer><!-- Footer End Here::--> </div>
    <!-- flexisel (for special offers) -->
    <script src="{{ Protocol::home() }}/content/assets/front-end/home/js/jquery.flexisel.js"></script>
    <script>
        $(function () {
            $("#flexiselDemo1").flexisel({
                visibleItems: 4,
                animationSpeed: 1000,
                autoPlay: true,
                autoPlaySpeed: 3000,
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: {
                    portrait: {
                        changePoint: 480,
                        visibleItems: 1
                    },
                    landscape: {
                        changePoint: 640,
                        visibleItems: 2
                    },
                    tablet: {
                        changePoint: 768,
                        visibleItems: 2
                    }
                }
            });
        })
    </script>
@endsection