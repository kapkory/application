@extends (Theme::get().'.layout.app')

@section('styles')
    <link rel="stylesheet" href="{{ url('content/assets/front-end/css/article-styles.css') }}">

    {{--<link class="rs-file" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/royalslider.css?v=1.2" rel="stylesheet">--}}
    {{--<link href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/skins/default/rs-default.css?v=1.2" rel="stylesheet">--}}
    {{--<link href="{{ Protocol::home() }}/content/assets/front-end/css/extras/starability-growRotate.css" rel="stylesheet">--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/emojionearea/emojionearea.css" media="screen">--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/products.css" >--}}

    {{--<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/mervick-tommorow.css" media="screen">--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/article.css" >--}}

    <!--    <link rel="stylesheet" type="text/css" href="http://mervick.github.io/lib/google-code-prettify/skins/tomorrow.css" media="screen">-->

    <style type="text/css">

        .penci-single-link-pages {
            display: block;
            width: 100%;
        }

        *, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td {
            margin: 0;
            padding: 0;
        }
        .post-tags {
            display: block;
            position: relative;
            z-index: 10;
            color: #888;
            margin-bottom: 0;
            line-height: 1.4;
            margin-top: 31px;
        }
        .post-tags a {
            text-transform: uppercase;
            color: #888;
            padding: 6px 12px 5px;
            margin-right: 8px;
            margin-bottom: 8px;
            display: inline-block;
            font-size: 11px !important;
            background: none;
            border: 1px solid #DEDEDE;
            transition: all 0.3s;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            outline: none;
            font-weight: normal;
            line-height: 1.2;
        }
        .post-entry a, .container-single .post-entry a {
            color: #e91e63;
        }

        .container-single .penci-standard-cat .cat > a.penci-cat-name {

            color: #e91e63;

        }
        .penci-standard-cat .cat > a.penci-cat-name {

            color: #e91e63;

        }
        .cat > a.penci-cat-name:last-child {

            margin-right: 0;
            padding: 0;

        }
        .cat > a.penci-cat-name {

            font-size: 13px;
            color: #6eb48c;
            line-height: 1.2;
            margin: 0 18px 0 0;
            margin-right: 18px;
            margin-bottom: 0px;
            padding-right: 10px;
            display: inline-block;
            vertical-align: top;
            background: none;
            transition: all 0.3s;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            font-weight: normal;
            margin-bottom: 5px;
            position: relative;
            text-decoration: none;

        }

        .cat{
            float: left;
        }
        a.wpdropbox-button {
            background: #2b92c2 !important;
            color: #ffffff !important;
            padding: 22px 50px !important;
            font-size: 30px !important;
            border: 2px solid #000000 !important;
            -webkit-border-radius: 5px !important;
            border-radius: 5px !important;
            display: inline-block;
            text-decoration: none;
            line-height: 1 !important;
        }
        .related-posts {
            margin-bottom: 40px;
        }
        article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
            display: block;
        }
        section{
            margin: 0;
            margin-bottom: 0px;
            padding: 0;
            border: 0;
            vertical-align: baseline;
            font-size: 100%;
            font-family: inherit;
        }

        .section-head {
            background: #fbfbfb;
            border-color: #eee;
            border-bottom-width: 2px;
            font-family: Roboto,Helvetica,sans-serif;
            font-size: 14px;
            letter-spacing: .02em;
            line-height: 36px;
            -webkit-font-smoothing: antialiased;
        }
        .related-posts ul {
            overflow: hidden;
        }
        .related-posts, .highlights-box.related-posts {
            margin-bottom: 28px;
        }
        .related-posts .highlights-box {
            margin-left: -10px;
            margin-right: -10px;
        }
        .related-posts .highlights-box .column.one-third {
            padding: 0 10px;
        }
        .col-4, .column.one-third {
            width: 33.333333333333336%;
        }
        .related-posts li {
            float: left;
        }
        .highlights {
            position: relative;
        }
        article{
            margin: 0;
            margin-bottom: 0px;
            padding: 0;
            border: 0;
            vertical-align: baseline;
            font-size: 100%;
            font-family: inherit;
        }
        .related-posts .highlights article {
            margin-bottom: 0;
        }
        .related-posts article {
            position: relative;
        }
        .highlights .image-link {
            min-height: 42px;
            display: block;
            position: relative;
        }
        a{
            margin: 0;
            padding: 0;
            border: 0;
            vertical-align: baseline;
            font-size: 100%;
            font-family: inherit;
        }
        ol, ul {
            list-style: none;
        }
        .related-posts .highlights h2 {
            padding-left: 0;
            margin-top: 12px;
        }
        .highlights h2 {
            color: #19232d;
            font-size: 14px;
            line-height: 1.3;
            padding-left: 14px;
            margin: 7px 0;
            margin-top: 7px;
            margin-bottom: 7px;
        }
        h2{
            font-weight: normal;
        }

        .related-posts .highlights h2 a {
            font-size: 15px;
            font-weight: 500;
            line-height: 1.33;
        }
        a {
            color: #161616;
            -webkit-transition: all .25s ease-in-out;
            transition: all .25s ease-in-out;
        }
        .highlights .image-link img {
            display: block;
            width: 100%;
            height: auto;
        }
        .appear {
            opacity: 1;
            -webkit-transition: all .4s ease-in-out;
            transition: all .4s ease-in-out;
        }
        .wp-post-image {
            -moz-transform: translate3d(0,0,0);
        }
        a img {
            border: 0;
        }

    </style>
@endsection

@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('content')

    <div class="container container-single penci_sidebar right-sidebar penci-enable-lightbox">
        <div id="main">
            <div class="theiaStickySidebar">
                <article id="post-834" class="post-834 post type-post status-publish format-gallery has-post-thumbnail hentry category-cardio category-featured tag-blog tag-cardio tag-fitness post_format-post-format-gallery">



                    <div class="header-standard header-classic single-header">
                        <div class="penci-standard-cat">
                            <span class="cat">
                                    <a class="penci-cat-name" href="{{ url('catalogue/country/'.str_replace(' ','-',$catalogue->country->name)) }}">{{ strtoupper($catalogue->country->name) }}</a>

                                {{--<a class="penci-cat-name" href="http://soledad.pencidesign.com/soledad-fitness-blog/category/featured/">Featured</a>--}}
                            </span>
                        </div>

                        <h1 class="post-title single-post-title">{{ $catalogue->title }}</h1>

                        <div class="post-box-meta-single">
                            {{--<span class="author-post"><span>written by <a class="author-url" href="#">{{ Profile::full_name_by_username($catalogue->user->username) }}</a></span></span>--}}
                            {{--<span>July 9, 2017</span>--}}
                        </div>
                    </div>





                    <div class="post-image">
                        <div class="penci-owl-carousel penci-owl-carousel-slider penci-nav-visible owl-loaded owl-drag" data-auto="true" data-lazy="true">


                        </div>
                    </div>





                    <div class="post-entry blockquote-style-2">
                        <div class="inner-post-entry">
                            <?php
                            $code = '<style> .blog_responsive_1 { width: 300px; height: 250px; } @media(min-width: 500px) { .blog_responsive_1 { width: 336px; height: 280px; } } @media(min-width: 800px) { .blog_responsive_1 { width: 728px; height: 90px; } } </style> <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> <!-- Responsive ad for sidebar --> <ins class="adsbygoogle blog_responsive_1" style="display:inline-block" data-ad-client="ca-pub-6803454939055727" data-ad-slot="1550184116"></ins> <script> (adsbygoogle = window.adsbygoogle || []).push({}); </script>';
                            $code1 = '<style>.adsense-ad-inside-text-left, .adsense-ad-inside-text-right { margin: 10px }.adsense-ad-inside-text-left p, .adsense-ad-inside-text-right p { text-align: center;}.adsense-ad-inside-text-right { float: right }.adsense-ad-inside-text-left  { float: left  }</style><div class="adsense-ad-inside-text-left"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- 336 280 size ad --><ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-6803454939055727" data-ad-slot="5049278750"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script></div>';
                            $code2 = '<style>.adsense-ad-inside-text-left, .adsense-ad-inside-text-right { margin: 10px }.adsense-ad-inside-text-left p, .adsense-ad-inside-text-right p { text-align: center;}.adsense-ad-inside-text-right { float: right }.adsense-ad-inside-text-left  { float: left  }</style><div class="adsense-ad-inside-text-right"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- 336 280 size ad --><ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-6803454939055727" data-ad-slot="5049278750"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script></div>';
                            $code3 = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- responsive link ads --><ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-6803454939055727" data-ad-slot="3448422476" data-ad-format="link" data-full-width-responsive="true"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>';

                            ?>
                            {{--{!! str_replace('[adsense_code]',$code,$catalogue->content) !!}--}}
                                {{--{!! str_replace(--}}
                               {{--['[adsense_code]',"[product_id","[store_id","]"],--}}
                               {{--[$code,"<div class='products' data-product-id","<div class='stores' data-store-id","><div>"],--}}
                               {{--$catalogue->content) !!}--}}

                                {!! str_replace(
                                    ['[adsense_code]','[adsense_code1]','[adsense_code2]','[adsense_code3]',"[product_id","[store_id","]]"],
                                    [$code,$code1,$code2,$code3,"<div class='products' data-product-id","<div class='stores' data-store-id","><div>"],
                                    $catalogue->content) !!}


                            <div class="penci-single-link-pages">
                            </div>


                            <div class="post-tags">
                             <a href="{{ $catalogue->document_url }}" class="wpdropbox-button"><h3 style="color: white">Download</h3></a>
                            </div>

                        </div>
                    </div>




                </article>


                <section class="related-posts">
                    <h3 class="section-head"><span class="color">Related</span> Posts</h3>
                    <ul class="highlights-box three-col related-posts">
                        @if($catalogues)
                            @foreach($catalogues as $r_catalogue)
                        <li class="highlights column one-third">
                            <article>
                                <a href="{{ url('catalogue/download/'.$r_catalogue->slug) }}" title="{{ $r_catalogue->title }}" class="image-link">
                                    <img src="{{ url($r_catalogue->image_url) }}" class="image wp-post-image no-display appear" alt="{{ $r_catalogue->title }}" title="{{ $r_catalogue->title }}"  sizes="(max-width: 214px) 100vw, 214px" width="214" height="140">
                                </a>
                                <h2>
                                    <a href="{{ url('catalogue/download/'.$r_catalogue->slug) }}" title="{{ $r_catalogue->title }}">{{ $r_catalogue->title }}</a>
                                </h2>
                                {{--<div class="cf listing-meta meta below">--}}
                                    {{--<time datetime="{{ url('catalogue/download/'.$r_catalogue->created_at) }}" class="meta-item">$r_catalogue->created_at</time>--}}
                                {{--</div>--}}
                            </article>
                        </li>
                            @endforeach
                        @endif

                    </ul>
                </section>

            </div>
        </div>

        <div id="sidebar" class="penci-sidebar-content style-9 pcalign-center">
            <div class="theiaStickySidebar">
                {{----}}
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-format="autorelaxed"
                     data-ad-client="ca-pub-6803454939055727"
                     data-ad-slot="9186404046"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>

            <style>
                .sidebar_responsive_1 { width: 300px; height: 250px; }
                @media(min-width: 500px) { .sidebar_responsive_1 { width: 336px; height: 280px; } }
                @media(min-width: 800px) { .sidebar_responsive_1 { width: 300px; height: 600px; } }
            </style>
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Responsive ad for sidebar -->
            <ins class="adsbygoogle sidebar_responsive_1"
                 style="display:inline-block"
                 data-ad-client="ca-pub-6803454939055727"
                 data-ad-slot="1550184116"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
        <!-- END CONTAINER -->
    </div>

    <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "DataCatalog",
  "headline": "{{ $catalogue->title }}",
  "mainEntityOfPage": "{{ url()->current() }}",
  "datePublished": "{{ $catalogue->created_at }}",
  "dateModified": "{{ Carbon\Carbon::now() }}",
  "description": "{{ $catalogue->meta_description }}",
 "articleBody": "{{ $catalogue->content }}",
  "image": {
    "@type": "ImageObject",
    "url": " {{ ($catalogues) ? $catalogues[0]->image_url: '' }}"
  },
  "author": "{{ $catalogue->user->first_name }}",
  "publisher": {
    "@type": "Organization",
    "logo": {
      "@type": "ImageObject",
      "url": "{{ url('uploads/settings/logo/logo.png') }}"
    },
    "name": "SanitaryWare"
  }

 }
</script>

{{--Product ads--}}
    <script>
        $(document).ready(function () {
            if($("#root").find('.products')){
                var products= $('.products').data('product-id');

                var url = "{{ url('/') }}"+'/api/product-id/'+products;
                console.log('url is '+url+' products is '+products);
                $.get(url,function (response) {
                    // console.log(response);
                    var ul = '<div class="col-xs-12"><ul class="ace-thumbnails clearfix">';
                    for (var i = 0 ; i < response.length; i++){
                        var resp = response[i];

                        ul += '<li>' +
                            '<a href="{{ url('product') }}'+"/"+resp.slug+'" data-rel="colorbox" class="cboxElement"> '+
                                {{--'<img width="150" height="150" alt="150x150" src="{{ url('uploads/images') }}'+'/617669297/thumbnails/thumbnail_0.jpg'+'">'+--}}
                                    '<img width="150" height="150" alt="150x150" src="{{ url('uploads/images') }}'+'/'+resp.ad_id+'/thumbnails/thumbnail_0.jpg'+'">'+
                            '<div class="text">' +
                            '<div class="inner">'+resp.title+'</div>'+
                            '</div>'+
                            '</a>'+
                            '</li>'
                    }
                    ul += '</ul></div>';
                    $('.products').prepend(ul);
                });

            }
            else
            {
                console.log('siko');
            }
        });
    </script>

    {{--Store ads--}}

    <script>
        $(document).ready(function () {
            if($("#root").find('.stores')){
                var stores= $('.stores').data('store-id');

                var url = "{{ url('/') }}"+'/api/store-id/'+stores;
                // console.log('url is '+url+' products is '+products);
                $.get(url,function (response) {
                    // console.log(response);
                    var ul = '<div class="col-xs-12"><ul class="ace-thumbnails clearfix">';
                    for (var i = 0 ; i < response.length; i++){
                        var resp = response[i];

                        ul += '<li>' +
                            '<a href="{{ url('store') }}'+"/"+resp.username+'" data-rel="colorbox" class="cboxElement"> '+
                                {{--'<img width="150" height="150" alt="150x150" src="{{ url('uploads/images') }}'+'/617669297/thumbnails/thumbnail_0.jpg'+'">'+--}}
                                    '<img width="150" height="150" alt="150x150" src="'+resp.logo+'">'+
                            '<div class="text">' +
                            '<div class="inner">'+resp.title+'</div>'+
                            '</div>'+
                            '</a>'+
                            '</li>'
                    }
                    ul += '</ul></div>';
                    $('.stores').prepend(ul);
                });

            }
            else
            {
                console.log('siko');
            }
        });
    </script>
@endsection