<?php
$data = 'data';
?>
@extends (Theme::get().'.layout.app')

@section('styles')
    <style type="text/css">

        .footer{background-color:#FFF;display:block;-webkit-box-flex:0;-webkit-flex:none;flex:none}.footer-pages{padding:25px 6% 25px 6%}.footer-pages h4{display:block;color:#656565;margin-bottom:10px;font-weight:500;font-family:Fira Sans;font-size:14px;text-transform:uppercase;letter-spacing:1px}.footer-pages .page-item{width:100%}.footer-pages .page-item a{display:block;color:#9b9b9b;margin-bottom:5px;font-family:Roboto;font-size:12px;transition:all 0.5s;text-transform:uppercase}.footer-pages .page-item a:hover{text-decoration:underline}.footer:not(.navbar-fixed-bottom){z-index:1000}.footer.navbar{left:0;right:0;bottom:0}body[class*=navbar-bottom] .footer:not(.navbar){display:none}.footer-boxed{left:0;right:0;padding-left:20px;padding-right:20px}

        .footer-pages {
            padding: 25px 6%;
            background: #2c3e50 !important;
            clear: both;
        }
        .footer-pages h4 {
            display: block;
            color: #fff;
            margin-bottom: 10px;
            font-weight: 500;
            font-family: Fira Sans;
            font-size: 14px;
            text-transform: uppercase;
            letter-spacing: 1px;
        }
        .footer-pages .page-item a:hover {
            text-decoration: underline;
        }
        .footer-pages .page-item a {
            display: block;
            color: #fff;
            margin-bottom: 5px;
            font-family: Roboto;
            font-size: 12px;
            transition: all .5s;
            text-transform: uppercase;
        }
        a {
            background-color: transparent;
        }

    </style>

    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/responsive.css" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/catalogue.css" media="screen">

@endsection

@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('content')
    <div class="main wrap">
        <div class="form-group form-search">

           <form method="get" action="{{ url('search/catalogue') }}">
               <input name="catalogue" class="form-control form-control-search" style="color:white;" placeholder="Search catalogue by country or title" autocomplete="off" type="text">
             {!! csrf_field() !!}
               <button type="submit" class="btn btn-default btn-round btn-submit legitRipple">
                   <i class="feather-search icon-2x"></i>
               </button>
           </form>

        </div>
    </div>
    <div class="row row-padding">
        @if($meta_title)
        <div class="col-md-12 col-sm-12">
            <div class="jumbotron" style="text-align: center;font-size: 14px">

                    <h1>{{ $meta_title  }}</h1>
                    <h3>{{ $meta_description  }}</h3>


            </div>
        </div>
        @endif

<div class="col-sm-12 " style="padding: 50px">
    {{--<div class="single-container bs-vc-content">--}}
    <div class="row vc_row wpb_row vc_row-fluid">
        <div class="bs-vc-wrapper">
            <div class="wpb_column bs-vc-column vc_column_container vc_col-sm-12">
                <div class="bs-vc-wrapper wpb_wrapper">
                    @if($catalogues)
                    <div class="bscb-58395 bs-listing bs-listing-listing-grid-1 bs-listing-single-tab pagination-animate"><h3 class="section-heading sh-t1 sh-s1 main-term-none"> <span class="h-text main-term-none main-link"> <h1>Catalogues</h1> </span></h3><style>.bscb-58395.bscb-58395 .section-heading.sh-t1 a:hover .h-text,.bscb-58395.bscb-58395 .section-heading.sh-t1 a.active .h-text,.bscb-58395.bscb-58395 .section-heading.sh-t1 > .h-text,.bscb-58395.bscb-58395 .section-heading.sh-t1 .main-link:first-child:last-child .h-text{color:#000}.bscb-58395.bscb-58395 .section-heading.sh-t1.sh-s5 > .main-link > .h-text:after,.bscb-58395.bscb-58395 .section-heading.sh-t1.sh-s5 > a:first-child:last-child > .h-text:after,.bscb-58395.bscb-58395 .section-heading.sh-t1.sh-s5>.h-text:first-child:last-child:after{color:#000 !important}.bscb-58395.bscb-58395.bscb-58395 .section-heading.sh-t1:after{background-color:#000}.bscb-58395.bscb-58395 .section-heading.sh-t1.sh-s8 .main-link .h-text:before,.bscb-58395.bscb-58395 .section-heading.sh-t1.sh-s8 .main-link.h-text:before,.bscb-58395.bscb-58395 .section-heading.sh-t1.sh-s8>.h-text:before{border-right-color:#000 !important}</style>
                        <div class="bs-pagination-wrapper main-term-none more_btn bs-slider-first-item">
                            <div class="listing listing-grid listing-grid-1 clearfix columns-3">
                                {{--<article class="post-238 type-post format-video has-post-thumbnail   listing-item listing-item-grid listing-item-grid-1 main-term-8"><div class="item-inner"><div class="featured clearfix"><div class="term-badges floated"><span class="term-badge term-8"><a href="http://demo.betterstudio.com/publisher/travel-guides/category/videos/">Videos</a></span></div> <a title="Everything You Need to Know to See the Northern Lights in Norway" data-bs-srcset="{&quot;baseurl&quot;:&quot;http:\/\/cdn.betterstudio.com\/publisher\/\/sites\/44\/2017\/07\/&quot;,&quot;sizes&quot;:{&quot;210&quot;:&quot;Travel-Guides-10-210x136.jpg&quot;,&quot;279&quot;:&quot;Travel-Guides-10-279x220.jpg&quot;,&quot;357&quot;:&quot;Travel-Guides-10-357x210.jpg&quot;,&quot;750&quot;:&quot;Travel-Guides-10-750x430.jpg&quot;,&quot;1200&quot;:&quot;Travel-Guides-10.jpg&quot;}}" class="img-holder    b-loaded" href="http://demo.betterstudio.com/publisher/travel-guides/how-to-find-cheap-apartments-in-playa-del-carmen/" style="background-image: url(&quot;http://cdn.betterstudio.com/publisher//sites/44/2017/07/Travel-Guides-10-750x430.jpg&quot;);"></a><span class="format-icon format-video"><i class="fa fa-play"></i></span></div><h2 class="title"> <a href="http://demo.betterstudio.com/publisher/travel-guides/how-to-find-cheap-apartments-in-playa-del-carmen/" class="post-title post-url"> Everything You Need to Know to See the Northern Lights in Norway </a></h2><div class="post-meta"><span class="time"><time class="post-published updated" datetime="2017-11-26T11:12:22+00:00">November 26, 2017</time></span> </div></div> </article>--}}


                                @foreach($catalogues as $catalogue)
                                <article class="post-{{ $catalogue->id }} type-post format-standard has-post-thumbnail   listing-item listing-item-grid listing-item-grid-1 main-term-12" itemtype="http://schema.org/Catalogue">
                                    <div class="item-inner">
                                        <div class="featured clearfix">
                                            <div class="term-badges floated">
                                                <span class="term-badge term-12"><a href="{{ url('catalogue/country/'.str_replace(' ','-',$catalogue->country->name)) }}">{{ $catalogue->country->name }}</a></span>
                                            </div>
                                            <a title="{{ $catalogue->title.$loop->iteration }}" data-bs-srcset="{{ url('catalogue/download/'.$catalogue->slug) }}" class="img-holder    b-loaded" href="{{ url('catalogue/download/'.$catalogue->slug) }}" style="background-image: url({{ url($catalogue->image_url) }});"></a>
                                        </div>
                                        <h2 class="title">
                                            <a href="{{ url('catalogue/download/'.$catalogue->slug) }}" class="post-title post-url"> {{ $catalogue->title }} </a>
                                        </h2>

                                    </div>
                                </article>

                                    @if($loop->iteration % 4 == 0)
                                        <article class="type-post format-standard has-post-thumbnail   listing-item listing-item-grid listing-item-grid-1 main-term-12">
                                            {{--<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- responsive link ads --><ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-6803454939055727" data-ad-slot="3448422476" data-ad-format="link" data-full-width-responsive="true"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>--}}
                                            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> <ins class="adsbygoogle" style="display:block" data-ad-format="fluid" data-ad-layout-key="-70+d4-34-4m+tt" data-ad-client="ca-pub-6803454939055727" data-ad-slot="8351553304"></ins> <script> (adsbygoogle = window.adsbygoogle || []).push({}); </script>

                                        </article>
                                    @endif
                                    @endforeach



                            </div>
                        </div>
                        <div class="bs-pagination bs-ajax-pagination more_btn main-term-none clearfix">
                            {!! $catalogues->links() !!}

                        </div>
                    </div>
                        @else
                        We currently do not have the searched catalogue
                    @endif
                </div>
            </div>
        </div>
    </div>
    {{--</div>--}}

</div>
    </div>
    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "{{ url('/') }}",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "{{ url('search/catalogue') }}?catalogue={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>
@endsection