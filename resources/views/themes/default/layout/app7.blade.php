<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
    <link rel="shortcut icon" href="{{ Protocol::home() }}/uploads/settings/favicon/favicon.png">

@yield('seo')

<!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900|Droid+Sans|Source+Sans+Pro|Open+Sans:300,400,700|Lato|Rubik|Fira+Sans:200,300,400" rel="stylesheet" type="text/css">
{{--<script type="text/javascript"--}}
{{--src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>--}}
{{--<script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>--}}
{{--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>--}}



    <!-- Icon Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--<link href="https://rawgit.com/mendelman/icons/master/icomoon/styles.css" rel="stylesheet" type="text/css">-->
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/icons/tonicons/style.css" rel="stylesheet" type="text/css">
<link href="{{ Protocol::home() }}/content/assets/front-end/css/icomoon_styles.css" rel="stylesheet" type="text/css">
	<link href="{{ Protocol::home() }}/content/assets/front-end/css/feather_style.css" rel="stylesheet" type="text/css">
	    <!--<link href="https://rawgit.com/mendelman/icons/master/feather/style.css" rel="stylesheet" type="text/css">-->
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/icons/material/css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />

    <!-- StyleSheets -->
    @if (config('app.rtl'))
        <link href="{{ Protocol::home() }}/content/assets/front-end/css/bootstrap-rtl.css?v=1.3" rel="stylesheet" type="text/css">
        <link href="{{ Protocol::home() }}/content/assets/front-end/css/core-rtl.css?v=1.3" rel="stylesheet" type="text/css">
        <link href="{{ Protocol::home() }}/content/assets/front-end/css/components-rtl.css?v=1.3" rel="stylesheet" type="text/css">
        <link href="{{ Protocol::home() }}/content/assets/front-end/css/style-rtl.css?v=1.3" rel="stylesheet" type="text/css">
    @else
        <link href="{{ Protocol::home() }}/content/assets/front-end/css/bootstrap.css?v=1.3" rel="stylesheet" type="text/css">
        <link href="{{ Protocol::home() }}/content/assets/front-end/css/core.css?v=1.3" rel="stylesheet" type="text/css">
        @if(!isset($data))
            <link href="{{ Protocol::home() }}/content/assets/front-end/css/components.css?v=1.3" rel="stylesheet" type="text/css">
        @endif
        <link href="{{ Protocol::home() }}/content/assets/front-end/css/style.css?v=1.3" rel="stylesheet" type="text/css">
    @endif
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/colors.css?v=1.3" rel="stylesheet" type="text/css">
    {{--<link href="{{ Protocol::home() }}/content/menu.css" rel="stylesheet" type="text/css">--}}

    @yield ('styles')

<!-- Core JS files -->
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/core/libraries/jquery_ui/core.min.js"></script>

    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/core/app.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/ui/ripple.min.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/forms/validation/validate.min.js"></script>
    @yield ('javascript')
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/notifications/noty.min.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/adblock/adblock.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/core/components.min.js?v=1.2"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/emojione/2.2.7/lib/js/emojione.min.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/loaders/pace.min.js"></script>

    <!-- Login using phone number -->
    <script src="https://identifyme.net/authRequest/lib.js" defer type="text/javascript"></script>

  
    @yield ('head')

<!-- Google Analytics Code -->
    {!! Helper::settings_seo()->google_analytics !!}

<!-- Header Code -->
    {!! Helper::settings_seo()->header_code !!}





</head>

<body id="root" data-root="{{ Protocol::home() }}">




{{--<!-- Main navbar -->--}}
<nav  class="navbar filter-bar {{ Route::currentRouteName() == 'home' ? 'navbar-transparent' : 'filled' }}" >

    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="icon7-bar"></span> <span class="icon7-bar"></span> <span class="icon7-bar"></span> </button>
            <div data-no-turbolink="">
                <a href="{{ Protocol::home() }}/" class="navbar-brand">
                    <div class="logo"> <img src="{{ Route::currentRouteName() == 'home' ? Protocol::home().'//uploads/settings/logo/logo.png' : Protocol::home().'/uploads/settings/logo/footer/logo.png' }}"> </div>
                </a>
            </div>
        </div>
        <div class="navbar-collapse navbar-ex1-collapse collapse" aria-expanded="true" style="">
            <ul class="nav navbar-nav navbar-right">



                <li>
                    <a href="{{ url('/') }}"> <i class="bf-icon  fa fa-home"></i>
                        <p>Home</p>
                    </a>
                </li>


                <li>
                    <a href="{{ url('article') }}"> <i class="bf-icon  fa fa-cubes"></i>
                        <p>Article</p>
                    </a>
                </li>

                <li>
                    <a href="{{ Protocol::home() }}/stores"> <i class="bf-icon  fa fa-shopping-bag"></i>
                        <p>Market</p>
                    </a>
                </li>


                <li>
                    <a href="{{ Protocol::home() }}/contact"> <i class="bf-icon  fa fa-play-circle"></i>
                        <p>Contact Us</p>
                    </a>
                </li>

            @if (Auth::check())
                <!-- User Account -->
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            @if (Helper::count_user_notifications(null))
                                <span class="notification-bubble"> {{ Helper::count_user_notifications(null) }} </span>
                            @endif
                            <div class="user-photo"> <img class="photo" src="{{ Profile::picture(Auth::id()) }}" alt="Thumb"> </div>
                            <p>{{ Auth::user()->first_name }} <span class="caret"></span></p>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-navbar">

                            <!-- Account Settings -->
                            <li>
                                <a href="{{ Protocol::home() }}/account/settings"> <i class="feather-cog"></i>
                                    <p>{{ Lang::get('header.lang_account_settings') }}</p>
                                </a>
                            </li>

                            <!-- My Ads -->
                            <li>
                                <a href="{{ Protocol::home() }}/account/ads"> <i class="feather-archive"></i>
                                    <p>{{ Lang::get('header.lang_my_submissions') }}</p>
                                </a>
                            </li>





                        @if (Profile::hasStore(Auth::id()))

                            <!-- My Store -->
                                <li>
                                    <a href="{{ Protocol::home() }}/store/{{ Profile::hasStore(Auth::id())->username }}"> <i class="feather-bag"></i>
                                        <p>{{ Lang::get('header.lang_my_store') }}</p>
                                    </a>
                                </li>

                                <!-- Store Settings -->
                                <li>
                                    <a href="{{ Protocol::home() }}/account/store/settings"> <i class="feather-cog"></i>
                                        <p>{{ Lang::get('header.lang_store_settings') }}</p>
                                    </a>
                                </li>

                                <!-- Store Feedback -->
                                <li>
                                    <a href="{{ Protocol::home() }}/account/store/feedback"> <i class="feather-paper-clip"></i>
                                        <p>{{ Lang::get('header.lang_store_feedback') }}</p>
                                    </a>
                                </li>

                        @elseif (Auth::user()->account_type)

                            <!-- Create Store -->
                                <li>
                                    <a href="{{ Protocol::home() }}/create/store"> <i class="feather-square-plus"></i>
                                        <p>{{ Lang::get('header.lang_create_store') }}</p>
                                    </a>
                                </li>

                        @endif

                        <!-- Messages -->
                            <li>
                                <a href="{{ Protocol::home() }}/account/inbox"> <i class="feather-mail"></i>
                                    <p>{{ Lang::get('header.lang_messages') }}</p>
                                    @if (Helper::count_user_notifications('messages'))
                                        <span class="notification-bubble" style="margin-top: 12px;"> {{ Helper::count_user_notifications(null) }} </span>
                                    @endif
                                </a>
                            </li>

                            <!-- Received Offers -->
                            <li>
                                <a href="{{ Protocol::home() }}/account/offers"> <i class="feather-loader"></i>
                                    <p>{{ Lang::get('header.lang_offers') }}</p>
                                    @if (Helper::count_user_notifications('offers'))
                                        <span class="notification-bubble" style="margin-top: 12px;"> {{ Helper::count_user_notifications('offers') }} </span>
                                    @endif
                                </a>
                            </li>

                            <!-- Notifications -->
                            <li>
                                <a href="{{ Protocol::home() }}/account/notifications"> <i class="feather-bell"></i>
                                    <p>{{ Lang::get('header.lang_notifications') }}</p>
                                    @if (Helper::count_user_notifications(null))
                                        <span class="notification-bubble" style="margin-top: 12px;"> {{ Helper::count_user_notifications(null) }} </span>
                                    @endif
                                </a>
                            </li>

                        @if (Auth::user()->is_admin)

                            <!-- Dashboard -->
                                <li>
                                    <a href="{{ Protocol::home() }}/dashboard" target="_blank"> <i class="feather-help"></i>
                                        <p>{{ Lang::get('header.lang_dashboard') }}</p>
                                    </a>
                                </li>

                        @endif

                        <!-- Logout -->
                            <li>
                                <a href="{{ Protocol::home() }}/auth/logout"> <i class="feather-power"></i>
                                    <p>{{ Lang::get('header.lang_logout') }}</p>
                                </a>
                            </li>

                        </ul>
                    </li>
            @else
                <!-- Login/Register -->
                    <li class="big-bundle">
                        <a href="{{ Protocol::home() }}/auth/login"> <i class="feather-head iconSize-2x"></i>
                            <p>{{ Lang::get('update_two.lang_my_account') }}</p>
                        </a>
                    </li>
            @endif

            <!-- create new ad -->
                <li class="big-bundle">
                    <a href="{{ Protocol::home() }}/create"> <i class="feather-plus iconSize-2x"></i>
                        <p>{{ Lang::get('update_two.lang_add_ad') }}</p>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>
{{--<!-- /main navbar -->--}}


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page Header -->
        @yield('pageHeader')

        <!-- Content area -->
            <div class="content">

                @yield('content')

            </div>
            <!-- /content area -->

            <!-- Footer -->
            <div class="footer text-muted">

                <!-- Page List -->
                <div class="footer-pages">
                    <div class="row">
                        <div class="col-md-2">
                            <h4>{{ Config::get('footer.column_one') }}</h4>
                            <div class="page-item">
                                @if (Helper::get_pages('col1'))
                                    @foreach (Helper::get_pages('col1') as $page)
                                        <a href="{{ Protocol::home() }}/page/{{ $page->page_slug }}">{{ $page->page_name }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h4>{{ Config::get('footer.column_two') }}</h4>
                            <div class="page-item">
                                @if (Helper::get_pages('col2'))
                                    @foreach (Helper::get_pages('col2') as $page)
                                        <a href="{{ Protocol::home() }}/page/{{ $page->page_slug }}">{{ $page->page_name }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h4>{{ Config::get('footer.column_three') }}</h4>
                            <div class="page-item">
                                @if (Helper::get_pages('col3'))
                                    @foreach (Helper::get_pages('col3') as $page)
                                        <a href="{{ Protocol::home() }}/page/{{ $page->page_slug }}">{{ $page->page_name }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h4>{{ Config::get('footer.column_four') }}</h4>
                            <div class="page-item">
                                <a href="{{ Protocol::home() }}/contact">{{ Lang::get('footer.lang_contact') }}</a>
                                <a href="{{ Protocol::home() }}/pricing">{{ Lang::get('header.lang_pricing') }}</a>
                                <a href="{{ Protocol::home() }}/blog">{{ Lang::get('update_two.lang_blog') }}</a>
                                @if (Helper::get_pages('col4'))
                                    @foreach (Helper::get_pages('col4') as $page)
                                        <a href="{{ Protocol::home() }}/page/{{ $page->page_slug }}">{{ $page->page_name }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="right-footer">

                                <!-- Footer Logo -->
                                <div class="footer-logo">
                                    <img src="{{ Protocol::home() }}/application/public/uploads/settings/logo/footer/logo.png" alt="{{ config('app.name') }}">
                                </div>

                                <!-- Newsletter -->
                                <div class="newsletter">
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <input type="email" class="form-control" placeholder="{{ Lang::get('footer.lang_subscribe_to_our_newsletter') }}" id="newsletterEmail">
                                                <span class="input-group-btn">
														<button id="newsletterSubscribe" class="btn bg-teal" type="button">{{ Lang::get('footer.lang_subscribe') }}</button>
													</span>
                                            </div>
                                            <span class="help-block">{{ Lang::get('footer.lang_get_an_email_once_month') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <!-- Accepted Payment Methods -->
                                <div class="accepted-payment-methods">
                                    <img src="{{ Protocol::home() }}/content/assets/front-end/images/payments.png" alt="">
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="bottom-footer">

                    <div class="footer-copyright">
                        <p>{{ config('footer.copyright') }} </p>
                    </div>
                    <!-- Social Media -->
                    <div class="footer-social-media">

                        <div class="footer-social-links">

                            <a href="#language" data-toggle="modal" data-target="#language" target="_blank"><i class="fa fa-globe"></i></a>

                            @if (config('social.facebook'))
                                <a href="{{ config('social.facebook') }}" target="_blank"><i class="fa fa-facebook"></i></a>
                            @endif

                            @if (config('social.twitter'))
                                <a href="{{ config('social.twitter') }}" target="_blank"><i class="fa fa-twitter"></i></a>
                            @endif

                            @if (config('social.google'))
                                <a href="{{ config('social.google') }}"  target="_blank"><i class="fa fa-google-plus"></i></a>
                            @endif

                            @if (config('social.android'))
                                <a href="{{ config('social.android') }}"  target="_blank"><i class="fa fa-android"></i></a>
                            @endif

                            @if (config('social.iphone'))
                                <a href="{{ config('social.iphone') }}"  target="_blank"><i class="fa fa-apple"></i></a>
                            @endif

                            @if (config('social.windows'))
                                <a href="{{ config('social.windows') }}"  target="_blank"><i class="fa fa-windows"></i></a>
                            @endif

                        </div>
                    </div>

                </div>

            </div>
            <!-- /footer -->

            <!-- Choose Language -->
            <div id="language" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="modal-title">{{ Lang::get('footer.lang_choose_language') }}</h6>
                        </div>

                        <div class="modal-body">

                            <!-- Available Languages -->
                            <div class="row">

                                @foreach (Countries::languages() as $key => $name)
                                    <div class="col-md-3">
                                        <div class="language-lnk">
                                            <a class="{{ $key }}" href="{{ URL::current() }}?lang={{ $key }}">{{ $name }}</a>
                                        </div>
                                    </div>
                                @endforeach

                            </div>

                        </div>

                        <div class="modal-footer">
                            <div class="help-translate">
                                {{ Lang::get('footer.lang_do_you_speak_multiple_languages') }} <a href="mailto:{{ config('mail.from.address') }}">{{ Lang::get('footer.lang_help_translate') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

<!-- Adblock detected -->
{{--<div class="adblock-detected" style="display: none">--}}
{{----}}
{{--<div class="adblock">--}}
{{--<p>We have detected that you are using an adblocking plugin in your browser.<br>Our website is made possible by displaying online advertisements to our visitors.--}}
{{--Please consider supporting us by disabling your ad blocker.</p>--}}
{{--</div>--}}

{{--</div>--}}

<script>
    var BS_Theme_Demo_Changer = (function ($) {
        "use strict";

        return {

            init: function () {

                this.loader();

                this.handler();

                this.responsive();
            },


            loader: function () {

                $('.bs-demo-changer .demo-item').hover(function () {

                    if ($(this).hasClass('coming'))
                        return;

                    var $loader = $('.bs-demo-changer .changer-preview');

                    $loader.show();

                    $loader.find('span').addClass($(this).data('demo-id'));

                }, function () {

                    if ($(this).hasClass('coming'))
                        return;

                    var $loader = $('.bs-demo-changer .changer-preview');

                    $loader.hide();

                    $loader.find('span').attr('class', '');

                });
            },


            //
            // Panel show/hide handler
            //
            handler: function () {

                $('.bs-demo-changer .handler').click(function () {

                    var $demo_changer = $(this).closest('.bs-demo-changer');

                    if ($demo_changer.hasClass('close')) {
                        $demo_changer.removeClass('close').addClass('open');
                    } else {
                        $demo_changer.addClass('close').removeClass('open');
                    }
                });
            },


            //
            // Smart show on large screens
            //
            responsive: function () {

                if ($(window).width() > 1700) {
                    $('.bs-demo-changer').addClass('open').removeClass('close');
                }
            }

        };// /return
    })(jQuery);

    // Load when ready
    jQuery(document).ready(function () {
        BS_Theme_Demo_Changer.init();
    });


</script>
</body>
</html>
