<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
    <link rel="shortcut icon" href="{{ Protocol::home() }}/uploads/settings/favicon/favicon.png">

@yield('seo')

<!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900|Droid+Sans|Source+Sans+Pro|Open+Sans:300,400,700|Lato|Rubik|Fira+Sans:200,300,400"
          rel="stylesheet" type="text/css">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('uploads/favicon.ico/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('uploads/favicon.ico/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('uploads/favicon.ico/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('uploads/favicon.ico/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('uploads/favicon.ico/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('uploads/favicon.ico/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('uploads/favicon.ico/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('uploads/favicon.ico/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('uploads/favicon.ico/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('uploads/favicon.ico/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('uploads/favicon.ico/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('uploads/favicon.ico/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('uploads/favicon.ico/favicon-16x16.png') }}">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url('uploads/favicon.ico/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">


    <!-- Icon Fonts -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ url('content/assets/front-end/css/icomoon_styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/icons/tonicons/style.css" rel="stylesheet" type="text/css">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/article_style.css" rel="stylesheet" type="text/css">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/feather_style.css" rel="stylesheet" type="text/css">
    <link href="{{ Protocol::home() }}/content/assets/front-end/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <!-- StyleSheets -->
    {{--home page styles--}}
    <link type="text/css" rel="stylesheet"
          href="{{ Protocol::home() }}/content/assets/front-end/home/css/style_3.14.css">
    <link type="text/css" rel="stylesheet"
          href="{{ Protocol::home() }}/content/assets/front-end/home/css/style_custom.css">
    @if (config('app.rtl'))
        {{--<link href="{{ Protocol::home() }}/content/assets/front-end/css/styles-rtl.css" rel="stylesheet"--}}
        {{--type="text/css">--}}

        {{--<link href="{{ Protocol::home() }}/content/assets/front-end/css/bootstrap-rtl.css" rel="stylesheet" type="text/css">--}}
        {{--<link href="{{ Protocol::home() }}/content/assets/front-end/css/core-rtl.css" rel="stylesheet" type="text/css">--}}
        {{--<link href="{{ Protocol::home() }}/content/assets/front-end/css/components-rtl.css" rel="stylesheet" type="text/css">--}}
        {{--<link href="{{ Protocol::home() }}/content/assets/front-end/css/style-rtl.css rel="stylesheet" type="text/css">--}}
    @else
        {{--<link href="{{ Protocol::home() }}/content/assets/front-end/css/styles-ltr.css" rel="stylesheet"--}}
        {{--type="text/css">--}}
        {{--<link href="{{ Protocol::home() }}/content/assets/front-end/css/bootstrap.css" rel="stylesheet" type="text/css">--}}
        <link href="{{ Protocol::home() }}/content/assets/front-end/css/core.css" rel="stylesheet" type="text/css">
        <link href="{{ Protocol::home() }}/content/assets/front-end/css/style.css" rel="stylesheet" type="text/css">
        @if(!isset($data))
            <link href="{{ Protocol::home() }}/content/assets/front-end/css/components.css" rel="stylesheet"
                  type="text/css">
        @endif
    @endif
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/colors.css" rel="stylesheet" type="text/css">

    @yield ('styles')


<!-- Core JS files -->
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/core/libraries/jquery.min.js"></script>
    {{--<script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/all.js"></script>--}}

    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/core/libraries/jquery_ui/core.min.js" defer></script>

    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/core/app.js" defer></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/ui/ripple.min.js" defer></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/forms/styling/uniform.min.js" defer></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/forms/validation/validate.min.js" defer></script>

    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/forms/selects/select2.min.js" defer></script>
    {{--<script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/core/libraries/bootstrap.min.js" defer></script>--}}
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/popper.min.js" ></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/notifications/noty.min.js" defer></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/core/components.min.js"
            defer></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/emojione.min.js" defer></script>
    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/plugins/loaders/pace.min.js" defer></script>

    <script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/js/smoothscroll.js"></script>
    {{--<script src="{{ Protocol::home() }}/content/assets/front-end/js/bootstrap.js"></script>--}}

    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
    @yield ('javascript')
    <link rel="canonical" href="{{ url()->current() }}"/>

    @yield ('head')
    <style>
        .example_responsive_1 {
            width: 320px;
            height: 100px;
        }
        #storageFrame {
            display: none

        }

        /* Make the image fully responsive */
        .carousel-inner img {
            width: 100%;
            height: 100%;
        }

        a:hover {
            color: #0056b3;
            text-decoration: none;
        }
        @media (min-width: 500px) {
            .example_responsive_1 {
                width: 468px;
                height: 60px;
            }
        }

        @media (min-width: 800px) {
            .example_responsive_1 {
                width: 728px;
                height: 90px;
            }
        }
    </style>

    <!-- Google Analytics Code -->
    {!! Helper::settings_seo()->google_analytics !!}

<!-- Header Code -->
    {!! Helper::settings_seo()->header_code !!}


    <style type="text/css">
        @media screen and (max-width: 480px) {
            .logo {
                width: 200px;
            }
        }

        /*#searchDiv{*/
        /*display: none;*/
        /*}*/
        .blockMore {
            padding-top: 5px;
            background: bisque;
            clear: both;
            width: 100%;
        }

        .closeForm::before {
            position: absolute;
            left: 15px;
            content: ' ';
            height: 20px;
            width: 2px;
            background-color: #333;
        }

        .closeForm {
            position: absolute;
            right: 20px;
            top: 14px;
            width: 20px;
            height: 20px;
            opacity: 1;
            /*display: block;*/
        }

        closeForm::after {
            -ms-transform: rotate(-45deg);
            transform: rotate(-45deg);
        }
    </style>


</head>

<body id="root" data-root="{{ Protocol::home() }}">


<!-- Main navbar -->
<nav id="navi" class="navbar navbar-light py-0   navbar-expand-lg py-md-0" style="color: #000!important;">
    <a class="navbar-brand" href="{{ Protocol::home() }}/">
        <img style="margin-top: -19px" class="logo" height="55" width="300"
             src="{{  Protocol::home().'/uploads/settings/logo/logo.png'  }}"
             alt="sanitaryware">
    </a>
    <button class="navbar-toggler mt-1 " type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars"></i>
    </button>
    <div class=" navbar-collapse collapse" id="navbarNav" style="padding-top: 9px; padding-bottom: 9px;">
        <ul class="navbar-nav">
            <li class="nav-item  py-0"><a href="{{ url('article') }}" class="nav-link custom-nav">Article</a></li>
            <li class="nav-item  py-0"><a href="{{ Protocol::home() }}/stores" class="nav-link custom-nav">Market</a></li>
            <li class="nav-item  py-0"><a href="{{ Protocol::home() }}/contact" class="nav-link custom-nav">Contact Us</a></li>
            <li class="nav-item  py-0"><a href="{{ Protocol::home() }}/catalogue" class="nav-link custom-nav">Catalogue</a></li>
            <li class="nav-item  py-0"><a href="{{ Protocol::home() }}/create" class="nav-link custom-nav">{{ Lang::get('update_two.lang_add_ad') }}</a></li>


        @if (Auth::check())
            <!-- User Account -->
                <li class="nav-item dropdown">
                    <a href="" id="href_profile" class=" nav-link  custom-nav dropdown-toggle"
                       data-toggle="dropdown"
                       aria-expanded="false">
                        {{--@if (Helper::count_user_notifications(null))--}}
                        {{--<span class="notification-bubble"> {{ Helper::count_user_notifications(null) }} </span>--}}
                        {{--@endif--}}
                        {{--<div class="user-photo"><img class="photo" src="{{ Profile::picture(Auth::id()) }}"--}}
                        {{--alt="Thumb"></div>--}}
                        <p>{{ Auth::user()->first_name }} <span class="caret"></span></p>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-navbar">

                        <!-- Account Settings -->
                        <li>
                            <a href="{{ Protocol::home() }}/account/settings"> <i class="fa fa-user"></i>
                                <p>{{ Lang::get('header.lang_account_settings') }}</p>
                            </a>
                        </li>

                        <!-- My Ads -->
                        <li>
                            <a href="{{ Protocol::home() }}/account/ads"> <i class="fa fa-file-archive-o"></i>
                                <p>{{ Lang::get('header.lang_my_submissions') }}</p>
                            </a>
                        </li>


                    @if (Profile::hasStore(Auth::id()))

                        <!-- My Store -->
                            <li>
                                <a href="{{ Protocol::home() }}/store/{{ Profile::hasStore(Auth::id())->username }}">
                                    <i class="fa fa-shopping-bag"></i>
                                    <p>{{ Lang::get('header.lang_my_store') }}</p>
                                </a>
                            </li>

                            <!-- Store Settings -->
                            <li>
                                <a href="{{ Protocol::home() }}/account/store/settings"> <i
                                            class="fa fa-archive"></i>
                                    <p>{{ Lang::get('header.lang_store_settings') }}</p>
                                </a>
                            </li>

                            <!-- Store Feedback -->
                            <li>
                                <a href="{{ Protocol::home() }}/account/store/feedback"> <i
                                            class="fa fa-comment-o"></i>
                                    <p>{{ Lang::get('header.lang_store_feedback') }}</p>
                                </a>
                            </li>

                    @elseif (Auth::user()->account_type)

                        <!-- Create Store -->
                            <li>
                                <a href="{{ Protocol::home() }}/create/store"> <i class="feather-square-plus"></i>
                                    <p>{{ Lang::get('header.lang_create_store') }}</p>
                                </a>
                            </li>

                    @endif

                    <!-- Messages -->
                        <li>
                            <a href="{{ Protocol::home() }}/account/inbox"> <i class="fa fa-envelope-o"></i>
                                <p>{{ Lang::get('header.lang_messages') }}</p>
                                @if (Helper::count_user_notifications('messages'))
                                    <span class="notification-bubble"
                                          style="margin-top: 12px;"> {{ Helper::count_user_notifications(null) }} </span>
                                @endif
                            </a>
                        </li>

                        <!-- Received Offers -->
                        <li>
                            <a href="{{ Protocol::home() }}/account/offers"> <i class="fa fa-download"></i>
                                <p>{{ Lang::get('header.lang_offers') }}</p>
                                @if (Helper::count_user_notifications('offers'))
                                    <span class="notification-bubble"
                                          style="margin-top: 12px;"> {{ Helper::count_user_notifications('offers') }} </span>
                                @endif
                            </a>
                        </li>

                        <!-- Notifications -->
                        <li>
                            <a href="{{ Protocol::home() }}/account/notifications"> <i class="fa fa-bell"></i>
                                <p>{{ Lang::get('header.lang_notifications') }}</p>
                                @if (Helper::count_user_notifications(null))
                                    <span class="notification-bubble"
                                          style="margin-top: 12px;"> {{ Helper::count_user_notifications(null) }} </span>
                                @endif
                            </a>
                        </li>

                    @if (Auth::user()->is_admin)

                        <!-- Dashboard -->
                            <li>
                                <a href="{{ Protocol::home() }}/dashboard" target="_blank"> <i
                                            class="fa fa-info-circle"></i>
                                    <p>{{ Lang::get('header.lang_dashboard') }}</p>
                                </a>
                            </li>

                    @endif

                    <!-- Logout -->
                        <li>
                            <a href="{{ Protocol::home() }}/auth/logout"> <i class="fa fa-power-off"></i>
                                <p>{{ Lang::get('header.lang_logout') }}</p>
                            </a>
                        </li>


                    </ul>
                </li>
        @else
            <!-- Login/Register -->
                <li class="nav-item  py-0"><a href="#"
                                              class="nav-link custom-nav">{{ Lang::get('update_two.lang_my_account') }}</a>
                </li>
                {{--<li class="big-bundle">--}}

                {{--<a id="href_login" href="{{ Protocol::home() }}/auth/login"> <i--}}
                {{--class="fa fa-user iconSize-2x"></i>--}}

                {{--<p>{{ Lang::get('update_two.lang_my_account') }}</p>--}}
                {{--</a>--}}
                {{--</li>--}}
            @endif
            <li class="nav-item  py-0"><a href="javascript:void(0)" onclick="display()" class="nav-link custom-nav"> <i class="fa fa-search "></i> Search

                </a></li>
        </ul>
    </div>
</nav>
{{--<!-- /main navbar -->--}}



<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">


        <!-- Main content -->
        <div class="content-wrapper">
            <div id="searchDiv" class="blockMore block-search " data-toggle="menu-search"
                 style="max-height: 140px; display: none">
                <form action="{{ Protocol::home() }}/search" accept-charset="UTF-8" method="get">
                    <div class="form-group form-search">

                        <input name="q" class="form-control form-control-search"
                               placeholder="{{ Lang::get('home.lang_search_what_are_you_looking') }}" type="text"
                               autocomplete="off">

                        <button type="submit" class="btn btn-default btn-round btn-submit">
                            <i class="feather-search icon-2x"></i>
                        </button>

                    </div>
                    <div style="text-align: center">
                        <span class="search-advanced" data-toggle="modal"
                              data-target="#search_form">{{ Lang::get('update_two.lang_advanced') }}</span>
                    </div>
                </form>
                <a href="#" onclick="display()" class="closeForm "><i class="glyphicon glyphicon-remove"></i></a>
            </div>
            <!-- Page Header -->
        @yield('pageHeader')

        <!-- Content area -->
            <div class="content" style="clear: both">

                @yield('content')

            </div>
            <!-- /content area -->

            <!-- Footer -->
            <div class="footer text-muted">

                <!-- Page List -->
                <div class="footer-pages">
                    <div class="row">
                        <div class="col-md-2">
                            <h4>{{ Config::get('footer.column_one') }}</h4>
                            <div class="page-item">
                                @if (Helper::get_pages('col1'))
                                    @foreach (Helper::get_pages('col1') as $page)
                                        <a href="{{ Protocol::home() }}/page/{{ $page->page_slug }}">{{ $page->page_name }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h4>{{ Config::get('footer.column_two') }}</h4>
                            <div class="page-item">
                                @if (Helper::get_pages('col2'))
                                    @foreach (Helper::get_pages('col2') as $page)
                                        <a href="{{ Protocol::home() }}/page/{{ $page->page_slug }}">{{ $page->page_name }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h4>{{ Config::get('footer.column_three') }}</h4>
                            <div class="page-item">
                                @if (Helper::get_pages('col3'))
                                    @foreach (Helper::get_pages('col3') as $page)
                                        <a href="{{ Protocol::home() }}/page/{{ $page->page_slug }}">{{ $page->page_name }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-2">
                            <h4>{{ Config::get('footer.column_four') }}</h4>
                            <div class="page-item">
                                <a href="{{ Protocol::home() }}/contact">{{ Lang::get('footer.lang_contact') }}</a>
                                <a href="{{ Protocol::home() }}/pricing">{{ Lang::get('header.lang_pricing') }}</a>
                                <a href="{{ Protocol::home() }}/article">{{ Lang::get('update_two.lang_blog') }}</a>
                                @if (Helper::get_pages('col4'))
                                    @foreach (Helper::get_pages('col4') as $page)
                                        <a href="{{ Protocol::home() }}/page/{{ $page->page_slug }}">{{ $page->page_name }}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="right-footer">

                                <!-- Footer Logo -->
                                <div class="footer-logo">
                                    <img src="{{ Protocol::home() }}/content/assets/front-end/images/bg-ft.png"
                                         alt="{{ config('app.name') }}">
                                </div>

                                <!-- Newsletter -->
                                <div class="newsletter">
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <input type="email" class="form-control"
                                                       placeholder="{{ Lang::get('footer.lang_subscribe_to_our_newsletter') }}"
                                                       id="newsletterEmail">
                                                <span class="input-group-btn">
                                                            <button id="newsletterSubscribe" class="btn bg-teal"
                                                                    type="button">{{ Lang::get('footer.lang_subscribe') }}</button>
                                                        </span>
                                            </div>
                                        <!--<span class="help-block">{{ Lang::get('footer.lang_get_an_email_once_month') }}</span>-->
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>

                <div class="bottom-footer">

                    <div class="footer-copyright " style="text-align: center">
                        <p>{{ config('footer.copyright') }} </p>
                    </div>


                </div>

            </div>
            <!-- /footer -->


        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->


<script>
    function display() {

        var x = document.getElementById("searchDiv");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }


</script>
@yield('footer')
</body>
</html>
