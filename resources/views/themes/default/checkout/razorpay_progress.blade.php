@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('content')

<style type="text/css">
    
    .razorpay-payment-button{
        border: none;
        background-color: #30b1e2;
        color: white;
        text-transform: uppercase;
        padding: 10px 20px;
        width: 100%;
        border-radius: 2px;
        font-family: 'Fira Sans', sans-serif;
        font-size: 14px;
    }

</style>

<div class="row">
    <div class="col-md-6" style="margin: 0 auto !important;float: none;">

        <!-- Session Messages -->
        @if (Session::has('error'))
        <div class="alert bg-danger alert-styled-left">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            {{ Session::get('error') }}
        </div>
        @endif

        @if (Session::has('success'))
        <div class="alert bg-success alert-styled-left">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            {{ Session::get('success') }}
        </div>
        @endif

         <form action="{{ Protocol::home() }}/checkout/razorpay/progress" class="panel panel-flat panel-body" method="POST" >

            <script src="https://checkout.razorpay.com/v1/checkout.js"
                    data-key="{{ config('razorpay.razor_key') }}"
                    data-amount="{{ $amount }}"
                    data-buttontext="Pay {{ $amount / 100 }} INR"
                    data-name="{{ config('app.name') }}"
                    data-description="Upgrade your account for {{ $days }} days"
                    data-image="{{ config('razorpay.logo') }}"
                    data-prefill.name=""
                    data-prefill.email=""
                    data-theme.color="#30b1e2">
            </script>
            
            {{ csrf_field() }}



        </form>

    </div>
</div>

@endsection