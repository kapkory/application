@extends (Theme::get().'.layout.app')

@section ('seo')

    {!! SEO::generate() !!}

@endsection




@section ('content')

    <div class="jumbotron">
    <h1 style="text-align: center">Stores in {{ $country_name }}</h1>
    </div>

    <div class="row-padding">

        <!-- Stores -->
        <div class="col-md-12">

            <div class="row">

                @if (count($stores))
                    @foreach ($stores as $store)

                        <div class="col-md-4">

                            <figure class="snip1447 hover">

                                <div class="store-cover" style="background-image: url({{ Profile::cover($store->cover) }})"></div>

                                <figcaption>
                                    <img src="{{ $store->logo }}" alt="{{ $store->title }}" class="profile">

                                    <h2>{{ $store->title }}</h2>

                                    <p>{{ substr($store->long_desc,0,200) }}</p><a href="{{ Protocol::home() }}/store/{{ $store->username }}" class="info" style="width: 100%">{{ Lang::get('update_two.lang_more_info') }}</a>
                                </figcaption>
                            </figure>

                        </div>

                    @endforeach
                @else
                    <div class="alert alert-info">
                     We currently don't have a store in  {{ $country_name }} <br>
                        <a href="{{ url('auth/register') }}">Sign up</a> to create a store with us <br>
                        Or <br>
                        <a href="{{ url('stores') }}">View all Our Stores Here</a>
                        <br>
                        Thank You

                    </div>
                @endif



            </div>

        </div>

    </div>

@endsection