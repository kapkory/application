
@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('pageHeader')

<!-- Open Your Own Store Now -->
<div class="create-store">

	<div class="create-intro">

		<p style="color: saddlebrown !important;">
			The Easiest Way to Create a <br> Stunning Online Store
{{--			{{ Lang::get('browse/stores.lang_easiest_way_to_create_store') }}--}}
		</p>
		 <span class="create-small-intro">
			{{ Lang::get('browse/stores.lang_easiest_way_to_create_store_p') }}
		</span>

	</div>



	<!-- Open Store Now -->
	<a class="create-btn" href="{{ Protocol::home() }}/pricing">{{ Lang::get('browse/stores.lang_open_store_now') }}</a>

</div>

@endsection


@section ('content')

<style>

</style>

<div class="row-padding">

	<!-- Stores -->
	<div class="col-md-12">

		<div class="row">

			@if (count($stores))
			@foreach ($stores as $store)

			<div class="col-md-4">

				<figure class="snip1447 hover">

					<div class="store-cover" style="background-image: url({{ Profile::cover($store->cover) }})"></div>

					 <figcaption>
					 	<img src="{{ $store->logo }}" alt="{{ $store->title }}" class="profile">

					    <h1>{{ $store->title }}</h1>

					    <p>{{ substr($store->long_desc,0,200) }}</p><a href="{{ Protocol::home() }}/store/{{ $store->username }}" class="info" style="width: 100%">{{ Lang::get('update_two.lang_more_info') }}</a>
					  </figcaption>
				</figure>

			</div>

			@endforeach
			@else
			@endif



		</div>

	</div>

</div>

@endsection