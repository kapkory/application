@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('content')

<div class="row">
	
	<div class="col-md-12">

		<!-- Company profile -->
		<div class="panel panel-flat">
			<div class="panel-body">
				<div class="media stack-media-on-mobile text-left content-group">
					<a href="#" class="media-left media-middle">
						<img src="http://demo.interface.club/limitless/layout_1/LTR/material/assets/images/demo/brands/transferwise.png" class="img-rounded img-lg" alt="">
					</a>

					<div class="media-body">
						<h5 class="media-heading text-semibold">Microsoft</h5>
						<ul class="list-inline list-inline-separate text-muted no-margin">
							<li>IT Services</li>
						</ul>
					</div>

					<div class="media-right media-middle text-nowrap">
						<ul class="list-inline no-margin">
							<li><a href="#" class="btn bg-blue"><i class="icon-menu7 position-left"></i> All positions</a></li>
							<li><a href="#" class="btn btn-default"><i class="icon-plus2 position-left"></i> Follow</a></li>
						</ul>											
					</div>
				</div>

				<p>Across a Global footprint, we believe we’re at our best when you’re at yours. From our diverse workforce, our flexible working policies to our creative work spaces, we embrace a culture of learning and sharing to develop our next stage growth. It’s in our hearts to push forward, to create a better future, to never rest and find new ways that help people communicate.</p>

				We are committed to developing the very best people by offering a flexible, motivating and inclusive workplace in which talent is truly recognised and rewarded. We respect, value and celebrate our people’s individual differences - we are not only multinational but multicultural too. That’s what we mean when we say Power to you.
			</div>

			<div class="panel-footer panel-footer-condensed"><a class="heading-elements-toggle"><i class="icon-more"></i></a>
				<div class="heading-elements">
					<ul class="list-inline list-inline-separate heading-text">
						<li><i class="icon-users position-left"></i> 382</li>
						<li><i class="icon-alarm position-left"></i> 60 hours</li>
						<li>
							<i class="icon-star-full2 text-size-base text-warning-300"></i>
							<i class="icon-star-full2 text-size-base text-warning-300"></i>
							<i class="icon-star-full2 text-size-base text-warning-300"></i>
							<i class="icon-star-full2 text-size-base text-warning-300"></i>
							<i class="icon-star-full2 text-size-base text-warning-300"></i>
							<span class="text-muted position-right">(49)</span>
						</li>
					</ul>

					<a href="#" class="heading-text pull-right">Subscribe <i class="icon-arrow-right14 position-right"></i></a>
				</div>
			</div>
			
		</div>
		<!-- /company profile -->


		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Visitors Maps</h5>
			</div>

			<div class="panel-body">
				<div class="map-container map-choropleth"></div>
			</div>
		</div>

	</div>

</div>

@endsection
