@extends (Theme::get().'.layout.app')

@section('styles')
<link rel="stylesheet" href="{{ url('content/assets/front-end/css/article-styles.css') }}">

<link class="rs-file" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/royalslider.css" rel="stylesheet">
<link href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/skins/default/rs-default.css" rel="stylesheet">
{{--<link href="{{ Protocol::home() }}/content/assets/front-end/css/extras/starability-growRotate.css" rel="stylesheet">--}}
{{--<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/emojionearea/emojionearea.css" media="screen">--}}
{{--<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/article.css" >--}}
{{--<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/products.css" >--}}

<style type="text/css">
    .entry-author {
        padding: 2em 0;
    }
    .flex-row {
        display: flex;
        width: 100%;
    }
    .circle {
        border-radius: 999px !important;
        object-fit: cover;
    }
    .mr {
        margin-right: 30px;
    }
    .flex-col {
        max-height: 100%;
    }
    #outer
    {
        width:100%;
        text-align: center;
    }
    .inner
    {
        display: inline-block;
    }

    .td-category {
        list-style: none;
        font-family: 'Open Sans', arial, sans-serif;
        font-size: 10px;
        margin-top: 0;
        margin-bottom: 10px;
        line-height: 1;
    }
    ul, ol {
        padding: 0;
    }
    * {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .td-category li {
        display: inline-block;
        margin: 0 5px 5px 0;
        line-height: 1;
    }
    .td-category a {
        color: #fff;
        background-color: blue;
        padding: 3px 6px 4px 6px;
        white-space: nowrap;
        display: inline-block;
    }


    .author-img {
        float: left;
        margin-right: 20px;
        border-radius: 100%;
        -webkit-border-radius: 100%;
        -moz-border-radius: 100%;
        overflow: hidden;
    }

    .post-author {
        margin: 0;
        overflow: hidden;
        padding: 38px 0 37px 0;
        border-top: 1px solid #E0E0E0;
    }
    .post-author .author-img img {
        margin: 0;
    }
    img {
        max-width: 100%;
        vertical-align: top;
        height: auto;
    }
    fieldset, img {
        border: 0;
    }
    *, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td {
        margin: 0;
        padding: 0;
    }

    .post-author .author-content {
        margin-left: 120px;
    }
    *, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td {
        margin: 0;
        margin-left: 0px;
        padding: 0;
    }

    .author_avatar {
        margin: 0 30px 19px 0;
        width: 100px;
        height: 100px;
    }
    .author-content p {
        margin-bottom: 16px;
    }

    .author-content h5 a {
        display: inline-block;
        margin: 0;
        color: #313131;
    }

    * {
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
    }
    .penci-single-link-pages {
        display: block;
        width: 100%;
    }

    *, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td {
        margin: 0;
        padding: 0;
    }
    .post-tags {
        display: block;
        position: relative;
        z-index: 10;
        color: #888;
        margin-bottom: 0;
        line-height: 1.4;
        margin-top: 31px;
    }
    .post-tags a {
        text-transform: uppercase;
        color: #888;
        padding: 6px 12px 5px;
        margin-right: 8px;
        margin-bottom: 8px;
        display: inline-block;
        font-size: 11px !important;
        background: none;
        border: 1px solid #DEDEDE;
        transition: all 0.3s;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        outline: none;
        font-weight: normal;
        line-height: 1.2;
    }

    .cat > a.penci-cat-name:last-child {

        margin-right: 0;
        padding: 0;

    }
    .cat > a.penci-cat-name {

        font-size: 13px;
        color: #6eb48c;
        line-height: 1.2;
        margin: 0 18px 0 0;
        margin-right: 18px;
        margin-bottom: 0px;
        padding-right: 10px;
        display: inline-block;
        vertical-align: top;
        background: none;
        transition: all 0.3s;
        -webkit-transition: all 0.3s;
        -moz-transition: all 0.3s;
        font-weight: normal;
        margin-bottom: 5px;
        position: relative;
        text-decoration: none;

    }

    .breadcrumb a:hover{
        background-color: grey;
    }
    .inner-post-entry{
        font-size: 14px;
    }
    #toc_container.toc_white {
        background: #fff;
    }
    #toc_container {
        background: #f9f9f9;
        border: 1px solid #aaa;
        padding: 10px;
        margin-bottom: 1em;
        width: auto;
        display: table;
        font-size: 95%;
    }
    #toc_container p.toc_title {
        text-align: center;
        font-weight: 700;
        margin: 0;
        padding: 0;
    }
    #toc_container p.toc_title {
        text-align: center;
        font-weight: 700;
        margin: 0;
        padding: 0;
    }
    #toc_container p.toc_title+ul.toc_list {
        margin-top: 1em;
    }

    #toc_container.no_bullets li, #toc_container.no_bullets ul, #toc_container.no_bullets ul li, .toc_widget_list.no_bullets, .toc_widget_list.no_bullets li {
        background: 0 0;
        list-style-type: none;
        list-style: none;
    }
    #toc_container li, #toc_container ul {
        margin: 0;
        padding: 0;
    }
    #toc_container li, #toc_container ul {
        margin: 0;
        padding: 0;
    }
    #toc_container a {
        text-decoration: none;
        text-shadow: none;
    }

    a {
        color: #1E73BE;
    }
    #toc_container li a{
        font-size: 16px
    }
    #toc_container ul ul {
        margin-left: 1.5em;
    }
    #toc_container ul {
        margin: 0;
        padding: 0;
    }
    .comment_submit {
        text-transform: uppercase;
        font-family: "Raleway", sans-serif;
        font-weight: bold;
        background: #F5F5F5;
        border-radius: 0;
        font-size: 14px;
        color: #313131;
        padding: 12px 20px;
        display: inline-block;
        -o-transition: .3s;
        -moz-transition: .3s;
        -webkit-transition: .3s;
        transition: .3s;
        cursor: pointer;
        width: auto;
        min-width: 120px;
        text-align: center;
        margin: 0;
        border: none;
    }
</style>

@endsection

@section ('seo')

{!! SEO::generate() !!}

@endsection
@section ('javascript')

<script type="text/javascript" src="{{ url('content/assets/front-end/js/dataTables.min.js') }}"></script>
{{--<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>--}}

@endsection



@section ('content')


<div class="container container-single penci_sidebar right-sidebar penci-enable-lightbox">
    <div id="main">
        <div  class="theiaStickySidebar"  >
            <!-- Add this code to article page: -->
            <article id="post-834" class="post-834 post type-post status-publish format-gallery has-post-thumbnail hentry category-cardio category-featured tag-blog tag-cardio tag-fitness post_format-post-format-gallery" >
                <hr>
                <div class="col-sm-12">
                    @if (Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error') }}
                    </div>
                    @endif
                    @if (Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                    @endif
                </div>

                <nav aria-label="breadcrumb" style="padding: 10px 0;">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('article-category/'.$article->articleCategory->category_slug) }}">{{ $article->articleCategory->name }}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $article->title }}</li>
                    </ol>
                </nav>
                {{--<hr>--}}
                <div >
                    <div class="header-standard header-classic single-header">



                        <h1 class="post-title single-post-title" >{{ $article->title }}</h1>

                        <div class="post-box-meta-single">
                            <span class="author-post" >
                                <span >
                                    @if($article->user->id ==1)
                                    Written by <a class="author-url"  href='https://plus.google.com/+VenkatmaniM'>{{ $article->user->first_name }}</a>
                                    @else
                                    Written by <a class="author-url"  href='#'>{{ $article->user->first_name    }}</a>
                                    @endif
                                </span>
                            </span>

                            {{--<span itemprop="datePublished">{{ $article->created_at }}</span>--}}
                        </div>
                        <ul class="td-category" style="text-align: center; font-size: 15px">
                            <li class="entry-category"><a href="{{ url('article-category/'.$article->articleCategory->category_slug) }}">{{ $article->articleCategory->name }}</a></li>
                        </ul>
                    </div>

                    {{--<meta itemprop='isFamilyFriendly' content='True'/>--}}



                    <div class="post-image">
                        <div class="penci-owl-carousel penci-owl-carousel-slider penci-nav-visible owl-loaded owl-drag" data-auto="true" data-lazy="true">






                        </div>
                    </div>


                    <div class="post-entry blockquote-style-2">
                        <div class="inner-post-entry" >
                            <?php
                            $code = '<style> .blog_responsive_1 { width: 300px; height: 250px; } @media(min-width: 500px) { .blog_responsive_1 { width: 336px; height: 280px; } } @media(min-width: 800px) { .blog_responsive_1 { width: 728px; height: 90px; } } </style>  <!-- Responsive ad for sidebar --> <ins class="adsbygoogle blog_responsive_1" style="display:inline-block" data-ad-client="ca-pub-6803454939055727" data-ad-slot="1550184116"></ins> <script> (adsbygoogle = window.adsbygoogle || []).push({}); </script>';
                            $code1 = '<style>.adsense-ad-inside-text-left, .adsense-ad-inside-text-right { margin: 10px }.adsense-ad-inside-text-left p, .adsense-ad-inside-text-right p { text-align: center;}.adsense-ad-inside-text-right { float: right }.adsense-ad-inside-text-left  { float: left  }</style><div class="adsense-ad-inside-text-left"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- 336 280 size ad --><ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-6803454939055727" data-ad-slot="5049278750"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script></div>';
                            $code2 = '<style>.adsense-ad-inside-text-left, .adsense-ad-inside-text-right { margin: 10px }.adsense-ad-inside-text-left p, .adsense-ad-inside-text-right p { text-align: center;}.adsense-ad-inside-text-right { float: right }.adsense-ad-inside-text-left  { float: left  }</style><div class="adsense-ad-inside-text-right"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- 336 280 size ad --><ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-6803454939055727" data-ad-slot="5049278750"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script></div>';
                            $code3 = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- responsive link ads --><ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-6803454939055727" data-ad-slot="3448422476" data-ad-format="link" data-full-width-responsive="true"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>';
                            $code4 = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> <ins class="adsbygoogle" style="display:block; text-align:center;" data-ad-layout="in-article" data-ad-format="fluid"      data-ad-client="ca-pub-6803454939055727"      data-ad-slot="7961469161"></ins> <script>      (adsbygoogle = window.adsbygoogle || []).push({}); </script>';
                            ?>
                               {{--<a href="{{ url('product/indian-sanitaryware-manufacturers-directory-2018-buy-digital-copy-2939235104') }}"> <img src="{{ url('uploads/general/sanitaryware-directory.jpeg') }}" height="50px" style="float: left;"/></a>--}}
{{--                            {!! str_replace('[adsense_code]',$code,$article->content) !!}--}}
                                {!! str_replace(
                                     ['[adsense_code]','[adsense_code1]','[adsense_code2]','[adsense_code3]','[adsense_code4]',"[product_id","[store_id","]]"],
                                     [$code,$code1,$code2,$code3,$code4,"<div class='products' data-product-id","<div class='stores' data-store-id","><div>"],
                                     $article->content) !!}



                            <div class="penci-single-link-pages">
                            </div>

                            <div class="post-tags" >
                                @if($tags)
                                @foreach($tags as $tag)
                                <a  href="{{  url('article-tag/'.$tag->name)  }}" rel="tag">{{ $tag->name }}</a>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>


                    <div class="tags-share-box center-box">

                        <span class="single-comment-o"><i class="fa fa-comment-o"></i>{{ $comment_count }} comments</span>

                        <div class="post-share">
                            {{--<span class="count-number-like">2</span><a class="penci-post-like single-like-button" data-post_id="834" title="Like" data-like="Like" data-unlike="Unlike"><i class="fa fa-heart-o"></i></a>                   <div class="list-posts-share">--}}
                                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}"><i class="fa fa-facebook"></i><span class="dt-share">Facebook</span></a>
                                <a target="_blank" href="https://twitter.com/intent/tweet?text={{ url()->current() }}"><i class="fa fa-twitter"></i><span class="dt-share">Twitter</span></a>
                                <a target="_blank" href="https://plus.google.com/share?url={{ url()->current() }}"><i class="fa fa-google-plus"></i><span class="dt-share">Google +</span></a>
                                <a data-pin-do="none" target="_blank" href="https://pinterest.com/pin/create/button/?url={{ url()->current() }}&amp;media={{ Protocol::home() }}/uploads/articles/{{ $article->cover }}&amp;description={{ $article->title }}"><i class="fa fa-pinterest"></i><span class="dt-share">Pinterest</span></a>
                            </div>
                            {{--</div>--}}
                    </div>

                    @if($article->show_author == 1)
                    <div class="post-author" >
                        <div class="author-img"  >
                            <img alt="{{ $article->user->first_name. ' '.$article->user->last_name }}" src="{{ ($article->user->profile_image) ? url($article->user->profile_image) : url('uploads/avatars/noavatar.png') }}" class="avatar avatar-100 photo" width="100" height="100">
                            <meta  content="{{ ($article->user->profile_image) ? url($article->user->profile_image) : url('uploads/avatars/noavatar.png') }}">

                        </div>
                        <div class="author-content">
                            <h5><a href="#" title="Posts by {{ $article->user->first_name }}" rel="author"><span >{{ $article->user->first_name }}</span></a></h5>

                            <p>{{ $article->user->bio }}</p>

                            <meta  content="SanitaryWare.org">
                            <a target="_blank" class="author-social" href="{{ $article->user->facebook_id }}"> <i class="fa fa-facebook"></i></a>
                            <a target="_blank" class="author-social" href="{{ $article->user->twitter_id }}"><i class="fa fa-twitter"></i></a>
                            <a target="_blank" class="author-social" href="{{ $article->user->google_id }}"><i class="fa fa-google-plus"></i></a>

                        </div>
                    </div>
                    @endif



                    @include('themes.default.blog.comment',['comments'=>$comments,'article'=>$article,'comment_count'=>$comment_count])

                    <script type="text/javascript">
                        $('input[name="article_id"]').val({{ $article->id }});

                        $('.display_reply').click(function () {
                            //                            alert('i have been hit');
                            var data = $(this).data('comment-id');
                            $('input[name="parent_id"]').val(data);

                            //                            $('#comment_reply').show();

                            var html ='<div id="respond" class="comment-respond">'+
                                '<h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="">Cancel Reply</a></small></h3>'
                                +'<form action="{{  Protocol::home() }}/articles/comments/reply" method="post" id="commentform" class="comment-form">'
                                +'<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" placeholder="Your Comment" aria-required="true"></textarea></p>'
                            @if(!Auth::check())
                                    +' <p class="comment-form-author"><input id="name" name="name" value="" placeholder="Name*" size="30" aria-required="true" type="text"></p>'
                                +'<p class="comment-form-email"><input id="email" name="email" value="" placeholder="Email*" size="30" aria-required="true" type="email"></p>'
                            @endif

                                +' <p class="comment-form-url">{{ csrf_field() }}<p>'
                                +'<p><input type="hidden" name="parent_id" value="'+data+'"></p>'
                                +'<p><input type="hidden" name="article_id" value="{{ $article->id }}"></p>'
                                +' <p class="form-submit"><input name="submit" id="submit" class="submit" value="Submit" type="submit">'
                            //                               + ' <input name="comment_post_ID" value="834" id="comment_post_ID" type="hidden">'
                            //                               +' <input name="comment_parent" id="comment_parent" value="59" type="hidden">'
                                +'</p></form>'
                                +' </div>';
                            var app = "reply_page_"+data;
                            //                            document.getElementById(app).append(html);
                            $("#"+app).append(html);
                        });



                    </script>






                </div>
            </article>

        </div>
    </div>

    <div id="sidebar" class="penci-sidebar-content style-9 pcalign-center">
        <div class="theiaStickySidebar">
            {{--<aside id="penci_about_widget-2" class="widget penci_about_widget"><h4 class="widget-title penci-border-arrow"><span class="inner-arrow">About Me</span></h4>--}}
                {{--<div class="about-widget pc_aligncenter">--}}
                    {{--<img class="penci-widget-about-image holder-square penci-lazy" src="http://cdn-soledad.pencidesign.com/soledad-fitness-blog/wp-content/uploads/sites/39/2017/07/aboutava.jpg" alt="About Me" style="border-radius: 50%; display: inline;">--}}


                    {{--</div>--}}

                {{--</aside>--}}

            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-format="autorelaxed"
                 data-ad-client="ca-pub-6803454939055727"
                 data-ad-slot="9186404046"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>

        </div>

        <style>
            .sidebar_responsive_1 { width: 300px; height: 250px; }
            @media(min-width: 500px) { .sidebar_responsive_1 { width: 336px; height: 280px; } }
            @media(min-width: 800px) { .sidebar_responsive_1 { width: 300px; height: 600px; } }
        </style>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Responsive ad for sidebar -->
        <ins class="adsbygoogle sidebar_responsive_1"
             style="display:inline-block"
             data-ad-client="ca-pub-6803454939055727"
             data-ad-slot="1550184116"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>


        <a href="{{ url('page/advertise-with-us') }}"><img src="{{ url('uploads/avatars/promote-your-brand.png') }}"></a>
    </div>
    <!-- END CONTAINER -->
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $('table').addClass('dataTable');
        $('table').dataTable();
    })

</script>

<script>
    $(document).ready(function () {
        var i = 0;
        var list_type ;
        var div = '<div id="toc_container" class="toc_white no_bullets"><p class="toc_title">Contents</p><ul class="toc_list">';

        var cnt = 0;
        var used_h3 = new Array();
            
        $('.inner-post-entry h2').each(function (m) {
            var k = this.textContent;
            var slug = k.replace(/\s+/g, "_");
            $(this).wrapInner("<span id='" + slug + "'></span>");
            m += 1;
            list_type = "<li><a href='#" + slug + "'><span class='toc_number'>" + m + ".&nbsp;&nbsp;</span>" + k + "</a>";
            
            if ($(this).find('h3')) {
                list_type += '<ul>';
                $($(this)).nextUntil('h2', 'h3').each(function (n) {
                    tex = $(this).text();
                    var h3_slug = tex.replace(/\s+/g, "_");
                    $(this).wrapInner("<span id='" + h3_slug + "'></span>");
                    n += 1;
                    cnt += 1;
                    used_h3[cnt] = tex;
                    list_type += "<li><a href='#" + h3_slug + "'><span class='toc_number'>" + m + '.' + n + ".&nbsp;&nbsp;</span>" + tex + "</a></li>";
                });
                if ($(this).find('h4')) {
                    list_type += '<ul>';
                    $($(this)).nextUntil('h2', 'h4').each(function (o) {
                        h4_text = $(this).text();
                        var h4_slug = h4_text.replace(/\s+/g, "_");
                        $(this).wrapInner("<span id='" + h4_slug  + "'></span>");
                        n = 0;
                        o += 1;
                        list_type += "<li><a href='#" + h4_slug + "'><span class='toc_number'>" + m + '.' + n + '.' + o + ".&nbsp;&nbsp;</span>" + h4_text + "</a></li>";
                    });
                    list_type += '</ul>';
                    list_type += '</li>';
                }
                list_type += '</ul>';
                list_type += '</li>';
            }
            div += list_type;
            $('.toc_list').append(list_type);

        });
        if('.inner-post-entry:has("h3")')
        {
            $('.inner-post-entry h3').each(function (m) {
//            alert(used_h3.indexOf(k));
                var k = this.textContent;
                if(used_h3.indexOf(k) <= 0){
                    var slug = k.replace(/\s+/g,"_");
                    $(this).wrapInner("<span id='"+slug+"'></span>");
                    m+=1;
                    list_type = "<li><a href='#"+slug+"'><span class='toc_number'>"+m+".&nbsp;&nbsp;</span>"+k+"</a>";

                    div += list_type;
                    $('.toc_list').append(list_type);
                }
            });
        }
        div+= '<ul><div>';
        $('.shortcode').append(div);

    });
</script>


@if($article->status == 1)
<script type="application/ld+json">
    {
    "@context": "https://schema.org/",
    "@type": "Article",
    "headline": "{{ $article->title }}",
    "mainEntityOfPage": "{{ url()->current() }}",
    "datePublished": "{{ $article->created_at }}",
    "dateModified": "{{ Carbon\Carbon::now() }}",
    "description": "{{ $article->meta_description }}",
    "articleBody": "{{ $article->content }}",
    "image": {
    "@type": "ImageObject",
    "url": " {{ url('uploads/articles/'.$article->cover) }}"
    },
    "author": "{{ $article->user->first_name }}",
    "publisher": {
    "@type": "Organization",
    "logo": {
    "@type": "ImageObject",
    "url": "{{ url('uploads/settings/logo/logo.png') }}"
    },
    "name": "SanitaryWare"
    }

    }
</script>


<script type="application/ld+json">
    {
    "@context": "https://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "item": {
    "@id": "{{ url('/') }}",
    "name": "Home"
    }
    },{
    "@type": "ListItem",
    "position": 2,
    "item": {
    "@id": "{{ url('article') }}",
    "name": "articles"
    }
    },{
    "@type": "ListItem",
    "position": 3,
    "item": {
    "@id": "{{ url('article/'.$article->slug) }}",
    "name": "{{ $article->title }}"
    }
    }]
    }
</script>
    @endif
@endsection

@section('footer')

    <script>
        $(document).ready(function () {
            if($("#root").find('.products')){
                var products= $('.products').data('product-id');

               if(products){
                   var url = "{{ url('/') }}"+'/api/product-id/'+products;
                   console.log('url is '+url+' products is '+products);
                   $.get(url,function (response) {
                       // console.log(response);
                       var ul = '<div class="col-xs-12"><ul class="ace-thumbnails clearfix">';
                       for (var i = 0 ; i < response.length; i++){
                           var resp = response[i];

                           ul += '<li>' +
                               '<a href="{{ url('product') }}'+"/"+resp.slug+'" data-rel="colorbox" class="cboxElement"> '+
                                   {{--'<img width="150" height="150" alt="150x150" src="{{ url('uploads/images') }}'+'/617669297/thumbnails/thumbnail_0.jpg'+'">'+--}}
                                       '<img width="150" height="150" alt="150x150" src="{{ url('uploads/images') }}'+'/'+resp.ad_id+'/thumbnails/thumbnail_0.jpg'+'">'+
                               '<div class="text">' +
                               '<div class="inner">'+resp.title+'</div>'+
                               '</div>'+
                               '</a>'+
                               '</li>'
                       }
                       ul += '</ul></div>';
                       $('.products').prepend(ul);
                   });
               }

            }
            else
            {
                console.log('siko');
            }
        });
    </script>


    {{--Store ads--}}

    <script>
        $(document).ready(function () {
            if($("#root").find('.stores')){
                var stores= $('.stores').data('store-id');

                var url = "{{ url('/') }}"+'/api/store-id/'+stores;
                // console.log('url is '+url+' products is '+products);
                $.get(url,function (response) {
                    // console.log(response);
                    var ul = '<div class="col-xs-12"><ul class="ace-thumbnails clearfix">';
                    for (var i = 0 ; i < response.length; i++){
                        var resp = response[i];

                        ul += '<li>' +
                            '<a href="{{ url('store') }}'+"/"+resp.username+'" data-rel="colorbox" class="cboxElement"> '+
                                {{--'<img width="150" height="150" alt="150x150" src="{{ url('uploads/images') }}'+'/617669297/thumbnails/thumbnail_0.jpg'+'">'+--}}
                                    '<img width="150" height="150" alt="150x150" src="'+resp.logo+'">'+
                            '<div class="text">' +
                            '<div class="inner">'+resp.title+'</div>'+
                            '</div>'+
                            '</a>'+
                            '</li>'
                    }
                    ul += '</ul></div>';
                    $('.stores').prepend(ul);
                });

            }
            else
            {
                console.log('siko');
            }
        });
    </script>



@endsection