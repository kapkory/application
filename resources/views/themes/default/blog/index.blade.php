@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('content')
<!--<link href="{{ Protocol::home() }}/content/assets/front-end/css/article_style.css" rel="stylesheet" type="text/css">-->
<!--<link href="{{ Protocol::home() }}/content/assets/front-end/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{ Protocol::home() }}/content/assets/front-end/css/responsive.css" rel="stylesheet" type="text/css">-->
<?php // @include('themes.css') ?>

<div class="row-padding">


    <!-- Articles -->
    @if (count($articles_cat_arr))
    @foreach ($articles_cat_arr as $catid => $article_arr)
    @if(isset($cat_info[$catid]))
    <div class=" col-sm-12">
        <div class="cat-hd col-sm-3">
            <h1>{!!$cat_info[$catid]!!}</h1>
        </div>
        <div class="cat-hd-line col-sm-6">&nbsp;</div>
    </div>
    @endif
    <?php $i = 0; ?> 
    <div class="col-sm-12">
        @foreach ($article_arr as $key => $a)
        <?php $i++; ?>
        @if($key <= 3)
        <div class="col-lg-3 col-sm-6 article-outer">
            <article class="blog-post">
                <div class="featured-post">
                    <a href="{{ Protocol::home() }}/article/{{ $a->slug }}">
                        <!--<img src="{{ Protocol::home() }}/content/assets/front-end/images/aticle-sample.png" alt="">-->
                        <img src="{{ Protocol::home() }}/uploads/articles/{{ $a->cover }}" style="height: 168px;width: 250px;" alt="">
                        <div class="overlay"></div>
                    </a>
                </div><!-- /.featured-post -->
                <div class="content-post">
                    <div class="entry-post">
                        <ul class="entry-meta">
                            <li class="topic">
                                <a href="#" title=""></a>
                            </li>
                            <li class="date">
                                <a href="#" title="">{!!date('d M,Y',strtotime($a->created_at))!!}</a>
                            </li>
                        </ul>
                        <h2 class="entry-title">
                            <a href="{{ Protocol::home() }}/article/{{ $a->slug }}">
                                {!!str_limit($a->title,150)!!}
                            </a>
                        </h2>
                    </div>
                    <p>
                        {!!str_limit(strip_tags($a->content),200)!!}
                    </p>
                    <div class="author-post">
                        By <a href="#" title="">{!!$a->first_name!!}</a>
                    </div>
                </div><!-- /.content-post -->
            </article><!-- /.blog-post -->
        </div>
        @endif
        @endforeach
    </div>
    <div class=" col-sm-12">
        <div class="col-sm-1 pull-right">
            <a class=" btn btn-primary" href="article-category/{!!$a->category_slug!!}"><i class="fa fa-long-arrow-right"></i>View All</a>
        </div>
    </div>
    @endforeach
    @endif
</div>

@endsection