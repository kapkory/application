<?php
$data = 'data';
?>
@extends (Theme::get().'.layout.app')

@section('styles')
    <style type="text/css">
        .entry-author {
            padding: 2em 0;
        }
        .flex-row {
            display: flex;
            width: 100%;
        }
        .circle {
            border-radius: 999px !important;
            object-fit: cover;
        }
        .mr {
            margin-right: 30px;
        }
        .flex-col {
            max-height: 100%;
        }
        .tab_class{
            padding: 28px 0;
        }
        .page-container{
            position: relative;
        }
        .posts-list .content {
            margin-left: 103px;
            line-height: 1;
            min-height: 73px;
            border-bottom: 1px solid #d9d9d9;
        }


        .footer{background-color:#FFF;display:block;-webkit-box-flex:0;-webkit-flex:none;flex:none}.footer-pages{padding:25px 6% 25px 6%}.footer-pages h4{display:block;color:#656565;margin-bottom:10px;font-weight:500;font-family:Fira Sans;font-size:14px;text-transform:uppercase;letter-spacing:1px}.footer-pages .page-item{width:100%}.footer-pages .page-item a{display:block;color:#9b9b9b;margin-bottom:5px;font-family:Roboto;font-size:12px;transition:all 0.5s;text-transform:uppercase}.footer-pages .page-item a:hover{text-decoration:underline}.footer:not(.navbar-fixed-bottom){z-index:1000}.footer.navbar{left:0;right:0;bottom:0}body[class*=navbar-bottom] .footer:not(.navbar){display:none}.footer-boxed{left:0;right:0;padding-left:20px;padding-right:20px}

    </style>
    <meta name="robots" content="noindex, follow">
    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/menu.css" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/responsive.css" media="screen">
    {{--<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/menu.css" media="screen">--}}

    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/mervick-tommorow.css" media="screen">
@endsection

@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('content')


    <div class="main wrap cf">
        <div class="row">

<div class="col-md-12 col-sm-12">
    <div class="jumbotron" style="text-align: center">
        Blog Articles
    </div>
</div>
            <div class="col-8 main-content">


                <div id="post-1846" class="post-1846 page type-page status-publish page-content">


                    {{--<header class="post-header">--}}


                        {{--<h1 class="main-heading">--}}
                            {{--Blog Articles--}}
                        {{--</h1>--}}
                    {{--</header><!-- .post-header -->--}}





                    <div class="posts-list listing-alt">

                        @foreach($articles as $article)
                            <article class="post-{{ $article->id }} post type-post status-publish format-standard has-post-thumbnail category-tidbits" itemscope="" itemtype="http://schema.org/Article">


                                <span class="cat-title cat-26"><a href="{{ url('article-category/'.$article->category_slug) }}">{{ $article->category_name }}</a></span>


                                {{--<a href="http://theme-sphere.com/smart-mag/mesmerizing-view-of-a-perfect-moonlit-night/" itemprop="url"><img src="http://theme-sphere.com/smart-mag/wp-content/uploads/2013/07/5362055586_611f9563d8_b-351x185.jpg" class="attachment-main-block wp-post-image no-display appear" alt="5362055586_611f9563d8_b" title="Mesmerizing View of a Perfect Moonlit Night" itemprop="image" width="351" height="185">--}}
                                <a href="{{ url('article/'.$article->slug) }}" itemprop="url"><img src="{{ Protocol::home() }}/uploads/articles/{{ $article->cover }}" class="attachment-main-block wp-post-image no-display appear" alt="{{ $article->title }}" title="{{ $article->title }}" itemprop="image" width="351" height="185">
                                    {{--<img src="" width="324" height="160">--}}



                                </a>

                                <div class="content">

{{--                                    <time datetime="{{ $article->created_at }}" itemprop="datePublished">{{ $article->created_at->diffForHumans() }} </time>--}}

                                    <span class="comments"><a href="{{ url('article/'.$article->slug) }}"><i class="fa fa-comments-o"></i>
						0</a></span>

                                    <a href="{{ url('article/'.$article->slug) }}" title="Mesmerizing View of a Perfect Moonlit Night" itemprop="name headline url">
                                        {{ $article->title }}</a>


                                    <div class="excerpt">
                                        <p>
                                            {{ str_limit(strip_tags($article->content), 100) }}
                                        </p>
                                        @if (strlen(strip_tags($article->content)) > 100)

                                            <div class="read-more"><a href="{{ url('article/'.$article->slug) }}" title="Read More">Read More</a></div>
                                        @endif


                                    </div>

                                </div>


                            </article>


                        @endforeach




                        {{ $articles->links() }}


                    </div>



                </div>

            </div>





            <aside class="col-4 sidebar">
                <ul>


                    <li id="bunyad-tabbed-recent-widget-10" class="widget tabbed">
                        <ul class="tabs-list">


                            <li class="tab_class">
                                <a href="#" data-tab="1">Popular</a>
                            </li>


                        </ul>

                        <div class="tabs-data">

                            <ul class="tab-posts active posts-list" id="recent-tab-1">
                                @if(isset($latests))

                                    @foreach($latests as $latest)
                                        <li>

                                            <a href="{{ url('article/'.$latest->slug) }}"><img src="{{ Protocol::home() }}/uploads/articles/{{ $latest->cover }}" class="attachment-post-thumbnail wp-post-image no-display appear" alt="{{ $latest->title }}" title="{{ $latest->title }}" width="110" height="96">

                                            </a>

                                            <div class="content">

                                                <time datetime="2014-12-16T15:57:52+00:00">Dec 16, 2014 </time>


                                                <a href="{{ url('article/'.$latest->slug) }}" title="{{ $latest->title }}">
                                                    {{ $latest->title }}</a>


                                            </div>

                                        </li>
                                    @endforeach
                                @endif



                            </ul>


                        </div>

                    </li>




                </ul>
            </aside>




        </div>

    </div>
@endsection