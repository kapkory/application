@extends (Theme::get().'.layout.app')

@section('styles')

    <link class="rs-file" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/royalslider.css?v=1.2" rel="stylesheet">
    <link href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/skins/default/rs-default.css?v=1.2" rel="stylesheet">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/extras/starability-growRotate.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/emojionearea/emojionearea.css" media="screen">
    <!--<link rel="stylesheet" type="text/css" href="http://mervick.github.io/lib/google-code-prettify/skins/tomorrow.css" media="screen">-->
    
    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/mervick-tommorow.css" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/article.css" >


    <style type="text/css">
        .entry-author {
            padding: 2em 0;
        }
        .flex-row {
            display: flex;
            width: 100%;
        }
        .circle {
            border-radius: 999px !important;
            object-fit: cover;
        }
        .mr {
            margin-right: 30px;
        }
        .flex-col {
            max-height: 100%;
        }
        #outer
        {
            width:100%;
            text-align: center;
        }
        .inner
        {
            display: inline-block;
        }

        .td-category {
            list-style: none;
            font-family: 'Open Sans', arial, sans-serif;
            font-size: 10px;
            margin-top: 0;
            margin-bottom: 10px;
            line-height: 1;
        }
        ul, ol {
            padding: 0;
        }
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .td-category li {
            display: inline-block;
            margin: 0 5px 5px 0;
            line-height: 1;
        }
        .td-category a {
            color: #fff;
            background-color: blue;
            padding: 3px 6px 4px 6px;
            white-space: nowrap;
            display: inline-block;
        }


        .author-img {
            float: left;
            margin-right: 20px;
            border-radius: 100%;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            overflow: hidden;
        }

        .post-author {
            margin: 0;
            overflow: hidden;
            padding: 38px 0 37px 0;
            border-top: 1px solid #E0E0E0;
        }
        .post-author .author-img img {
            margin: 0;
        }
        img {
            max-width: 100%;
            vertical-align: top;
            height: auto;
        }
        fieldset, img {
            border: 0;
        }
        *, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td {
            margin: 0;
            padding: 0;
        }

        .post-author .author-content {
            margin-left: 120px;
        }
        *, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td {
            margin: 0;
            margin-left: 0px;
            padding: 0;
        }

        .author_avatar {
            margin: 0 30px 19px 0;
            width: 100px;
            height: 100px;
        }
        .author-content p {
            margin-bottom: 16px;
        }

        .author-content h5 a {
            display: inline-block;
            margin: 0;
            color: #313131;
        }

        * {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
        }
        .penci-single-link-pages {
            display: block;
            width: 100%;
        }

        *, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td {
            margin: 0;
            padding: 0;
        }
        .post-tags {
            display: block;
            position: relative;
            z-index: 10;
            color: #888;
            margin-bottom: 0;
            line-height: 1.4;
            margin-top: 31px;
        }
        .post-tags a {
            text-transform: uppercase;
            color: #888;
            padding: 6px 12px 5px;
            margin-right: 8px;
            margin-bottom: 8px;
            display: inline-block;
            font-size: 11px !important;
            background: none;
            border: 1px solid #DEDEDE;
            transition: all 0.3s;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            outline: none;
            font-weight: normal;
            line-height: 1.2;
        }
        .post-entry a, .container-single .post-entry a {
            color: #e91e63;
        }

        .container-single .penci-standard-cat .cat > a.penci-cat-name {

            color: #e91e63;

        }
        .penci-standard-cat .cat > a.penci-cat-name {

            color: #e91e63;

        }
        .cat > a.penci-cat-name:last-child {

            margin-right: 0;
            padding: 0;

        }
        .cat > a.penci-cat-name {

            font-size: 13px;
            color: #6eb48c;
            line-height: 1.2;
            margin: 0 18px 0 0;
            margin-right: 18px;
            margin-bottom: 0px;
            padding-right: 10px;
            display: inline-block;
            vertical-align: top;
            background: none;
            transition: all 0.3s;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            font-weight: normal;
            margin-bottom: 5px;
            position: relative;
            text-decoration: none;

        }

        .breadcrumb a:hover{
            background-color: grey;
        }
        .inner-post-entry{
            font-size: 14px;
        }

    </style>
@endsection

@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('content')


    <div class="container container-single penci_sidebar right-sidebar penci-enable-lightbox">
        <div id="main">
            <div  class="theiaStickySidebar"  >
                <!-- Add this code to article page: -->
                <article id="post-834" class="post-834 post type-post status-publish format-gallery has-post-thumbnail hentry category-cardio category-featured tag-blog tag-cardio tag-fitness post_format-post-format-gallery" >
                    <hr>
                    <div class="col-sm-12">
                        @if (Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        @if (Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                    </div>

                    <ul class="breadcrumb">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('article-category/'.$article->articleCategory->category_slug) }}">{{ $article->articleCategory->name }}</a></li>
                        <li><a  href="{{ url('article/'.$article->slug) }}">{{ $article->title }}</a></li>

                    </ul>
                    {{--<hr>--}}
                    <div >
                        <div class="header-standard header-classic single-header">
                            <ul class="td-category" style="float: left">
                                <li class="entry-category"><a href="{{ url('article-category/'.$article->articleCategory->category_slug) }}">{{ $article->articleCategory->name }}</a></li>
                            </ul>


                            <h1 class="post-title single-post-title" >{{ $article->title }}</h1>

                            <div class="post-box-meta-single">
							<span class="author-post" >
								<span >
									@if($article->user->id ==1)
                                        Written by <a class="author-url"  href='https://plus.google.com/+VenkatmaniM'>{{ $article->user->first_name }}</a>
                                    @else
                                        Written by <a class="author-url"  href='#'>{{ $article->user->first_name    }}</a>
                                    @endif
								</span>
							</span>
                                {{--<span itemprop="datePublished">{{ $article->created_at }}</span>--}}
                            </div>
                        </div>

                        {{--<meta itemprop='isFamilyFriendly' content='True'/>--}}



                        {{--<div class="post-image">--}}
                        {{--<div class="penci-owl-carousel penci-owl-carousel-slider penci-nav-visible owl-loaded owl-drag" data-auto="true" data-lazy="true">--}}






                        {{--<div class="owl-stage-outer owl-height" style="height: 550px;">--}}
                        {{--<div class="owl-stage" style="transform: translate3d(-1560px, 0px, 0px); transition: all 0.6s ease 0s; width: 4680px;">--}}
                        {{--<div class="owl-item cloned" style="width: 780px;">--}}
                        {{--<figure>--}}
                        {{--<img class="owl-lazy" data-src="http://cdn-soledad.pencidesign.com/soledad-fitness-blog/wp-content/uploads/sites/39/2017/07/cardio-1170x825.jpg" src="http://cdn-soledad.pencidesign.com/soledad-fitness-blog/wp-content/uploads/sites/39/2017/07/cardio-1170x825.jpg" style="opacity: 1;">--}}
                        {{--</figure>--}}
                        {{--</div>--}}
                        {{--<div class="owl-item cloned" style="width: 780px;"><figure>--}}
                        {{--<img class="owl-lazy" data-src="http://cdn-soledad.pencidesign.com/soledad-fitness-blog/wp-content/uploads/sites/39/2017/07/gallery-1170x775.jpg" src="http://cdn-soledad.pencidesign.com/soledad-fitness-blog/wp-content/uploads/sites/39/2017/07/gallery-1170x775.jpg" style="opacity: 1;">--}}
                        {{--</figure></div>--}}
                        {{--<div class="owl-item active" style="width: 780px;">--}}
                        {{--<figure>--}}
                        {{--<img class="owl-lazy"  data-src="{{ Protocol::home() }}/uploads/articles/{{ $article->cover }}" src="{{ Protocol::home() }}/uploads/articles/{{ $article->cover }}" style="opacity: 1;">--}}
                        {{--</figure>--}}
                        {{--</div>--}}

                        {{--</div></div>--}}
                        {{--</div>--}}
                        {{--</div>--}}




                        <div class="post-entry blockquote-style-2">
                            <div class="inner-post-entry" >
                                {!! $article->content !!}


                                <div class="penci-single-link-pages">
                                </div>

                                <div class="post-tags" >
                                    @if($tags)
                                        @foreach($tags as $tag)
                                            <a  href="{{  url('article-tag/'.$tag->name)  }}" rel="tag">{{ $tag->name }}</a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="tags-share-box center-box">

                            <span class="single-comment-o"><i class="fa fa-comment-o"></i>{{ $comment_count }} comments</span>

                            <div class="post-share">
                                {{--<span class="count-number-like">2</span><a class="penci-post-like single-like-button" data-post_id="834" title="Like" data-like="Like" data-unlike="Unlike"><i class="fa fa-heart-o"></i></a>					<div class="list-posts-share">--}}
                                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}"><i class="fa fa-facebook"></i><span class="dt-share">Facebook</span></a>
                                <a target="_blank" href="https://twitter.com/intent/tweet?text={{ url()->current() }}"><i class="fa fa-twitter"></i><span class="dt-share">Twitter</span></a>
                                <a target="_blank" href="https://plus.google.com/share?url={{ url()->current() }}"><i class="fa fa-google-plus"></i><span class="dt-share">Google +</span></a>
                                <a data-pin-do="none" target="_blank" href="https://pinterest.com/pin/create/button/?url={{ url()->current() }}&amp;media={{ Protocol::home() }}/uploads/articles/{{ $article->cover }}&amp;description={{ $article->title }}"><i class="fa fa-pinterest"></i><span class="dt-share">Pinterest</span></a>
                            </div>
                            {{--</div>--}}
                        </div>



                        <div class="post-author" >
                            <div class="author-img"  >
                                <img alt="{{ $article->user->first_name. ' '.$article->user->last_name }}" src="{{ ($article->user->profile_image) ? url($article->user->profile_image) : url('uploads/avatars/noavatar.png') }}" class="avatar avatar-100 photo" width="100" height="100">
                                <meta  content="{{ ($article->user->profile_image) ? url($article->user->profile_image) : url('uploads/avatars/noavatar.png') }}">

                            </div>
                            <div class="author-content">
                                <h5><a href="#" title="Posts by {{ $article->user->first_name }}" rel="author"><span >{{ $article->user->first_name }}</span></a></h5>

                                <p>{{ $article->user->bio }}</p>

                                <meta  content="SanitaryWare.org">
                                <a target="_blank" class="author-social" href="http://facebook.com/#"><i class="fa fa-facebook"></i></a>
                                <a target="_blank" class="author-social" href="http://twitter.com/#"><i class="fa fa-twitter"></i></a>
                                <a target="_blank" class="author-social" href="http://plus.google.com/#?rel=author"><i class="fa fa-google-plus"></i></a>
                                <a target="_blank" class="author-social" href="http://instagram.com/#"><i class="fa fa-instagram"></i></a>
                                <a target="_blank" class="author-social" href="http://pinterest.com/#"><i class="fa fa-pinterest"></i></a>
                                <a target="_blank" class="author-social" href="http://#.tumblr.com/"><i class="fa fa-tumblr"></i></a>
                            </div>
                        </div>



                        @include('themes.default.blog.comment',['comments'=>$comments,'article'=>$article,'comment_count'=>$comment_count])



                        <script type="text/javascript">
                            $('input[name="article_id"]').val({{ $article->id }});

                            $('.display_reply').click(function () {
//                            alert('i have been hit');
                                var data = $(this).data('comment-id');
                                $('input[name="parent_id"]').val(data);

//                            $('#comment_reply').show();

                                var html ='<div id="respond" class="comment-respond">'+
                                    '<h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" href="">Cancel Reply</a></small></h3>'
                                    +'<form action="{{  Protocol::home() }}/articles/comments/reply" method="post" id="commentform" class="comment-form">'
                                    +'<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" placeholder="Your Comment" aria-required="true"></textarea></p>'
                                        @if(!Auth::check())
                                    +' <p class="comment-form-author"><input id="name" name="name" value="" placeholder="Name*" size="30" aria-required="true" type="text"></p>'
                                    +'<p class="comment-form-email"><input id="email" name="email" value="" placeholder="Email*" size="30" aria-required="true" type="text"></p>'
                                        @endif
                                    +' <p class="comment-form-url">{{ csrf_field() }}<p>'
                                    +'<p><input type="hidden" name="parent_id" value="'+data+'"></p>'
                                    +'<p><input type="hidden" name="article_id" value="{{ $article->id }}"></p>'
                                    +' <p class="form-submit"><input name="submit" id="submit" class="submit" value="Submit" type="submit">'
//                               + ' <input name="comment_post_ID" value="834" id="comment_post_ID" type="hidden">'
//                               +' <input name="comment_parent" id="comment_parent" value="59" type="hidden">'
                                    +'</p></form>'
                                    +' </div>';
                                var app = "reply_page_"+data;
//                            document.getElementById(app).append(html);
                                $("#"+app).append(html);
                            });



                        </script>




                    </div>
                </article>

            </div>
        </div>

        <div id="sidebar" class="penci-sidebar-content style-9 pcalign-center">
            <div class="theiaStickySidebar">
                <aside id="penci_about_widget-2" class="widget penci_about_widget"><h4 class="widget-title penci-border-arrow"><span class="inner-arrow">About Me</span></h4>
                    <div class="about-widget pc_aligncenter">
                        {{--<img class="penci-widget-about-image holder-square penci-lazy" src="http://cdn-soledad.pencidesign.com/soledad-fitness-blog/wp-content/uploads/sites/39/2017/07/aboutava.jpg" alt="About Me" style="border-radius: 50%; display: inline;">--}}

                        <ul class="list-inline no-margin">
                            <li>
                                <a href="https://www.facebook.com/sharer.php?u={{ Protocol::home() }}/article/{{ $article->slug }}" target="_blank" class="btn bg-green btn-icon content-group">
                                    <i class="icon-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/share?url={{ Protocol::home() }}/article/{{ $article->slug }}&text={{ $article->title }}" target="_blank" class="btn bg-green btn-icon content-group">
                                    <i class="icon-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/share?url={{ Protocol::home() }}/article/{{ $article->slug }}" target="_blank" class="btn bg-green btn-icon content-group">
                                    <i class="icon-google-plus"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.stumbleupon.com/submit?url={{ Protocol::home() }}/article/{{ $article->slug }}&title={{ $article->title }}" target="_blank" class="btn bg-green btn-icon content-group">
                                    <i class="icon-stumbleupon"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </aside>

            </div>
        </div>
        <!-- END CONTAINER -->
    </div>

    <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Article",
  "headline": "{{ $article->title }}",
  "mainEntityOfPage": "{{ url()->current() }}",
  "datePublished": "{{ $article->created_at }}",
  "dateModified": "{{ Carbon\Carbon::now() }}",
  "description": "{{ $article->meta_description }}",
 "articleBody": "{!! $article->content !!}",
  "image": {
    "@type": "ImageObject",
    "url": " {{ url('uploads/articles/'.$article->cover) }}"
  },
  "author": "{{ $article->user->first_name }}",
  "publisher": {
    "@type": "Organization",
    "logo": {
      "@type": "ImageObject",
      "url": "{{ url('uploads/settings/logo/logo.png') }}"
    },
    "name": "SanitaryWare"
  }

 }
</script>
@endsection