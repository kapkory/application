
<div id="respond" class="comment-respond">
    <h3 id="reply-title" class="comment-reply-title"><span>Leave a Comment</span> <small>
            <a rel="nofollow" id="cancel-comment-reply-link" href="#" style="">Cancel Reply</a></small></h3>

    <form action="{{  Protocol::home() }}/articles/comments/reply" method="POST" id="comment_form" class="comment-form">

        {!! app('captcha')->render('en'); !!}
        
        <p class="comment-form-comment">
            <textarea id="comment_r" name="comment" cols="45" rows="8" placeholder="Your Comment" aria-required="true" required></textarea>
        </p>
        <input type="hidden" name="parent_id" value="0">
        <input type="hidden" name="article_id" value="{{ $article->id }}">




        {!! csrf_field() !!}
        @if (!Auth::check())
            <p class="comment-form-author">
                <input id="author" name="name" value="" placeholder="Name*" size="30" aria-required="true" type="text">
            </p>
            <p class="comment-form-email">
                <input id="email" name="email" value="" placeholder="Email*" size="30" aria-required="true" type="email">
            </p>
        @endif

        <p class="form-submit">
            <input class="comment_submit" name="submitForm" value="submit"  type="submit">

            <br/>
        </p>

    </form>

</div><!-- #comment-## -->

<div class="post-comments" id="comments">
    <div class="post-title-box">@if($comment_count > 0)
         <span>  {{ $comment_count }} comments on this article, let us know what you think about  {{ $article->title }}</span>

                                    @else
            <span>Be the first one to comment on  {{ $article->title }}</span>

                                 @endif
        </div>
    <div class="comments">
        @if($comments)
            @foreach($comments as $comment)
                <div class="comment byuser comment-author-sanitryware bypostauthor even thread-even depth-1" id="{{ url()->current() }}#commentid-{{ $comment->id }}" itemscope itemtype="http://schema.org/UserComments">
                    <div class="thecomment">
                        <div class="author-img">
                            @if (Profile::hasStore($comment->user_id))
                                <img alt="{{ Profile::hasStore($comment->user_id) }}" src="{{ url(Profile::comment_picture($comment->user_id)) }}" class="avatar avatar-100 photo" width="100" height="100">
                        </div>


                        @elseif($comment->user_id == 0)
                            <img alt="{{ $comment->name }}" src="{{ url('uploads/avatars/noavatar.png') }}" class="avatar avatar-100 photo" width="100" height="100">
                    </div>



                    @else
                        <img alt="{{ Profile::first_name($comment->user_id) }}" src="{{ Profile::picture($comment->user_id) }}" class="avatar avatar-100 photo" width="100" height="100">
                </div>


                @endif

                <div class="comment-text">
                    @if($comment->user_id == 0)
                        <span itemprop="creator"  itemscope itemtype="http://schema.org/Person" class="author"><a href="#commentid-{{ $comment->id }}" rel="external nofollow" class="url"><span itemprop="name">{{ $comment->name }}</span></a></span><span class="{{ Profile::hasStore($comment->user_id) ? 'trusted-badge' : '' }} label label-primary label-rounded"></span>
                        <a href="#" class="{{ Profile::hasStore($comment->user_id) ? 'trusted-badge' : '' }} label label-primary label-rounded"></a>
                    @else
                        <span itemprop="creator"  itemscope itemtype="http://schema.org/Person"  class="author"><a href="#commentid-{{ $comment->id }}" rel="external nofollow" class="url " ><span itemprop="name">{{ Profile::hasStore($comment->user_id) ? Profile::first_name($comment->user_id) : Profile::first_name($comment->user_id) }}</span></a></span>

                    @endif

                    <div id="commentid-{{ $comment->id }}" class="comment-content" itemprop="commentText"><p> {!! nl2br($comment->content) !!}</p>
                    </div>
                    <span class="reply">
						<a rel="nofollow" class="display_reply comment-reply-link"  data-comment-id="{{ $comment->id }}"  aria-label="Reply">Reply</a>

                    </span>
                </div>
    </div>


    <p id="{{ 'reply_page_'.$comment->id }}">
        {{--<div class="reply_page">--}}
    </p>



    @if ($comment->children->count() > 0)

        @foreach ($comment->children as $comment)

            @include('themes.default.blog.comment_replies', ['comment' => $comment])

        @endforeach
    @endif



</div><!-- #comment-## -->
@endforeach
@endif




</div>
<div id="comments_pagination"></div>	<div id="wp-temp-form-div" style="display: none;"></div><!-- #respond -->
</div> <!-- end comments div -->