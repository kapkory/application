@extends (Theme::get().'.layout.app')

@section('styles')
    <style type="text/css">
        .entry-author {
            padding: 2em 0;
        }
        .flex-row {
            display: flex;
            width: 100%;
        }
        .circle {
            border-radius: 999px !important;
            object-fit: cover;
        }
        .mr {
            margin-right: 30px;
        }
        .flex-col {
            max-height: 100%;
        }
        #outer
        {
            width:100%;
            text-align: center;
        }
        .inner
        {
            display: inline-block;
        }

        .td-category {
            list-style: none;
            font-family: 'Open Sans', arial, sans-serif;
            font-size: 10px;
            margin-top: 0;
            margin-bottom: 10px;
            line-height: 1;
        }
        ul, ol {
            padding: 0;
        }
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .td-category li {
            display: inline-block;
            margin: 0 5px 5px 0;
            line-height: 1;
        }
        .td-category a {
            color: #fff;
            background-color: blue;
            padding: 3px 6px 4px 6px;
            white-space: nowrap;
            display: inline-block;
        }
    </style>
    <link class="rs-file" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/royalslider.css?v=1.2" rel="stylesheet">
    <link href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/skins/default/rs-default.css?v=1.2" rel="stylesheet">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/extras/starability-growRotate.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/emojionearea/emojionearea.css" media="screen">
<!--    <link rel="stylesheet" type="text/css" href="http://mervick.github.io/lib/google-code-prettify/skins/tomorrow.css" media="screen">-->

    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/mervick-tommorow.css" media="screen">

@endsection

@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('content')

    <div class="row">
        <div class="col-md-9">

            <!-- Post -->
            <div class="panel">
                <div class="panel-body">
                    <div class="content-group-lg">
                        <ul class="td-category">
                            <li class="entry-category"><a href="{{ url('article-category/'.$article->articleCategory->category_slug) }}">{{ $article->articleCategory->name }}</a></li>
                        </ul>
                        {{--<a class="btn btn-primary" href="">Category</a> --}}

                        <h3 class="text-semibold mb-5">
                            <a href="#" class="text-default">{{ $article->title }}</a>
                        </h3>



                        <ul class="list-inline list-inline-separate text-muted content-group">
                            <li>By <a href="#" class="text-muted">{{ Profile::full_name_by_username($article->username) }}</a></li>
                            <li>{{ Helper::date_string($article->created_at) }}</li>
                        </ul>
                        <div class="content-group text-center">
                            <a href="#" class="display-inline-block">
                                <img src="{{ Protocol::home() }}/uploads/articles/{{ $article->cover }}" class="img-responsive" alt="">
                            </a>
                        </div>


                        <div class="content-group">
                            {!! $article->content !!}
                        </div>

                        <br>
                        <br>
                        <br>
                        <br>

                        {{--<div class="col-md-9">--}}
                        {{--{{ Profile::user_bio($article->username) }}--}}
                        {{--</div>--}}

                        <div class="flex-row align-top">
                            <div class="flex-col mr circle">
                                <div class="blog-author-image">

                                    <img src="{{ url(Profile::user_picture_by_username($article->username)) }}" alt="{{ Profile::full_name_by_username($article->username) }}" class="avatar avatar-90 wp-user-avatar wp-user-avatar-90 alignnone photo" width="90" height="90">
                                </div>
                            </div><!-- .flex-col -->
                            <div class="flex-col flex-grow">
                                <h3 class="author-name uppercase pt-half">
                                    {{ Profile::full_name_by_username($article->username) }}
                                </h3>
                                <p class="author-desc small">{{ Profile::user_bio($article->username) }}</p>
                            </div><!-- .flex-col -->
                        </div>

                        <div class="container">
                            @if($tags)

                                @foreach($tags as $tag)

                                    <a class="btn btn-default btn-xs" href="{{  url('article-tag/'.$tag->name)  }}">{{ $tag->name }}</a>
                                @endforeach
                            @endif
                        </div>
                    </div>

                </div>



            </div>
            <!-- /post -->

        </div>


        <div class="col-md-3">

            <div class="sidebar sidebar-default sidebar-separate">
                <div class="sidebar-content">


                    <!-- Share -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            {{--						<img src="{{ Profile::user_picture_by_username($article->username) }}" class="img-circle" alt="Author Picture" width="186" height="142">--}}

                            <span>Share</span>

                        </div>

                        <div class="category-content no-padding-bottom text-center">
                            <ul class="list-inline no-margin">
                                <li>
                                    <a href="https://www.facebook.com/sharer.php?u={{ Protocol::home() }}/blog/{{ $article->slug }}" target="_blank" class="btn bg-green btn-icon content-group">
                                        <i class="icon-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/share?url={{ Protocol::home() }}/blog/{{ $article->slug }}&text={{ $article->title }}" target="_blank" class="btn bg-green btn-icon content-group">
                                        <i class="icon-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/share?url={{ Protocol::home() }}/blog/{{ $article->slug }}" target="_blank" class="btn bg-green btn-icon content-group">
                                        <i class="icon-google-plus"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.stumbleupon.com/submit?url={{ Protocol::home() }}/blog/{{ $article->slug }}&title={{ $article->title }}" target="_blank" class="btn bg-green btn-icon content-group">
                                        <i class="icon-stumbleupon"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /share -->

                </div>
            </div>

            <!-- Advertisements -->
            @if (Helper::ifCanSeeAds())
                <div class="advertisment">
                    {!! Helper::advertisements()->ad_sidebar !!}
                </div>
            @endif

        </div>
    </div>


    <div class="row">
        <div class="col-md-9">


            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title text-semiold">Discussion<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                    <div class="heading-elements">
                        <ul class="list-inline list-inline-separate heading-text text-muted">
                            <li>{{ count($comments) }} Comment</li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">


                    @if (count($comments))
                        <div id="commentScroll">

                            <ul class="media-list content-group-lg stack-media-on-mobile">

                                @foreach($comments as $comment)

                                    @include('themes.default.blog.comment',['comment'=>$comment])

                                    <hr>
                                @endforeach

                                <div style="display: none;">
                                    {{--									{{ $comments->links() }}--}}
                                </div>

                            </ul>

                        </div>
                        <ul class="media-list content-group-lg stack-media-on-mobile">
                            <div id="newComment"></div>
                        </ul>




                        <script type="text/javascript">


                            $('.display_reply').click(function () {
                                var data = $(this).data('comment-id');
                                $('input[name="parent_id"]').val(data);
                                $('input[name="article_id"]').val({{ $article->id }});
//       console.log('data is '+data + 'or is ' + $(this).attr('data-comment-id'));
                                $('#replyComment').show();


                            });
                        </script>
                    @else
                        <div id="hideNoCommentsNotice" class="alert bg-info alert-styled-left">
                            @lang ('return/info.lang_no_comments_right_now')
                        </div>
                        <div id="newComment"></div>
                    @endif
                </div>



                <div class="panel-body">

                    <p class="text-muted text-right-rtl">Add comment - spam and offensive comments will be removed</p>

                    <form action="{{  Protocol::home() }}/articles/comments/create" method="POST" id="createArticleComment" >


                        <div class="content-group" id="spinnerDark">
                            <textarea rows="5" cols="5" class="form-control" placeholder="{{ Lang::get('ads/show.lang_add_comment_placeholder') }}" name="comment" id="commentContent"></textarea>
                        </div>
                        <input type="hidden" name="article_id" value="{{ $article->id }}">
                        @if (!Auth::check())
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Name *</label>
                                    <input type="text" name="name" class="form-control">
                                </div>

                                <div class="col-md-3">
                                    <label>Email *</label>
                                    <input type="email" name="email" class="form-control">
                                </div>

                                <div class="col-md-3">
                                    <label>Website</label>
                                    <input type="text" name="website" class="form-control">
                                </div>
                            </div>
                        @endif
                        {!! csrf_field() !!}

                        <div class="text-right">
                            <button type="submit" class="btn bg-blue legitRipple" id="spinner-dark-6"><i id="spinnerIcon" style="display: none;" class="icon-spinner4 spinner position-left"></i> Add comment
                            </button>
                        </div>

                    </form>




                </div>
            </div>



        </div>
    </div>

@endsection