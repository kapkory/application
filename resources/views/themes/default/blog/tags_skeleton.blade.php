@extends (Theme::get().'.layout.app')

@section('styles')
    <style type="text/css">
        .entry-author {
            padding: 2em 0;
        }
        .flex-row {
            display: flex;
            width: 100%;
        }
        .circle {
            border-radius: 999px !important;
            object-fit: cover;
        }
        .mr {
            margin-right: 30px;
        }
        .flex-col {
            max-height: 100%;
        }

    </style>
    <link class="rs-file" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/royalslider.css?v=1.2" rel="stylesheet">
    <link href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/skins/default/rs-default.css?v=1.2" rel="stylesheet">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/extras/starability-growRotate.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/emojionearea/emojionearea.css" media="screen">
<!--    <link rel="stylesheet" type="text/css" href="http://mervick.github.io/lib/google-code-prettify/skins/tomorrow.css" media="screen">-->

    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/mervick-tommorow.css" media="screen">

@endsection

@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('content')
    <div class="col-md-12">
        <div class="row">

            @foreach($articles as $article)


                {{--{{ $article }}--}}
                <div class="col-md-4">

                    <figure class="snip1447 hover">

                        <div class="store-cover">
                            <img src="{{ Protocol::home() }}/application/public/uploads/articles/{{ $article->cover }}" alt="{{ $article->title }}" class="profile">
                        </div>
                        <div class="img card-ad-cover"  title="new product">
                            <img src="{{ Protocol::home() }}/uploads/articles/{{ $article->cover }}" width="324" height="160">

                        </div>

                        <figcaption>


                            <h2>{{ $article->title }}
                                <span>{{ $article->category_name }}</span>
                            </h2>

                            <p></p><a href="{{ url('article/'.$article->slug) }}" class="info" style="width: 100%">More Info</a>
                        </figcaption>
                    </figure>

                </div>

            @endforeach
        </div>

    </div>


@endsection