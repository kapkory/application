<div class="comment byuser comment-author-sanitaryware bypostauthor odd alt depth-2" id="{{ url()->current() }}#commentid-{{ $comment->id }}" itemscope itemtype="http://schema.org/UserComments">
<div class="thecomment">
<div class="author-img">
    @if (Profile::hasStore($comment->user_id))
        <img alt="{{ Profile::hasStore($comment->user_id) }}" src="{{ url(Profile::comment_picture($comment->user_id)) }}" class="avatar avatar-100 photo" width="100" height="100">

    @elseif($comment->user_id == 0)
        <img alt="{{ $comment->name }}" src="{{ url('uploads/avatars/noavatar.png') }}" class="avatar avatar-100 photo" width="100" height="100">

    @else
        <img alt="{{ Profile::first_name($comment->user_id) }}" src="{{ Profile::picture($comment->user_id) }}" class="avatar avatar-100 photo" width="100" height="100">


@endif
</div>
<div class="comment-text">
    @if($comment->user_id == 0)
        <span itemprop="creator" itemscope itemtype="http://schema.org/Person" class="author"><a href="#commentid-{{ $comment->id }}" rel="external nofollow" class="url" ><span itemprop="name">{{ $comment->name }}</span></a></span><span class="{{ Profile::hasStore($comment->user_id) ? 'trusted-badge' : '' }} label label-primary label-rounded"></span>
    @else
        <span itemprop="creator" itemscope itemtype="http://schema.org/Person" class="author"><a href="#commentid-{{ $comment->id }}" rel="external nofollow" class="url" ><span itemprop="name">{{ Profile::hasStore($comment->user_id) ? Profile::first_name($comment->user_id) : Profile::first_name($comment->user_id) }}</span></a></span>
    @endif

<div id="commentid-{{ $comment->id }}" class="comment-content" itemprop="commentText"><p> {!! nl2br($comment->content) !!}</p>
</div>
<span class="reply">
  <a rel="nofollow" class="display_reply comment-reply-link"  data-comment-id="{{ $comment->id }}">Reply</a>

</span>
</div>
</div>
</div><!-- #comment-## -->

<p id="{{ 'reply_page_'.$comment->id }}">
</p>

@if ($comment->children->count() > 0)
        {{--<a  id="replies">Replies</a>--}}
        @foreach ($comment->children as $comment)
                {{--<div style="margin-left: 50px; " class="replies">--}}
                @include('themes.default.blog.comment_replies', ['comment' => $comment])
                {{--</div>--}}
        @endforeach
@endif