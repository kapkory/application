@extends (Theme::get().'.layout.app')

@section('styles')

    <link class="rs-file" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/royalslider.css?v=1.2" rel="stylesheet">
    <link href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/slider/skins/default/rs-default.css?v=1.2" rel="stylesheet">
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/extras/starability-growRotate.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/js/plugins/emojionearea/emojionearea.css" media="screen">
<!--    <link rel="stylesheet" type="text/css" href="http://mervick.github.io/lib/google-code-prettify/skins/tomorrow.css" media="screen">-->

    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/css/mervick-tommorow.css" media="screen">
    {{--<link rel="stylesheet" type="text/css" href="http://cdn-soledad.pencidesign.com/soledad-fitness-blog/wp-content/themes/soledad/style.css?ver=4.1" >--}}


    <style type="text/css">
        .entry-author {
            padding: 2em 0;
        }
        .flex-row {
            display: flex;
            width: 100%;
        }
        .circle {
            border-radius: 999px !important;
            object-fit: cover;
        }
        .mr {
            margin-right: 30px;
        }
        .flex-col {
            max-height: 100%;
        }
        #outer
        {
            width:100%;
            text-align: center;
        }
        .inner
        {
            display: inline-block;
        }

        .td-category {
            list-style: none;
            font-family: 'Open Sans', arial, sans-serif;
            font-size: 10px;
            margin-top: 0;
            margin-bottom: 10px;
            line-height: 1;
        }
        ul, ol {
            padding: 0;
        }
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .td-category li {
            display: inline-block;
            margin: 0 5px 5px 0;
            line-height: 1;
        }
        .td-category a {
            color: #fff;
            background-color: blue;
            padding: 3px 6px 4px 6px;
            white-space: nowrap;
            display: inline-block;
        }


        .author-img {
            float: left;
            margin-right: 20px;
            border-radius: 100%;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            overflow: hidden;
        }

        .post-author {
            margin: 0;
            overflow: hidden;
            padding: 38px 0 37px 0;
            border-top: 1px solid #E0E0E0;
        }
        .post-author .author-img img {
            margin: 0;
        }
        img {
            max-width: 100%;
            vertical-align: top;
            height: auto;
        }
        fieldset, img {
            border: 0;
        }
        *, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td {
            margin: 0;
            padding: 0;
        }

        .post-author .author-content {
            margin-left: 120px;
        }
        *, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td {
            margin: 0;
            margin-left: 0px;
            padding: 0;
        }

        .author_avatar {
            margin: 0 30px 19px 0;
            width: 100px;
            height: 100px;
        }
        .author-content p {
            margin-bottom: 16px;
        }

        .author-content h5 a {
            display: inline-block;
            margin: 0;
            color: #313131;
        }

        * {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
        }
        .penci-single-link-pages {
            display: block;
            width: 100%;
        }

        *, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td {
            margin: 0;
            padding: 0;
        }
        .post-tags {
            display: block;
            position: relative;
            z-index: 10;
            color: #888;
            margin-bottom: 0;
            line-height: 1.4;
            margin-top: 31px;
        }
        .post-tags a {
            text-transform: uppercase;
            color: #888;
            padding: 6px 12px 5px;
            margin-right: 8px;
            margin-bottom: 8px;
            display: inline-block;
            font-size: 11px !important;
            background: none;
            border: 1px solid #DEDEDE;
            transition: all 0.3s;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            outline: none;
            font-weight: normal;
            line-height: 1.2;
        }
        .post-entry a, .container-single .post-entry a {
            color: #e91e63;
        }



    </style>
@endsection

@section ('seo')

    {!! SEO::generate() !!}

@endsection

@section ('content')

    <div class="row">
    <div class="col-md-9">

    <!-- Post -->
    <div class="panel">
    <div class="panel-body">
    <div class="content-group-lg">
    <ul class="td-category">
    <li class="entry-category"><a href="{{ url('article-category/'.$article->articleCategory->category_slug) }}">{{ $article->articleCategory->name }}</a></li>
    </ul>
    {{--<a class="btn btn-sm btn-primary" href="">Category</a>--}}

    <h3 class="text-semibold mb-5">
    <a href="#" class="text-default">{{ $article->title }}</a>
    </h3>



    <ul class="list-inline list-inline-separate text-muted content-group">
    <li>By <a href="#" class="text-muted">{{ Profile::full_name_by_username($article->username) }}</a></li>
    <li>{{ Helper::date_string($article->created_at) }}</li>
    </ul>
    <div class="content-group text-center">
    <a href="#" class="display-inline-block">
    <img src="{{ Protocol::home() }}/uploads/articles/{{ $article->cover }}" class="img-responsive" alt="">
    </a>
    </div>


    <div class="content-group">
    {!! $article->content !!}
    </div>


    <div class="penci-single-link-pages">
    </div>
    <div class="container">
    @if($tags)
    <div class="post-tags">
    @foreach($tags as $tag)
    <a href="{{  url('article-tag/'.$tag->name)  }}" rel="tag">{{ $tag->name }}</a>

    @endforeach
    </div>

    @endif
    </div>


    <div class="post-author">
    <div class="author-img">
    <img alt="{{ Profile::full_name_by_username($article->username) }}" src="{{ url(Profile::user_picture_by_username($article->username)) }}" class="author_avatar" width="100" height="100">	</div>
    <div class="author-content">
    <h5><a href="#" title="Posts by {{ Profile::full_name_by_username($article->username) }}" rel="author">{{ Profile::full_name_by_username($article->username) }}</a></h5>
    <p>{{ Profile::user_bio($article->username) }}</p>
    {{--<a target="_blank" class="author-social" href="https:"><i class="fa fa-globe"></i></a>--}}
    {{--<a target="_blank" class="author-social" href="http://facebook.com/#"><i class="fa fa-facebook"></i></a>--}}
    {{--<a target="_blank" class="author-social" href="http://twitter.com/#"><i class="fa fa-twitter"></i></a>--}}
    {{--<a target="_blank" class="author-social" href="http://plus.google.com/#?rel=author"><i class="fa fa-google-plus"></i></a>--}}
    {{--<a target="_blank" class="author-social" href="http://instagram.com/#"><i class="fa fa-instagram"></i></a>--}}
    {{--<a target="_blank" class="author-social" href="http://pinterest.com/#"><i class="fa fa-pinterest"></i></a>--}}
    {{--<a target="_blank" class="author-social" href="http://#.tumblr.com/"><i class="fa fa-tumblr"></i></a>--}}
    </div>
    </div>

    </div>

    </div>



    </div>
    <!-- /post -->

    </div>


    <div class="col-md-3">

    <div class="sidebar sidebar-default sidebar-separate">
    <div class="sidebar-content">


    <!-- Share -->
    <div class="sidebar-category">
    <div class="category-title">
    						<img src="{{ Profile::user_picture_by_username($article->username) }}" class="img-circle" alt="Author Picture" width="186" height="142">

    <span>Share</span>

    </div>

    <div class="category-content no-padding-bottom text-center">
    <ul class="list-inline no-margin">
    <li>
    <a href="https://www.facebook.com/sharer.php?u={{ Protocol::home() }}/article/{{ $article->slug }}" target="_blank" class="btn bg-green btn-icon content-group">
    <i class="icon-facebook"></i>
    </a>
    </li>
    <li>
    <a href="https://twitter.com/share?url={{ Protocol::home() }}/article/{{ $article->slug }}&text={{ $article->title }}" target="_blank" class="btn bg-green btn-icon content-group">
    <i class="icon-twitter"></i>
    </a>
    </li>
    <li>
    <a href="https://plus.google.com/share?url={{ Protocol::home() }}/article/{{ $article->slug }}" target="_blank" class="btn bg-green btn-icon content-group">
    <i class="icon-google-plus"></i>
    </a>
    </li>
    <li>
    <a href="https://www.stumbleupon.com/submit?url={{ Protocol::home() }}/article/{{ $article->slug }}&title={{ $article->title }}" target="_blank" class="btn bg-green btn-icon content-group">
    <i class="icon-stumbleupon"></i>
    </a>
    </li>
    </ul>
    </div>
    </div>
    <!-- /share -->

    </div>
    </div>

    <!-- Advertisements -->
    @if (Helper::ifCanSeeAds())
    <div class="advertisment">
    {!! Helper::advertisements()->ad_sidebar !!}
    </div>
    @endif

    </div>
    </div>


    <div class="row">
    <div class="col-md-9">


    <div class="panel panel-flat">
    <div class="panel-heading">
    <h6 class="panel-title text-semiold">Discussion<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
    <div class="heading-elements">
    <ul class="list-inline list-inline-separate heading-text text-muted">
    <li>{{ count($comments) }} Comment</li>
    </ul>
    </div>
    </div>

    <div class="panel-body">


    @if (count($comments))
    <div id="commentScroll">

    <ul class="media-list content-group-lg stack-media-on-mobile">

    @foreach($comments as $comment)

    @include('themes.default.blog.comment',['comment'=>$comment])

    <hr>
    @endforeach

    <div style="display: none;">
    									{{ $comments->links() }}
    </div>

    </ul>

    </div>
    <ul class="media-list content-group-lg stack-media-on-mobile">
    <div id="newComment"></div>
    </ul>




    <script type="text/javascript">


    $('.display_reply').click(function () {
    var data = $(this).data('comment-id');
    $('input[name="parent_id"]').val(data);
    $('input[name="article_id"]').val({{ $article->id }});
    //       console.log('data is '+data + 'or is ' + $(this).attr('data-comment-id'));
    $('#replyComment').show();


    });
    </script>
    @else
    <div id="hideNoCommentsNotice" class="alert bg-info alert-styled-left">
    @lang ('return/info.lang_no_comments_right_now')
    </div>
    <div id="newComment"></div>
    @endif
    </div>



    <div class="panel-body">

    <p class="text-muted text-right-rtl">Add comment - spam and offensive comments will be removed</p>

    <form action="{{  Protocol::home() }}/articles/comments/create" method="POST" id="createArticleComment" >

    {!! csrf_field() !!}
    <div class="content-group" id="spinnerDark">
    <textarea rows="5" cols="5" class="form-control" placeholder="{{ Lang::get('ads/show.lang_add_comment_placeholder') }}" name="comment" id="commentContent"></textarea>
    </div>
    <input type="hidden" name="article_id" value="{{ $article->id }}">
    @if (!Auth::check())
    <div class="row">
    <div class="col-md-3">
    <label>Name *</label>
    <input type="text" name="name" class="form-control">
    </div>

    <div class="col-md-3">
    <label>Email *</label>
    <input type="email" name="email" class="form-control">
    </div>

    <div class="col-md-3">
    <label>Website</label>
    <input type="text" name="website" class="form-control">
    </div>
    </div>
    @endif
    {!! csrf_field() !!}

    <div class="text-right">
    <button type="submit" class="btn bg-blue legitRipple" id="spinner-dark-6"><i id="spinnerIcon" style="display: none;" class="icon-spinner4 spinner position-left"></i> Add comment
    </button>
    </div>

    </form>




    </div>
    </div>



    </div>
    </div>

@endsection