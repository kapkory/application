
<li class="media commentScroll">
    <div class="media-left">
        @if (Profile::hasStore($comment->user_id))
            <a href="#"><img src="{{ url(Profile::comment_picture($comment->user_id)->profile_image) }}"  class="img-circle img-sm" alt="{{ Profile::hasStore($comment->user_id)->title }}"></a>
        @elseif($comment->user_id == 0)
            <a href="#"><img src="{{ url('uploads/avatars/noavatar.png') }}" class="img-circle img-sm" alt=""></a>

        @else
            <a href="#"><img src="{{ Profile::picture($comment->user_id) }}" class="img-circle img-sm" alt=""></a>
        @endif
    </div>

    <div class="media-body">
        <div class="media-heading comments-heading">
            @if($comment->user_id == 0)
                <a href="#" class="{{ Profile::hasStore($comment->user_id) ? 'trusted-badge' : '' }} label label-primary label-rounded">{{ $comment->name }}</a>
            @else
                <a href="#" class="{{ Profile::hasStore($comment->user_id) ? 'trusted-badge' : '' }} label label-primary label-rounded">{{ !Profile::hasStore($comment->user_id) ? Profile::hasStore($comment->user_id)->title : Profile::first_name($comment->user_id) }}</a>
            @endif
            {{--<span class="media-annotation dotted">&nbsp;</span>--}}
            @if ($comment->is_pinned)
                <span class="media-annotation dotted text-black text-uppercase">Pinned</span>
            @endif
        </div>

        <p style="margin-top: 10px;" class="emojioneareaCm">{!! nl2br($comment->content) !!}</p>

        <ul class="list-inline list-inline-separate text-size-small">

        @if (Auth::check())


            @if (Auth::id() === $comment->user_id)
                <!-- Edit Comment -->
                {{--<li><a href="{{ Protocol::home() }}/account/comments/edit/{{ $comment->id }}">{{ Lang::get('ads/show.lang_edit_comment') }}</a></li>--}}

                <!-- Delete Comment -->
                    {{--<li><a href="{{ Protocol::home() }}/account/comments/delete/{{ $comment->id }}">{{ Lang::get('ads/show.lang_delete_comment') }}</a></li>--}}

                @else
                    {{--<li><a href="">Reply</a> </li>--}}
                    <li><a  class="display_reply"  data-comment-id="{{ $comment->id }}">Reply</a></li>
                @endif

                {{--<li><a href="#" class="reportComment" data-comment-id="{{ $comment->id }}">{{ Lang::get('ads/show.lang_report_comment') }}</a></li>--}}
            @else
                <li><a  class="display_reply"  data-comment-id="{{ $comment->id }}">Reply</a></li>

            @endif

        </ul>

    </div>

</li>
<hr>


@if ($comment->children->count() > 0)
    {{--<a  id="replies">Replies</a>--}}
    @foreach ($comment->children as $comment)
        <div style="margin-left: 50px; " class="replies">
            @include('themes.default.blog.comment', ['comment' => $comment])
        </div>
    @endforeach
@endif

<div>
    <form action="{{  Protocol::home() }}/articles/comments/reply" method="POST" id="replyComment"  style="display: none">

        {!! csrf_field() !!}

        <div class="content-group" id="spinnerDark">
            <textarea id="comment" rows="5" cols="5" class="form-control" placeholder="{{ Lang::get('ads/show.lang_add_comment_placeholder') }}" name="comment" id="commentContent"></textarea>
        </div>
        <input type="hidden" name="parent_id">
        <input type="hidden" name="article_id">
        @if(!Auth::check())
            <div class="row">
                <div class="col-md-3">
                    <label>Name *</label>
                    <input name="name" class="form-control" type="text">
                </div>

                <div class="col-md-3">
                    <label>Email *</label>
                    <input name="email" class="form-control" type="email">
                </div>

                <div class="col-md-3">
                    <label>Website</label>
                    <input name="website" class="form-control" type="text">
                </div>
            </div>
        @endif

        <div class="text-right">
            <button type="submit" class="btn bg-blue" id="submit_reply"><i id="spinnerIcon" style="display: none;" class="icon-spinner4 spinner position-left"></i> Add Reply
            </button>
        </div>

    </form>
</div>



