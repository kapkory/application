@extends (Theme::get().'.layout.app')

@section ('seo')

{!! SEO::generate() !!}

@endsection

@section ('styles')

{{--<link href="{{ Protocol::home() }}/content/assets/front-end/css/icons/et-line-font/et-line.css" rel="stylesheet" type="text/css">--}}
<style type="text/css">
    .affiliate{
        margin: 0;
        padding: 0;
        left: 10px;
        top: 10px;
        list-style: none;
        position: absolute;
        z-index: 9;
        height: 30px;
    }
    .affiliate li::before {
        content: "";
        float: left;
        position: absolute;
        top: 0;
        left: -12px;
        width: 0;
        height: 0;
        border-color: transparent #2d5ead transparent transparent;
        border-style: solid;
        border-width: 12px 12px 12px 0;
    }
    .affiliate li {
        float: right;
        height: 24px;
        line-height: 24px;
        position: relative;
        margin: 2px 5px 2px 12px;
        padding: 0 10px 0 12px;
        background: #2d5ead;
        color: #fff;
        text-decoration: none;
        -moz-border-radius-bottom-right: 2px;
        -webkit-border-bottom-right-radius: 2px;
        border-bottom-right-radius: 2px;
        -moz-border-radius-top-right: 2px;
        -webkit-border-top-right-radius: 2px;
        border-top-right-radius: 2px;
        font-family: 'Fira Sans','Droid Arabic Kufi',sans-serif;
        text-transform: uppercase;
        font-size: 12px;
        letter-spacing: 1px;
    }
    .affiliate li::after {
        content: "";
        position: absolute;
        top: 10px;
        left: 0;
        float: left;
        width: 4px;
        height: 4px;
        -moz-border-radius: 2px;
        -webkit-border-radius: 2px;
        border-radius: 2px;
        background: #fff;
        -moz-box-shadow: -1px -1px 2px #004977;
        -webkit-box-shadow: -1px -1px 2px #004977;
        box-shadow: -1px -1px 2px #004977;
    }
    .marketing-button--small.marketing-button--secondary {
        padding: 0.6875em 1.75em;
    }
    .marketing-button--small {
        padding: 0.8125em 1.875em;
    }
    .marketing-button--secondary {
        -webkit-box-shadow: none;
        box-shadow: none;
        background-color: transparent;
        color: #5c6ac4;
        border-width: 0.125em;
        border-style: solid;
        border-color: #5c6ac4;
        padding: 1em 1.75em;
    }
    .marketing-button, .marketing-button:hover, .marketing-button:focus {
        text-decoration: none;
    }
    .rounded {
        -moz-border-radius:10px 10px 10px 10px;  // rounds corners for firefox
        border-radius:10px;  //rounds corners for other browsers
        border:solid 1px #000;
        background-color:#acf;
        padding:10px;
    }
    .highlights{
        font-weight: 500;
        line-height: 1.2;
        margin-top: 0;
    }
    .create-store {

        height: 500px;
        background: rgba(0,0,0,.5);
        background-position: 70% 15%;
        opacity: 1;
        background-size: cover;
        background-repeat: no-repeat;
        position: relative;
        /*margin-bottom: 50px;*/
        /*background: #000;*/
        z-index: 0;
    }
    .showroom{
        text-align: center;
        font-weight: 700;
    }
    .showroom h2{
        color: white;
    }
    .store a:hover{
        color: lightblue;
    }

    .test{
        background-color: #627f9a;
    }
    hr.vertical
    {
        width: 0px;
        height: 100%; /* or height in PX */
    }

    .col-showroom{
        padding: 5px;
        border-style: dotted;
        border-width: thin;
    }

    .col-showroom :hover{
        position: relative;
        top:-5px;
    }
    .overlay-hd-img{
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(76, 161, 175, 0.9);
        background: -webkit-linear-gradient(-90deg, rgba(76, 161, 175, 0.9), rgba(44, 62, 80, 0.9));
        background: -o-linear-gradient(-90deg, rgba(76, 161, 175, 0.9), rgba(44, 62, 80, 0.9));
        background: -moz-linear-gradient(-90deg, rgba(76, 161, 175, 0.9), rgba(44, 62, 80, 0.9));
        background: linear-gradient(-90deg, rgba(76, 161, 175, 0.9), rgba(44, 62, 80, 0.78));
    }
    /*Start new css */
    .content{
        background:white !important;
    }
    .flat-row{
        margin:27px 0 72px 0;
    }
    .iconbox{
        text-align: center;
    }
    .box-title{font-family:'Playfair Display';}
    .box-desc{color:#c2c2c2;}
    .row-bg-img{background-repeat: no-repeat;background-image:url( 'content/assets/front-end/images/bg-01.png');}

    .flat-row-title{margin:52px;}
    .flat-row-clr{background: #4ca1af;color:white;}

    .row-padding-max{
        padding:75px 0 100px 0px;
    }
    .flat-row-title h2{
        font-family: 'Playfair Display';
        font-size: 30px;
        font-weight: bold;
        margin-bottom: 30px;
    }
    .flat-row-title p{
        font-weight: 300;
        font-size:16px;
    }
    /*End new css */
</style>
@endsection

@section ('pageHeader')


<div class="create-store " style="background-image: url({{ url('uploads/general/banner.jpg') }})">
    <div class="overlay-hd-img">
        <div class="create-intro ">
            <p>
                {{ Lang::get('frontpage.lang_front_title') }}
            </p>
            <span class="create-small-intro" style="color: white">
                {{ Lang::get('frontpage.lang_front_para') }}
            </span>
        </div>
    </div>
    <!-- Open Store Now -->
    <a class="create-btn" href="{{ Protocol::home() }}/create">Add Your Product</a>
</div>

<!-- Advance Search in the website -->
<div id="search_form" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">

            <form action="{{ Protocol::home() }}/search" method="GET" class="main-search p-20">

                <div class="row">

                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="has-feedback has-feedback-left">
                                <input type="text" name="q" class="form-control input-xlg" placeholder="{{ Lang::get('home.lang_search_what_are_you_looking') }}">
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-muted"></i>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">

                    @if (is_null($states))

                    <div class="col-sm-4">

                        <div class="form-group">
                            <select class="select" name="country" id="countryGetStates">
                                <option selected="" value="all">{{ Lang::get('home.lang_any_country') }}</option>
                                @foreach ($countries as $country)
                                <option value="{{ $country->sortname }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    @if (Helper::settings_geo()->states_enabled)
                    <div class="col-sm-4">

                        <div class="form-group">
                            <select class="select" name="state" id="statesByCountry" disabled="">
                                <option selected="" value="all">{{ Lang::get('home.lang_any_state') }}</option>
                            </select>
                        </div>

                    </div>
                    @endif

                    @else 

                    @if (Helper::settings_geo()->states_enabled)
                    <div class="col-sm-6">

                        <div class="form-group">
                            <select class="select" name="state" id="statesByCountry">
                                <option selected="" value="all">{{ Lang::get('home.lang_any_state') }}</option>
                                @foreach ($states as $state)
                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    @endif

                    @endif

                    @if (Helper::settings_geo()->cities_enabled)
                    <div class="{{ is_null($states) ? 'col-sm-4' : 'col-sm-6' }}">

                        <div class="form-group">
                            <select class="select" name="city" id="citiesBySate" disabled="">
                                <option selected="" value="all">{{ Lang::get('home.lang_any_city') }}</option>
                            </select>
                        </div>

                    </div>
                    @endif

                </div>

                <div class="row">

                    <div class="col-sm-6">

                        <div class="form-group">
                            <select class="select" name="category">
                                <option value="all">{{ Lang::get('home.lang_all_categories') }}</option>
                                @if(count(Helper::parent_categories()))
                                @foreach (Helper::parent_categories() as $parent)
                                <optgroup label="{{ $parent->category_name }}">
                                    @if (count(Helper::sub_categories($parent->id)))
                                    @foreach (Helper::sub_categories($parent->id) as $sub)
                                    <option value="{{ $sub->id }}">{{ $sub->category_name }}</option>
                                    @endforeach
                                    @endif
                                </optgroup>
                                @endforeach
                                @endif
                            </select>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="form-group">
                            <select class="select" name="sort">
                                <option value="newest">{{ Lang::get('home.lang_newest') }}</option>
                                <option value="oldest">{{ Lang::get('home.lang_oldest') }}</option>
                                <option value="featured">{{ Lang::get('home.lang_featured') }}</option>
                                <option value="views">{{ Lang::get('home.lang_views') }}</option>
                                <option value="rating">{{ Lang::get('home.lang_rating') }}</option>
                            </select>
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                            <input type="text" name="min" class="form-control" placeholder="{{ Lang::get('home.lang_min_price') }}">
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                            <input type="text" name="max" class="form-control" placeholder="{{ Lang::get('home.lang_max_price') }}">
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">
                            <select class="select" name="currency">
                                @foreach (Currencies::database() as $currency)
                                <option value="{{ $currency->code }}">{{ $currency->code }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <button style="width: 100%;" type="submit" class="btn btn-success btn-loading">{{ Lang::get('home.lang_search') }}</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>

@endsection

@section ('content')

<div class="row">

    <!--Start New HTML-->
    <section class="flat-row ">
        <div class="container">
            <div class="">
                <div class="col-md-12">
                    <div class="flat-row-title center">
                        <h2>See How It Works</h2>
                        <p>
                            Discover how Sanitaryware can you help you find everything you want.
                        </p>
                    </div><!-- /.flat-row-title -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-sm-4">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="iconbox-icon">
                                <img src="{{ Protocol::home() }}/content/assets/front-end/images/iconbox-03.png" alt="">
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-content">
                            <h4 class="box-title">Choose What To Do</h4>
                            <div class="box-desc">
                                Open Your showroom  Where your Customers are !!
                            </div>
                        </div><!-- /.box-content -->
                    </div><!-- /.iconbox style1 -->
                </div><!-- /.col-sm-4 -->
                <div class="col-sm-4">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="iconbox-icon">
                                <img src="{{ Protocol::home() }}/content/assets/front-end/images/iconbox-02.png" alt="">
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-content">
                            <h4 class="box-title">Find What You Want</h4>
                            <div class="box-desc">
                                See what you can get for 100$/month
                            </div>
                        </div><!-- /.box-content -->
                    </div><!-- /.iconbox style1 -->
                </div><!-- /.col-sm-4 -->
                <div class="col-sm-4">
                    <div class="iconbox style1">
                        <div class="box-header">
                            <div class="iconbox-icon">
                                <img src="{{ Protocol::home() }}/content/assets/front-end/images/iconbox-01.png" alt="">
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-content">
                            <h4 class="box-title">Explore Amazing Business</h4>
                            <div class="box-desc">
                                Create an Online Showroom now
                            </div>
                        </div><!-- /.box-content -->
                    </div><!-- /.iconbox style1 -->
                </div><!-- /.col-sm-4 -->
            </div><!-- /.row -->

        </div><!-- /.container -->
    </section>


    <!-- Latest Stores -->
    @if (count($stores) > 0)
    <div class="your-class">

        @foreach ($stores as $store)

        <div class="col-md-4">

            <div class="card card-blog">
                <div class="card-image">
                    <a href="{{ Protocol::home() }}/store/{{ $store->username }}" >
                        <div class="img card-ad-cover" style="background-image: url({{ $store->logo }});" title="{{ $store->title }}"></div>
                    </a>
                </div>
                <h5 class="store"  style="text-align: center; ">
                    <a  href="{{ Protocol::home() }}/store/{{ $store->username }}">{{ $store->title }}</a>
                </h5>
            </div>
        </div>
        @endforeach

    </div>
    @endif

    <!-- Latest Ads -->
    <div class="col-md-12 row-bg-img">
        <div class="row-padding">
            <!-- Section Title -->
            <div class="flat-row-title center">
                <h2>{{ Lang::get('home.lang_latest_ads') }}</h2>
                <p>Search and Find what you are looking for. Best spots are here for you</p>
            </div>
            <div class="row">
                @if (count($latest_ads))
                @foreach ($latest_ads as $ad)
                <div class="col-md-3">
                    <div class="card card-blog">
                        <ul class="affiliate" style="color: blue">
                            @if (!is_null($ad->affiliate_link))
                            <li><a href="{{ $ad->affiliate_link }}">{{ Lang::get('update_two.lang_shopping') }}</a> </li>
                            @endif
                        </ul>
                        <ul class="tags">
                            @if ($ad->is_featured)
                            <li>{{ Lang::get('home.lang_featured') }}</li>
                            @endif

                            @if ($ad->is_oos)
                            <li class="oos">{{ Lang::get('update_three.lang_out_of_stock') }}</li>
                            @endif
                        </ul>
                        <div class="card-image">
                            <a href="{{ Protocol::home() }}/product/{{ $ad->slug }}" {{ !is_null($ad->affiliate_link) ? 'target="_blank"' : '' }}>
                                <div class="img card-ad-cover" style="background-image: url({{ EverestCloud::getThumnail($ad->ad_id, $ad->images_host) }});" title="{{ $ad->title }}"></div>
                            </a>
                        </div>
                        <div class="card-block">
                            <h5 class="card-title">
                                <a href="{{ Protocol::home() }}/product/{{ $ad->slug }}">{{ $ad->title }}</a>
                            </h5>
                            <div class="card-body">
                                {!!str_limit($ad->description,200)!!}
                            </div>
                            <div class="card-footer">
                                <div id="price">
                                    @if (!is_null($ad->regular_price))
                                    <span class="price price-old"> {{ number_format($ad->regular_price, 2) }} {{ $ad->currency }}</span>
                                    @endif
                                    <span class="price price-new">
                                        {{ ($ad->price) ? number_format($ad->price, 2) .' '.$ad->currency : 'Contact Seller For Price' }}

                                    </span>
                                </div>
                                <div class="author">
                                    <div class="card__avatar"><a href="{{ Profile::hasStore($ad->user_id) ? Protocol::home().'/store/'.Profile::hasStore($ad->user_id)->username : '#' }}" class="avatar__wrapper--verified avatar__wrapper avatar__wrapper--40"><img src="{{ Profile::picture($ad->user_id) }}" alt="{{ Profile::hasStore($ad->user_id) ? Profile::hasStore($ad->user_id)->title : Profile::full_name($ad->user_id) }}" class="avatar" width="40" height="40">@if (Profile::hasStore($ad->user_id))<i class="icon-checkmark3" data-popup="tooltip" data-placement="top" data-container="body" title="{{ Lang::get('update_two.lang_verified_account') }}"></i>@endif</a></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                @endforeach
                @endif
            </div>
            <!-- Browse All -->
            <div class="btn-morphing"><a href="{{ Protocol::home() }}/browse" class="btn btn-default btn-round btn-toggle">{{ Lang::get('home.lang_see_more') }}</a></div>
            <br>
            <br>
        </div>
    </div>

    <br>
    <div class="col-md-12 row-padding-max" style="color: blanchedalmond;">
        <div class="panel panel-flat">
            <div class="panel-body page_content">
                <h2 style="text-align: center">Sanitaryware.org is a salesman for your product Globally<br></h2>
                <div class="col-md-offset-2">
                    <div style="text-align: center" >
                        <h3 style="color: black">Want to increase your Sale</h3>
                        <br>
                        <span class="rounded">Yes</span> &nbsp;&nbsp; <span class="rounded">No</span>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Browse By Categories -->
    <div class="col-md-12 flat-row-clr row-padding" >
        <!-- Browse By Categories -->
            <div class="flat-row-title center">
                <h2>{{ Lang::get('home.lang_browse_categories') }}</h2>
                <p>Search and Find what you are looking for.</p>
            </div>
        @if(count(Helper::parent_categories()))
        <!-- Browse By Category -->
        <div class="row cat_single_wrap">
            @foreach (Helper::parent_categories() as $parent_category)
            <div class="col-md-2 text-center">
                <div class="cat_single">
                    <div class="cat_single_bg">
                        <div class="overlay_color panel" style="transform: skewX(-6deg);">
                        </div>
                    </div>
                    <div class="cat_single_content">
                        <a href="{{ Protocol::home() }}/supplier-manufacturer/{{ $parent_category->category_slug }}" style="color: rgb(255, 255, 255);">
                            <img src="{{ $parent_category->icon }}" alt="">
                            <span class="cat_title">{{ $parent_category->category_name }}</span>
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>
    <hr>
    <!-- Latest Articles -->
    <div class="col-md-12 row-bg-img">
        <div class="row-padding">

            <!-- Section Titles -->
            <div class="flat-row-title center">
                <h2>{{ Lang::get('home.lang_latest_articles') }}</h2>
                <p>Browse the latest articles from our blog.</p>
            </div>

            <div class="row">

                @if (count($articles))

                @foreach ($articles as $key => $a)

                <div class="col-lg-3 col-sm-6 article-outer">
                    <article class="blog-post">
                        <div class="featured-post">
                            <a href="{{ Protocol::home() }}/article/{{ $a->slug }}">
                                <!--<img src="{{ Protocol::home() }}/content/assets/front-end/images/aticle-sample.png" alt="">-->
                                <img src="{{ Protocol::home() }}/uploads/articles/{{ $a->cover }}" style="height: 168px;width: 250px;" alt="">
                                <div class="overlay"></div>
                            </a>
                        </div><!-- /.featured-post -->
                        <div class="content-post">
                            <div class="entry-post">
                                <ul class="entry-meta">
                                    <li class="topic">
                                        <a href="#" title=""></a>
                                    </li>
                                    <li class="date">
                                        <a href="#" title="">{!!date('d M,Y',strtotime($a->created_at))!!}</a>
                                    </li>
                                </ul>
                                <h2 class="entry-title">
                                    <a href="{{ Protocol::home() }}/article/{{ $a->slug }}">
                                        {!!str_limit($a->title,150)!!}
                                    </a>
                                </h2>
                            </div>
                            <p>
                                {!!str_limit($a->content,200)!!}
                            </p>
                            <div class="author-post">
                                By <a href="#" title="">{!!$a->first_name!!}</a>
                            </div>
                        </div><!-- /.content-post -->
                    </article><!-- /.blog-post -->
                </div>
                @endforeach

                @endif

            </div>

            <!-- Browse All -->
            <div class="btn-morphing"><a href="{{ Protocol::home() }}/article" class="btn btn-default btn-round btn-toggle">{{ Lang::get('home.lang_see_more') }}</a></div>

        </div>
    </div>



    <hr>
    <div class="col-sm-12 flat-row">
        <div class="row">
            <div style="text-align: center">
                <h2> Contact Us </h2><a class="marketing-button marketing-button--secondary marketing-button--small" href="{{ url('contact') }}">Now</a>
            </div>
        </div>
    </div>
</div>

<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "{{ url('/') }}",
    "potentialAction": {
    "@type": "SearchAction",
    "target": "{{ url('/') }}/search?q={search_term_string}",
    "query-input": "required name=search_term_string"
    }
    }
</script>

<script type="application/ld+json">
    {
    "@context":"http://schema.org",
    "@type":"ItemList",
    "itemListElement":[
    {
    "@type":"SiteNavigationElement",
    "position":1,
    "name": "Home",
    "description": "Homes",
    "url":"{{ url('/') }}"
    },
    {
    "@type":"SiteNavigationElement",
    "position":2,
    "name": "Article",
    "description": "SanitaryWare Articles",
    "url":"{{ url('article') }}"
    },
    {
    "@type":"SiteNavigationElement",
    "position":3,
    "name": "stores",
    "description": "SanitaryWare Stores",
    "url":"{{ url('stores') }}"
    },
    {
    "@type":"SiteNavigationElement",
    "position":4,
    "name": "Catalogues",
    "description": "SanitaryWare Catalogues",
    "url":"{{ url('catalogue') }}"
    },
    {
    "@type":"SiteNavigationElement",
    "position":5,
    "name": "Contact Us",
    "description": "SanitaryWare.org Contact Us page",
    "url":"{{ url('contact') }}"
    }

    ]
    }
</script>


<script type="application/ld+json">[
    {
    "@context": "http://schema.org",
    "@type": "SiteNavigationElement",
    "name": "Home",
    "url": "{{ url('/') }}"
    },
    {
    "@context": "http://schema.org",
    "@type": "SiteNavigationElement",
    "name": "Article",
    "url": "{{ url('article') }}"
    },
    {
    "@context": "http://schema.org",
    "@type": "SiteNavigationElement",
    "name": "Stores",
    "url": "{{ url('stores') }}"
    },
    {
    "@context": "http://schema.org",
    "@type": "SiteNavigationElement",
    "name": "Catalogue",
    "url": "{{ url('catalogue') }}"
    },
    {
    "@context": "http://schema.org",
    "@type": "SiteNavigationElement",
    "name": "Products",
    "url": "{{ url('browse') }}"
    },
    {
    "@context": "http://schema.org",
    "@type": "SiteNavigationElement",
    "name": "Contact Us",
    "url": "{{ url('contact') }}"
    }
    ]</script>
<!-- Carousel Plugin JS -->


<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/flat/plugins/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/front-end/flat/plugins/slick/slick-theme.css"/>
<script type="text/javascript" src="{{ Protocol::home() }}/content/assets/front-end/flat/plugins/slick/slick.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.your-class').slick({
            autoplay: false,
            arrows: false,
            dots: true,
            infinite: true,
            pauseOnFocus: true,
            pauseOnHover: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                        }
                    },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                        }
                    },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                        }
                    }
                ]
            });
        });
    </script>
<script type="application/ld+json">
    {
    "@context": "http://schema.org",
    "@type": "Organization",
    "@id":  "{{ url('/') }}",
    "url": "{{ url('/') }}",
    "name": "Sanitaryware",
    "description":"Find Sanitaryware &amp; Faucet Products from Multiple Manufacturers &amp; Suppliers around the world for your business. Import and export to gain more profit in sanitaryware business. And tons of article about sanitaryware products &amp; production process. Open an online showroom for your business to reach globally. ",
    "logo": "{{ url('uploads/settings/logo/logo.png') }}",
    "sameAs": [
    "https://www.facebook.com/sanitaryware.org/",
    "https://twitter.com/sanitarywareorg",
    "http://youtube.com/c/SanitarywareOrg123"
    ],
    "contactPoint": [
    {
    "@type": "ContactPoint",
    "telephone": "+919500693318",
    "email": "admin@sanitaryware.org",
    "contactType": "Customer Service"
    }
    ]
    }
</script>
@endsection