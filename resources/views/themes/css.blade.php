<style type="text/css">
    /* Article page css Start*/
    article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section { 
        display: block;
    }
    blockquote, q { 
        -webkit-hyphens: none;
        -moz-hyphens: none;
        -ms-hyphens: none;
        hyphens: none;
        quotes: none;
    }

    figure {
        margin: 0;
    }

    :focus {
        outline: 0;
    }

    a {		
        text-decoration: none;
        color: #2c3e50;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }

    a:hover,
    a:focus {
        color: #4ca1af;
        text-decoration: none;
        outline: 0;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }

    ul, ol {
        padding: 0;
    }

    img {
        max-width: 100%;
        height: auto;		
    }

    b, strong {
        font-weight: 900;
    }

    .clearfix {
        clear: both;
    }

    .left {
        text-align: left;
    }

    .center {
        text-align: center;
    }

    .background-color {
        background-color: #f5f5f5;
    }

    /* Flat Main Blog
    -------------------------------------------------------------- */
    .flat-main-blog {
        padding: 70px 0 80px;
    }

    /* Article Blog Post */
    article.blog-post {
        background-color: #ffffff;
        box-shadow: 0px 7px 16px 0px rgba(0, 0, 0, 0.09);
        /*min-height: 430px;*/
        min-height: 530px;
    }

    article.blog-post .featured-post {
        padding: 10px;
    }

    article.blog-post .featured-post a {
        display: block;
        position: relative;
        overflow: hidden;
    }

    article.blog-post .featured-post a .overlay {
        position: absolute;
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
        opacity: 0;
        background-color: rgba(76, 161, 175, 0.5);
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }

    article.blog-post .featured-post a img {
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }

    article.blog-post:hover .featured-post a .overlay {
        opacity: 1;
    }

    article.blog-post:hover .featured-post a img {
        transform: scale(1.15);
    }

    article.blog-post .content-post {
        padding: 0 20px 5px;
        position: relative;
    }

    article.blog-post .content-post .entry-post ul.entry-meta {
        margin: 2px 0 0 2px;
        list-style:none;
    }

    article.blog-post .content-post .entry-post ul.entry-meta li.topic {
        float: left;
        font-family: 'Playfair Display';
    }

    article.blog-post .content-post .entry-post ul.entry-meta li.date {
        text-align: right;
        line-height: 23px;
    }

    article.blog-post .content-post .entry-post ul.entry-meta li a {
        color: #c2c2c2;
        font-size: 12px;
    }

    article.blog-post .content-post .entry-post h2.entry-title {
        font-size: 18px;
        font-weight: 500;
        margin-top: 9px;
        margin-bottom: 20px;
    }

    article.blog-post .content-post p {
        font-size: 14px;
        left: 21px;
        right: 25px;
        position: absolute;
        /*top: 93px;*/
    }

    article.blog-post .content-post .author-post {
        color: #c2c2c2;
        font-size: 12px;
        margin: 127px 0 0 4px;
        line-height: 30px;
        position: absolute;
        top: 181px;
    }

    article.blog-post .content-post .author-post a:not(:hover) {
        color: #c2c2c2;
    }

    .height50 {
        height: 50px;
    }
    .article-outer{
        padding: 20px;
    }
    /*articleEND*/
    .cat-hd{

        background-color: grey;
        color: white;
        padding: 3px 12px 3px 9px;
        font-size: 17px;
        margin: 30px 0px 0 0;
    }
    .cat-hd-line{
        border-bottom: 1px black solid;
        padding: 0 0px 41px 0px;
    }
    /* Article page css END */
</style>