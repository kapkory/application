{{ Profile::full_name($offer->offer_to) }} Refused your offer {{ number_format($offer->price) }} {{ Helper::settings_geo()->default_currency }}

Contact the seller via phone

{{ Profile::phone($offer->offer_to) }}

{{ Protocol::home() }}/vi/{{ $offer->ad_id }}