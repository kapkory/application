@extends ('dashboard.layout.app')

@section ('content')

<div class="row">
    
    <div class="col-md-12">
        
        <!-- Sessions Messages -->
        @if (Session::has('success'))
        <div class="custom-alerts alert alert-success fade in">
            {{ Session::get('success') }}
        </div>
        @endif

        @if (Session::has('error'))
        <div class="custom-alerts alert alert-danger fade in">
            {{ Session::get('error') }}
        </div>
        @endif

        <div class="portlet light ">

            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue bold uppercase">SMTP Settings</span>
                </div>
            </div>

            <div class="portlet-body">

                <form action="{{ Protocol::home() }}/dashboard/settings/smtp" method="POST">
                    
                    {{ csrf_field() }}

                    <!-- Mail Host Server -->
                    <div class="form-group form-md-line-input {{ $errors->has('host') ? 'has-error' : '' }}">
                        <input class="form-control" id="host" name="host" placeholder="Mail Host Server" value="{{ config('mail.host') }}" type="text">
                        <label for="host">Mail Host Server</label>
                        @if ($errors->has('host'))
                        <span class="help-block">{{ $errors->first('host') }}</span>
                        @endif
                    </div>

                    <!-- Mail Port -->
                    <div class="form-group form-md-line-input {{ $errors->has('port') ? 'has-error' : '' }}">
                        <input class="form-control" id="host" name="port" placeholder="Mail Port" value="{{ config('mail.port') }}" type="text">
                        <label for="port">Mail Port</label>
                        @if ($errors->has('port'))
                        <span class="help-block">{{ $errors->first('port') }}</span>
                        @endif
                    </div>

                    <!-- Mail Username -->
                    <div class="form-group form-md-line-input {{ $errors->has('username') ? 'has-error' : '' }}">
                        <input class="form-control" id="host" name="username" placeholder="Mail Username" value="{{ config('mail.username') }}" type="text">
                        <label for="username">Mail Username</label>
                        @if ($errors->has('username'))
                        <span class="help-block">{{ $errors->first('username') }}</span>
                        @endif
                    </div>

                    <!-- Mail Password -->
                    <div class="form-group form-md-line-input {{ $errors->has('password') ? 'has-error' : '' }}">
                        <input class="form-control" id="host" name="password" placeholder="***********" type="password">
                        <label for="password">Mail Password</label>
                        @if ($errors->has('password'))
                        <span class="help-block">{{ $errors->first('password') }}</span>
                        @endif
                    </div>

                    <!-- Sender Email Address -->
                    <div class="form-group form-md-line-input {{ $errors->has('email') ? 'has-error' : '' }}">
                        <input class="form-control" id="host" name="email" placeholder="Sender Email Address" value="{{ config('mail.from.address') }}" type="text">
                        <label for="email">Sender Email Address</label>
                        @if ($errors->has('email'))
                        <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif
                    </div>

                    <!-- Sender Name -->
                    <div class="form-group form-md-line-input {{ $errors->has('name') ? 'has-error' : '' }}">
                        <input class="form-control" id="host" name="name" placeholder="Sender Name" value="{{ config('mail.from.name') }}" type="text">
                        <label for="name">Sender Name</label>
                        @if ($errors->has('name'))
                        <span class="help-block">{{ $errors->first('name') }}</span>
                        @endif
                    </div>

                    <button style="width: 100%" type="submit" class="btn default">Update Settings</button>

                </form>

            </div>

        </div>

    </div>

</div>

@endsection