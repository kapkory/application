@extends ('dashboard.layout.app')

@section ('content')

<div class="row">
    
    <div class="col-md-12">

        <!-- Session Messages -->
        @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }} 
        </div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }} 
        </div>
        @endif
        
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-blue uppercase">MemberShip Settings</span>
                </div>
            </div>
            <div class="portlet-body">
                <form role="form" action="{{ Protocol::home() }}/dashboard/settings/membership" method="POST">
                
                    {{ csrf_field() }}

                    <!-- PayPal Gateway -->
                    <div class="form-group {{ $errors->has('is_paypal') ? 'has-error' : '' }}">
                        <label class="control-label">PayPal Gateway</label>
                        <select class="form-control" id="is_paypal" name="is_paypal">
                            @if ($settings->is_paypal)
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                            @else 
                            <option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                            @endif
                        </select>
                        @if ($errors->has('is_paypal'))
                        <span class="help-block">{{ $errors->first('is_paypal') }}</span>
                        @endif
                    </div>

                    <!-- 2checkout Gateway -->
                    <div class="form-group {{ $errors->has('is_2checkout') ? 'has-error' : '' }}">
                        <label class="control-label">2checkout Gateway</label>
                        <select class="form-control" id="is_2checkout" name="is_2checkout">
                            @if ($settings->is_2checkout)
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                            @else 
                            <option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                            @endif
                        </select>
                        @if ($errors->has('is_2checkout'))
                        <span class="help-block">{{ $errors->first('is_2checkout') }}</span>
                        @endif
                    </div>

                    <!-- Stripe Gateway -->
                    <div class="form-group {{ $errors->has('is_stripe') ? 'has-error' : '' }}">
                        <label class="control-label">Stripe Gateway</label>
                        <select class="form-control" id="is_stripe" name="is_stripe">
                            @if ($settings->is_stripe)
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                            @else 
                            <option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                            @endif
                        </select>
                        @if ($errors->has('is_stripe'))
                        <span class="help-block">{{ $errors->first('is_stripe') }}</span>
                        @endif
                    </div>

                    <!-- Mollie Gateway -->
                    <div class="form-group {{ $errors->has('is_mollie') ? 'has-error' : '' }}">
                        <label class="control-label">Mollie Gateway</label>
                        <select class="form-control" id="is_mollie" name="is_mollie">
                            @if ($settings->is_mollie)
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                            @else 
                            <option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                            @endif
                        </select>
                        @if ($errors->has('is_mollie'))
                        <span class="help-block">{{ $errors->first('is_mollie') }}</span>
                        @endif
                    </div>

                    <!-- PayStack Gateway -->
                    <div class="form-group {{ $errors->has('is_paystack') ? 'has-error' : '' }}">
                        <label class="control-label">PayStack Gateway</label>
                        <select class="form-control" id="is_paystack" name="is_paystack">
                            @if ($settings->is_paystack)
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                            @else 
                            <option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                            @endif
                        </select>
                        @if ($errors->has('is_paystack'))
                        <span class="help-block">{{ $errors->first('is_paystack') }}</span>
                        @endif
                    </div>

                    <!-- PaySafeCard Gateway -->
                    <div class="form-group {{ $errors->has('is_paysafecard') ? 'has-error' : '' }}">
                        <label class="control-label">PaySafeCard Gateway</label>
                        <select class="form-control" id="is_paysafecard" name="is_paysafecard">
                            @if ($settings->is_paysafecard)
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                            @else 
                            <option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                            @endif
                        </select>
                        @if ($errors->has('is_paysafecard'))
                        <span class="help-block">{{ $errors->first('is_paysafecard') }}</span>
                        @endif
                    </div>

                    <!-- Barion Gateway -->
                    <div class="form-group {{ $errors->has('is_barion') ? 'has-error' : '' }}">
                        <label class="control-label">Barion Gateway</label>
                        <select class="form-control" id="is_barion" name="is_barion">
                            @if ($settings->is_barion)
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                            @else 
                            <option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                            @endif
                        </select>
                        @if ($errors->has('is_barion'))
                        <span class="help-block">{{ $errors->first('is_barion') }}</span>
                        @endif
                    </div>

                    <!-- RazorPay Gateway -->
                    <div class="form-group {{ $errors->has('is_razorpay') ? 'has-error' : '' }}">
                        <label class="control-label">RazorPay Gateway</label>
                        <select class="form-control" id="is_razorpay" name="is_razorpay">
                            @if ($settings->is_razorpay)
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                            @else 
                            <option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                            @endif
                        </select>
                        @if ($errors->has('is_razorpay'))
                        <span class="help-block">{{ $errors->first('is_razorpay') }}</span>
                        @endif
                    </div>

                    <!-- RazorPay Gateway -->
                    <div class="form-group {{ $errors->has('is_cashu') ? 'has-error' : '' }}">
                        <label class="control-label">CashU Gateway</label>
                        <select class="form-control" id="is_cashu" name="is_cashu">
                            @if ($settings->is_cashu)
                            <option value="1">Enabled</option>
                            <option value="0">Disabled</option>
                            @else 
                            <option value="0">Disabled</option>
                            <option value="1">Enabled</option>
                            @endif
                        </select>
                        @if ($errors->has('is_cashu'))
                        <span class="help-block">{{ $errors->first('is_cashu') }}</span>
                        @endif
                    </div>

                    <hr>

                    <div class="row">

                        <!-- Free Account Ads Per Day -->
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('free_ads_per_day') ? 'has-error' : '' }}">
                                <label class="control-label">Free account ads per day</label>
                                <input type="text" class="form-control" id="free_ads_per_day" name="free_ads_per_day" value="{{ $settings_membership->free_ads_per_day }}">
                                @if ($errors->has('free_ads_per_day'))
                                <span class="help-block">{{ $errors->first('free_ads_per_day') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Pro Account Ads Per Day -->
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('pro_ads_per_day') ? 'has-error' : '' }}">
                                <label class="control-label">Pro account ads per day</label>
                                <input type="text" class="form-control" id="pro_ads_per_day" name="pro_ads_per_day" value="{{ $settings_membership->pro_ads_per_day }}">
                                @if ($errors->has('pro_ads_per_day'))
                                <span class="help-block">{{ $errors->first('pro_ads_per_day') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Free Account max Ad Images -->
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('free_ad_images') ? 'has-error' : '' }}">
                                <label class="control-label">Free account max ad images</label>
                                <input type="text" class="form-control" id="free_ad_images" name="free_ad_images" value="{{ $settings_membership->free_ad_images }}">
                                @if ($errors->has('free_ad_images'))
                                <span class="help-block">{{ $errors->first('free_ad_images') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Pro Account max Ad Images -->
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('pro_ad_images') ? 'has-error' : '' }}">
                                <label class="control-label">Pro account max ad images</label>
                                <input type="text" class="form-control" id="pro_ad_images" name="pro_ad_images" value="{{ $settings_membership->pro_ad_images }}">
                                @if ($errors->has('pro_ad_images'))
                                <span class="help-block">{{ $errors->first('pro_ad_images') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Free Ad expiration days -->
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('free_ad_life') ? 'has-error' : '' }}">
                                <label class="control-label">Free Ad expiration days</label>
                                <input type="text" class="form-control" id="free_ad_life" name="free_ad_life" value="{{ $settings_membership->free_ad_life }}">
                                @if ($errors->has('free_ad_life'))
                                <span class="help-block">{{ $errors->first('free_ad_life') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Pro Ad expiration days -->
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('pro_ad_life') ? 'has-error' : '' }}">
                                <label class="control-label">Pro Ad expiration days</label>
                                <input type="text" class="form-control" id="pro_ad_life" name="pro_ad_life" value="{{ $settings_membership->pro_ad_life }}">
                                @if ($errors->has('pro_ad_life'))
                                <span class="help-block">{{ $errors->first('pro_ad_life') }}</span>
                                @endif
                            </div>
                        </div>

                    </div>

                    <!-- Save Changes -->
                    <div class="margin-top-10">
                        <button type="submit" class="btn default" style="width: 100%">Save Changes </button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>

@endsection