@extends ('dashboard.layout.app')

@section ('content')

    <!-- Articles -->
    <div class="row">

        <div class="col-md-12">

            <!-- Sessions Messages -->
            @if (Session::has('success'))
                <div class="custom-alerts alert alert-success fade in">
                    {{ Session::get('success') }}
                </div>
            @endif

            @if (Session::has('error'))
                <div class="custom-alerts alert alert-danger fade in">
                    {{ Session::get('error') }}
                </div>
            @endif

            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <span class="caption-subject font-blue bold uppercase">Advert With Us Responses</span>
                    </div>
                </div>

                <div class="portlet-body">

                    <div class="">
                        <table class="table table-hover table-outline m-b-0 hidden-sm-down">
                            <thead class="thead-default">
                            <tr>
                                <th class="text-center"><i class="icon-link"></i></th>
                                <th>Name</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Company Name</th>
                                <th class="text-center">Remarks</th>
                                <th class="text-center">Created at</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if (count($adverts))
                                @foreach ($adverts as $advert)
                                    <tr>

                                        <!-- Articles ID -->
                                        <td class="text-center text-muted">
                                            {{ $advert->id }}
                                        </td>

                                        <td class="text-center text-muted">
                                            {{ $advert->name }}
                                        </td>

                                        <td class="text-center text-muted">
                                            {{ $advert->number }}
                                        </td>

                                        <td class="text-center text-muted">
                                            {{ $advert->email }}
                                        </td>

                                        <td class="text-center text-muted">
                                            {{ $advert->company_name }}
                                        </td>

                                        <td class="text-center text-muted">
                                            {{ $advert->remarks }}
                                        </td>

                                        <td class="text-center text-muted">
                                            {{ Helper::date_ago($advert->created_at) }}
                                        </td>

                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>

                        @if (count($adverts) > 0)
                            <div class="text-center">
                                {{ $adverts->links() }}
                            </div>
                        @endif

                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection