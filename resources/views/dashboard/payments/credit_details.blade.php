@extends ('dashboard.layout.app')

@section ('content')

<!-- Payments -->
<div class="row">
    
    <div class="col-md-12">

        <!-- Session Messages -->
        @if (Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }} 
        </div>
        @endif
        @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }} 
        </div>
        @endif

        <div class="portlet light">

            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <span class="caption-subject font-blue-madison bold uppercase">Payment Details</span>
                </div>
            </div>

            <div class="portlet-body">

                <div class="portlet grey-cascade box">
                    <div class="portlet-title">
                        <div class="caption">
                            Payment Details </div>
                        <div class="actions">
                            @if (is_null($payment->is_accepted))
                            <a href="{{ Protocol::home() }}/dashboard/payments/credit/accept/{{ $payment->payment_id }}" class="btn btn-default btn-sm">
                                <i class="fa fa-check"></i> Accept </a>
                            <a href="{{ Protocol::home() }}/dashboard/payments/credit/refuse/{{ $payment->payment_id }}" class="btn btn-default btn-sm">
                                <i class="fa fa-ban"></i> Refuse </a>
                            @endif
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row static-info">
                            <div class="col-md-5 name"> Balance Transaction: </div>
                            <div class="col-md-7 value">
                                <span class="label label-info label-sm"> {{ $payment->balance_transaction }} </span>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name"> Payment ID: </div>
                            <div class="col-md-7 value">
                                <span class="label label-info label-sm"> {{ $payment->payment_id }} </span>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name"> Credit Card: </div>
                            <div class="col-md-7 value">
                                <span class="label label-info label-sm"> {{ $payment->card_number }} </span>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name"> Brand: </div>
                            <div class="col-md-7 value"> {{ $payment->brand }} </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>

@endsection