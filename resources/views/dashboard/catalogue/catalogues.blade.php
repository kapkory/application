@extends ('dashboard.layout.app')

@section ('content')

    <div class="row">

        <div class="col-md-12">

            <!-- Sessions Messages -->
            @if (Session::has('success'))
                <div class="custom-alerts alert alert-success fade in">
                    {{ Session::get('success') }}
                </div>
            @endif

            @if (Session::has('error'))
                <div class="custom-alerts alert alert-danger fade in">
                    {{ Session::get('error') }}
                </div>
            @endif

            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue bold uppercase">Catalogue Settings</span>
                    </div>
                </div>

                <div class="portlet-body">
                    <table class="table table-hover table-outline m-b-0 hidden-sm-down">
                        <thead class="thead-default">
                        <tr>
                            <th class="text-center"><i class="icon-link"></i></th>
                            <th>Title</th>
                            <th class="text-center">Country</th>
                            <th class="text-center">Content</th>
                            <th class="text-center">Meta title</th>
                            <th class="text-center">Meta Description</th>
                            <th class="text-center">Document</th>
                            <th class="text-center">Options</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if ($catalogues)
                            @foreach ($catalogues as $catalogue)
                                <tr>

                                    <!-- Ad Image -->
                                    <td class="text-center">
                                        <div class="avatar">
                                            <img src="{{ url($catalogue->image_url) }}" class="img-avatar" alt="{{ $catalogue->title }}">

                                        </div>
                                    </td>

                                    <!-- Ad Info -->
                                    <td>
                                        <a target="_blank" href="{{ url('catalogue/download/'. $catalogue->slug) }}" class="text-dots tooltips" data-container="body" data-placement="top" data-original-title="{{ $catalogue->title }}">{{ $catalogue->title }}</a>
                                        <div class="small text-muted">
                                           | {{ Helper::date_ago($catalogue->created_at) }}
                                        </div>
                                    </td>

                                    <!-- catalogue Country -->
                                    <td class="text-center">
                                        <a target="_blank" href="{{ url('catalogue/country/'.$catalogue->country->name) }}">
                                            {{ $catalogue->country->name }}
                                        </a>
                                    </td>

                                    <!-- Ad Price -->
                                    <td class="text-center text-muted">
                                        {{ str_limit(strip_tags($catalogue->content), 50) }}
                                    </td>

                                    <!-- Is a featured Ad -->
                                    <td class="text-center">
                                        {{ $catalogue->meta_title }}
                                    </td>

                                    <!-- Is a Archived Ad -->
                                    <td class="text-center">
                                        {{--{{ $catalogue->meta_description }}--}}
                                        {{ str_limit(strip_tags($catalogue->meta_description), 50) }}
                                    </td>

                                    <!-- Ad Ends at -->
                                    <td class="text-center text-muted">
                                        <a target="_blank" href="{{ $catalogue->document_url }}">
                                            Document
                                        </a>
                                    </td>

                                    <!-- Options -->
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <i style="color: #405a72;font-size: 18px;cursor: pointer;" class="icon-settings dropdown-toggle" data-toggle="dropdown"></i>

                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li>
                                                    <a href="{{ Protocol::home() }}/dashboard/catalogue/edit/{{ $catalogue->id }}">
                                                        <i class="glyphicon glyphicon-pencil"></i> Edit Ad</a>
                                                </li>

                                                <li class="divider"> </li>
                                                <li>
                                                    <a style="color: #dd2c2c;text-transform: uppercase;" href="{{ Protocol::home() }}/dashboard/catalogue/delete/{{ $catalogue->id }}">
                                                        <i style="color: #dd2c2c;" class="glyphicon glyphicon-trash"></i> Delete catalogue</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                    </table>

                    @if (count($catalogues) > 0)
                        <div class="text-center">
                            {{ $catalogues->links() }}
                        </div>
                    @endif

                </div>
            </div>

        </div>

    </div>

@endsection