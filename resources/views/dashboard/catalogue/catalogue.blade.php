@extends ('dashboard.layout.app')

@section ('content')

    <div class="row">
        <div class="col-md-12">

            <!-- Session Messages -->
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <span class="caption-subject font-blue-madison bold uppercase">Create New Catalogue</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal" action="{{ Protocol::home() }}/dashboard/catalogue/create" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">

                            <!-- Title -->
                            <div class="form-group form-md-line-input {{ $errors->has('title') ? 'has-error' :'' }}">
                                <label class="col-md-2 control-label" for="title">Catalogue Title</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="title" placeholder="Article Title" name="title" value="{{ old('title') }}">
                                    <div class="form-control-focus"> </div>
                                    @if ($errors->has('title'))
                                        <span class="help-block">{{ $errors->first('title') }}</span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group form-md-line-input {{ $errors->has('slug') ? 'has-error' :'' }}">
                                <label class="col-md-2 control-label" for="cover">Catalogue Slug</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="cover"  name="slug" value="{{ old('slug') }}">
                                    <div class="form-control-focus"> </div>
                                    @if ($errors->has('slug'))
                                        <span class="help-block">{{ $errors->first('slug') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Cover -->
                            <div class="form-group form-md-line-input {{ $errors->has('cover') ? 'has-error' :'' }}">
                                <label class="col-md-2 control-label" for="cover">Catalogue Cover</label>
                                <div class="col-md-10">
                                    <input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">
                                    <div class="form-control-focus"> </div>
                                    @if ($errors->has('cover'))
                                        <span class="help-block">{{ $errors->first('cover') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group form-md-line-input ">
                                <label class="col-md-2 control-label" for="tags">Document URL</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="document_url" placeholder="Enter URL" value="">
                                    {{--<input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">--}}
                                    <div class="form-control-focus"> </div>

                                </div>
                            </div>



                            <div class="form-group form-md-line-input ">
                                <label class="col-md-2 control-label" for="tags">Country</label>
                                <div class="col-md-10">
                                    <select name="country" class="form-control">


                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                    {{--<input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">--}}
                                    <div class="form-control-focus"> </div>

                                </div>
                            </div>



                            <!-- Content -->
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="content">Catalogue Content</label>
                                <div class="col-md-10">
                                    <textarea name="content">{{ old('content') }}</textarea>
                                    <script>
                                        CKEDITOR.replace( 'content' );
                                    </script>
                                </div>
                            </div>

                            <div class="form-group form-md-line-input ">
                                <label class="col-md-2 control-label" for="tags">Catalogue Meta Title</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="meta_title" placeholder="Enter Meta Title" value="">
                                    {{--<input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">--}}
                                    <div class="form-control-focus"> </div>

                                </div>
                            </div>

                            <div class="form-group form-md-line-input ">
                                <label class="col-md-2 control-label" for="tags">Catalogue Meta Description</label>
                                <div class="col-md-10">
                            <textarea class="form-control" name="meta_description" rows="4" cols="50">

                            </textarea>
                                    <div class="form-control-focus"> </div>

                                </div>
                            </div>


                            <hr>



                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    <button type="submit" class="btn blue">Create Catalogue</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

@endsection