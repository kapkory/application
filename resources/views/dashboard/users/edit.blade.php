@extends ('dashboard.layout.app')

@section ('content')

<div class="row">

    <div class="col-md-12">

        <!-- Sessions Messages -->
        @if (Session::has('success'))
        <div class="custom-alerts alert alert-success fade in">
            {{ Session::get('success') }}
        </div>
        @endif

        @if (Session::has('error'))
        <div class="custom-alerts alert alert-danger fade in">
            {{ Session::get('error') }}
        </div>
        @endif

        <!-- Edit User -->
        <div class="portlet light ">

            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Edit "{{ $user->first_name }} {{ $user->last_name }}" Profile</span>
                </div>
            </div>

            <div class="portlet-body">

                <form method="POST" action="{{ Protocol::home() }}/dashboard/users/edit/{{ $user->username }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="row">

                        <!-- First Name -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                <input type="text" class="form-control" id="first_name" placeholder="Enter first name" value="{{ $user->first_name }}" name="first_name">
                                <label for="first_name">First Name</label>
                                @if ($errors->has('first_name'))
                                <span class="help-block">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Last Name -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                <input type="text" class="form-control" id="last_name" placeholder="Enter last name" value="{{ $user->last_name }}" name="last_name">
                                <label for="last_name">Last Name</label>
                                @if ($errors->has('last_name'))
                                <span class="help-block">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Username -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('username') ? 'has-error' : '' }}">
                                <input type="text" class="form-control" id="username" placeholder="Enter username" value="{{ $user->username }}" name="username">
                                <label for="username">Username</label>
                                @if ($errors->has('username'))
                                <span class="help-block">{{ $errors->first('username') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- E-mail Address -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('email') ? 'has-error' : '' }}">
                                <input type="text" class="form-control" id="email" placeholder="Enter e-mail Address" value="{{ $user->email }}" name="email">
                                <label for="email">E-mail Address</label>
                                @if ($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- User Country -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('country') ? 'has-error' : '' }}">
                                 <select class="form-control" id="country" name="country" onchange="getStates(this.value)">
                                    @foreach ($countries as $country)
                                    <option value="{{ $country->sortname }}" {{ $country->sortname == $user->country_code ? 'selected' : '' }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                                <label for="country">Country</label>
                                @if ($errors->has('country'))
                                <span class="help-block">{{ $errors->first('country') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- User State -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('state') ? 'has-error' : '' }}">
                                 <select class="form-control" id="putStates" name="state" onchange="getCities(this.value)">
                                    @foreach ($states as $state)
                                    <option value="{{ $state->id }}" {{ $state->id == $user->state ? 'selected' : '' }}>{{ $state->name }}</option>
                                    @endforeach
                                </select>
                                <label for="putStates">State</label>
                                @if ($errors->has('state'))
                                <span class="help-block">{{ $errors->first('state') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- User City -->
                        {{--<div class="col-md-6">--}}
                            {{--<div class="form-group form-md-line-input {{ $errors->has('city') ? 'has-error' : '' }}">--}}
                                 {{--<select class="form-control" id="putCities" name="city">--}}
                                    {{--@foreach ($cities as $city)--}}
                                    {{--<option value="{{ $city->id }}" {{ $city->id == $user->city ? 'selected' : '' }}>{{ $city->name }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                                {{--<label for="putCities">City</label>--}}
                                {{--@if ($errors->has('city'))--}}
                                {{--<span class="help-block">{{ $errors->first('city') }}</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <!-- Phone Number -->
                        <div class="col-md-6">

                            <div class="row">

                                <!-- Phone Code -->
                                <div class="col-md-3">
                                    <div class="form-group form-md-line-input {{ $errors->has('phonecode') ? 'has-error' : '' }}">
                                        <input type="text" readonly="" class="form-control" id="putPhoneCode" value="+{{ $default_country->phonecode }}" name="phonecode">
                                        <label for="putPhoneCode">Code</label>
                                        @if ($errors->has('phonecode'))
                                        <span class="help-block">{{ $errors->first('phonecode') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <!-- Phone Number -->
                                <div class="col-md-9">
                                    <div class="form-group form-md-line-input {{ $errors->has('phone') ? 'has-error' : '' }}">
                                        <input type="text" class="form-control" id="phone" placeholder="Enter phone number" value="{{ $user->phone }}" name="phone">
                                        <label for="phone">Phone Number</label>
                                        @if ($errors->has('phone'))
                                        <span class="help-block">{{ $errors->first('phone') }}</span>
                                        @endif
                                    </div>
                                </div>


                            </div>

                        </div>

                        <!-- Hide Phone Number -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('phone_hidden') ? 'has-error' : '' }}">
                                 <select class="form-control" id="phone_hidden" name="phone_hidden">
                                    @if ($user->phone_hidden)
                                    <option value="1">Hidden</option>
                                    <option value="0">Visible</option>
                                    @else
                                    <option value="0">Visible</option>
                                    <option value="1">Hidden</option>
                                    @endif
                                </select>
                                <label for="phone_hidden">Hide Phone Number</label>
                                @if ($errors->has('phone_hidden'))
                                <span class="help-block">{{ $errors->first('phone_hidden') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Gender -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('gender') ? 'has-error' : '' }}">
                                 <select class="form-control" id="gender" name="gender">
                                    @if ($user->gender)
                                    <option value="1">Male</option>
                                    <option value="0">Female</option>
                                    @else
                                    <option value="0">Female</option>
                                    <option value="1">Male</option>
                                    @endif
                                </select>
                                <label for="gender">Gender</label>
                                @if ($errors->has('gender'))
                                <span class="help-block">{{ $errors->first('gender') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Is Administrator -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('is_admin') ? 'has-error' : '' }}">
                                 <select class="form-control" id="is_admin" name="is_admin">
                                    @if ($user->is_admin)
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                    @else
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                    @endif
                                </select>
                                <label for="is_admin">Is Administrator</label>
                                @if ($errors->has('is_admin'))
                                <span class="help-block">{{ $errors->first('is_admin') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Account Type -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('account_type') ? 'has-error' : '' }}">
                                 <select class="form-control" id="account_type" name="account_type">
                                    @if ($user->account_type)
                                    <option value="1">Professional</option>
                                    <option value="0">Standard</option>
                                    @else
                                    <option value="0">Standard</option>
                                    <option value="1">Professional</option>
                                    @endif
                                </select>
                                <label for="account_type">Account Type</label>
                                @if ($errors->has('account_type'))
                                <span class="help-block">{{ $errors->first('account_type') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Status -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('status') ? 'has-error' : '' }}">
                                 <select class="form-control" id="status" name="status">
                                    @if ($user->status)
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                    @else
                                    <option value="0">Inactive</option>
                                    <option value="1">Active</option>
                                    @endif
                                </select>
                                <label for="status">Status</label>
                                @if ($errors->has('status'))
                                <span class="help-block">{{ $errors->first('status') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <!-- Change Avatar -->
                            <div class="form-group form-md-line-input {{ $errors->has('avatar') ? 'has-error' : '' }}">
                                <input type="file" name="avatar" class="form-control" id="avatar">
                                <label for="avatar">Edit Avatar</label>
                                @if ($errors->has('avatar'))
                                <span class="help-block">{{ $errors->first('avatar') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-6">
                            <!-- Change Avatar -->
                            <div class="form-group form-md-line-input">
                                <input type="file" name="profile_image" class="form-control" id="avatar">
                                <label for="avatar">Upload Profile Image</label>

                            </div>
                        </div>


                        <hr>

                        <!-- Update Password -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('password') ? 'has-error' : '' }}">
                                <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password">
                                <label for="password">New Password</label>
                                @if ($errors->has('password'))
                                <span class="help-block">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Confirm Password -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                <input type="password" class="form-control" id="password_confirmation" placeholder="Confirm Password" name="password_confirmation">
                                <label for="password_confirmation">Confirm Password</label>
                                @if ($errors->has('password_confirmation'))
                                <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                            </div>
                        </div>



                        <div class="col-md-6 ">
                            <label>Users Bio</label>
                            {{--<div class="col-md-10">--}}
                            <textarea class="form-control" rows="3" name="bio" placeholder="Enter your bio">
                             {{ $user->bio }}
                            </textarea>
                                <div class="form-control-focus"> </div>

                            {{--</div>--}}
                        </div>

                        <div class="col-md-6 ">
                            <label>is Author&nbsp;&nbsp;</label>
                            <input type="checkbox"  name="is_author"{{ $user->is_author ? 'checked' : '' }} value="1">

                            <div class="form-control-focus"> </div>

                        </div>
                        <br>

                        <!-- Update Google Profile -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('google_id') ? 'has-error' : '' }}">
                                <input type="text" class="form-control" value="{{ $user->google_id }}" id="google_id" placeholder="Enter Google Url" name="google_id">
                                <label for="password">Google Url Profile</label>
                                @if ($errors->has('google_id'))
                                    <span class="help-block">{{ $errors->first('google_id') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Update Facebook Profile -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('facebook_id') ? 'has-error' : '' }}">
                                <input type="text" class="form-control" value="{{ $user->facebook_id }}" id="facebook_id" placeholder="Enter Facebook Profile Url" name="facebook_id">
                                <label for="facebook_id">Facebook Profile Url</label>
                                @if ($errors->has('facebook_id'))
                                    <span class="help-block">{{ $errors->first('facebook_id') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Update Twitter Profile -->
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input {{ $errors->has('twitter_id') ? 'has-error' : '' }}">
                                <input type="text" value="{{ $user->twitter_id }}" class="form-control" id="twitter_id" placeholder="Enter Twitter Url" name="twitter_id">
                                <label for="twitter_id">Twitter Profile Url</label>
                                @if ($errors->has('twitter_id'))
                                    <span class="help-block">{{ $errors->first('twitter_id') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" style="width: 100%" class="btn blue">Update</button>
                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

</div>

<script type="text/javascript">

    /**
    * Get States
    */
    function getStates(country) {
        var _root = $('#root').attr('data-root');
        var country_id = country;
        $.ajax({
            type: "GET",
            url: _root + '/tools/geo/states/states_by_country',
            data: {
                country_id: country_id
            },
            success: function(response) {
                if (response.status == 'success') {
                    $('#putStates').find('option').remove();
                    $('#putStates').append($('<option>', {
                        text: 'Selecionar Estado',
                        value: 'all'
                    }));
                    $.each(response.data, function(array, object) {
                        $('#putStates').append($('<option>', {
                            value: object.id,
                            text: object.name
                        }))
                    });

                    // Change phonecode
                    document.getElementById('putPhoneCode').value = '+'+response.phonecode;
                }
                if (response.status == 'error') {
                    alert(response.msg)
                }
            }
        })
    }

    /**
    * Get Cities
    */
    function getCities(state) {
        var _root = $('#root').attr('data-root');
        var state_id = state;
        $.ajax({
            type: "GET",
            url: _root + '/tools/geo/cities/cities_by_state',
            data: {
                state_id: state_id
            },
            success: function(response) {
                if (response.status == 'success') {
                    $('#putCities').find('option').remove();
                    $('#putCities').append($('<option>', {
                        text: 'Selecionar Cidade',
                        value: 'all'
                    }));
                    $.each(response.data, function(array, object) {
                        $('#putCities').append($('<option>', {
                            value: object.id,
                            text: object.name
                        }))
                    });
                }
                if (response.status == 'error') {
                    alert(response.msg)
                }
            }
        })
    }

</script>

@endsection