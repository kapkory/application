@extends ('dashboard.layout.app')

@section ('content')

    <!-- Edit Store -->
    <div class="row">

        <div class="col-md-12">

            <!-- Session Messages -->
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <div class="portlet light ">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <span class="caption-subject font-blue bold uppercase">Create Store</span>
                    </div>
                </div>

                <div class="portlet-body">

                    <form method="POST" action="{{ Protocol::home() }}/dashboard/stores/create" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="row">

                            <!-- Store Title -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('title') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" id="title" placeholder="Enter store title" value="" name="title">
                                    <label for="title">Store Title</label>
                                    @if ($errors->has('title'))
                                        <span class="help-block">{{ $errors->first('title') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Store Username -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('username') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" id="username" placeholder="Enter store username" value="" name="username">
                                    <label for="username">Store Username</label>
                                    @if ($errors->has('username'))
                                        <span class="help-block">{{ $errors->first('username') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Short Description -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('short_desc') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" id="short_desc" placeholder="Enter short description" value="" name="short_desc">
                                    <label for="short_desc">Short Description</label>
                                    @if ($errors->has('short_desc'))
                                        <span class="help-block">{{ $errors->first('short_desc') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Long Description -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('long_desc') ? 'has-error' : '' }}">
                                    <textarea rows="4" class="form-control" id="long_desc" placeholder="Enter long description" name="long_desc"></textarea>
                                    <label for="long_desc">Long Description</label>
                                    @if ($errors->has('long_desc'))
                                        <span class="help-block">{{ $errors->first('long_desc') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Store Category -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('category') ? 'has-error' : '' }}">
                                    <select class="form-control" id="category" name="category">
                                        <option></option>
                                        @if(count(Helper::parent_categories()))
                                            @foreach (Helper::parent_categories() as $parent)
                                                <optgroup label="{{ $parent->category_name }}">
                                                    @if (count(Helper::sub_categories($parent->id)))
                                                        @foreach (Helper::sub_categories($parent->id) as $sub)
                                                            <option value="{{ $sub->id }}">{{ $sub->category_name }}</option>
                                                        @endforeach
                                                    @endif
                                                </optgroup>
                                            @endforeach
                                        @endif
                                    </select>
                                    <label for="category">Store Category</label>
                                    @if ($errors->has('category'))
                                        <span class="help-block">{{ $errors->first('category') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Country -->
                            <div class="col-md-6">
                                <div class="form-group form-group-material {{ $errors->has('country') ? 'has-error' : '' }}">
                                    <label>{{ Lang::get('create/ad.lang_country') }}</label>
                                    <select required="" class="form-control" name="country" onchange="getStates(this.value)">
                                        @foreach ($countries as $country)
                                            <option value="{{ $country->sortname }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('country'))
                                        <span class="help-block">
										{{ $errors->first('country') }}
									</span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-12">
{{--                            @if (Helper::settings_geo()->states_enabled)--}}
                                <!-- State -->
                                    <div class="{{ !Helper::settings_geo()->cities_enabled ? 'col-md-12' : 'col-md-6' }}">
                                        <div class="form-group form-group-material {{ $errors->has('state') ? 'has-error' : '' }}">
                                            <label>{{ Lang::get('create/ad.lang_state') }}</label>
                                            <select required="" data-placeholder="{{ Lang::get('create/ad.lang_select_state') }}" class="form-control" name="state" onchange="getCities(this.value)" id="putStates">

                                            </select>
                                            @if ($errors->has('state'))
                                                <span class="help-block">{{ $errors->first('state') }}</span>
                                            @endif
                                        </div>
                                    </div>
                            {{--@endif--}}


{{--                            @if (Helper::settings_geo()->cities_enabled)--}}
                                <!-- city -->
                                    {{--<div class="col-md-6">--}}
                                        {{--<div class="form-group form-md-line-input {{ $errors->has('city') ? 'has-error' : '' }}">--}}
                                            {{--<select required="" data-placeholder="{{ Lang::get('create/ad.lang_select_city') }}" class="form-control" name="city" id="putCities">--}}


                                            {{--</select>--}}
                                            {{--<label>{{ Lang::get('create/ad.lang_city') }}</label>--}}
                                            {{--@if ($errors->has('city'))--}}
                                                {{--<span class="help-block">{{ $errors->first('city') }}</span>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                            </div>



                            @if($users)
                                <div class="col-md-6">
                                    <div class="form-group form-group-material {{ $errors->has('users') ? 'has-error' : '' }}">
                                        <label>Select User</label>
                                        <select required="" class="form-control" name="user" >
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->first_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('country'))
                                            <span class="help-block">
										{{ $errors->first('country') }}
									</span>
                                        @endif
                                    </div>
                                </div>
                        @endif









                            <!-- Status -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('status') ? 'has-error' : '' }}">
                                    <select class="form-control" id="status" name="status">

                                            <option value="0">Inactive</option>
                                            <option value="1">Active</option>

                                    </select>
                                    <label for="status">Store Status</label>
                                    @if ($errors->has('status'))
                                        <span class="help-block">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Change Logo -->
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input {{ $errors->has('logo') ? 'has-error' : '' }}">
                                    <input accept="image/*" type="file" name="logo" class="form-control" id="logo">
                                    <label for="logo">Edit Logo</label>
                                    @if ($errors->has('logo'))
                                        <span class="help-block">{{ $errors->first('logo') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Change Cover -->
                            <div class="col-md-12">
                                <div class="form-group form-md-line-input {{ $errors->has('cover') ? 'has-error' : '' }}">
                                    <input accept="image/*" type="file" name="cover" class="form-control" id="cover">
                                    <label for="cover">Edit Cover</label>
                                    @if ($errors->has('cover'))
                                        <span class="help-block">{{ $errors->first('cover') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Store Address -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('address') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" id="address" placeholder="Enter store address" value="" name="address">
                                    <label for="address">Store Address</label>
                                    @if ($errors->has('address'))
                                        <span class="help-block">{{ $errors->first('address') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Facebook Page -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('fb_page') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" id="fb_page" placeholder="Enter facebook page" value="" name="fb_page">
                                    <label for="fb_page">Facebook Page</label>
                                    @if ($errors->has('fb_page'))
                                        <span class="help-block">{{ $errors->first('fb_page') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Twitter Page -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('tw_page') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" id="tw_page" placeholder="Enter twitter page" value="" name="tw_page">
                                    <label for="tw_page">Twitter Page</label>
                                    @if ($errors->has('tw_page'))
                                        <span class="help-block">{{ $errors->first('tw_page') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Google Page -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('go_page') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" id="go_page" placeholder="Enter google page" value="" name="go_page">
                                    <label for="fb_page">Google Page</label>
                                    @if ($errors->has('go_page'))
                                        <span class="help-block">{{ $errors->first('go_page') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Youtube Page -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('yt_page') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" id="yt_page" placeholder="Enter youtube page" value="" name="yt_page">
                                    <label for="fb_page">Youtube Page</label>
                                    @if ($errors->has('yt_page'))
                                        <span class="help-block">{{ $errors->first('yt_page') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Website -->
                            <div class="col-md-6">
                                <div class="form-group form-md-line-input {{ $errors->has('website') ? 'has-error' : '' }}">
                                    <input type="text" class="form-control" id="website" placeholder="Enter website" value="" name="website">
                                    <label for="fb_page">Website</label>
                                    @if ($errors->has('website'))
                                        <span class="help-block">{{ $errors->first('website') }}</span>
                                    @endif
                                </div>
                            </div>
                            <!-- Meta Title -->


                            <!-- Store Website -->
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                                    <label>Meta Title</label>
                                    <input class="form-control input-sm" placeholder="Meta Title" type="text" value="" name="meta_title">
                                    @if ($errors->has('meta_title'))
                                        <span class="help-block">
										{{ $errors->first('meta_title') }}
									</span>
                                    @endif
                                </div>
                            </div>



                            <!-- Long Description -->
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                                    <label>Meta Description</label>
                                    <textarea required="" class="form-control input-sm" placeholder="Meta Description field" rows="4" name="meta_description"></textarea>
                                    @if ($errors->has('meta_description'))
                                        <span class="help-block">
										{{ $errors->first('meta_description') }}
									</span>
                                    @endif
                                </div>
                            </div>


                            <div class="col-md-12">
                                <button style="width: 100%" type="submit" class="btn default">Create Store</button>
                            </div>

                        </div>

                    </form>

                </div>
            </div>

        </div>

    </div>
    <script type="text/javascript">

        /**
         * Get States
         */
        function getStates(country) {
            var _root = $('#root').attr('data-root');
            var country_id = country;
            $.ajax({
                type: "GET",
                url: _root + '/tools/geo/states/states_by_country',
                data: {
                    country_id: country_id
                },
                success: function(response) {
                    if (response.status == 'success') {

                        // Check if states enabled
                        if (response.states) {

                            $('#putStates').find('option').remove();
                            $('#putStates').append($('<option>', {
                                text: 'Select state',
                                value: 'all'
                            }));
                            $.each(response.data, function(array, object) {
                                $('#putStates').append($('<option>', {
                                    value: object.id,
                                    text: object.name
                                }))
                            });

                        }
                        else if (response.cities) {

                            // Cities
                            $('#putCities').find('option').remove();
                            $('#putCities').append($('<option>', {
                                text: 'Select city',
                                value: 'all'
                            }));
                            $.each(response.data, function(array, object) {
                                $('#putCities').append($('<option>', {
                                    value: object.id,
                                    text: object.name
                                }))
                            });

                        }
                    }
                    if (response.status == 'error') {
                        alert(response.msg)
                    }
                }
            })
        }

        /**
         * Get Cities
         */
        function getCities(state) {
            var _root = $('#root').attr('data-root');
            var state_id = state;
            $.ajax({
                type: "GET",
                url: _root + '/tools/geo/cities/cities_by_state',
                data: {
                    state_id: state_id
                },
                success: function(response) {
                    if (response.status == 'success') {
                        $('#putCities').find('option').remove();
                        $('#putCities').append($('<option>', {
                            text: 'Select city',
                            value: 'all'
                        }));
                        $.each(response.data, function(array, object) {
                            $('#putCities').append($('<option>', {
                                value: object.id,
                                text: object.name
                            }))
                        });
                    }
                    if (response.status == 'error') {
                        alert(response.msg)
                    }
                }
            })
        }

    </script>
@endsection