@extends ('dashboard.layout.app')

{{--@section('head')--}}
    {{--<script src="plugins.js"></script>--}}
    {{--@endsection--}}
@section ('content')

<div class="row">

	<div class="col-md-12">

        <!-- Session Messages -->
        @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
        </div>
        @endif

		<div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject bold font-blue uppercase">Edit Article</span>
                </div>
            </div>
            <div class="portlet-light">
                <form role="form" action="{{ Protocol::home() }}/dashboard/articles/edit/{{ $article->id }}" method="POST" enctype="multipart/form-data">

                	{{ csrf_field() }}

                	<!-- Article Title -->
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label class="control-label">Article Title</label>
                        <input type="text" class="form-control" name="title" value="{{ $article->title }}">
                        @if ($errors->has('title'))
                        <span class="help-block">{{ $errors->first('title') }}</span>
                        @endif
                    </div>


                        <!-- Article Slug -->
                        <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                            <label class="control-label">Article Slug</label>
                            <input type="text" class="form-control" name="slug" value="{{ $article->slug }}">
                            @if ($errors->has('title'))
                                <span class="help-block">{{ $errors->first('slug') }}</span>
                            @endif
                        </div>

                    <!-- Upload Cover -->
                    <div class="form-group {{ $errors->has('cover') ? 'has-error' : '' }}">
                        <label class="control-label">Edit Cover</label>
                        <input type="file" name="cover"/>
                        @if ($errors->has('cover'))
                        <span class="help-block">{{ $errors->first('cover') }}</span>
                        @endif
                    </div>

                        <div class="form-group form-md-line-input ">
                            <label class="control-label" for="tags">Tags Name</label>
                             {{--{{ $article->tags->name }}--}}
                                <input type="text" class="form-control" name="tags" placeholder="tags name separated by comma" value="
                                       @if($article->tags)
                                        @foreach($article->tags as $tag)
                                        {{ $tag->name.',' }}
                                        @endforeach
                                        @endif
" data-role="tagsinput">


                        </div>

                        <div class="form-group form-md-line-input ">
                            <label class="control-label" for="tags">Category</label>

                                <select name="article_category" class="form-control">
                                    @foreach($categories as $category)
                                       @if($category->id == $article->article_category_id)
                                            <option selected value="{{ $category->id }}">{{ $category->name }}</option>

                                        @else
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                       @endif
                                            @endforeach
                                </select>

                                <div class="form-control-focus"> </div>


                        </div>

                    <!-- Content -->
                    <div class="form-group form-md-line-input">
                        <label class="control-label">Article Content</label>
                        <textarea name="content"><?php echo  str_replace('<div class="shortcode"></div>','[toc]',$article->content);  ?></textarea>
                        <script>
                            CKEDITOR.replace( 'content',{
                                allowedContent :true
                            } );
                        </script>
                    </div>



                        <div class="form-group form-md-line-input ">
                            <label class="control-label" for="tags">Meta Title</label>

                                <input type="text" class="form-control" name="meta_title" placeholder="Enter Meta Title" value="{{ $article->meta_title }}">
                                {{--<input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">--}}
                                <div class="form-control-focus"> </div>


                        </div>

                        <div class="form-group form-md-line-input ">
                            <label class=" control-label" for="tags">Meta Description</label>

                            <textarea class="form-control" name="meta_description" rows="4" cols="50">
                          {{ $article->meta_description }}
                            </textarea>


                        </div>

                        <div class="form-group form-md-line-input ">

                            <div class="col-md-10">

                                <div class="checkbox">
                                    <label class="col-md-2 control-label">Show Author </label>&nbsp;&nbsp;
                                    <input type="checkbox" {{ ($article->show_author == 1  ) ? 'checked' : '' }} name="show_author" value="" >
                                </div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input ">

                            {{--<label class="col-md-2 control-label" for="tags">Is Featured</label>--}}
                            <div class="col-md-10">
                                {{--<input type="checkbox" class="form-control" name="is_featured"  value="" style="float: left">--}}

                                {{--<div class="form-control-focus"> </div>--}}
                                <div class="checkbox">
                                    <label class="col-md-2 control-label">Featured</label>&nbsp;&nbsp;
                                    <input type="checkbox" {{ ($article->is_featured == 1) ? 'checked' : '' }} name="is_featured" value="" >
                                </div>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input ">
                            <label class="col-md-2 control-label" for="tags">Post as</label>
                            <div class="col-md-10">
                                <select name="user_id" class="form-control">
                                    @foreach($users as $n_user)
                                        <option value="{{ $n_user->id }}" {{ ($n_user->id == $article->user_id) ? 'selected' : '' }}>{{ $n_user->first_name.'  '.$n_user->last_name }}</option>
                                    @endforeach
                                </select>
                                {{--<input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">--}}
                                <div class="form-control-focus"> </div>

                            </div>
                        </div>
                    <!-- Save Changes -->
                    <div class="margin-top-10">
                        <button type="submit" class="btn default" style="width: 100%;">Save Changes </button>
                    </div>
                </form>
            </div>
        </div>

	</div>

</div>



@endsection