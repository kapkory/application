@extends ('dashboard.layout.app')

@section ('content')

    <!-- Comments -->
    <div class="row">

        <div class="col-md-12">

            <!-- Session Messages -->
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <div class="portlet light">

                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <span class="caption-subject font-blue-madison bold uppercase">Comments Settings</span>
                    </div>
                </div>

                <div class="portlet-body">

                    <div class="table-responsive">

                        <table class="table table-hover table-outline m-b-0 hidden-sm-down">
                            <thead class="thead-default">
                            <tr>
                                <th class="text-center"><i class="icon-people"></i></th>
                                <th>User</th>
                                <th class="text-center">Article ID</th>
                                <th class="text-center">Content</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Created At</th>
                                <th class="text-center">Options</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if($comments)
                                @foreach ($comments as $comment)
                                    <tr>

                                        <!-- User Avatar -->
                                        <td class="text-center">
                                            <div class="avatar">

                                                @if (Profile::hasStore($comment->user_id))
                                                    <img src="{{ url(Profile::comment_picture($comment->user_id)) }}" class="img-avatar" alt="{{ Profile::hasStore($comment->user_id)->title }}">

                                               @elseif($comment->user_id == 0)
                                                    <img src="{{ url('uploads/avatars/noavatar.png') }}" class="img-avatar" alt="{{ ($comment->user_id == 0) ? $comment->name : Helper::username_by_id($comment->user_id) }}">

                                                @else
                                                    <img src="{{ Profile::picture($comment->user_id) }}" class="img-avatar" alt="{{ ($comment->user_id == 0) ? $comment->name : Helper::username_by_id($comment->user_id) }}">
/
                                                @endif
                                            </div>
                                        </td>

                                        <!-- User Info -->
                                        <td>
                                            @if (Profile::hasStore($comment->user_id))
                                                <a href="{{ Protocol::home() }}/dashboard/users/details/{{ Helper::username_by_id($comment->user_id) }}">{{ Profile::hasStore($comment->user_id)->title }}</a>
                                                <div class="small text-muted">
                                                    <span>{{ Profile::hasStore($comment->user_id)->username }}</span> | {{ Helper::date_ago($comment->created_at) }}
                                                </div>
                                            @elseif($comment->user_id == 0)
                                                <b>{{ ucwords($comment->name) }}</b>
                                                <br><a href="#">{{ $comment->email }}</a>

                                            @else
                                                <a href="{{ Protocol::home() }}/dashboard/users/details/{{ Helper::username_by_id($comment->user_id) }}">{{ Profile::full_name($comment->user_id) }}</a>
                                                <div class="small text-muted">
                                                    <span>{{ Helper::username_by_id($comment->user_id) }}</span> | {{ Helper::date_ago($comment->created_at) }}
                                                </div>
                                            @endif
                                        </td>

                                        <!-- Ad ID -->
                                        <td class="text-center">
                                            <a href="{{ Protocol::home() }}/article/{{ Helper::getArticle($comment->article_id)->slug }}" target="_blank" class="text-muted">{{  Helper::getArticle($comment->article_id)->title }}</a>
                                        </td>

                                        <td class="text-center">
                                            {{ $comment->content }}
                                        </td>

                                        <!-- Status -->
                                        <td class="text-center">
                                            @if ($comment->status)
                                                <span class="badge badge-success badge-roundless"> Published </span>
                                            @else
                                                <span class="badge badge-danger badge-roundless"> Pending </span>
                                            @endif
                                        </td>

                                        <!-- Created At -->
                                        <td class="text-center text-muted">
                                            {{ Helper::date_ago($comment->created_at) }}
                                        </td>

                                        <!-- Options -->
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <i style="color: #405a72;font-size: 18px;cursor: pointer;" class="icon-settings dropdown-toggle" data-toggle="dropdown"></i>

                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li>
                                                        <a href="{{ Protocol::home() }}/dashboard/article-comments/read/{{ $comment->id }}">
                                                            <i class="glyphicon glyphicon-eye-open"></i> Read Comment</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ Protocol::home() }}/dashboard/article-comments/edit/{{ $comment->id }}">
                                                            <i class="glyphicon glyphicon-pencil"></i> Edit Comment</a>
                                                    </li>
                                                    <li>
                                                        @if ($comment->status)
                                                            <a href="{{ Protocol::home() }}/dashboard/article-comments/inactive/{{ $comment->id }}">
                                                                <i class="glyphicon glyphicon-remove"></i> Inactive Comment</a>
                                                        @else
                                                            <a href="{{ Protocol::home() }}/dashboard/article-comments/active/{{ $comment->id }}">
                                                                <i class="glyphicon glyphicon-ok"></i> Active Comment</a>
                                                        @endif
                                                    </li>

                                                    <li class="divider"> </li>
                                                    <li>
                                                        <a style="color: #dd2c2c;text-transform: uppercase;" href="{{ Protocol::home() }}/dashboard/article-comments/delete/{{ $comment->id }}">
                                                            <i style="color: #dd2c2c;" class="glyphicon glyphicon-trash"></i> Delete Comment</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>

                    </div>

                    @if ($comments)
                        <div class="text-center">
                            {{ $comments->links() }}
                        </div>
                    @endif

                </div>

            </div>

        </div>

    </div>

@endsection