@extends ('dashboard.layout.app')

@section ('content')
<!-- Articles -->
<div class="row">
	
	<div class="col-md-12">

		<!-- Sessions Messages -->
        @if (Session::has('success'))
        <div class="custom-alerts alert alert-success fade in">
            {{ Session::get('success') }}
        </div>
        @endif

        @if (Session::has('error'))
        <div class="custom-alerts alert alert-danger fade in">
            {{ Session::get('error') }}
        </div>
        @endif
		
		<div class="portlet light ">

            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <span class="caption-subject font-blue bold uppercase">Articles Settings</span>
                </div>
                <div class="actions">
                	<a href="{{ Protocol::home() }}/dashboard/articles/create" class="btn dark btn-outline sbold uppercase">Create Article</a>
                </div>
            </div>

			<div class="portlet-body">

				<div class="">
					<table class="table table-hover table-outline m-b-0 hidden-sm-down">
			            <thead class="thead-default">
			                <tr>
			                    <th class="text-center"><i class="icon-link"></i></th>
			                    <th>Title</th>
			                    <th class="text-center">Cover</th>
			                    <th class="text-center">Created at</th>
			                    <th class="text-center">Status</th>
			                    <th class="text-center">Options</th>
			                </tr>
			            </thead>
			            <tbody>

							@if (count($articles))
							@foreach ($articles as $article)
			                <tr>

			                	<!-- Articles ID -->
			                    <td class="text-center text-muted">
			                        {{ $article->id }}
			                    </td>

			                    <!-- Title -->
			                    <td class="text-muted">
			                    	<a href="{{ Protocol::home() }}/article/{{ $article->slug }}" class="text-muted" target="_blank">{{ $article->title }}</a>
			                    </td>

			                    <!-- Cover -->
			                    <td class="text-center">
			                    	<a class="text-muted" href="{{ Protocol::home() }}/uploads/articles/{{ $article->cover }}">View Cover</a>
			                    </td>

			                    <!-- Created at -->
			                    <td class="text-muted text-center"> 
			                    	{{ Helper::date_ago($article->created_at) }}
			                    </td>

			                    <!-- Updated at -->
			                    <td class="text-muted text-center">
									@if($article->status == 0)
										<span class="label label-info" style="border-radius: .25em !important;">Draft</span>
										@else
										<span class="label label-success" style="background-color: green;border-radius: .25em !important;">Published</span>
										@endif

			                    </td>

			                    <!-- Options -->
	                            <td class="text-center">
	                                <div class="btn-group">
	                                    <i style="color: #405a72;font-size: 18px;cursor: pointer;" class="icon-settings dropdown-toggle" data-toggle="dropdown"></i>

	                                    <ul class="dropdown-menu pull-right" role="menu">
	                                        <li>
	                                            <a href="{{ Protocol::home() }}/dashboard/articles/edit/{{ $article->id }}">
	                                                <i class="glyphicon glyphicon-pencil"></i> Edit Article</a>
	                                        </li>
											<li>
												<a class="delete_article"   data-href="{{ Protocol::home() }}/dashboard/articles/delete/{{ $article->id }}">
													<i class="glyphicon glyphicon-remove"></i> Delete Article</a>
											</li>
											@if($article->status == 0)
	                                        <li>
	                                            <a href="{{ Protocol::home() }}/dashboard/articles/publish/{{ $article->id }}">
	                                                <i class="glyphicon glyphicon-ok"></i> Publish Article</a>
	                                        </li>
												@endif
	                                    </ul>
	                                </div>
	                            </td>
			                </tr>
			                @endforeach
			                @endif

			            </tbody>
			        </table>

					@if (count($articles) > 0)
					<div class="text-center">
						{{ $articles->links() }}
					</div>
					@endif
                    <script>
                        $('.delete_article').click(function () {
                            var href = $(this).data('href');
                            console.log('delete url is '+href);
                            Swal({
                                title: 'Are you sure?',
                                text: 'You will not be able to recover this imaginary file!',
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonText: 'Yes, delete it!',
                                cancelButtonText: 'No, keep it'
                            }).then((result) => {
                                if (result.value) {
                                    console.log('url is '+href);
                                    $.get(href,function (response) {
                                        // console.log('your file has been deleted');
                                        window.location.href = "{{ url()->current() }}";
                                    })
                                // Swal(
                                //     'Deleted!',
                                //     'Your imaginary file has been deleted.',
                                //     'success'
                                // )
                                // For more information about handling dismissals please visit
                                // https://sweetalert2.github.io/#handling-dismissals
                            } else if (result.dismiss === Swal.DismissReason.cancel) {
                                Swal(
                                    'Cancelled',
                                    'Your Article is safe :)',
                                    'error'
                                )
                            }
                        })
                        })
                    </script>
		        </div>

		    </div>
		</div>

	</div>

</div>

@endsection