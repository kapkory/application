@extends ('dashboard.layout.app')

@section ('content')

<div class="row">
    <div class="col-md-12">

        <!-- Session Messages -->
        @if (Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }} 
        </div>
        @endif
        @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }} 
        </div>
        @endif

        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <span class="caption-subject font-blue-madison bold uppercase">Create New Article</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" class="form-horizontal" action="{{ Protocol::home() }}/dashboard/articles/create" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-body">

                        <!-- Title -->
                        <div class="form-group form-md-line-input {{ $errors->has('title') ? 'has-error' :'' }}">
                            <label class="col-md-2 control-label" for="title">Article Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="title" placeholder="Article Title" name="title" value="{{ old('title') }}">
                                <div class="form-control-focus"> </div>
                                @if ($errors->has('title'))
                                <span class="help-block">{{ $errors->first('title') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group form-md-line-input {{ $errors->has('slug') ? 'has-error' :'' }}">
                            <label class="col-md-2 control-label" for="cover">Article Slug</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="cover"  name="slug" value="{{ old('slug') }}">
                                <div class="form-control-focus"> </div>
                                @if ($errors->has('slug'))
                                    <span class="help-block">{{ $errors->first('slug') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Cover -->
                        <div class="form-group form-md-line-input {{ $errors->has('cover') ? 'has-error' :'' }}">
                            <label class="col-md-2 control-label" for="cover">Article Cover</label>
                            <div class="col-md-10">
                                <input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">
                                <div class="form-control-focus"> </div>
                                @if ($errors->has('cover'))
                                <span class="help-block">{{ $errors->first('cover') }}</span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group form-md-line-input ">
                            <label class="col-md-2 control-label" for="tags">Tags Name</label>
                            <div class="col-md-10">
                                <input type="text" name="tags" placeholder="tags name separated by comma" value="" data-role="tagsinput">
                                {{--<input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">--}}
                                <div class="form-control-focus"> </div>

                            </div>
                        </div>


                        <div class="form-group form-md-line-input ">
                            <label class="col-md-2 control-label" for="tags">Category</label>
                            <div class="col-md-10">
                                <select name="article_category" class="form-control">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                </select>
                                {{--<input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">--}}
                                <div class="form-control-focus"> </div>

                            </div>
                        </div>



                        <!-- Content -->
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="content">Article Content</label>
                            <div class="col-md-10">
                                <textarea name="content">{{ old('content') }}</textarea>
                                <script>
                                    CKEDITOR.replace( 'content' );
                                </script>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input ">
                            <label class="col-md-2 control-label" for="tags">Meta Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="meta_title" placeholder="Enter Meta Title" value="">
                                {{--<input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">--}}
                                <div class="form-control-focus"> </div>

                            </div>
                        </div>

                        <div class="form-group form-md-line-input ">
                            <label class="col-md-2 control-label" for="tags">Meta Description</label>
                            <div class="col-md-10">
                            <textarea class="form-control" name="meta_description" cols="40" rows="4">

                            </textarea>
                                <div class="form-control-focus"> </div>

                            </div>
                        </div>



                        <div class="form-group form-md-line-input ">
                            <label class="col-md-2 control-label" for="tags">Post as</label>
                            <div class="col-md-10">
                                <select name="user_id" class="form-control">
                                    @foreach($users as $n_user)
                                        <option value="{{ $n_user->id }}">{{ $n_user->first_name.'  '.$n_user->last_name }}</option>
                                    @endforeach
                                </select>
                                {{--<input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">--}}
                                <div class="form-control-focus"> </div>

                            </div>
                        </div>

                        <div class="form-group form-md-line-input ">

                            <div style="padding-left: 100px">

                                <div class="checkbox">
                                    <label class="col-md-2 control-label">Show Author</label>&nbsp;&nbsp;
                                    <input type="checkbox" name="show_author" checked >
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input ">

                            {{--<label class="col-md-2 control-label" for="tags">Is Featured</label>--}}
                            <div style="padding-left: 100px">
                                {{--<input type="checkbox" class="form-control" name="is_featured"  value="" style="float: left">--}}

                                {{--<div class="form-control-focus"> </div>--}}
                                <div class="checkbox">
                                    <label class="col-md-2 control-label">Featured</label>&nbsp;&nbsp;
                                    <input type="checkbox" name="is_featured" >
                                </div>
                            </div>
                        </div>






                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="submit" class="btn blue">Create Article</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection