<table class="table table-hover table-outline m-b-0 hidden-sm-down">
    <thead class="thead-default">
    <tr>
        {{--<th class="text-center"><i class="icon-link"></i></th>--}}
        <th>File</th>
        <th class="text-center">Author</th>
        <th class="text-center">Name</th>
        <th class="text-center">Date</th>
        {{--<th class="text-center">Meta Description</th>--}}
        {{--<th class="text-center">Document</th>--}}
        <th class="text-center">Options</th>
    </tr>
    </thead>
    <tbody>
    
    @if ($images)
        @foreach ($images as $image)

            <tr>

                <!-- Ad Image -->
                <td class="text-center" style="width: 15%;">
                    {{--<a target="_blank" href="#" class="text-dots tooltips" data-container="body" data-placement="top" data-original-title="{{ $image->name }}">{{ $image->name }}</a>--}}

                    <div class="avatar">
                        <a data-fancybox data-caption="My caption" data-width="1024" data-height="683">
                        <img src="{{ url($image->path) }}" class="img-avatar" alt="{{ $image->name }}">
                        </a>
                    </div>
                </td>

                <!-- Ad Info -->
                <td>
                    {{--<a target="_blank" href="{{ url('catalogue/download/'. $image->slug) }}" class="text-dots tooltips" data-container="body" data-placement="top" data-original-title="{{ $image->title }}">{{ $image->title }}</a>--}}
                    <div class="small text-muted">
                        {{ $image->user->first_name }}
                    </div>
                </td>



                <!-- Ad Price -->
                <td class="text-center text-muted">
                    {{ $image->name }}
                </td>

                <!-- Is a featured Ad -->
                <td class="text-center">
                    {{ $image->created_at->diffForHumans() }}
                </td>




                <!-- Options -->
                <td class="text-center">
                    <div class="btn-group">
                        <i style="color: #405a72;font-size: 18px;cursor: pointer;" class="icon-settings dropdown-toggle" data-toggle="dropdown"></i>

                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a  onclick="return copyURL('{{ url($image->path) }}')" class="text-dots tooltips" data-container="body" data-placement="top" data-original-title="{{ $image->title }}">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Copy</a>

                            </li>

                            <li>
                                <a href="{{ Protocol::home() }}/dashboard/images/delete/{{ $image->id }}">
                                    <i class="glyphicon glyphicon-trash"></i> Delete</a>
                            </li>


                        </ul>
                    </div>
                </td>

            </tr>
        @endforeach
    @endif

    </tbody>
</table>

@if (count($images) > 0)
    <div class="text-center">
        {{ $images->links() }}
    </div>
@endif