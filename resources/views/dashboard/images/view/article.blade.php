<table class="table table-hover table-outline m-b-0 hidden-sm-down">
    <thead class="thead-default">
    <tr>
        {{--<th class="text-center"><i class="icon-link"></i></th>--}}
        <th>File</th>
        <th class="text-center">Author</th>
        <th class="text-center">Name</th>
        <th class="text-center">Date</th>
        {{--<th class="text-center">Meta Description</th>--}}
        {{--<th class="text-center">Document</th>--}}
        <th class="text-center">Options</th>
    </tr>
    </thead>
    <tbody>

    @if ($articles)
        @foreach ($articles as $article)

            <tr>

                <!-- Ad Image -->
                <td class="text-center" style="width: 15%;">

                    <div class="avatar">
                       <a data-fancybox data-caption="My caption" data-width="1024" data-height="683">
                           <img src="{{ url('uploads/articles/'.$article->cover) }}" class="img-avatar" alt="{{ $article->title }}">
                       </a>
                    </div>
                </td>

                {{--<!-- Ad Info -->--}}
                <td>
                    <div class="small text-muted">
                        {{ $article->user->first_name }}
                    </div>
                </td>



                {{--<!-- Ad Price -->--}}
                <td class="text-center text-muted">
                    {{ basename(url('uploads/articles/'.$article->cover)) }}
                </td>

                {{--<!-- Is a featured Ad -->--}}
                <td class="text-center">
                    {{ $article->created_at->diffForHumans() }}
                </td>




                <!-- Options -->
                <td class="text-center">
                    <div class="btn-group">
                        <i style="color: #405a72;font-size: 18px;cursor: pointer;" class="icon-settings dropdown-toggle" data-toggle="dropdown"></i>

                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a  onclick="return copyURL('{{  url('uploads/articles/'.$article->cover) }}')" class="text-dots tooltips" data-container="body" data-placement="top" data-original-title="{{ $article->title }}">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Copy</a>

                            </li>

                            <li>
                                <a href="{{ Protocol::home() }}/dashboard/images/delete/{{ $article->id }}">
                                    <i class="glyphicon glyphicon-trash"></i> Delete</a>
                            </li>


                        </ul>
                    </div>
                </td>

            </tr>
        @endforeach
    @endif

    </tbody>
</table>

@if (count($articles) > 0)
    <div class="text-center">
        {{ $articles->links() }}
    </div>
@endif