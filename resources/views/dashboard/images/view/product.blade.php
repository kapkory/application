<table class="table table-hover table-outline m-b-0 hidden-sm-down">
    <thead class="thead-default">
    <tr>
        {{--<th class="text-center"><i class="icon-link"></i></th>--}}
        <th>File</th>
        <th class="text-center">Author</th>
        <th class="text-center">Name</th>
        <th class="text-center">Date</th>
        {{--<th class="text-center">Meta Description</th>--}}
        {{--<th class="text-center">Document</th>--}}
        <th class="text-center">Options</th>
    </tr>
    </thead>
    <tbody>

    @if ($products)
        @foreach ($products as $product)

            <tr>

                <!-- Ad Image -->
                <td class="text-center" style="width: 15%;">

                    <div class="avatar">
                        <a data-fancybox data-caption="My caption" data-width="1024" data-height="683">
                        <img src="{{ url('uploads/images'.$product->photos) }}" class="img-avatar" alt="{{ $product->title }}">
                        </a>
                    </div>
                </td>

                <!-- Ad Info -->
                <td>
                    <div class="small text-muted">
                        {{ $product->user->first_name }}
                    </div>
                </td>



                <!-- Ad Price -->
                <td class="text-center text-muted">
                    {{ $product->title }}
                </td>

                <!-- Is a featured Ad -->
                <td class="text-center">
                    {{ $product->created_at->diffForHumans() }}
                </td>




                <!-- Options -->
                <td class="text-center">
                    <div class="btn-group">
                        <i style="color: #405a72;font-size: 18px;cursor: pointer;" class="icon-settings dropdown-toggle" data-toggle="dropdown"></i>

                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a  onclick="return copyURL('{{ url('uploads/images'.$product->photos) }}')" class="text-dots tooltips" data-container="body" data-placement="top" data-original-title="{{ $product->title }}">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Copy</a>

                            </li>




                        </ul>
                    </div>
                </td>

            </tr>
        @endforeach
    @endif

    </tbody>
</table>

@if (count($products) > 0)
    <div class="text-center">
        {{ $products->links() }}
    </div>
@endif