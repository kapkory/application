<table class="table table-hover table-outline m-b-0 hidden-sm-down">
    <thead class="thead-default">
    <tr>
        <th>File</th>
        <th class="text-center">Author</th>
        <th class="text-center">Name</th>
        <th class="text-center">Date</th>

        <th class="text-center">Options</th>
    </tr>
    </thead>
    <tbody>

    @if ($catalogues)
        @foreach ($catalogues as $catalogue)

            <tr>

                <!-- Ad Image -->
                <td class="text-center" style="width: 15%;">

                    <div class="avatar">
                        <a data-fancybox data-caption="My caption" data-width="1024" data-height="683">
                        <img src="{{ url($catalogue->image_url) }}" class="img-avatar" alt="{{ $catalogue->name }}">
                        </a>
                    </div>
                </td>

                <!-- Ad Info -->
                <td>
                    <div class="small text-muted">
                        {{ $catalogue->user->first_name }}
                    </div>
                </td>



                <!-- Ad Price -->
                <td class="text-center text-muted">
                    {{ $catalogue->title }}
                </td>

                <!-- Is a featured Ad -->
                <td class="text-center">
                    {{ $catalogue->created_at->diffForHumans() }}
                </td>




                <!-- Options -->
                <td class="text-center">
                    <div class="btn-group">
                        <i style="color: #405a72;font-size: 18px;cursor: pointer;" class="icon-settings dropdown-toggle" data-toggle="dropdown"></i>

                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <a  onclick="return copyURL('{{ url($catalogue->image_url) }}')" class="text-dots tooltips" data-container="body" data-placement="top" data-original-title="{{ $catalogue->title }}">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Copy</a>

                            </li>


                        </ul>
                    </div>
                </td>

            </tr>
        @endforeach
    @endif

    </tbody>
</table>

@if (count($catalogues) > 0)
    <div class="text-center">
        {{ $catalogues->links() }}
    </div>
@endif