@extends ('dashboard.layout.app')

@section ('content')

    <div class="row">
        <div class="col-md-12">

            <!-- Session Messages -->
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                        <span class="caption-subject font-blue bold uppercase"> Upload Image</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal" action="{{ Protocol::home() }}/dashboard/image-new" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">

                            <!-- Currency Code -->
                            <div class="form-group form-md-line-input {{ $errors->has('image') ? 'has-error' :'' }}">
                                <label class="col-md-2 control-label" for="code">Currency Code</label>
                                <div class="col-md-10">
                                    <input type="file" class="form-control" id="image"  name="image">
                                    <div class="form-control-focus"> </div>
                                    @if ($errors->has('image'))
                                        <span class="help-block">{{ $errors->first('image') }}</span>
                                    @endif
                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn default btn-block">Upload</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection