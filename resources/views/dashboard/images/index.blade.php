@extends ('dashboard.layout.app')

@section('head')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.css" rel="stylesheet" type="text/css" >
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>

@endsection
@section ('content')

    <div class="row">

        <div class="col-md-12">

            <!-- Sessions Messages -->
            @if (Session::has('success'))
                <div class="custom-alerts alert alert-success fade in">
                    {{ Session::get('success') }}
                </div>
            @endif

            @if (Session::has('error'))
                <div class="custom-alerts alert alert-danger fade in">
                    {{ Session::get('error') }}
                </div>
            @endif
            <div class="col-md-12">

            <div class="portlet light ">
                <div class="portlet-body">

                <div class="container">

                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">  <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue bold uppercase">Image Settings</span></a></li>
                        <li><a data-toggle="tab" href="#menu1">Articles</a></li>
                        <li><a data-toggle="tab" href="#menu2">Catalogue</a></li>
                        <li><a data-toggle="tab" href="#menu3">Product</a></li>
                    </ul>

                    <div class="tab-content col-md-10" >
                        <div id="home" class="tab-pane fade in active">
                                @include('dashboard.images.view.settings',['images'=>$images])

                            </div>

                        <div id="menu1" class="tab-pane fade">
                            @include('dashboard.images.view.article',['articles'=>$articles])
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            @include('dashboard.images.view.catalogue',['catalogues'=>$catalogues])
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            @include('dashboard.images.view.product',['products'=>$products])
                        </div>
                    </div>
                </div>
                </div>



            </div>
            </div>

        </div>

    </div>

    <script type="text/javascript">
        function copyURL(url) {
     alert(url);
        }
    </script>

@endsection