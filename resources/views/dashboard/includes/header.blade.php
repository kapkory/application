<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />

    <title>{{ Helper::settings_general()->title }} | Dashboard</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link rel="shortcut icon" href="{{ Protocol::home() }}/uploads/settings/favicon/favicon.png">



    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Protocol::home() }}/content/assets/front-end/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/pages/css/search.min.css" rel="stylesheet" type="text/css" />

    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />

    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/global/css/components.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />

    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />

    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />

    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/cubeportfolio/cubeportfolio.css" rel="stylesheet" type="text/css" />

    <!-- JQuery Map Plugin -->
    <link rel="stylesheet" type="text/css" href="{{ Protocol::home() }}/content/assets/css/components.min.css">

    <script src="{{ Protocol::home() }}/content/assets/back-end/js/jquery.min.js"></script>

    <!-- ckEditor CDN -->
    <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
{{--    <script src="{{ url('content/assets/back-end/js/ckeditor.js') }}"></script>--}}

    <link href="{{ Protocol::home() }}/content/assets/bootstrap-tagsinput.css" rel="stylesheet" type="text/css">
    <script src="{{ url('content/assets/bootstrap-tagsinput.js') }}" type="text/javascript"></script>


    <!-- Modals -->
    {{--<link href="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css" />--}}
    {{--<link href="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />--}}
    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ Protocol::home() }}/content/assets/back-end/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Uploader Css Files -->
    <link href="{{ Protocol::home() }}/content/assets/jquery.filer.css" rel="stylesheet">
    <link href="{{ Protocol::home() }}/content/assets/jquery.filer-dragdropbox-theme.css" rel="stylesheet">
    @yield('head')
    {!! Charts::assets() !!}

    {{--SweetAlert--}}
    <link rel="stylesheet" type="text/css" href="{{ url('content/assets/back-end/assets/sweetalert2.min.css') }}">
    <script src="{{ url('content/assets/back-end/js/sweetalert2.js') }}"></script>

    <style type="text/css">
        @media screen and (max-width: 480px) {
            .logo{
                width: 200px;
            }
        }
    </style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" data-root="{{ Protocol::home() }}" id="root">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{ Protocol::home() }}/dashboard">
                    <img class="logo" src="{{ Protocol::home() }}/uploads/settings/logo/logo.png" alt="logo" class="logo-default" /> </a>
                    {{--<img style="margin: -3px 0 0;" src="{{ Protocol::home() }}/uploads/settings/logo/logo.png" alt="logo" class="logo-default" /> </a>--}}
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    
                    <!-- Ads Notifications -->
                    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/ads" class="dropdown-toggle">
                            <i class="icon-list"></i>
                            @if (Helper::count_notifications('ads') > 0)
                            <span class="badge badge-default"> {{ Helper::count_notifications('ads') }} </span>
                            @endif
                        </a>
                    </li>

                    <!-- Payments Notifications -->
                    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/payments" class="dropdown-toggle">
                            <i class="icon-wallet"></i>
                            @if (Helper::count_notifications('payments') > 0)
                            <span class="badge badge-default"> {{ Helper::count_notifications('payments') }} </span>
                            @endif
                        </a>
                    </li>

                    <!-- Comments Notifications -->
                    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="{{ Protocol::home() }}/dashboard/comments" class="dropdown-toggle">
                            <i class="icon-bubbles"></i>
                            @if (Helper::count_notifications('comments') > 0)
                            <span class="badge badge-default"> {{ Helper::count_notifications('comments') }} </span>
                            @endif
                        </a>
                    </li>

                    <!-- Reports Notifications -->
                    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/reports" class="dropdown-toggle">
                            <i class="icon-flag"></i>
                            @if (Helper::count_notifications('reports') > 0)
                            <span class="badge badge-default"> {{ Helper::count_notifications('reports') }} </span>
                            @endif
                        </a>
                    </li>

                    <!-- Messages Notifications -->
                    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="{{ Protocol::home() }}/dashboard/messages/admin" class="dropdown-toggle">
                            <i class="icon-envelope"></i>
                            @if (Helper::count_notifications('messages') > 0)
                            <span class="badge badge-default"> {{ Helper::count_notifications('messages') }} </span>
                            @endif
                        </a>
                    </li>

                    <!-- Stores Notifications -->
                    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/stores" class="dropdown-toggle">
                            <i class="icon-basket"></i>
                            @if (Helper::count_notifications('stores') > 0)
                            <span class="badge badge-default"> {{ Helper::count_notifications('stores') }} </span>
                            @endif
                        </a>
                    </li>
                    
                    <!-- Users Notifications -->
                    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/users" class="dropdown-toggle">
                            <i class="icon-users"></i>
                            @if (Helper::count_notifications('users') > 0)
                            <span class="badge badge-default"> {{ Helper::count_notifications('users') }} </span>
                            @endif
                        </a>
                    </li>
                    
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="{{ Profile::picture(Auth::id()) }}" />
                            <span class="username username-hide-on-mobile">  
                                {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{ Protocol::home() }}/account/settings" target="_blank">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>
                            <li>
                                <a target="_blank" href="{{ Protocol::home() }}/create">
                                    <i class="icon-pencil"></i> Post AD
                                </a>
                            </li>
                            <li class="divider"> </li>
                            <li>
                                <a href="{{ Protocol::home() }}/" target="_blank">
                                    <i class="icon-eye"></i> View Site </a>
                            </li>
                            <li>
                                <a target="_blank" href="http://www.mendelman.me/support">
                                    <i class="icon-envelope"></i> Contact MendelMan </a>
                            </li>
                            <li>
                                <a href="{{ Protocol::home() }}/auth/logout">
                                    <i class="icon-power"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <div class="clearfix"> </div>