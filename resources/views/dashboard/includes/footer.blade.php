 <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> &copy; @php echo date('Y'); @endphp
                <a href="#" target="_blank">MendelManGroup</a>
                All Rights Reserved.
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <script src="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{ Protocol::home() }}/content/assets/back-end/assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>

        <script src="{{ Protocol::home() }}/content/assets/back-end/assets/global/scripts/app.min.js" type="text/javascript"></script>

        <script src="{{ Protocol::home() }}/content/assets/back-end/assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>

        <script src="{{ Protocol::home() }}/content/assets/back-end/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        {{--<script src="{{ Protocol::home() }}/content/assets/back-end/assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>--}}
        <script src="{{ Protocol::home() }}/content/assets/back-end/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        @if (Profile::hasStore(Auth::id()))
        <script type="text/javascript">
            var _limit_images = {{ Helper::settings_membership()->pro_ad_images }};
        </script>
        @else
        <script type="text/javascript">
            var _limit_images = {{ Helper::settings_membership()->free_ad_images }};
        </script>
        @endif
        <script src="{{ Protocol::home() }}/content/assets/jquery.filer.js" type="text/javascript"></script>
        <script src="{{ Protocol::home() }}/content/assets/custom.js" type="text/javascript"></script>

        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>