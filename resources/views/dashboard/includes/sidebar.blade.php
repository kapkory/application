<div class="page-sidebar-wrapper">

    <div class="page-sidebar navbar-collapse collapse">
        
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                    <span></span>
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>

            <!-- Dashboard Home Page -->
            <li class="nav-item start">
                <a href="{{ Protocol::home() }}/dashboard" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <!-- Ads Settings -->
            <li class="nav-item  ">
                <a href="{{ Protocol::home() }}/dashboard/ads" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Ads Settings</span>
                </a>
            </li>

            <!-- Blog Articles -->
            <li class="nav-item  ">
                <a href="{{ Protocol::home() }}/dashboard/articles" class="nav-link nav-toggle">
                    <i class="icon-feed"></i>
                    <span class="title">Blog Articles</span>
                </a>
            </li>
            <!-----Image management----->
            <!-- Comments Management -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-image3"></i>
                    <span class="title">Images </span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/view/images" class="nav-link ">
                            <i class="icon-list"></i>
                            <span class="title">Library </span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ url('dashboard/image-new') }}" class="nav-link ">
                            <i class="icon-info22"></i>
                            <span class="title">Add</span>
                        </a>
                    </li>
                </ul>
                {{--<a href="{{ Protocol::home() }}/dashboard/comments" class="nav-link nav-toggle">--}}
                {{--</a>--}}
            </li>

            <!-- Comments Management -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-bubbles"></i>
                    <span class="title">Comments Management</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/comments" class="nav-link ">
                            <i class="icon-flag"></i>
                            <span class="title">Blog comments</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/article-comments" class="nav-link ">
                            <i class="icon-envelope"></i>
                            <span class="title">Articles Comments</span>
                        </a>
                    </li>
                </ul>
                {{--<a href="{{ Protocol::home() }}/dashboard/comments" class="nav-link nav-toggle">--}}
                {{--</a>--}}
            </li>

            <!-- Categories Management -->
            <li class="nav-item  ">
                <a href="{{ Protocol::home() }}/dashboard/categories" class="nav-link nav-toggle">
                    <i class="icon-list"></i>
                    <span class="title">Categories Settings</span>
                </a>
            </li>

            <!-- Catalogue Management -->
            <li class="nav-item  ">
                <a  class="nav-link nav-toggle">
                    <i class="icon-badge"></i>
                    <span class="title">Catalogue</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/catalogue" class="nav-link ">
                            <i class="icon-add"></i>
                            <span class="title">Add</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/catalogues" class="nav-link ">
                            <i class="icon-list"></i>
                            <span class="title">View</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Notifications -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-bell"></i>
                    <span class="title">Notifications</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/ads" class="nav-link ">
                            <i class="icon-list"></i>
                            <span class="title">Ads</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/payments" class="nav-link ">
                            <i class="icon-wallet"></i>
                            <span class="title">Payments</span>
                        </a>
                    </li>
                    <!-- <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/comments" class="nav-link ">
                            <i class="icon-bubbles"></i>
                            <span class="title">Comments</span>
                        </a>
                    </li> -->
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/reports" class="nav-link ">
                            <i class="icon-flag"></i>
                            <span class="title">Reports</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/messages/admin" class="nav-link ">
                            <i class="icon-envelope"></i>
                            <span class="title">Messages</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/stores" class="nav-link ">
                            <i class="icon-basket"></i>
                            <span class="title">Stores</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/notifications/users" class="nav-link ">
                            <i class="icon-users"></i>
                            <span class="title">Users</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Mail Box -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-envelope"></i>
                    <span class="title">Messages Settings</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/messages/stores" class="nav-link ">
                            <i class="icon-bubbles"></i>
                            <span class="title">Stores Feedback</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/messages/users" class="nav-link ">
                            <i class="icon-users"></i>
                            <span class="title">Users Messages</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/messages/admin" class="nav-link ">
                            <i class="icon-diamond"></i>
                            <span class="title">Admin Messages</span>
                        </a>
                    </li>
                </ul>
            </li>
            
            <!-- Users Settings -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">Users Settings</span>
                    <span class="arrow"></span>
                </a>


                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/users" class="nav-link ">
                            <i class="icon-bubbles"></i>
                            <span class="title">View</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/users/add" class="nav-link ">
                            <i class="icon-users"></i>
                            <span class="title">Add Users</span>
                        </a>
                    </li>

                </ul>
            </li>

            <!-- Stores Settings -->
            <li class="nav-item  ">
                <a href="{{ Protocol::home() }}/dashboard/stores" class="nav-link nav-toggle">
                    <i class="icon-basket"></i>
                    <span class="title">Stores Management</span>
                </a>
            </li>

            <!-- Offers Settings -->
            <li class="nav-item  ">
                <a href="{{ Protocol::home() }}/dashboard/offers" class="nav-link nav-toggle">
                    <i class="icon-tag"></i>
                    <span class="title">Offers Settings</span>
                </a>
            </li>

            <!-- Advertisement Settings -->
            <li class="nav-item  ">
                <a href="#" class="nav-link nav-toggle">
                    <i class="icon-rocket"></i>
                    <span class="title">Advertisement</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ url('dashboard/advertisements') }}" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Ad ShortCode</span>
                        </a>
                    </li>

                    <li class="nav-item start">
                        <a href="{{ url('dashboard/adverts-requests') }}" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Adverts Requests</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Payments Settings -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-credit-card"></i>
                    <span class="title">Payments Management</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/payments/accounts" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Accounts Payments</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/payments/ads" class="nav-link">
                            <i class="icon-badge"></i>
                            <span class="title">ADS Payments</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Currencies Settings -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle" href="{{ Protocol::home() }}/dashboard/currencies">
                    <i class="fa fa-dollar"></i>
                    <span class="title">Currencies Settings</span>
                </a>
            </li>

            <!-- Payments Gateways -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="fa fa-cc-stripe"></i>
                    <span class="title">Payments Gateways</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/payments/paypal" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">PayPal Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/payments/2checkout" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">2Checkout Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/payments/stripe" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">Stripe Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/payments/mollie" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">Mollie Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/payments/paystack" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">PayStack Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/payments/paysafecard" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">PaySafeCard Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/payments/razorpay" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">RazorPay Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/payments/barion" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">Barion Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/payments/cashu" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">CashU Settings</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- SMS Gateways -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-paper-plane"></i>
                    <span class="title">SMS Gateways</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/sms/nexmo" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">Nexmo Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/sms/identifyme" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">IdentifyMe Settings</span>
                        </a>
                    </li>
                    <!-- <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/sms/smscru" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">SmscRu Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/sms/smsgateway" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">SmsGateway Settings</span>
                        </a>
                    </li> -->
                </ul>
            </li>

            <!-- Cloud Services -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-cloud"></i>
                    <span class="title">Cloud Services</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/cloud/amazon" class="nav-link ">
                            <i class="fa fa-amazon"></i>
                            <span class="title">Amazon S3</span>
                        </a>
                    </li>
                    <!-- <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/cloud/google" class="nav-link ">
                            <i class="fa fa-google"></i>
                            <span class="title">Google Cloud</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/cloud/rackspace" class="nav-link ">
                            <i class="fa fa-rocket"></i>
                            <span class="title">RackSpace Cloud</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/cloud/cloudinary" class="nav-link ">
                            <i class="fa fa-cloud-upload"></i>
                            <span class="title">Cloudinary Cloud</span>
                        </a>
                    </li> -->
                </ul>
            </li>

            <!-- Geo Settings -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-globe"></i>
                    <span class="title">Geo Management</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/geo/countries" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">Countries</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/geo/states" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">States</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/geo/cities" class="nav-link ">
                            <i class="fa fa-circle"></i>
                            <span class="title">Cities</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Pages Settings -->
            <li class="nav-item  ">
                <a href="{{ Protocol::home() }}/dashboard/pages" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">Pages Settings</span>
                </a>
            </li>

            <!-- Failed Login -->
            <li class="nav-item  ">
                <a href="{{ Protocol::home() }}/dashboard/login/history" class="nav-link nav-toggle">
                    <i class="icon-shield"></i>
                    <span class="title">Failed Login</span>
                </a>
            </li>

            <!-- IP Blocker -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-ban"></i>
                    <span class="title">IP Blocker</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/firewall/add" class="nav-link ">
                            <i class="icon-plus"></i>
                            <span class="title">Block IP</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/firewall" class="nav-link ">
                            <i class="icon-list"></i>
                            <span class="title">Blocked List</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Themes Management -->
            <li class="nav-item  ">
                <a href="{{ Protocol::home() }}/dashboard/themes" class="nav-link nav-toggle">
                    <i class="icon-picture"></i>
                    <span class="title">Themes Management</span>
                </a>
            </li>

            <!-- Site Settings -->
            <li class="nav-item  ">
                <a class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">App Settings</span>
                    <span class="arrow"></span>
                </a>

                <ul class="sub-menu">
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/general" class="nav-link ">
                            <i class="icon-equalizer"></i>
                            <span class="title">General Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start">
                        <a href="{{ Protocol::home() }}/dashboard/settings/home" class="nav-link ">
                            <i class="icon-home"></i>
                            <span class="title">Home Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ Protocol::home() }}/dashboard/settings/security" class="nav-link ">
                            <i class="icon-shield"></i>
                            <span class="title">Security Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ Protocol::home() }}/dashboard/settings/smtp" class="nav-link ">
                            <i class="icon-envelope"></i>
                            <span class="title">SMTP Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ Protocol::home() }}/dashboard/settings/seo" class="nav-link ">
                            <i class="icon-rocket"></i>
                            <span class="title">SEO Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ Protocol::home() }}/dashboard/settings/watermark" class="nav-link ">
                            <i class="fa fa-image"></i>
                            <span class="title">Watermark Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ Protocol::home() }}/dashboard/settings/geo" class="nav-link ">
                            <i class="icon-pointer"></i>
                            <span class="title">GEO Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ Protocol::home() }}/dashboard/settings/auth" class="nav-link ">
                            <i class="fa fa-lock"></i>
                            <span class="title">Auth Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ Protocol::home() }}/dashboard/settings/membership" class="nav-link ">
                            <i class="icon-star"></i>
                            <span class="title">Membership Settings</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ Protocol::home() }}/dashboard/settings/footer" class="nav-link ">
                            <i class="fa fa-arrow-down"></i>
                            <span class="title">Footer Settings</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Maintenance -->
            <li class="nav-item  ">
                <a href="{{ Protocol::home() }}/dashboard/maintenance" class="nav-link nav-toggle">
                    <i class="icon-power"></i>
                    <span class="title">Maintenance Mode</span>
                </a>
            </li>
                        
        </ul>

    </div>
