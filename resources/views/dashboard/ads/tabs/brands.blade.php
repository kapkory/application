
<button type="button" style="margin: 5px" class="btn btn-info btn-md pull-right" data-toggle="modal" data-target="#addBrand">Add Brand</button>

<table class="table table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Description</th>
        <th>Option</th>
    </tr>
    </thead>
    <tbody>
    @if($brands)
        @foreach($brands as $brand)
            <tr>
                <td>{{ $brand->id }}</td>
                <td>{{ $brand->name }}</td>
                <td>{{ $brand->description }}</td>
                <td><a class="delete_brand"   data-href="{{ url('dashboard/ads/brand/delete/'.$brand->id) }}">
                        <i class="glyphicon glyphicon-remove"></i> Delete Brand</a></td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
@if (count($brands) > 0)
    <div class="text-center">
        {{ $brands->links() }}
    </div>
@endif

<div class="modal fade" id="addBrand" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Brand</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ url('dashboard/ads/brand/create') }}">
                    <div class="form-group">
                        <label for="exampleInput">Brand Name*</label>
                        <input type="text" class="form-control" name="name" id="exampleInput" required placeholder="Enter Brand Name">
                    </div>
                    <div class="form-group">
                        <label for="description"> Description</label>
                        <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                    </div>
                    {{ csrf_field() }}

                    <button type="submit" class="btn btn-primary">Submit</button>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('.delete_brand').click(function () {
        var href = $(this).data('href');
        console.log('delete url is '+href);
        Swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this imaginary file!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
            console.log('url is '+href);
            $.get(href,function (response) {
                // console.log('your file has been deleted');
                window.location.href = "{{ url()->current() }}";
            })
            // Swal(
            //     'Deleted!',
            //     'Your imaginary file has been deleted.',
            //     'success'
            // )
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal(
                'Cancelled',
                'Your Article is safe :)',
                'error'
            )
        }
    })
    })
</script>