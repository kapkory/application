@extends ('dashboard.layout.app')

@section ('content')

    <div class="row">

        <div class="col-md-12">

            <!-- Sessions Messages -->
            @if (Session::has('success'))
                <div class="custom-alerts alert alert-success fade in">
                    {{ Session::get('success') }}
                </div>
            @endif

            @if (Session::has('error'))
                <div class="custom-alerts alert alert-danger fade in">
                    {{ Session::get('error') }}
                </div>
            @endif

            <div class="portlet light ">
    <h2>Ads Page</h2>

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
            <li><a data-toggle="tab" href="#brands">Ads Brands</a></li>
        </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">

            @include('dashboard.ads.tabs.settings')
        </div>
        <div id="brands" class="tab-pane fade">
            @include('dashboard.ads.tabs.brands')
        </div>
    </div>

            </div>

        </div>

    </div>




    @endsection