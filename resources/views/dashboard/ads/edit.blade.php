@extends ('dashboard.layout.app')

@section ('content')

<div class="row">
	<div class="col-md-12">

		<!-- Session Messages -->
        @if (Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif

        @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif 
		
		<div class="profile">
			<div class="portlet light ">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue bold uppercase">Edit Ad</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">

                        <div class="tab-pane active" id="tab_1_1">
                            <form role="form" action="{{ Protocol::home() }}/dashboard/ads/edit/{{ $ad->ad_id }}" method="POST" enctype="multipart/form-data">

                            	{{ csrf_field() }}

                            	<!-- Ad Title -->
                                <div class="form-group {{ $errors->has('title') ? 'has-error' :'' }}">
                                    <label class="control-label">Title</label>
                                    <input placeholder="Title" value="{{ $ad->title }}" class="form-control" type="text" name="title"> 
                                    @if ($errors->has('title'))
	                                <span class="help-block">{{ $errors->first('title') }}</span>
	                                @endif
                                </div>

                                <!-- Ad Description -->
                                <div class="form-group {{ $errors->has('description') ? 'has-error' :'' }}">
                                    <label class="control-label">Description</label>
                                    <textarea class="form-control" rows="10" placeholder="Description" name="description">{{ $ad->description }}</textarea> 
                                    @if ($errors->has('description'))
	                                <span class="help-block">{{ $errors->first('description') }}</span>
	                                @endif
                                </div>

                                <!-- Ad Category -->
                                <div class="form-group {{ $errors->has('category') ? 'has-error' :'' }}">
                                    <label class="control-label">Select Category</label>
                                    <select class="form-control" name="category">
                                        <option value=""></option>

										@if(count(Helper::parent_categories()))
                                        @foreach (Helper::parent_categories() as $parent)
                                        <optgroup label="{{ $parent->category_name }}">
                                            @if (count(Helper::sub_categories($parent->id)))
                                            @foreach (Helper::sub_categories($parent->id) as $sub)
                                            <option value="{{ $sub->id }}" {{ $ad->category == $sub->id ? 'selected' : '' }}>{{ $sub->category_name }}</option>
                                            @endforeach
                                                @else
                                                <option value="{{ $parent->id }}" {{ $ad->category == $parent->id ? 'selected' : '' }}>{{ $parent->category_name }}</option>

                                            @endif
                                        </optgroup>
                                        @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('category'))
	                                <span class="help-block">{{ $errors->first('category') }}</span>
	                                @endif
                                </div>

                                <!-- Regular Price -->
                                <div class="form-group {{ $errors->has('regular_price') ? 'has-error' :'' }}">
                                    <label class="control-label">Regular Price</label>
                                    <input placeholder="Regular Price" value="{{ $ad->regular_price }}" class="form-control" type="text" name="regular_price"> 
                                    @if ($errors->has('regular_price'))
	                                <span class="help-block">{{ $errors->first('regular_price') }}</span>
	                                @endif
                                </div>

                                <!-- Ad Price -->
                                <div class="form-group {{ $errors->has('price') ? 'has-error' :'' }}">
                                    <label class="control-label">Sale Price</label>
                                    <input placeholder="Sale Price" value="{{ $ad->price }}" class="form-control" type="text" name="price"> 
                                    @if ($errors->has('price'))
                                    <span class="help-block">{{ $errors->first('price') }}</span>
                                    @endif
                                </div>

                                <!-- Currency -->
                                <div class="form-group {{ $errors->has('currency') ? 'has-error' :'' }}">
                                    <label class="control-label">Currency</label>
                                    <select class="form-control" name="currency">
                                    	@foreach (Currencies::database() as $currency)
                                        <option value="{{ $currency->code }}" {{ $ad->currency == $currency->code ? 'selected' : '' }}>{{ $currency->code }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('currency'))
	                                <span class="help-block">{{ $errors->first('currency') }}</span>
	                                @endif
                                </div>

                                    <!-- Ad Title -->
                                    <div class="form-group {{ $errors->has('supply_time') ? 'has-error' :'' }}">
                                        <label class="control-label">Supply Time</label>
                                        <input placeholder="Supply Time in Days" value="{{ $ad->supply_time }}" class="form-control" type="text" name="supply_time">
                                        @if ($errors->has('supply_time'))
                                            <span class="help-block">{{ $errors->first('supply_time') }}</span>
                                        @endif
                                    </div>

                                    <!-- Ad Title -->
                                    <div class="form-group {{ $errors->has('minimum_order_quantity') ? 'has-error' :'' }}">
                                        <label class="control-label">Minimum order Quantity</label>
                                        <input placeholder="Minimum order Quantity" value="{{ $ad->minimum_order_quantity }}" class="form-control" type="number" name="minimum_order_quantity">
                                        @if ($errors->has('minimum_order_quantity'))
                                            <span class="help-block">{{ $errors->first('minimum_order_quantity') }}</span>
                                        @endif
                                    </div>

                                    @if($stores)
                                    <!-- Status -->
                                    <div class="form-group {{ $errors->has('stores') ? 'has-error' :'' }}">
                                        <label class="control-label">Select Store</label>
                                        <select class="form-control" name="store">
                                            @foreach($stores as $store)
                                                <option value="{{ $store->id }}" {{ $ad->store_id == $store->id ? 'selected' : '' }}>{{ $store->username }}</option>

                                            @endforeach
                                        </select>
                                        @if ($errors->has('stores'))
                                            <span class="help-block">{{ $errors->first('stores') }}</span>
                                        @endif
                                    </div>
                                    @endif

                                <!-- Status -->
                                <div class="form-group {{ $errors->has('status') ? 'has-error' :'' }}">
                                    <label class="control-label">Status</label>
                                    <select class="form-control" name="status">
                                    	@if ($ad->status)
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                        @else
                                        <option value="0">Inactive</option>
                                        <option value="1">Active</option>
                                        @endif
                                    </select>
                                    @if ($errors->has('status'))
	                                <span class="help-block">{{ $errors->first('status') }}</span>
	                                @endif
                                </div>

                                <!-- Featured -->
                                <div class="form-group {{ $errors->has('featured') ? 'has-error' :'' }}">
                                    <label class="control-label">Featured</label>
                                    <select class="form-control" name="featured">
                                    	@if ($ad->is_featured)
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                        @else
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                        @endif
                                    </select>
                                    @if ($errors->has('featured'))
	                                <span class="help-block">{{ $errors->first('featured') }}</span>
	                                @endif
                                </div>

                                <!-- Archived -->
                                <div class="form-group {{ $errors->has('archived') ? 'has-error' :'' }}">
                                    <label class="control-label">Archived</label>
                                    <select class="form-control" name="archived">
                                    	@if ($ad->is_archived)
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                        @else
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                        @endif
                                    </select>
                                    @if ($errors->has('archived'))
	                                <span class="help-block">{{ $errors->first('archived') }}</span>
	                                @endif
                                </div>

                                <!-- Youtube -->
                                <div class="form-group {{ $errors->has('youtube') ? 'has-error' :'' }}">
                                    <label class="control-label">Youtube Video</label>
                                    <input placeholder="Youtube Video" value="{{ $ad->youtube }}" class="form-control" type="text" name="youtube"> 
                                    @if ($errors->has('youtube'))
                                    <span class="help-block">{{ $errors->first('youtube') }}</span>
                                    @endif
                                </div>

                                <!-- Affiliate Link -->
                                <div class="form-group {{ $errors->has('affiliate_link') ? 'has-error' :'' }}">
                                    <label class="control-label">Affiliate Link</label>
                                    <input placeholder="Amazon, eBay, Aliexpress... Affiliate link here" value="{{ $ad->affiliate_link }}" class="form-control" type="text" name="affiliate_link"> 
                                    @if ($errors->has('affiliate_link'))
                                    <span class="help-block">{{ $errors->first('affiliate_link') }}</span>
                                    @endif
                                </div>


                                    <?php
                                    $brands = \App\Models\Brand::all();
                                    ?>
                                    <div id="brand" class="form-group {{ $errors->has('brand') ? 'has-error' : '' }}" @if(!$ad->affiliate_link) style="display: none" @endif>
                                        <label>Select A Brand</label>
                                            <select  data-placeholder="Select A Brand" class="form-control" name="brand">

                                                @foreach ($brands as $brand)
                                                    <option value="{{ $brand->id }}" {{ ($brand->id == $ad->brand_id) ? 'selected' : '' }}>{{ $brand->name }}</option>
                                                @endforeach
                                            </select>

                                    </div>

                                <!-- Edit Images -->
                                <div class="form-group">
                                    <label class="control-label">Edit Images</label>
                                    <input type="file" class="form-control" name="photos[]" id="uploader" multiple="multiple" accept="image/*">
                                </div>

                                    <!-- Store Website -->
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                                            <label>Meta Title</label>
                                            <input class="form-control input-sm" placeholder="Meta Title" type="text" value="{{ $ad->meta_title }}" name="meta_title">
                                            @if ($errors->has('meta_title'))
                                                <span class="help-block">
										{{ $errors->first('meta_title') }}
									</span>
                                            @endif
                                        </div>
                                    </div>



                                    <!-- Long Description -->
                                    <div class="col-md-12">
                                        <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                                            <label>Meta Description</label>
                                            <textarea required="" class="form-control input-sm" placeholder="Meta Description field" rows="4" name="meta_description">{{ $ad->meta_description }}</textarea>
                                            @if ($errors->has('meta_description'))
                                                <span class="help-block">
										{{ $errors->first('meta_description') }}
									</span>
                                            @endif
                                        </div>
                                    </div>


                                            <div class="form-group select-size-lg {{ $errors->has('user') ? 'has-error' : '' }}">
                                                <div class="col-md-12">
                                                    <select required="" data-placeholder="{{ Lang::get('create/ad.lang_select_user') }}" class="form-control" name="user">
                                                        @foreach ($users as $ad_user)
                                                            <option value="{{ $ad_user->id }}"  {{ $ad->user_id == $ad_user->id ? 'selected' : '' }}>{{ $ad_user->first_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('user'))
                                                        <span class="help-block">{{ $errors->first('user') }}</span>
                                                    @endif
                                                </div>
                                            </div>


                                <div class="margiv-top-10">
                                    <button type="submit" class="btn default" style="width: 100%;text-transform: uppercase;"> Update Ad </button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
		</div>

	</div>
</div>

    <script>
        $('input[name="affiliate_link"]').click(function () {
            $('#brand').show();
        })
    </script>
@endsection