@extends ('dashboard.layout.app')

@section ('content')

    <div class="row">
        <div class="col-md-12">

            <!-- Session Messages -->
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                        <span class="caption-subject font-blue bold uppercase"> Create New Article Category</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal" action="{{ Protocol::home() }}/dashboard/article_categories/create" method="POST">
                        {{ csrf_field() }}
                        <div class="form-body">

                            <!-- Category Name -->
                            <div class="form-group form-md-line-input {{ $errors->has('category_name') ? 'has-error' :'' }}">
                                <label class="col-md-2 control-label" for="category_name">Category Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="category_name" placeholder="Category Name" name="category_name" value="{{ old('category_name') }}">
                                    <div class="form-control-focus"> </div>
                                    @if ($errors->has('category_name'))
                                        <span class="help-block">{{ $errors->first('category_name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="category_slug">Category Slug</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="category_slug" placeholder="Category Slug" name="category_slug" value="">
                                    <div class="form-control-focus"> </div>

                                </div>
                            </div>

                            <!-- Category Slug -->
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="category_slug">Category Description</label>
                                <div class="col-md-10">
                                    <textarea row="3" class="form-control" name="description" placeholder="Category Description"></textarea>
                                    <div class="form-control-focus"> </div>

                                </div>
                            </div>

                        @if (count(Helper::article_parent_categories()))
                            <!-- Parent Category-->
                            <div class="form-group form-md-line-input">
                                <label class="col-md-2 control-label" for="is_sub">Parent Category</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="parent_category" name="parent_category">
                                        <option value="0">None</option>
                                        @foreach (Helper::article_parent_categories() as $parent_category)
                                            <option value="{{ $parent_category->id }}">{{ $parent_category->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="form-control-focus"> </div>
                                </div>
                            </div>
                            @endif

                            <div class="form-group form-md-line-input ">
                                <label class="col-md-2 control-label" for="tags">Meta Title</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="meta_title" placeholder="Enter Meta Title" value="">
                                    {{--<input type="file" class="form-control" id="cover"  name="cover" value="{{ old('cover') }}">--}}
                                    <div class="form-control-focus"> </div>

                                </div>
                            </div>

                            <div class="form-group form-md-line-input ">
                                <label class="col-md-2 control-label" for="tags">Meta Description</label>
                                <div class="col-md-10">
                            <textarea class="form-control" name="meta_description">

                            </textarea>
                                    <div class="form-control-focus"> </div>

                                </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn default btn-block">Create Article Category</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
            @endsection


