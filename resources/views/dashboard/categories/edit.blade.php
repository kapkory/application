@extends ('dashboard.layout.app')

@section ('content')

<div class="row">
    <div class="col-md-12">

    	<!-- Session Messages -->
    	@if (Session::has('error'))
    	<div class="alert alert-danger">
           	{{ Session::get('error') }} 
        </div>
        @endif
        @if (Session::has('success'))
    	<div class="alert alert-success">
           	{{ Session::get('success') }} 
        </div>
        @endif

        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <span class="caption-subject font-blue-madison bold uppercase"> Edit Category</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" class="form-horizontal" action="{{ Protocol::home() }}/dashboard/categories/edit/{{ $category->id }}" method="POST" enctype="multipart/form-data">

                	{{ csrf_field() }}

                    <div class="form-body">

                    	<!-- Category Name -->
                        <div class="form-group form-md-line-input {{ $errors->has('category_name') ? 'has-error' :'' }}">
                            <label class="col-md-2 control-label" for="category_name">Category Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="category_name" placeholder="Category Name" name="category_name" value="{{ $category->category_name }}">
                                <div class="form-control-focus"> </div>
                                @if ($errors->has('category_name'))
                                <span class="help-block">{{ $errors->first('category_name') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Category Slug -->
                        <div class="form-group form-md-line-input {{ $errors->has('category_slug') ? 'has-error' :'' }}">
                            <label class="col-md-2 control-label" for="category_slug">Category Slug</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="category_slug" placeholder="Category Slug" name="category_slug" value="{{ $category->category_slug }}">
                                <div class="form-control-focus"> </div>
                                @if ($errors->has('category_slug'))
                                <span class="help-block">{{ $errors->first('category_slug') }}</span>
                                @endif
                            </div>
                        </div>

                      @if($category->is_sub == 1)
                          @if (count(Helper::parent_categories()))

                              <!-- Parent Category-->
                                  <div class="form-group form-md-line-input">
                                      <label class="col-md-2 control-label" for="is_sub">Parent Category</label>
                                      <div class="col-md-10">
                                          <select class="form-control" id="parent_category" name="parent_category">
                                              @foreach (Helper::parent_categories() as $parent_category)
                                                  <option {{ $category->parent_category == $parent_category->id  ? 'selected' : '' }} value="{{ $parent_category->id }}">{{ $parent_category->category_name  }}</option>
                                              @endforeach
                                          </select>
                                          <div class="form-control-focus"> </div>
                                      </div>
                                  </div>
                          @endif
                          @endif

                        <!-- Category icon -->
                        <div class="form-group form-md-line-input {{ $errors->has('icon') ? 'has-error' :'' }}">
                            <label class="col-md-2 control-label" for="icon">Update Category Icon</label>
                            <div class="col-md-10">
                                <input type="file" class="form-control" id="icon"  name="icon">
                                <div class="form-control-focus"> </div>
                                @if ($errors->has('icon'))
                                <span class="help-block">{{ $errors->first('icon') }}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Meta title -->
                        <div class="form-group form-md-line-input {{ $errors->has('meta_title') ? 'has-error' :'' }}">
                            <label class="col-md-2 control-label" for="icon">Meta Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" placeholder="Meta Title" id="meta_title"  name="meta_title" value="{{ $category->meta_title }}">
                                <div class="form-control-focus"> </div>
                                @if ($errors->has('meta_title'))
                                    <span class="help-block">{{ $errors->first('meta_title') }}</span>
                                @endif
                            </div>
                        </div>




                        <!-- Long Description -->
                        <div class="form-group form-md-line-input {{ $errors->has('meta_description') ? 'has-error' :'' }}">
                            <label class="col-md-2 control-label" for="icon">Meta Description</label>
                            <div class="col-md-10">
                                <textarea required="" class="form-control" placeholder="Meta Description field" rows="4" name="meta_description">
                                    {{ $category->meta_title }}
                                </textarea>
                                <div class="form-control-focus"> </div>
                                @if ($errors->has('meta_description'))
                                    <span class="help-block">{{ $errors->first('meta_description') }}</span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="submit" class="btn blue">Edit Category</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection