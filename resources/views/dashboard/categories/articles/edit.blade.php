@extends ('dashboard.layout.app')

@section ('content')

    <div class="row">
        <div class="col-md-12">

            <!-- Session Messages -->
            @if (Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-green-haze">
                        <span class="caption-subject font-blue-madison bold uppercase"> Edit Category</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" class="form-horizontal" action="{{ Protocol::home() }}/dashboard/article_category/edit/{{ $article_category->id }}" method="POST" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="form-body">

                            <!-- Category Name -->
                            <div class="form-group form-md-line-input {{ $errors->has('category_name') ? 'has-error' :'' }}">
                                <label class="col-md-2 control-label" for="category_name">Category Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="category_name" placeholder="Category Name" name="category_name" value="{{ $article_category->name }}">
                                    <div class="form-control-focus"> </div>
                                    @if ($errors->has('category_name'))
                                        <span class="help-block">{{ $errors->first('category_name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Category Slug -->
                            <div class="form-group form-md-line-input {{ $errors->has('category_slug') ? 'has-error' :'' }}">
                                <label class="col-md-2 control-label" for="category_slug">Category Slug</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="category_slug" placeholder="Category Slug" name="category_slug" value="{{ $article_category->category_slug }}">
                                    <div class="form-control-focus"> </div>
                                    @if ($errors->has('category_slug'))
                                        <span class="help-block">{{ $errors->first('category_slug') }}</span>
                                    @endif
                                </div>
                            </div>

                            <!-- Category icon -->
                            <div class="form-group form-md-line-input {{ $errors->has('meta_title') ? 'has-error' :'' }}">
                                <label class="col-md-2 control-label" for="meta_title">Meta Title</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" id="meta_title" placeholder="Meta Title" name="meta_title" value="{{ $article_category->meta_title }}">
                                    <div class="form-control-focus"> </div>
                                    @if ($errors->has('category_slug'))
                                        <span class="help-block">{{ $errors->first('category_slug') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group form-md-line-input ">
                                <label class="col-md-2 control-label" for="tags">Meta Description</label>
                                <div class="col-md-10">
                            <textarea class="form-control" name="meta_description">
                                {{ $article_category->meta_description }}
                            </textarea>
                                    <div class="form-control-focus"> </div>

                                </div>
                            </div>

                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-10">
                                    <button type="submit" class="btn blue">Edit Category</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

@endsection