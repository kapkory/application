<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdsTableOrderQuantity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ads', function(Blueprint $table) {
            $table->dropColumn('negotiable');
            $table->dropColumn('is_used');
            $table->integer('store_id')->default(1);
            $table->integer('minimum_order_quantity')->default(1);
            $table->string('supply_time')->nullable();
        });
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads', function(Blueprint $table) {

            $table->dropColumn('minimum_order_quantity')->default(1);
            $table->dropColumn('supply_time');
        });
    }
}
