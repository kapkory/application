const { mix } = require('laravel-mix');

mix.js('resources/assets/js/app.js','/home/master/applications/nznszrptrc/public_html/v2/js/app.js')
    .sass('resources/assets/sass/app.scss', 'public/css');

// mix.styles([
//     'content/assets/front-end/js/plugins/slider/royalslider.css?v=1.2',
//     'content/assets/front-end/js/plugins/slider/skins/default/rs-default.css?v=1.2',
//     'content/assets/front-end/css/extras/starability-growRotate.css',
//     'content/assets/front-end/js/plugins/emojionearea/emojionearea.css',
//     'content/assets/front-end/css/article.css',
//     'content/assets/front-end/css/products.css',
//     'content/assets/front-end/css/jquery.dataTables.min.css'
// ],'content/assets/front-end/css/article-styles.css')
//     .options({
//     processCssUrls: false
// });
//
// mix.scripts(["public/css/frontend/bootstrap.css",
//         'public/content/assets/front-end/js/core/libraries/jquery.min.js',
//         'public/content/assets/front-end/js/core/libraries/jquery_ui/core.min.js',
//         'public/content/assets/front-end/js/core/app.js',
//         'public/content/assets/front-end/js/plugins/ui/ripple.min.js',
//         'public/content/assets/front-end/js/plugins/forms/styling/uniform.min.js',
//         'public/content/assets/front-end/js/plugins/forms/validation/validate.min.js',
//         'public/content/assets/front-end/js/plugins/forms/selects/select2.min.js',
//         'public/content/assets/front-end/js/core/libraries/bootstrap.min.js',
//         'public/content/assets/front-end/js/plugins/notifications/noty.min.js',
//         'public/content/assets/front-end/js/core/components.min.js?v=1.2',
//         'public/content/assets/front-end/js/emojione.min.js',
//         'public/content/assets/front-end/js/plugins/loaders/pace.min.js',
//         'public/content/assets/front-end/js/smoothscroll.js'
//     ],
//     'public/js/all.js');
